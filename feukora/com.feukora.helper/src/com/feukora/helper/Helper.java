package com.feukora.helper;

import com.feukora.server.webservice.SetupWSService;

public class Helper {

	public static void main(String[] args) {
		try {
			SetupWSService service = new SetupWSService();
			service.getSetupWSPort().init();
			System.out.println("-------- WEBSERVICE & RMI INITIALISIERUNG ERFOLGREICH --------");
		} catch (Exception e) {
			System.out.println("-------- WEBSERVICE & RMI INITIALISIERUNG FEHLGESCHLAGEN --------");
		}
	}

}
