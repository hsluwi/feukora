package com.feukora.server.junit;

import static org.junit.Assert.*;

import java.util.ArrayList;

import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Mitarbeiter;
import com.feukora.server.models.MitarbeiterRolle;
import com.feukora.server.models.PlzOrt;
import com.feukora.server.repositories.IRepository;
import com.feukora.server.repositories.MitarbeiterRepository;


/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 21.11.2015
 *
 *Klasse um MitarbeiterRepository zu testen. 
 *
 */
public class MitarbeiterTest {

	private static IRepository<Mitarbeiter> mitarbeiterRepository = new MitarbeiterRepository(new ConfigManager().getTestDatabaseName());
	private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

	/**
	 * Es werden 10 Brennerart-Objekte mit Mitarbeiterrolle und PlzOrt generiert.
	 * Die ersten 5 Objekte werden benutzt, um die Werte der Variablen zu �berpr�fen und zu �ndern.
	 * Mit den anderen 5 Objekten wird die L�schfunktion getestet.
	 */
	@BeforeClass
	public static void init() {
		
		Validator validator = factory.getValidator();
		
		
		MitarbeiterRolle miaRolle1 = new MitarbeiterRolle();
		miaRolle1.setBezeichnung("Sachbearbeiter");
		
		MitarbeiterRolle miaRolle2 = new MitarbeiterRolle();
		miaRolle2.setBezeichnung("Kontrolleur");
		
		PlzOrt plzOrt1 = new PlzOrt();
		plzOrt1.setOrt("Z�rich");
		plzOrt1.setPlz(8001);
		
		PlzOrt plzOrt2 = new PlzOrt();
		plzOrt2.setOrt("Luzern");
		plzOrt2.setPlz(6005);
		
		Mitarbeiter mia1 = new Mitarbeiter();
		mia1.setVorname("Peter");
		mia1.setNachname("Brandenberger");
		mia1.setEmail("p.brandenberger@gmail.ch");
		mia1.setStrasse("Schlossgasse 42");
		mia1.setTelefon("079 451 23 21");
		mia1.setMobile("079 451 23 21");
		mia1.setPlzOrt(plzOrt1);
		mia1.setRolle(miaRolle1);
		mia1.setUserName("pbrandenberger");
		mia1.setPasswort("p.Br458?");
		System.out.println(0);
		Set<ConstraintViolation<Mitarbeiter>> constraintViolations = validator.validate(mia1);
		constraintViolations.forEach(m -> System.out.println(m.getMessage()));
				
		Mitarbeiter mia2 = new Mitarbeiter();
		mia2.setVorname("Hans");
		mia2.setNachname("Vogel");
		mia2.setEmail("hans.vogel@gmail.ch");
		mia2.setStrasse("Kellerstrasse 3");
		mia2.setTelefon("078 651 23 12");
		mia2.setMobile("078 651 23 12");
		mia2.setPlzOrt(plzOrt2);
		mia2.setRolle(miaRolle2);
		mia2.setUserName("hvogel");
		mia2.setPasswort("ad.234ME");
		
		System.out.println(1);
		constraintViolations = validator.validate(mia2);
		constraintViolations.forEach(m -> System.out.println(m.getMessage()));
		
		Mitarbeiter mia3 = new Mitarbeiter();
		mia3.setVorname("Urs");
		mia3.setNachname("Reuter");
		mia3.setEmail("urs.reuter@gmail.ch");
		mia3.setStrasse("Hohenrain 25");
		mia3.setTelefon("076 122 45 54");
		mia3.setMobile("076 122 45 54");
		mia3.setPlzOrt(plzOrt2);
		mia3.setRolle(miaRolle1);
		mia3.setUserName("ureuter");
		mia3.setPasswort("ad34*DAA");
		
		System.out.println(2);
		constraintViolations = validator.validate(mia3);
		constraintViolations.forEach(m -> System.out.println(m.getMessage()));
		
		Mitarbeiter mia4 = new Mitarbeiter();
		mia4.setVorname("Pierre");
		mia4.setNachname("Depard");
		mia4.setEmail("p.depard@gmail.com");
		mia4.setStrasse("Burgstrasse 4");
		mia4.setTelefon("079 222 35 35");
		mia4.setMobile("079 222 35 35");
		mia4.setPlzOrt(plzOrt1);
		mia4.setRolle(miaRolle1);
		mia4.setUserName("pdepard");
		mia4.setPasswort("P@ssword");
		
		System.out.println(3);
		constraintViolations = validator.validate(mia4);
		constraintViolations.forEach(m -> System.out.println(m.getMessage()));
		
		Mitarbeiter mia5 = new Mitarbeiter();
		mia5.setVorname("Edgard");
		mia5.setNachname("Schneebeli");
		mia5.setEmail("e.schneebeli@gmail.ch");
		mia5.setStrasse("Hochdorfstrasse 89");
		mia5.setTelefon("079 451 23 35");
		mia5.setMobile("079 451 23 35");
		mia5.setPlzOrt(plzOrt1);
		mia5.setRolle(miaRolle1);
		mia5.setUserName("eschneebeli");
		mia5.setPasswort("AHA*?aha");
		
		Mitarbeiter mia6 = new Mitarbeiter();
		mia6.setVorname("Michael");
		mia6.setNachname("Meier");
		mia6.setEmail("m.meier@gmail.ch");
		mia6.setStrasse("Steingasse 1");
		mia6.setTelefon("079 222 23 44");
		mia6.setMobile("079 222 23 44");
		mia6.setPlzOrt(plzOrt1);
		mia6.setRolle(miaRolle1);
		mia6.setUserName("mmeier");
		mia6.setPasswort("56asfADF");
		
		Mitarbeiter mia7 = new Mitarbeiter();
		mia7.setVorname("Ivo");
		mia7.setNachname("Braunschweiler");
		mia7.setEmail("ivo.braunschweiler@gmail.ch");
		mia7.setStrasse("Kantonstrasse 13");
		mia7.setTelefon("076 223 23 21");
		mia7.setMobile("076 223 23 21");
		mia7.setPlzOrt(plzOrt2);
		mia7.setRolle(miaRolle2);
		mia7.setUserName("ibraunschweiler");
		mia7.setPasswort("Winter1015");
		
		Mitarbeiter mia8 = new Mitarbeiter();
		mia8.setVorname("Florence");
		mia8.setNachname("Boloise");
		mia8.setEmail("f.boloise@gmail.ch");
		mia8.setStrasse("Blumengasse 2b");
		mia8.setTelefon("075 562 23 22");
		mia8.setMobile("075 562 23 22");
		mia8.setPlzOrt(plzOrt2);
		mia8.setRolle(miaRolle1);
		mia8.setUserName("fboloise");
		mia8.setPasswort("Sommer1990");
		
		Mitarbeiter mia9 = new Mitarbeiter();
		mia9.setVorname("Ursula");
		mia9.setNachname("Lustenberger");
		mia9.setEmail("ursula.hat@lust.ch");
		mia9.setStrasse("Winkelriedstrasse 77");
		mia9.setTelefon("076 432 90 98");
		mia9.setMobile("076 432 90 98");
		mia9.setPlzOrt(plzOrt1);
		mia9.setRolle(miaRolle2);
		mia9.setUserName("ulustenberger");
		mia9.setPasswort("Herbst89");
		
		Mitarbeiter mia10 = new Mitarbeiter();
		mia10.setVorname("Rosali");
		mia10.setNachname("Mi Amor");
		mia10.setEmail("rosali@dirosesindfuerdimiamor.ch");
		mia10.setStrasse("Rosengarten 1");
		mia10.setTelefon("079 974 12 65");
		mia10.setMobile("079 974 12 65");
		mia10.setPlzOrt(plzOrt1);
		mia10.setRolle(miaRolle1);
		mia10.setUserName("rmiamor");
		mia10.setPasswort("Fruehling123");
		
		mitarbeiterRepository.persist(mia1);
		mitarbeiterRepository.persist(mia2, mia3);
		
		ArrayList<Mitarbeiter> mListe = new ArrayList<Mitarbeiter>();
		mListe.add(mia4);
		mListe.add(mia5);
		mListe.add(mia6);
		mListe.add(mia7);
		mListe.add(mia8);
		mListe.add(mia9);
		mListe.add(mia10);
		
		mitarbeiterRepository.persist(mListe);
	}

	/**
	 * Die Variablen vom 1. Mitarbeiter-Objekt wird auf Gleichheit �berpr�ft.
	 */	
	@Test
	public void testCanGetMitarbeiter()
	{
		Optional<Mitarbeiter> mia = mitarbeiterRepository.get(1);
		assertEquals("Peter", mia.get().getVorname());
		assertEquals("Brandenberger", mia.get().getNachname());
		assertEquals("Schlossgasse 42", mia.get().getStrasse());
		assertEquals("079 451 23 21", mia.get().getTelefon());
		assertEquals("079 451 23 21", mia.get().getMobile());
		assertEquals("Z�rich", mia.get().getPlzOrt().getOrt());
		assertEquals(8001, (int)mia.get().getPlzOrt().getPlz());
		assertEquals("Sachbearbeiter", mia.get().getRolle().getBezeichnung());
		assertEquals("pbrandenberger", mia.get().getUserName());
		
	}

	/**
	 * Die ersten 5 Mitarbeiter-Objekte werden in ein Set geholt.
	 * Anschliessend wird �berpr�ft, ob das Set aus 5 Objekten besteht.
	 */
	@Test
	public void testCanGetMitarbeiters()
	{
		Set<Mitarbeiter> mListe = mitarbeiterRepository.get(m -> m.getId() <= 5);
		assertEquals(5, mListe.size());
	}

	/**
	 * Bei den ersten 5 Mitarbeiter-Objekte wird die Variable Passwort ge�ndert.
	 * Die Objekte werden anschliessend wieder in die Datenbank geschrieben.
	 * Danach wird das Passwort vom Objekt wieder ausgelesen. 
	 * Es wird �berpr�ft, ob die Variablen ge�ndert wurden.
	 */
	@Test
	public void testCanUpdateMitarbeiters()
	{
		Optional<Mitarbeiter> Mitarbeiter = mitarbeiterRepository.get(1);
		Mitarbeiter.get().setPasswort("neuPass89");
		
		mitarbeiterRepository.persist(Mitarbeiter.get());
		assertEquals("neuPass89", mitarbeiterRepository.get(1).get().getPasswort());
		
		Mitarbeiter mia2 = mitarbeiterRepository.get(2).get();
		mia2.setPasswort("NeuesPasswort45");
		
		Mitarbeiter mia3 = mitarbeiterRepository.get(3).get();
		mia3.setPasswort("NeuPWORT12");
		
		mitarbeiterRepository.persist(mia2, mia3);
		assertEquals("NeuesPasswort45", mitarbeiterRepository.get(2).get().getPasswort());
		assertEquals("NeuPWORT12", mitarbeiterRepository.get(3).get().getPasswort());
		
		Mitarbeiter mia4 = mitarbeiterRepository.get(4).get();
		mia4.setPasswort("neuPASS42?");
		
		Mitarbeiter mia5 = mitarbeiterRepository.get(5).get();
		mia5.setPasswort("nPasswort4*");
		
		ArrayList<Mitarbeiter> mListe = new ArrayList<Mitarbeiter>();
		mListe.add(mia3);
		mListe.add(mia4);
		mListe.add(mia5);
	
		mitarbeiterRepository.persist(mListe);
		
		assertEquals("neuPASS42?", mitarbeiterRepository.get(4).get().getPasswort());
		assertEquals("nPasswort4*", mitarbeiterRepository.get(5).get().getPasswort());
	}

	/**
	 * Die Mitarbeiter-Objekte mit der Id gr�sser als 5 werden gel�scht. 
	 */
	@Test
	public void testCanRemoveMitarbeiters()
	{
		mitarbeiterRepository.remove(6);
		assertEquals(9, mitarbeiterRepository.get().size());
		
		mitarbeiterRepository.remove(mitarbeiterRepository.get(7).get(), mitarbeiterRepository.get(8).get());
		assertEquals(7, mitarbeiterRepository.get().size());
		
		Set<Mitarbeiter> mListe = mitarbeiterRepository.get(m -> m.getId() > 5 && m.getId() <= 10);
		mitarbeiterRepository.remove(mListe);
		assertEquals(0, mitarbeiterRepository.get(m -> m.getId() > 5 && m.getId() <= 10).size());
	}

	/**
	 * Bean-Validation wird getestet.
	 * Es wird ein Mitarbeiter-Objekt erstellt.
	 * Zuerst werden in die Felder ung�ltige Werte geschrieben.
	 * Danach wird die Anzahl der ung�ltigen Werte gez�hlt und Schritt f�r Schritt
	 * mit g�ltigen Werte abgef�llt, bis die Anzahl der ung�ltigen Werte auf 0 ist.
	 */
	@Test
	public void testValidateMitarbeiter()
	{
		Mitarbeiter mia1 = new Mitarbeiter();
		mia1.setVorname("Ribeye shankle pork loin hamburger, bresaola sirloin jowl beef tenderloin alcatra andouille ground round pancetta. Filet mignon turducken sirloin pork chop hamburger ball tip");
		mia1.setNachname("Hamburger t-bone ham sirloin kevin bacon. Boudin pork belly beef, doner ground round shoulder pastrami frankfurter.");
		mia1.setEmail("p-brandenberger-gmail-ch");
		mia1.setStrasse("Andouille pork chop alcatra tail, kevin drumstick boudin corned beef turducken. Ground round cupim capicola biltong pastrami.");
		mia1.setTelefon("079 451 23 21Pancetta venison pork belly frankfurter kevin bresaola, prosciutto leberkas ham. ");
		mia1.setMobile("Ground round beef prosciutto cow shank biltong strip steak boudin rump shankle swine pork loin pig tri-tip");
		mia1.setUserName("user??*343243243242342342342322");
		mia1.setPasswort("kurzgadfadfdafaafdafadfdafdfdfdfdfdfdf");
		//mia1.setRolle(miaRolle1);
		//mia1.setPlzOrt(plzOrt1);
		
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Mitarbeiter>> constraintViolations = validator.validate(mia1);
	
		assertEquals(10, constraintViolations.size());
		
		mia1.setVorname("Ueli");
		constraintViolations = validator.validate(mia1);
		assertEquals(9, constraintViolations.size());
		
		mia1.setNachname("Maurer");
		constraintViolations = validator.validate(mia1);
		assertEquals(8, constraintViolations.size());
		
		mia1.setEmail("ueli.maurer@admin.ch");
		constraintViolations = validator.validate(mia1);
		assertEquals(7, constraintViolations.size());
		
		mia1.setStrasse("Schweizerstrasse 1");
		constraintViolations = validator.validate(mia1);
		assertEquals(6, constraintViolations.size());
		
		mia1.setTelefon("+41 52 222 47 08");
		constraintViolations = validator.validate(mia1);
		assertEquals(5, constraintViolations.size());
		
		mia1.setMobile("+41 79 100 10 10");
		constraintViolations = validator.validate(mia1);
		assertEquals(4, constraintViolations.size());
		
		
		mia1.setUserName("uMaurer3");
		constraintViolations = validator.validate(mia1);
		assertEquals(3, constraintViolations.size());
		
		mia1.setPasswort("P@ssw0rd");
		constraintViolations = validator.validate(mia1);
		assertEquals(2, constraintViolations.size());
		
		PlzOrt plzOrt = new PlzOrt();
		plzOrt.setOrt("Bern");
		plzOrt.setPlz(3000);
		mia1.setPlzOrt(plzOrt);	
		constraintViolations = validator.validate(mia1);
		assertEquals(1, constraintViolations.size());
		
		MitarbeiterRolle miaRolle = new MitarbeiterRolle();
		miaRolle.setBezeichnung("Sachbearbeiter");
		mia1.setRolle(miaRolle);
		constraintViolations = validator.validate(mia1);
		assertEquals(0, constraintViolations.size());
		
	}
}
