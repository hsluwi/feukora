package com.feukora.server.junit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Kontrolle;
import com.feukora.server.models.Messergebnis;
import com.feukora.server.repositories.IRepository;
import com.feukora.server.repositories.MessergebnisRepository;

public class MessergebnisRepositoryTest {

	private static IRepository<Messergebnis> messergebnisRepository = new MessergebnisRepository(new ConfigManager().getTestDatabaseName());
	private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	
	// Generiere 10 Messergenisse
	// Nur die ersten 5 werden geupdatet
	// Nur die zwischen 6 und 10 werden gel�scht
	@BeforeClass
	public static void init() {
		
			
		Kontrolle kontrolle1 = new Kontrolle();
		kontrolle1.setBemerkungen("Alles schlecht");
		kontrolle1.setBestanden(false);
		kontrolle1.setEinregulierungMoeglich(true);
		kontrolle1.setFailAbgasverlust(true);
		kontrolle1.setFailCo(true);
		kontrolle1.setFailNo(false);
		kontrolle1.setFailOel(false);
		kontrolle1.setFailRusszahl(true);
		kontrolle1.setKenntnisnahmeKunde(true);
		kontrolle1.setKontrollart("Abnahmekontrolle");
		
		Kontrolle kontrolle2 = new Kontrolle();
		kontrolle2.setBemerkungen("");
		kontrolle2.setBestanden(true);
		kontrolle2.setFailAbgasverlust(false);
		kontrolle2.setFailCo(false);
		kontrolle2.setFailNo(false);
		kontrolle2.setFailOel(false);
		kontrolle2.setFailRusszahl(false);
		kontrolle2.setKenntnisnahmeKunde(true);
		kontrolle2.setKontrollart("Routinekontrolle");
	
		Messergebnis messergebnis1 = new Messergebnis();
		messergebnis1.setAbgastemperatur(80.0);
		messergebnis1.setAbgasverluste(2.3);
		messergebnis1.setCo3PVol(23.3);
		messergebnis1.setKontrolle(kontrolle1);
		messergebnis1.setMessungNr(1);
		messergebnis1.setNox3PVol(42.0);
		messergebnis1.setO2Gehalt(15.1);
		messergebnis1.setOelanteile(false);
		messergebnis1.setRusszahl(13.0);
		messergebnis1.setStufe(1);
		messergebnis1.setVerbrennLuftTemp(92.34);
		messergebnis1.setWaermeErzTemp(74.2);
		
		Messergebnis messergebnis2 = new Messergebnis();
		messergebnis2.setAbgastemperatur(80.0);
		messergebnis2.setAbgasverluste(2.3);
		messergebnis2.setCo3PVol(23.3);
		messergebnis2.setKontrolle(kontrolle2);
		messergebnis2.setMessungNr(1);
		messergebnis2.setNox3PVol(42.0);
		messergebnis2.setO2Gehalt(15.1);
		messergebnis2.setOelanteile(false);
		messergebnis2.setRusszahl(13.0);
		messergebnis2.setStufe(1);
		messergebnis2.setVerbrennLuftTemp(92.34);
		messergebnis2.setWaermeErzTemp(74.2);
		
		Messergebnis messergebnis3 = new Messergebnis();
		messergebnis3.setAbgastemperatur(83.0);
		messergebnis3.setAbgasverluste(2.33);
		messergebnis3.setCo3PVol(2.3);
		messergebnis3.setKontrolle(kontrolle1);
		messergebnis3.setMessungNr(1);
		messergebnis3.setNox3PVol(4.0);
		messergebnis3.setO2Gehalt(15.1);
		messergebnis3.setOelanteile(true);
		messergebnis3.setRusszahl(13.0);
		messergebnis3.setStufe(1);
		messergebnis3.setVerbrennLuftTemp(22.34);
		messergebnis3.setWaermeErzTemp(74.2);
		
		Messergebnis messergebnis4 = new Messergebnis();
		messergebnis4.setAbgastemperatur(80.0);
		messergebnis4.setAbgasverluste(2.3);
		messergebnis4.setCo3PVol(23.3);
		messergebnis4.setKontrolle(kontrolle2);
		messergebnis4.setMessungNr(1);
		messergebnis4.setNox3PVol(42.0);
		messergebnis4.setO2Gehalt(15.1);
		messergebnis4.setOelanteile(true);
		messergebnis4.setRusszahl(13.0);
		messergebnis4.setStufe(1);
		messergebnis4.setVerbrennLuftTemp(92.34);
		messergebnis4.setWaermeErzTemp(74.2);
		
		Messergebnis messergebnis5 = new Messergebnis();
		messergebnis5.setAbgastemperatur(80.0);
		messergebnis5.setAbgasverluste(2.3);
		messergebnis5.setCo3PVol(23.3);
		messergebnis5.setKontrolle(kontrolle1);
		messergebnis5.setMessungNr(1);
		messergebnis5.setNox3PVol(5.0);
		messergebnis5.setO2Gehalt(77.1);
		messergebnis5.setOelanteile(false);
		messergebnis5.setRusszahl(138.0);
		messergebnis5.setStufe(2);
		messergebnis5.setVerbrennLuftTemp(92.34);
		messergebnis5.setWaermeErzTemp(74.2);
		
		Messergebnis messergebnis6 = new Messergebnis();
		messergebnis6.setAbgastemperatur(550.0);
		messergebnis6.setAbgasverluste(32.3);
		messergebnis6.setCo3PVol(2.3);
		messergebnis6.setKontrolle(kontrolle2);
		messergebnis6.setMessungNr(1);
		messergebnis6.setNox3PVol(42.0);
		messergebnis6.setO2Gehalt(15.1);
		messergebnis6.setOelanteile(true);
		messergebnis6.setRusszahl(1.0);
		messergebnis6.setStufe(1);
		messergebnis6.setVerbrennLuftTemp(92.34);
		messergebnis6.setWaermeErzTemp(94.2);
		
		Messergebnis messergebnis7 = new Messergebnis();
		messergebnis7.setAbgastemperatur(80.0);
		messergebnis7.setAbgasverluste(2.3);
		messergebnis7.setCo3PVol(23.3);
		messergebnis7.setKontrolle(kontrolle1);
		messergebnis7.setMessungNr(1);
		messergebnis7.setNox3PVol(42.0);
		messergebnis7.setO2Gehalt(15.1);
		messergebnis7.setOelanteile(true);
		messergebnis7.setRusszahl(13.0);
		messergebnis7.setStufe(1);
		messergebnis7.setVerbrennLuftTemp(92.34);
		messergebnis7.setWaermeErzTemp(74.2);
		
		Messergebnis messergebnis8 = new Messergebnis();
		messergebnis8.setAbgastemperatur(80.0);
		messergebnis8.setAbgasverluste(2.3);
		messergebnis8.setCo3PVol(23.3);
		messergebnis8.setKontrolle(kontrolle2);
		messergebnis8.setMessungNr(1);
		messergebnis8.setNox3PVol(42.0);
		messergebnis8.setO2Gehalt(15.1);
		messergebnis8.setOelanteile(false);
		messergebnis8.setRusszahl(13.0);
		messergebnis8.setStufe(4);
		messergebnis8.setVerbrennLuftTemp(92.34);
		messergebnis8.setWaermeErzTemp(74.2);
		
		Messergebnis messergebnis9 = new Messergebnis();
		messergebnis9.setAbgastemperatur(80.0);
		messergebnis9.setAbgasverluste(2.3);
		messergebnis9.setCo3PVol(23.3);
		messergebnis9.setKontrolle(kontrolle1);
		messergebnis9.setMessungNr(1);
		messergebnis9.setNox3PVol(42.0);
		messergebnis9.setO2Gehalt(15.1);
		messergebnis9.setOelanteile(false);
		messergebnis9.setRusszahl(13.0);
		messergebnis9.setStufe(1);
		messergebnis9.setVerbrennLuftTemp(92.34);
		messergebnis9.setWaermeErzTemp(74.2);
		
		Messergebnis messergebnis10 = new Messergebnis();
		messergebnis10.setAbgastemperatur(80.0);
		messergebnis10.setAbgasverluste(2.3);
		messergebnis10.setCo3PVol(23.3);
		messergebnis10.setKontrolle(kontrolle2);
		messergebnis10.setMessungNr(1);
		messergebnis10.setNox3PVol(42.0);
		messergebnis10.setO2Gehalt(15.1);
		messergebnis10.setOelanteile(false);
		messergebnis10.setRusszahl(13.0);
		messergebnis10.setStufe(1);
		messergebnis10.setVerbrennLuftTemp(92.34);
		messergebnis10.setWaermeErzTemp(74.2);
				
		
		messergebnisRepository.persist(messergebnis1);
		messergebnisRepository.persist(messergebnis2, messergebnis3);
		
		ArrayList<Messergebnis> mListe = new ArrayList<Messergebnis>();
		mListe.add(messergebnis4);
		mListe.add(messergebnis5);
		mListe.add(messergebnis6);
		mListe.add(messergebnis7);
		mListe.add(messergebnis8);
		mListe.add(messergebnis9);
		mListe.add(messergebnis10);
		
		messergebnisRepository.persist(mListe);
	}
		
	@Test
	public void testGetKontrolle()
	{
		Optional<Messergebnis> messergebnis = messergebnisRepository.get(1);
		assertEquals(80.0, messergebnis.get().getAbgastemperatur(),0.1);
		assertEquals(2.3, messergebnis.get().getAbgasverluste(),0.1);
		assertEquals(23.3, messergebnis.get().getCo3PVol(),0.1);
		assertEquals("Alles schlecht", messergebnis.get().getKontrolle().getBemerkungen());
		assertEquals("Abnahmekontrolle", messergebnis.get().getKontrolle().getKontrollart());
		assertEquals(42.0, messergebnis.get().getNox3PVol(),0.1);
		assertEquals(15.1, messergebnis.get().getO2Gehalt(),0.1);
		assertEquals(false, messergebnis.get().getOelanteile());
		assertEquals(13.0, messergebnis.get().getRusszahl(),0.1);
		assertEquals(1, messergebnis.get().getStufe(),0);
		assertEquals(92.34, messergebnis.get().getVerbrennLuftTemp(),0.1);
		assertEquals(74.2, messergebnis.get().getWaermeErzTemp(),0.1);
	}

	
	@Test
	public void testGetKontrollen()
	{
		Set<Messergebnis> mList = messergebnisRepository.get(m -> m.getId() <= 5);
		assertEquals(5, mList.size());
	}
	
	@Test
	public void testUpdateKontrolle()
	{
		Messergebnis messergebnis = messergebnisRepository.get(1).get();
		messergebnis.setMessungNr(12);
		
		messergebnisRepository.persist(messergebnis);
		assertEquals(12,messergebnisRepository.get(1).get().getMessungNr(),0);
		
		Messergebnis messergebnis2 = messergebnisRepository.get(2).get();
		messergebnis2.setMessungNr(13);
		
		Messergebnis messergebnis3 = messergebnisRepository.get(3).get();
		messergebnis3.setMessungNr(14);
		
		messergebnisRepository.persist(messergebnis2, messergebnis3);

		assertEquals(13,messergebnisRepository.get(2).get().getMessungNr(),0);
		assertEquals(14,messergebnisRepository.get(3).get().getMessungNr(),0);
		
		
		Messergebnis messergebnis4 = messergebnisRepository.get(4).get();
		messergebnis4.setMessungNr(15);
		
		Messergebnis messergebnis5 = messergebnisRepository.get(5).get();
		messergebnis5.setMessungNr(16);
		
		ArrayList<Messergebnis> mListe = new ArrayList<Messergebnis>();
		mListe.add(messergebnis4);
		mListe.add(messergebnis5);
		
		messergebnisRepository.persist(mListe);
		
		assertEquals(15,messergebnisRepository.get(4).get().getMessungNr(),0);
		assertEquals(16,messergebnisRepository.get(5).get().getMessungNr(),0);
		
	}
	
	@Test
	public void testCanRemoveKontrolle()
	{
		messergebnisRepository.remove(6);
		assertEquals(9,messergebnisRepository.get().size());
		
		messergebnisRepository.remove(messergebnisRepository.get(7).get(),messergebnisRepository.get(8).get());
		assertEquals(7,messergebnisRepository.get().size());
		
		Set<Messergebnis> mList =messergebnisRepository.get(m -> m.getId() > 5 && m.getId() <= 10);
		messergebnisRepository.remove(mList);
		assertEquals(0,messergebnisRepository.get(m -> m.getId() > 5 && m.getId() <= 10).size());
	}
	
	@Test
	public void testValidatePerson()
	{
		
		Messergebnis messergebnis1 = new Messergebnis();
		messergebnis1.setAbgastemperatur(null);
		messergebnis1.setAbgasverluste(null);
		messergebnis1.setCo3PVol(null);
		messergebnis1.setMessungNr(null);
		messergebnis1.setNox3PVol(null);
		messergebnis1.setO2Gehalt(null);
		messergebnis1.setOelanteile(null);
		messergebnis1.setRusszahl(null);
		messergebnis1.setStufe(null);
		messergebnis1.setVerbrennLuftTemp(null);
		messergebnis1.setWaermeErzTemp(null);
		
		
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Messergebnis>> constraintViolations = validator.validate(messergebnis1);
	
		assertEquals(12, constraintViolations.size());

		
		messergebnis1.setAbgastemperatur(1.0);
		constraintViolations = validator.validate(messergebnis1);
		assertEquals(11, constraintViolations.size());
		
		messergebnis1.setAbgasverluste(1.0);
		constraintViolations = validator.validate(messergebnis1);
		assertEquals(10, constraintViolations.size());
		
		messergebnis1.setCo3PVol(1.0);
		constraintViolations = validator.validate(messergebnis1);
		assertEquals(9, constraintViolations.size());
		
		messergebnis1.setMessungNr(1);
		constraintViolations = validator.validate(messergebnis1);
		assertEquals(8, constraintViolations.size());
		
		messergebnis1.setNox3PVol(1.0);
		constraintViolations = validator.validate(messergebnis1);
		assertEquals(7, constraintViolations.size());

		messergebnis1.setO2Gehalt(1.0);
		constraintViolations = validator.validate(messergebnis1);
		assertEquals(6, constraintViolations.size());
		
		messergebnis1.setOelanteile(false);
		constraintViolations = validator.validate(messergebnis1);
		assertEquals(5, constraintViolations.size());

		messergebnis1.setRusszahl(1.0);
		constraintViolations = validator.validate(messergebnis1);
		assertEquals(4, constraintViolations.size());
		
		messergebnis1.setStufe(1);
		constraintViolations = validator.validate(messergebnis1);
		assertEquals(3, constraintViolations.size());
		
		messergebnis1.setVerbrennLuftTemp(1.0);
		constraintViolations = validator.validate(messergebnis1);
		assertEquals(2, constraintViolations.size());
		
		messergebnis1.setWaermeErzTemp(1.0);
		constraintViolations = validator.validate(messergebnis1);
		assertEquals(1, constraintViolations.size());
	}
}
