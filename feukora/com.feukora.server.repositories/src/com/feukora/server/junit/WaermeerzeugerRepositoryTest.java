package com.feukora.server.junit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Brennstoff;
import com.feukora.server.models.Waermeerzeuger;
import com.feukora.server.models.Waermeerzeugermodell;
import com.feukora.server.repositories.IRepository;
import com.feukora.server.repositories.WaermeerzeugerRepository;


/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 21.11.2015
 *
 *Klasse um WaermeerzeugerRepository zu testen. 
 *
 */
public class WaermeerzeugerRepositoryTest{

	private static IRepository<Waermeerzeuger> waermeerzeugerRepository = new WaermeerzeugerRepository(new ConfigManager().getTestDatabaseName());
	private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

	/**
	 * Es werden 10 Waermeerzeuger-Objekte mit Waermeerzeugermodell und Brennstoffen generiert.
	 * Die ersten 5 Objekte werden benutzt, um die Werte der Variablen zu �berpr�fen und zu �ndern.
	 * Mit den anderen 5 Objekten wird die L�schfunktion getestet.
	 */
	@BeforeClass
	public static void init() {
		
		Brennstoff brennstoff1 = new Brennstoff();
		brennstoff1.setBezeichnung("Holz");
		
		Brennstoff brennstoff2 = new Brennstoff();
		brennstoff2.setBezeichnung("Oel");
					
		Waermeerzeugermodell waermeerzeugerm1 = new Waermeerzeugermodell();
		waermeerzeugerm1.setBezeichnung("Model 1");
		waermeerzeugerm1.setHersteller("Arnold GmbH");
		waermeerzeugerm1.setBrennstoff(brennstoff1);
		
		Waermeerzeugermodell waermeerzeugerm2 = new Waermeerzeugermodell();
		waermeerzeugerm2.setBezeichnung("Model 2");
		waermeerzeugerm2.setHersteller("Brenn AG");
		waermeerzeugerm2.setBrennstoff(brennstoff2);
			
		Waermeerzeuger waermeerzeuger1 = new Waermeerzeuger();
		waermeerzeuger1.setWaermeerzeugermodell(waermeerzeugerm1);
		waermeerzeuger1.setBaujahr(1998);
		waermeerzeuger1.setZulassungsNr("1574GK445G");
		
		Waermeerzeuger waermeerzeuger2 = new Waermeerzeuger();
		waermeerzeuger2.setWaermeerzeugermodell(waermeerzeugerm1);
		waermeerzeuger2.setBaujahr(2007);
		waermeerzeuger2.setZulassungsNr("45555");
		
		Waermeerzeuger waermeerzeuger3 = new Waermeerzeuger();
		waermeerzeuger3.setWaermeerzeugermodell(waermeerzeugerm2);
		waermeerzeuger3.setBaujahr(2009);
		waermeerzeuger3.setZulassungsNr("adDUe12");
		
		Waermeerzeuger waermeerzeuger4 = new Waermeerzeuger();
		waermeerzeuger4.setWaermeerzeugermodell(waermeerzeugerm1);
		waermeerzeuger4.setBaujahr(2014);
		waermeerzeuger4.setZulassungsNr("458hg");
		
		Waermeerzeuger waermeerzeuger5 = new Waermeerzeuger();
		waermeerzeuger5.setWaermeerzeugermodell(waermeerzeugerm2);
		waermeerzeuger5.setBaujahr(2008);
		waermeerzeuger5.setZulassungsNr("1574GK445G");
		
		Waermeerzeuger waermeerzeuger6 = new Waermeerzeuger();
		waermeerzeuger6.setWaermeerzeugermodell(waermeerzeugerm2);
		waermeerzeuger6.setBaujahr(1999);
		waermeerzeuger6.setZulassungsNr("AB45EJFff");
		
		Waermeerzeuger waermeerzeuger7 = new Waermeerzeuger();
		waermeerzeuger7.setWaermeerzeugermodell(waermeerzeugerm1);
		waermeerzeuger7.setBaujahr(1998);
		waermeerzeuger7.setZulassungsNr("45ADJr8F");
		
		Waermeerzeuger waermeerzeuger8 = new Waermeerzeuger();
		waermeerzeuger8.setWaermeerzeugermodell(waermeerzeugerm2);
		waermeerzeuger8.setBaujahr(1998);
		waermeerzeuger8.setZulassungsNr("fg56tzg");
		
		Waermeerzeuger waermeerzeuger9 = new Waermeerzeuger();
		waermeerzeuger9.setWaermeerzeugermodell(waermeerzeugerm2);
		waermeerzeuger9.setBaujahr(1998);
		waermeerzeuger9.setZulassungsNr("adf4566gg");
		
		Waermeerzeuger waermeerzeuger10 = new Waermeerzeuger();
		waermeerzeuger10.setWaermeerzeugermodell(waermeerzeugerm1);
		waermeerzeuger10.setBaujahr(1997);
		waermeerzeuger10.setZulassungsNr("45df458d");
		
		
		waermeerzeugerRepository.persist(waermeerzeuger1);
		waermeerzeugerRepository.persist(waermeerzeuger2, waermeerzeuger3);
		
		ArrayList<Waermeerzeuger> wListe = new ArrayList<Waermeerzeuger>();
		wListe.add(waermeerzeuger4);
		wListe.add(waermeerzeuger5);
		wListe.add(waermeerzeuger6);
		wListe.add(waermeerzeuger7);
		wListe.add(waermeerzeuger8);
		wListe.add(waermeerzeuger9);
		wListe.add(waermeerzeuger10);
		
		waermeerzeugerRepository.persist(wListe);
	}

	/**
	 * Die Variablen vom 1. Waermeerzeuger-Objekt wird auf Gleichheit �berpr�ft.
	 */		
	@Test
	public void testCanGetWaermeerzeuger()
	{
		Optional<Waermeerzeuger> waermeerzeuger = waermeerzeugerRepository.get(1);
		assertEquals("Model 1", waermeerzeuger.get().getWaermeerzeugermodell().getBezeichnung());
		assertEquals("Holz", waermeerzeuger.get().getWaermeerzeugermodell().getBrennstoff().getBezeichnung());
		assertEquals(1998, (int)waermeerzeuger.get().getBaujahr());
			
	}

	/**
	 * Die ersten 5 Waermeerzeuger-Objekte werden in ein Set geholt.
	 * Anschliessend wird �berpr�ft, ob das Set aus 5 Objekten besteht.
	 */
	@Test
	public void testCanGetWaermeerzeugers()
	{
		Set<Waermeerzeuger> bList = waermeerzeugerRepository.get(m -> m.getId() <= 5);
		assertEquals(5, bList.size());
	}

	/**
	 * Bei den ersten 5 Waermeerzeuger-Objekte wird die Variable ZulassungsNr ge�ndert.
	 * Die Objekte werden anschliessend wieder in die Datenbank geschrieben.
	 * Danach wird die ZulassungsNr vom Objekt wieder ausgelesen. 
	 * Es wird �berpr�ft, ob die Variablen ge�ndert wurden.
	 */
	@Test
	public void testCanUpdateWaermeerzeugers()
	{
		Optional<Waermeerzeuger> waermeerzeuger1 = waermeerzeugerRepository.get(1);
		waermeerzeuger1.get().setZulassungsNr("neueNummer123");
		
		waermeerzeugerRepository.persist(waermeerzeuger1.get());
		assertEquals("neueNummer123", waermeerzeugerRepository.get(1).get().getZulassungsNr());
		
		Waermeerzeuger waermeerzeuger2 = waermeerzeugerRepository.get(2).get();
		waermeerzeuger2.setZulassungsNr("neueNummer45");
		
		Waermeerzeuger waermeerzeuger3 = waermeerzeugerRepository.get(3).get();
		waermeerzeuger3.setZulassungsNr("NN45");
		
		waermeerzeugerRepository.persist(waermeerzeuger2, waermeerzeuger3);
		assertEquals("neueNummer45", waermeerzeugerRepository.get(2).get().getZulassungsNr());
		assertEquals("NN45", waermeerzeugerRepository.get(3).get().getZulassungsNr());
		
		Waermeerzeuger waermeerzeuger4 = waermeerzeugerRepository.get(4).get();
		waermeerzeuger4.setZulassungsNr("ZulassungsnummerNeu754");
		
		Waermeerzeuger waermeerzeuger5 = waermeerzeugerRepository.get(5).get();
		waermeerzeuger5.setZulassungsNr("NummerTest33");
		
		ArrayList<Waermeerzeuger> bListe = new ArrayList<Waermeerzeuger>();
		bListe.add(waermeerzeuger3);
		bListe.add(waermeerzeuger4);
		bListe.add(waermeerzeuger5);
	
		waermeerzeugerRepository.persist(bListe);
		
		assertEquals("ZulassungsnummerNeu754", waermeerzeugerRepository.get(4).get().getZulassungsNr());
		assertEquals("NummerTest33", waermeerzeugerRepository.get(5).get().getZulassungsNr());
	}

	/**
	 * Die Waermeerzeuger-Objekte mit der Id gr�sser als 5 werden gel�scht. 
	 */
	@Test
	public void testCanRemoveWaermeerzeugers()
	{
		waermeerzeugerRepository.remove(6);
		assertEquals(9, waermeerzeugerRepository.get().size());
		
		waermeerzeugerRepository.remove(waermeerzeugerRepository.get(7).get(), waermeerzeugerRepository.get(8).get());
		assertEquals(7, waermeerzeugerRepository.get().size());
		
		Set<Waermeerzeuger> bList = waermeerzeugerRepository.get(m -> m.getId() > 5 && m.getId() <= 10);
		waermeerzeugerRepository.remove(bList);
		assertEquals(0, waermeerzeugerRepository.get(m -> m.getId() > 5 && m.getId() <= 10).size());
	}

	/**
	 * Bean-Validation wird getestet.
	 * Es wird ein Waermeerzeuger-Objekt erstellt.
	 * Zuerst werden in die Felder ung�ltige Werte geschrieben.
	 * Danach wird die Anzahl der ung�ltigen Werte gez�hlt und Schritt f�r Schritt
	 * mit g�ltigen Werte abgef�llt, bis die Anzahl der ung�ltigen Werte auf 0 ist.
	 */
	@Test
	public void testValidateWaermeerzeuger()
	{
		Waermeerzeuger waermeerzeuger = new Waermeerzeuger();
		//waermeerzeuger.setWaermeerzeugermodell(brennermodell1);
		waermeerzeuger.setBaujahr(1800);
		waermeerzeuger.setZulassungsNr("1574GK445G adf  45af45af4d  54adf4  54adf4afd Andouille pork chop alcatra tail, kevin drumstick boudi Andouille pork chop alcatra tail, kevin drumstick boudi Andouille pork chop alcat");
		
		
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Waermeerzeuger>> constraintViolations = validator.validate(waermeerzeuger);
	
		assertEquals(3, constraintViolations.size());
		
		waermeerzeuger.setBaujahr(1990);
		constraintViolations = validator.validate(waermeerzeuger);
		assertEquals(2, constraintViolations.size());
		
		waermeerzeuger.setZulassungsNr("45DHJ4584");
		constraintViolations = validator.validate(waermeerzeuger);
		assertEquals(1, constraintViolations.size());
		
		Brennstoff brennstoff = new Brennstoff();
		brennstoff.setBezeichnung("Brennstoff");
		Waermeerzeugermodell waermeerzeugermodell = new Waermeerzeugermodell();
		waermeerzeugermodell.setBezeichnung("Bezeichnung");
		waermeerzeugermodell.setHersteller("Hersteller");
		waermeerzeugermodell.setBrennstoff(brennstoff);
		waermeerzeuger.setWaermeerzeugermodell(waermeerzeugermodell);
		constraintViolations = validator.validate(waermeerzeuger);
		assertEquals(0, constraintViolations.size());
				
	}
}
