package com.feukora.server.junit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Brennerart;
import com.feukora.server.models.Brennermodell;
import com.feukora.server.repositories.BrennermodellRepository;
import com.feukora.server.repositories.IRepository;

/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 19.11.2015
 *
 *Klasse um BrennermodellRepository zu testen. 
 *
 */
public class BrennermodellRepositoryTest {

	private static IRepository<Brennermodell> brennermodellRepository = new BrennermodellRepository(new ConfigManager().getTestDatabaseName());
	//private static IRepository<Brennerart> brennerArtRepository = new BrennerartRepository(Settings.DataBaseTest);
	
	private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	
	/**
	 * Es werden 10 Brennermodell-Objekte mit Brennerart generiert.
	 * Die ersten 5 Objekte werden benutzt, um die Werte der Variablen zu �berpr�fen und zu �ndern.
	 * Mit den anderen 5 Objekten wird die L�schfunktion getestet.
	 */
	@BeforeClass
	public static void init() {
		
		Brennerart brennerart1 = new Brennerart();
		brennerart1.setBezeichnung("Brennerart 1");
		
		Brennerart brennerart2 = new Brennerart();
		brennerart2.setBezeichnung("Brennerart 2");
		
		Brennerart brennerart3 = new Brennerart();
		brennerart3.setBezeichnung("Brennerart 3");
		
		Brennerart brennerart4 = new Brennerart();
		brennerart4.setBezeichnung("Brennerart 4");
		
		Brennerart brennerart5 = new Brennerart();
		brennerart5.setBezeichnung("Brennerart 5");
		
		
		Brennermodell brennermodell1 = new Brennermodell();
		brennermodell1.setBezeichnung("Brennermodell 1");
		brennermodell1.setFeuerungswaermeleistung(200);
		brennermodell1.setHersteller("Burny");
		brennermodell1.setBrennerart(brennerart1);
		
		Brennermodell brennermodell2 = new Brennermodell();
		brennermodell2.setBezeichnung("Brennermodell 2");
		brennermodell2.setFeuerungswaermeleistung(200);
		brennermodell2.setHersteller("Hello");
		brennermodell2.setBrennerart(brennerart2);
		
		Brennermodell brennermodell3 = new Brennermodell();
		brennermodell3.setBezeichnung("Brennermodell 3");
		brennermodell3.setFeuerungswaermeleistung(200);
		brennermodell3.setHersteller("Amazonas");
		brennermodell3.setBrennerart(brennerart3);
		
		Brennermodell brennermodell4 = new Brennermodell();
		brennermodell4.setBezeichnung("BM RJ4");
		brennermodell4.setFeuerungswaermeleistung(200);
		brennermodell4.setHersteller("Burny");
		brennermodell4.setBrennerart(brennerart5);
		
		Brennermodell brennermodell5 = new Brennermodell();
		brennermodell5.setBezeichnung("Brennermodell 5");
		brennermodell5.setFeuerungswaermeleistung(200);
		brennermodell5.setHersteller("Feura");
		brennermodell5.setBrennerart(brennerart2);
		
		Brennermodell brennermodell6 = new Brennermodell();
		brennermodell6.setBezeichnung("Brennermodell 6");
		brennermodell6.setFeuerungswaermeleistung(200);
		brennermodell6.setHersteller("Marmor");
		brennermodell6.setBrennerart(brennerart1);
		
		Brennermodell brennermodell7 = new Brennermodell();
		brennermodell7.setBezeichnung("BM ZZ");
		brennermodell7.setFeuerungswaermeleistung(200);
		brennermodell7.setHersteller("Amazon");
		brennermodell7.setBrennerart(brennerart3);
		
		Brennermodell brennermodell8 = new Brennermodell();
		brennermodell8.setBezeichnung("Quick");
		brennermodell8.setFeuerungswaermeleistung(200);
		brennermodell8.setHersteller("Burny");
		brennermodell8.setBrennerart(brennerart2);
		
		Brennermodell brennermodell9 = new Brennermodell();
		brennermodell9.setBezeichnung("Brennermodell ZUG");
		brennermodell9.setFeuerungswaermeleistung(200);
		brennermodell9.setHersteller("Marama");
		brennermodell9.setBrennerart(brennerart4);
		
		Brennermodell brennermodell10 = new Brennermodell();
		brennermodell10.setBezeichnung("Brennermodell XWZ");
		brennermodell10.setFeuerungswaermeleistung(200);
		brennermodell10.setHersteller("ZUG");
		brennermodell10.setBrennerart(brennerart5);
		
				
		brennermodellRepository.persist(brennermodell1);
		brennermodellRepository.persist(brennermodell2, brennermodell3);
		
		ArrayList<Brennermodell> bListe = new ArrayList<Brennermodell>();
		bListe.add(brennermodell4);
		bListe.add(brennermodell5);
		bListe.add(brennermodell6);
		bListe.add(brennermodell7);
		bListe.add(brennermodell8);
		bListe.add(brennermodell9);
		bListe.add(brennermodell10);
		
		brennermodellRepository.persist(bListe);
	}
	
	/**
	 * Die Variablen vom 1. Brennermodell-Objekt wird auf Gleichheit �berpr�ft.
	 */	
	@Test
	public void testCanGetBrennermodell()
	{
		Optional<Brennermodell> brennermodell = brennermodellRepository.get(1);
		assertEquals("Brennermodell 1", brennermodell.get().getBezeichnung());
		assertEquals(200, (int)brennermodell.get().getFeuerungswaermeleistung());
		assertEquals("Burny", brennermodell.get().getHersteller());
		assertEquals("Brennerart 1", brennermodell.get().getBrennerart().getBezeichnung());
		
	}

	/**
	 * Die ersten 5 Brennermodell-Objekte werden in ein Set geholt.
	 * Anschliessend wird �berpr�ft, ob das Set aus 5 Objekten besteht.
	 */
	@Test
	public void testCanGetBrennermodelle()
	{

		Set<Brennermodell> bList = brennermodellRepository.get(m -> m.getId() <= 5);
		assertEquals(5, bList.size());
	}
	
	/**
	 * Bei den ersten 5 Brennermodell-Objekte wird die Variable Hersteller ge�ndert.
	 * Die Objekte werden anschliessend wieder in die Datenbank geschrieben.
	 * Danach wird der Hersteller vom Objekt wieder ausgelesen. 
	 * Es wird �berpr�ft, ob die Variablen ge�ndert wurden.
	 */
	@Test
	public void testCanUpdateBrennermodelle()
	{
		Optional<Brennermodell> brennermodell = brennermodellRepository.get(1);
		brennermodell.get().setHersteller("Neuer Hersteller");
		
		brennermodellRepository.persist(brennermodell.get());
		assertEquals("Neuer Hersteller", brennermodellRepository.get(1).get().getHersteller());
		
		Brennermodell brennermodell2 = brennermodellRepository.get(2).get();
		brennermodell2.setHersteller("Meyer AG");
		
		Brennermodell brennermodell3 = brennermodellRepository.get(3).get();
		brennermodell3.setHersteller("Ruedi Rudolf");
		
		brennermodellRepository.persist(brennermodell2, brennermodell3);
		assertEquals("Meyer AG", brennermodellRepository.get(2).get().getHersteller());
		assertEquals("Ruedi Rudolf", brennermodellRepository.get(3).get().getHersteller());
		
		Brennermodell brennermodell4 = brennermodellRepository.get(4).get();
		brennermodell4.setHersteller("Hersteller 56");
		
		Brennermodell brennermodell5 = brennermodellRepository.get(5).get();
		brennermodell5.setHersteller("NH");
		
		ArrayList<Brennermodell> bListe = new ArrayList<Brennermodell>();
		bListe.add(brennermodell3);
		bListe.add(brennermodell4);
		bListe.add(brennermodell5);
	
		brennermodellRepository.persist(bListe);
		
		assertEquals("Hersteller 56", brennermodellRepository.get(4).get().getHersteller());
		assertEquals("NH", brennermodellRepository.get(5).get().getHersteller());
	}
	
	/**
	 * Die Brennermodell-Objekte mit der Id gr�sser als 5 werden gel�scht. 
	 */
	@Test
	public void testCanRemoveBrennermodelle()
	{
		brennermodellRepository.remove(6);
		assertEquals(9, brennermodellRepository.get().size());
		
		brennermodellRepository.remove(brennermodellRepository.get(7).get(), brennermodellRepository.get(8).get());
		assertEquals(7, brennermodellRepository.get().size());
		
		Set<Brennermodell> kList = brennermodellRepository.get(m -> m.getId() > 5 && m.getId() <= 10);
		brennermodellRepository.remove(kList);
		assertEquals(0, brennermodellRepository.get(m -> m.getId() > 5 && m.getId() <= 10).size());
	}
	
	/**
	 * Bean-Validation wird getestet.
	 * Es wird ein Brennermodell-Objekt erstellt.
	 * Zuerst werden in die Felder ung�ltige Werte geschrieben.
	 * Danach wird die Anzahl der ung�ltigen Werte gez�hlt und Schritt f�r Schritt
	 * mit g�ltigen Werte abgef�llt, bis die Anzahl der ung�ltigen Werte auf 0 ist.
	 */
	@Test
	public void testValidateBrennermodell()
	{

		Brennermodell brennermodell = new Brennermodell();
		brennermodell.setBezeichnung("Brennermodell Ribeye shankle pork loin hamburger, bresaola sirloin jowl beef tenderloin alcatra andouille ground rounburger ball tip");
		brennermodell.setFeuerungswaermeleistung(20045);
		brennermodell.setHersteller("Blle pork chop alcatra tail, kevin drumstick boudin cor lle pork chop alcatra tail, kevin drumstick boudin cor");
		//brennermodell.setBrennerart(brennerart);
		
		
		
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Brennermodell>> constraintViolations = validator.validate(brennermodell);
	
		assertEquals(4, constraintViolations.size());
		
		brennermodell.setBezeichnung("Neustes Brennermodell");
		constraintViolations = validator.validate(brennermodell);
		assertEquals(3, constraintViolations.size());
		
		brennermodell.setFeuerungswaermeleistung(250);
		constraintViolations = validator.validate(brennermodell);
		assertEquals(2, constraintViolations.size());
		
		brennermodell.setHersteller("Future AG");
		constraintViolations = validator.validate(brennermodell);
		assertEquals(1, constraintViolations.size());
		
		Brennerart brennerart = new Brennerart();
		brennerart.setBezeichnung("Brennerart");
		brennermodell.setBrennerart(brennerart);
		constraintViolations = validator.validate(brennermodell);
		assertEquals(0, constraintViolations.size());

		
	}
}
