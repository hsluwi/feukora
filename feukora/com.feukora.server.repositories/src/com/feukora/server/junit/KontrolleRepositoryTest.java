package com.feukora.server.junit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Anlagenstandort;
import com.feukora.server.models.Brenner;
import com.feukora.server.models.Brennerart;
import com.feukora.server.models.Brennermodell;
import com.feukora.server.models.Brennstoff;
import com.feukora.server.models.Gemeinde;
import com.feukora.server.models.Kontrolle;
import com.feukora.server.models.Kunde;
import com.feukora.server.models.Mitarbeiter;
import com.feukora.server.models.MitarbeiterRolle;
import com.feukora.server.models.PlzOrt;
import com.feukora.server.models.Termin;
import com.feukora.server.models.Waermeerzeuger;
import com.feukora.server.models.Waermeerzeugermodell;
import com.feukora.server.repositories.IRepository;
import com.feukora.server.repositories.KontrolleRepository;

public class KontrolleRepositoryTest {

	private static IRepository<Kontrolle> kontrolleRepository = new KontrolleRepository(new ConfigManager().getTestDatabaseName());
	private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	
	// Generiere 10 Kontrollen
	// Nur die ersten 5 werden geupdatet
	// Nur die zwischen 6 und 10 werden gel�scht
	@BeforeClass
	public static void init() {
		Gemeinde gemeinde1 = new Gemeinde();
		gemeinde1.setBezeichnung("Adliswil");
		gemeinde1.setKanton("Z�rich");
				
		Brennstoff brennstoff1 = new Brennstoff();
		brennstoff1.setBezeichnung("Holz");
		
		Brennerart brennerArt1 = new Brennerart();
		brennerArt1.setBezeichnung("Holzkessel");
		
		Waermeerzeugermodell waermeerzeugermodell1 = new Waermeerzeugermodell();
		waermeerzeugermodell1.setBezeichnung("Monitec 2000");
		waermeerzeugermodell1.setBrennstoff(brennstoff1);
		waermeerzeugermodell1.setHersteller("Monikas Heizungen");
		
		Waermeerzeuger waermeerzeuger1 = new Waermeerzeuger();
		waermeerzeuger1.setBaujahr(2008);
		waermeerzeuger1.setWaermeerzeugermodell(waermeerzeugermodell1);
		waermeerzeuger1.setZulassungsNr("CH-39128");
		
		Brennermodell brennermodell1 = new Brennermodell();
		brennermodell1.setBezeichnung("Heat 150");
		brennermodell1.setBrennerart(brennerArt1);
		brennermodell1.setFeuerungswaermeleistung(2000);
		brennermodell1.setHersteller("immerWarm (r)");
		
		Brenner brenner1 = new Brenner();
		brenner1.setBaujahr(2009);
		brenner1.setBrennermodell(brennermodell1);
		brenner1.setZulassungsNr("CH-3892");
	
		PlzOrt plzOrt1 = new PlzOrt();
		plzOrt1.setOrt("Z�rich");
		plzOrt1.setPlz(8001);
		
		Kunde kunde1 = new Kunde();
		kunde1.setVorname("Hans");
		kunde1.setNachname("Vogel");
		kunde1.setEmail("hans.vogel@gmail.ch");
		kunde1.setStrasse("Kellerstrasse 3");
		kunde1.setTelefon("078 651 23 12");
		kunde1.setMobile("078 651 23 12");
		kunde1.setFirma("Vogel Heizungen GmbH");
		kunde1.setPlzOrt(plzOrt1);
		kunde1.setKundenArt("Verwaltung");
	
		MitarbeiterRolle mitarbeiterRolle1 = new MitarbeiterRolle();
		mitarbeiterRolle1.setBezeichnung("Kontrolleur");
		
		Mitarbeiter mitarbeiter1 = new Mitarbeiter();
		mitarbeiter1.setVorname("Peter");
		mitarbeiter1.setNachname("Brandenberger");
		mitarbeiter1.setEmail("p.brandenberger@gmail.ch");
		mitarbeiter1.setStrasse("Schlossgasse 42");
		mitarbeiter1.setTelefon("079 451 23 21");
		mitarbeiter1.setMobile("079 451 23 21");
		mitarbeiter1.setPlzOrt(plzOrt1);
		mitarbeiter1.setRolle(mitarbeiterRolle1);
		mitarbeiter1.setUserName("pbrandenberger");
		mitarbeiter1.setPasswort("p.Br458?");
		
		Anlagenstandort anlagenstandort1 = new Anlagenstandort();
		anlagenstandort1.setGemeinde(gemeinde1);
		anlagenstandort1.setHauswart(kunde1);
		anlagenstandort1.setKunde(kunde1);
		anlagenstandort1.setPlzOrt(plzOrt1);
		anlagenstandort1.setStrasse("Grundstrasse 20");
		anlagenstandort1.setBrenner(brenner1);
		anlagenstandort1.setWaermeerzeuger(waermeerzeuger1);
		
		Termin termin1 = new Termin();
		termin1.setAnlagenstandort(anlagenstandort1);
		termin1.setDatumZeit(new GregorianCalendar(2015, 12, 28, 10, 00));
		termin1.setMitarbeiter(mitarbeiter1);

		
		Kontrolle kontrolle1 = new Kontrolle();
		kontrolle1.setBemerkungen("Alles schlecht");
		kontrolle1.setBestanden(false);
		kontrolle1.setEinregulierungMoeglich(true);
		kontrolle1.setFailAbgasverlust(true);
		kontrolle1.setFailCo(true);
		kontrolle1.setFailNo(false);
		kontrolle1.setFailOel(false);
		kontrolle1.setFailRusszahl(true);
		kontrolle1.setKenntnisnahmeKunde(true);
		kontrolle1.setKontrollart("Abnahmekontrolle");
		
		Kontrolle kontrolle2 = new Kontrolle();
		kontrolle2.setBemerkungen("");
		kontrolle2.setBestanden(true);
		kontrolle2.setFailAbgasverlust(false);
		kontrolle2.setFailCo(false);
		kontrolle2.setFailNo(false);
		kontrolle2.setFailOel(false);
		kontrolle2.setFailRusszahl(false);
		kontrolle2.setKenntnisnahmeKunde(true);
		kontrolle2.setKontrollart("Routinekontrolle");
		
		Kontrolle kontrolle3 = new Kontrolle();
		kontrolle3.setBemerkungen("");
		kontrolle3.setBestanden(false);
		kontrolle3.setEinregulierungMoeglich(true);
		kontrolle3.setFailAbgasverlust(true);
		kontrolle3.setFailCo(true);
		kontrolle3.setFailNo(false);
		kontrolle3.setFailOel(false);
		kontrolle3.setFailRusszahl(true);
		kontrolle3.setKenntnisnahmeKunde(true);
		kontrolle3.setKontrollart("Abnahmekontrolle");
		
		Kontrolle kontrolle4 = new Kontrolle();
		kontrolle4.setBemerkungen("");
		kontrolle4.setBestanden(false);
		kontrolle4.setEinregulierungMoeglich(true);
		kontrolle4.setFailAbgasverlust(true);
		kontrolle4.setFailCo(true);
		kontrolle4.setFailNo(false);
		kontrolle4.setFailOel(false);
		kontrolle4.setFailRusszahl(true);
		kontrolle4.setKenntnisnahmeKunde(true);
		kontrolle4.setKontrollart("Abnahmekontrolle");
		
		Kontrolle kontrolle5 = new Kontrolle();
		kontrolle5.setBemerkungen("");
		kontrolle5.setBestanden(false);
		kontrolle5.setEinregulierungMoeglich(true);
		kontrolle5.setFailAbgasverlust(true);
		kontrolle5.setFailCo(true);
		kontrolle5.setFailNo(false);
		kontrolle5.setFailOel(false);
		kontrolle5.setFailRusszahl(true);
		kontrolle5.setKenntnisnahmeKunde(true);
		kontrolle5.setKontrollart("Abnahmekontrolle");
		
		Kontrolle kontrolle6 = new Kontrolle();
		kontrolle6.setBemerkungen("");
		kontrolle6.setBestanden(false);
		kontrolle6.setEinregulierungMoeglich(true);
		kontrolle6.setFailAbgasverlust(true);
		kontrolle6.setFailCo(true);
		kontrolle6.setFailNo(false);
		kontrolle6.setFailOel(false);
		kontrolle6.setFailRusszahl(true);
		kontrolle6.setKenntnisnahmeKunde(true);
		kontrolle6.setKontrollart("Abnahmekontrolle");
		
		
		Kontrolle kontrolle7 = new Kontrolle();
		kontrolle7.setBemerkungen("");
		kontrolle7.setBestanden(false);
		kontrolle7.setEinregulierungMoeglich(true);
		kontrolle7.setFailAbgasverlust(true);
		kontrolle7.setFailCo(true);
		kontrolle7.setFailNo(false);
		kontrolle7.setFailOel(false);
		kontrolle7.setFailRusszahl(true);
		kontrolle7.setKenntnisnahmeKunde(true);
		kontrolle7.setKontrollart("Abnahmekontrolle");
		
		Kontrolle kontrolle8 = new Kontrolle();
		kontrolle8.setBemerkungen("");
		kontrolle8.setBestanden(false);
		kontrolle8.setEinregulierungMoeglich(true);
		kontrolle8.setFailAbgasverlust(true);
		kontrolle8.setFailCo(true);
		kontrolle8.setFailNo(false);
		kontrolle8.setFailOel(false);
		kontrolle8.setFailRusszahl(true);
		kontrolle8.setKenntnisnahmeKunde(true);
		kontrolle8.setKontrollart("Abnahmekontrolle");
		
		Kontrolle kontrolle9 = new Kontrolle();
		kontrolle9.setBemerkungen("");
		kontrolle9.setBestanden(false);
		kontrolle9.setEinregulierungMoeglich(true);
		kontrolle9.setFailAbgasverlust(true);
		kontrolle9.setFailCo(true);
		kontrolle9.setFailNo(false);
		kontrolle9.setFailOel(false);
		kontrolle9.setFailRusszahl(true);
		kontrolle9.setKenntnisnahmeKunde(true);
		kontrolle9.setKontrollart("Abnahmekontrolle");
		
		Kontrolle kontrolle10 = new Kontrolle();
		kontrolle10.setBemerkungen("");
		kontrolle10.setBestanden(false);
		kontrolle10.setEinregulierungMoeglich(true);
		kontrolle10.setFailAbgasverlust(true);
		kontrolle10.setFailCo(true);
		kontrolle10.setFailNo(false);
		kontrolle10.setFailOel(false);
		kontrolle10.setFailRusszahl(true);
		kontrolle10.setKenntnisnahmeKunde(true);
		kontrolle10.setKontrollart("Abnahmekontrolle");
	
		
		kontrolleRepository.persist(kontrolle1);
		kontrolleRepository.persist(kontrolle2, kontrolle3);
		
		ArrayList<Kontrolle> kListe = new ArrayList<Kontrolle>();
		kListe.add(kontrolle4);
		kListe.add(kontrolle5);
		kListe.add(kontrolle6);
		kListe.add(kontrolle7);
		kListe.add(kontrolle8);
		kListe.add(kontrolle9);
		kListe.add(kontrolle10);
		
		kontrolleRepository.persist(kListe);
	}
		
	@Test
	public void testGetKontrolle()
	{
		Optional<Kontrolle> kontrolle = kontrolleRepository.get(1);
		assertEquals(false, kontrolle.get().getBestanden());
		assertEquals(true, kontrolle.get().getEinregulierungMoeglich());
		assertEquals(true, kontrolle.get().getFailAbgasverlust());
		assertEquals(true, kontrolle.get().getFailCo());
		assertEquals(false, kontrolle.get().getFailNo());
		assertEquals(false, kontrolle.get().getFailOel());
		assertEquals(true, kontrolle.get().getFailRusszahl());
		assertEquals(true, kontrolle.get().getKenntnisnahmeKunde());
		assertEquals("Abnahmekontrolle", kontrolle.get().getKontrollart());
		
	}

	
	@Test
	public void testGetKontrollen()
	{
		Set<Kontrolle> kList = kontrolleRepository.get(m -> m.getId() <= 5);
		assertEquals(5, kList.size());
	}
	
	// Hier werden
	@Test
	public void testUpdateKontrolle()
	{
		Kontrolle kontrolle = kontrolleRepository.get(1).get();
		kontrolle.setBemerkungen("Alles schlecht");
		
		kontrolleRepository.persist(kontrolle);
		assertEquals("Alles schlecht", kontrolleRepository.get(1).get().getBemerkungen());
		
		Kontrolle kontrolle2 = kontrolleRepository.get(2).get();
		kontrolle2.setBemerkungen("Alles schlechter");
		
		Kontrolle kontrolle3 = kontrolleRepository.get(3).get();
		kontrolle3.setBemerkungen("Alles noch viel schlechter");
		
		kontrolleRepository.persist(kontrolle2, kontrolle3);

		assertEquals("Alles schlechter", kontrolleRepository.get(2).get().getBemerkungen());
		assertEquals("Alles noch viel schlechter", kontrolleRepository.get(3).get().getBemerkungen());
		
		
		Kontrolle kontrolle4 = kontrolleRepository.get(4).get();
		kontrolle4.setBemerkungen("schlechter als schlecht");
		
		Kontrolle kontrolle5 = kontrolleRepository.get(5).get();
		kontrolle5.setBemerkungen("Absolute Katastrophe");
		
		ArrayList<Kontrolle> kListe = new ArrayList<Kontrolle>();
		kListe.add(kontrolle3);
		kListe.add(kontrolle4);
		kListe.add(kontrolle5);
		
		kontrolleRepository.persist(kListe);
		
		assertEquals("schlechter als schlecht", kontrolleRepository.get(4).get().getBemerkungen());
		assertEquals("Absolute Katastrophe", kontrolleRepository.get(5).get().getBemerkungen());
		
	}
	
	@Test
	public void testCanRemoveKontrolle()
	{
		kontrolleRepository.remove(6);
		assertEquals(9, kontrolleRepository.get().size());
		
		kontrolleRepository.remove(kontrolleRepository.get(7).get(), kontrolleRepository.get(8).get());
		assertEquals(7, kontrolleRepository.get().size());
		
		Set<Kontrolle> kList = kontrolleRepository.get(m -> m.getId() > 5 && m.getId() <= 10);
		kontrolleRepository.remove(kList);
		assertEquals(0, kontrolleRepository.get(m -> m.getId() > 5 && m.getId() <= 10).size());
	}
	
	@Test
	public void testValidatePerson()
	{
		Kontrolle kontrolle1 = new Kontrolle();
		kontrolle1.setBemerkungen("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet cl");
		kontrolle1.setBestanden(null);
		kontrolle1.setFailAbgasverlust(null);
		kontrolle1.setFailCo(null);
		kontrolle1.setFailNo(null);
		kontrolle1.setFailOel(null);
		kontrolle1.setFailRusszahl(null);
		kontrolle1.setKenntnisnahmeKunde(null);
		
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Kontrolle>> constraintViolations = validator.validate(kontrolle1);
	
		assertEquals(8, constraintViolations.size());
		
		kontrolle1.setBemerkungen("kurz");
		constraintViolations = validator.validate(kontrolle1);
		assertEquals(7, constraintViolations.size());
		
		kontrolle1.setBestanden(true);
		constraintViolations = validator.validate(kontrolle1);
		assertEquals(6, constraintViolations.size());
		
		kontrolle1.setFailAbgasverlust(true);
		constraintViolations = validator.validate(kontrolle1);
		assertEquals(5, constraintViolations.size());

		kontrolle1.setFailCo(true);
		constraintViolations = validator.validate(kontrolle1);
		assertEquals(4, constraintViolations.size());

		kontrolle1.setFailNo(true);
		constraintViolations = validator.validate(kontrolle1);
		assertEquals(3, constraintViolations.size());
		
		kontrolle1.setFailOel(true);
		constraintViolations = validator.validate(kontrolle1);
		assertEquals(2, constraintViolations.size());
		
		kontrolle1.setFailRusszahl(true);
		constraintViolations = validator.validate(kontrolle1);
		assertEquals(1, constraintViolations.size());
		
		kontrolle1.setKenntnisnahmeKunde(true);
		constraintViolations = validator.validate(kontrolle1);
		assertEquals(0, constraintViolations.size());
		
		
	}
}
