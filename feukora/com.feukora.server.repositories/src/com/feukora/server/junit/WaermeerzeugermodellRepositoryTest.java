package com.feukora.server.junit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Brennstoff;
import com.feukora.server.models.Waermeerzeugermodell;
import com.feukora.server.repositories.IRepository;
import com.feukora.server.repositories.WaermeerzeugermodellRepository;

/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 21.11.2015
 *
 *Klasse um WaermeerzeugermodellRepository zu testen. 
 *
 */
public class WaermeerzeugermodellRepositoryTest {

	private static IRepository<Waermeerzeugermodell> waermeerzeugermodellRepository = new WaermeerzeugermodellRepository(new ConfigManager().getTestDatabaseName());
	private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	
	/**
	 * Es werden 10 Waermeerzeugermodell-Objekte mit Brennstoffen generiert.
	 * Die ersten 5 Objekte werden benutzt, um die Werte der Variablen zu �berpr�fen und zu �ndern.
	 * Mit den anderen 5 Objekten wird die L�schfunktion getestet.
	 */
	@BeforeClass
	public static void init() {
		
		Brennstoff brennstoff1 = new Brennstoff();
		brennstoff1.setBezeichnung("Holz");
		
		Brennstoff brennstoff2 = new Brennstoff();
		brennstoff2.setBezeichnung("Oel");
		
		Brennstoff brennstoff3 = new Brennstoff();
		brennstoff3.setBezeichnung("Gas");

		
		Waermeerzeugermodell waermeerzeugerm1 = new Waermeerzeugermodell();
		waermeerzeugerm1.setBezeichnung("Model 1");
		waermeerzeugerm1.setHersteller("Arnold GmbH");
		waermeerzeugerm1.setBrennstoff(brennstoff1);
		
		Waermeerzeugermodell waermeerzeugerm2 = new Waermeerzeugermodell();
		waermeerzeugerm2.setBezeichnung("Model 2");
		waermeerzeugerm2.setHersteller("Brenn AG");
		waermeerzeugerm2.setBrennstoff(brennstoff2);
		
		Waermeerzeugermodell waermeerzeugerm3 = new Waermeerzeugermodell();
		waermeerzeugerm3.setBezeichnung("Model 2");
		waermeerzeugerm3.setHersteller("Schmudo Verein");
		waermeerzeugerm3.setBrennstoff(brennstoff3);
		
		Waermeerzeugermodell waermeerzeugerm4 = new Waermeerzeugermodell();
		waermeerzeugerm4.setBezeichnung("Model 4");
		waermeerzeugerm4.setHersteller("Mundo AG");
		waermeerzeugerm4.setBrennstoff(brennstoff1);
		
		Waermeerzeugermodell waermeerzeugerm5 = new Waermeerzeugermodell();
		waermeerzeugerm5.setBezeichnung("Model 5");
		waermeerzeugerm5.setHersteller("Hitze und Feuer");
		waermeerzeugerm5.setBrennstoff(brennstoff3);
		
		Waermeerzeugermodell waermeerzeugerm6 = new Waermeerzeugermodell();
		waermeerzeugerm6.setBezeichnung("Model 6");
		waermeerzeugerm6.setHersteller("Gebr�der Keller");
		waermeerzeugerm6.setBrennstoff(brennstoff2);
		
		Waermeerzeugermodell waermeerzeugerm7 = new Waermeerzeugermodell();
		waermeerzeugerm7.setBezeichnung("Model 7");
		waermeerzeugerm7.setHersteller("Sole Mole");
		waermeerzeugerm7.setBrennstoff(brennstoff2);
		
		Waermeerzeugermodell waermeerzeugerm8 = new Waermeerzeugermodell();
		waermeerzeugerm8.setBezeichnung("Model 8");
		waermeerzeugerm8.setHersteller("Mundo AG");
		waermeerzeugerm8.setBrennstoff(brennstoff1);
		
		Waermeerzeugermodell waermeerzeugerm9 = new Waermeerzeugermodell();
		waermeerzeugerm9.setBezeichnung("Model 9");
		waermeerzeugerm9.setHersteller("Feuer und Flamme");
		waermeerzeugerm9.setBrennstoff(brennstoff3);
		
		Waermeerzeugermodell waermeerzeugerm10 = new Waermeerzeugermodell();
		waermeerzeugerm10.setBezeichnung("Model 10");
		waermeerzeugerm10.setHersteller("Mundo AG");
		waermeerzeugerm10.setBrennstoff(brennstoff1);
		
			
		
		waermeerzeugermodellRepository.persist(waermeerzeugerm1);
		waermeerzeugermodellRepository.persist(waermeerzeugerm2, waermeerzeugerm3);
		
		ArrayList<Waermeerzeugermodell> bListe = new ArrayList<Waermeerzeugermodell>();
		bListe.add(waermeerzeugerm4);
		bListe.add(waermeerzeugerm5);
		bListe.add(waermeerzeugerm6);
		bListe.add(waermeerzeugerm7);
		bListe.add(waermeerzeugerm8);
		bListe.add(waermeerzeugerm9);
		bListe.add(waermeerzeugerm10);
		
		waermeerzeugermodellRepository.persist(bListe);
	}

	/**
	 * Die Variablen vom 1. Waermeerzeugermodell-Objekt wird auf Gleichheit �berpr�ft.
	 */	
	@Test
	public void testCanGetPerson()
	{
		Optional<Waermeerzeugermodell> waermeerzeugerm = waermeerzeugermodellRepository.get(1);
		assertEquals("Arnold GmbH", waermeerzeugerm.get().getHersteller());
		assertEquals("Holz", waermeerzeugerm.get().getBrennstoff().getBezeichnung());
		
	}

	/**
	 * Die ersten 5 Waermeerzeugermodell-Objekte werden in ein Set geholt.
	 * Anschliessend wird �berpr�ft, ob das Set aus 5 Objekten besteht.
	 */
	@Test
	public void testCanGetPersons()
	{
		Set<Waermeerzeugermodell> bList = waermeerzeugermodellRepository.get(m -> m.getId() <= 5);
		assertEquals(5, bList.size());
	}

	/**
	 * Bei den ersten 5 Waermeerzeugermodell-Objekte wird die Variable Bezeichnung ge�ndert.
	 * Die Objekte werden anschliessend wieder in die Datenbank geschrieben.
	 * Danach wird die Bezeichnung vom Objekt wieder ausgelesen. 
	 * Es wird �berpr�ft, ob die Variablen ge�ndert wurden.
	 */
	@Test
	public void testCanUpdatePersons()
	{
		Optional<Waermeerzeugermodell> waermeerzeugerm1 = waermeerzeugermodellRepository.get(1);
		waermeerzeugerm1.get().setBezeichnung("neue Bezeichnung 1");
		
		waermeerzeugermodellRepository.persist(waermeerzeugerm1.get());
		assertEquals("neue Bezeichnung 1", waermeerzeugermodellRepository.get(1).get().getBezeichnung());
		
		Waermeerzeugermodell waermeerzeugerm2 = waermeerzeugermodellRepository.get(2).get();
		waermeerzeugerm2.setBezeichnung("neue Bezeichnung 2");
		
		Waermeerzeugermodell waermeerzeugerm3 = waermeerzeugermodellRepository.get(3).get();
		waermeerzeugerm3.setBezeichnung("neue Bezeichnung 3");
		
		waermeerzeugermodellRepository.persist(waermeerzeugerm2, waermeerzeugerm3);
		assertEquals("neue Bezeichnung 2", waermeerzeugermodellRepository.get(2).get().getBezeichnung());
		assertEquals("neue Bezeichnung 3", waermeerzeugermodellRepository.get(3).get().getBezeichnung());
		
		Waermeerzeugermodell waermeerzeugerm4 = waermeerzeugermodellRepository.get(4).get();
		waermeerzeugerm4.setBezeichnung("neue Bezeichnung 4");
		
		Waermeerzeugermodell waermeerzeugerm5 = waermeerzeugermodellRepository.get(5).get();
		waermeerzeugerm5.setBezeichnung("neue Bezeichnung 5");
		
		ArrayList<Waermeerzeugermodell> bListe = new ArrayList<Waermeerzeugermodell>();
		bListe.add(waermeerzeugerm3);
		bListe.add(waermeerzeugerm4);
		bListe.add(waermeerzeugerm5);
	
		waermeerzeugermodellRepository.persist(bListe);
		
		assertEquals("neue Bezeichnung 4", waermeerzeugermodellRepository.get(4).get().getBezeichnung());
		assertEquals("neue Bezeichnung 5", waermeerzeugermodellRepository.get(5).get().getBezeichnung());
	}

	/**
	 * Die Waermeerzeugermodell-Objekte mit der Id gr�sser als 5 werden gel�scht. 
	 */
	@Test
	public void testCanRemovePersons()
	{
		waermeerzeugermodellRepository.remove(6);
		assertEquals(9, waermeerzeugermodellRepository.get().size());
		
		waermeerzeugermodellRepository.remove(waermeerzeugermodellRepository.get(7).get(), waermeerzeugermodellRepository.get(8).get());
		assertEquals(7, waermeerzeugermodellRepository.get().size());
		
		Set<Waermeerzeugermodell> bList = waermeerzeugermodellRepository.get(m -> m.getId() > 5 && m.getId() <= 10);
		waermeerzeugermodellRepository.remove(bList);
		assertEquals(0, waermeerzeugermodellRepository.get(m -> m.getId() > 5 && m.getId() <= 10).size());
	}

	/**
	 * Bean-Validation wird getestet.
	 * Es wird ein Waermeerzeugermodell-Objekt erstellt.
	 * Zuerst werden in die Felder ung�ltige Werte geschrieben.
	 * Danach wird die Anzahl der ung�ltigen Werte gez�hlt und Schritt f�r Schritt
	 * mit g�ltigen Werte abgef�llt, bis die Anzahl der ung�ltigen Werte auf 0 ist.
	 */
	@Test
	public void testValidatePerson()
	{
		Waermeerzeugermodell waermeerzeugerm = new Waermeerzeugermodell();
		waermeerzeugerm.setBezeichnung("Ribeye shankle pork loin hamburger, bresaola sirloin jowl beef tenderloin alcatra andouille ground round pancetta. Filet mignon turducken sirloin pork chop hamburger ball tip");
		waermeerzeugerm.setHersteller("Hamburger t-bone ham sirloin kevin bacon. Boudin pork belly beef, doner ground round shoulder pastrami frankfurter.");
		//waermeerzeugerm.setBrennstoff(brennstoff1);

		
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Waermeerzeugermodell>> constraintViolations = validator.validate(waermeerzeugerm);
	
		assertEquals(3, constraintViolations.size());
		
		waermeerzeugerm.setBezeichnung("Ueli");
		constraintViolations = validator.validate(waermeerzeugerm);
		assertEquals(2, constraintViolations.size());
		
		waermeerzeugerm.setHersteller("Maurer");
		constraintViolations = validator.validate(waermeerzeugerm);
		assertEquals(1, constraintViolations.size());
		
		Brennstoff brennstoff = new Brennstoff();
		brennstoff.setBezeichnung("Modell 1");
		waermeerzeugerm.setBrennstoff(brennstoff);
		constraintViolations = validator.validate(waermeerzeugerm);
		assertEquals(0, constraintViolations.size());
		
				
	}
}
