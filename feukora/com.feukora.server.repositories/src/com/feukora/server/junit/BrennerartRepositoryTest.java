package com.feukora.server.junit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Brennerart;
import com.feukora.server.repositories.IRepository;
import com.feukora.server.repositories.BrennerartRepository;

/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 21.11.2015
 *
 *Klasse um BrennerartRepository zu testen. 
 *
 */
public class BrennerartRepositoryTest {

	private static IRepository<Brennerart> brennerartRepository = new BrennerartRepository(new ConfigManager().getTestDatabaseName());
	private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	
	/**
	 * Es werden 10 Brennerart-Objekte generiert.
	 * Die ersten 5 Objekte werden benutzt, um die Werte der Variablen zu �berpr�fen und zu �ndern.
	 * Mit den anderen 5 Objekten wird die L�schfunktion getestet.
	 */
	@BeforeClass
	public static void init() {
		
		Brennerart brennerart1 = new Brennerart();
		brennerart1.setBezeichnung("Brennerart 1");
		
		Brennerart brennerart2 = new Brennerart();
		brennerart2.setBezeichnung("Brennerart 2");
		
		Brennerart brennerart3 = new Brennerart();
		brennerart3.setBezeichnung("Brennerart 3");
		
		Brennerart brennerart4 = new Brennerart();
		brennerart4.setBezeichnung("Brennerart 4");
		
		Brennerart brennerart5 = new Brennerart();
		brennerart5.setBezeichnung("Brennerart 5");
		
		Brennerart brennerart6 = new Brennerart();
		brennerart6.setBezeichnung("Brennerart 6");
		
		Brennerart brennerart7 = new Brennerart();
		brennerart7.setBezeichnung("Brennerart 7");
		
		Brennerart brennerart8 = new Brennerart();
		brennerart8.setBezeichnung("Brennerart 8");
		
		Brennerart brennerart9 = new Brennerart();
		brennerart9.setBezeichnung("Brennerart 9");
		
		Brennerart brennerart10 = new Brennerart();
		brennerart10.setBezeichnung("Brennerart 10");
		
		brennerartRepository.persist(brennerart1);
		brennerartRepository.persist(brennerart2, brennerart3);
		
		ArrayList<Brennerart> bListe = new ArrayList<Brennerart>();
		bListe.add(brennerart4);
		bListe.add(brennerart5);
		bListe.add(brennerart6);
		bListe.add(brennerart7);
		bListe.add(brennerart8);
		bListe.add(brennerart9);
		bListe.add(brennerart10);
		
		brennerartRepository.persist(bListe);
	}
	
	/**
	 * Die Variablen vom 1. Brennerart-Objekt wird auf Gleichheit �berpr�ft.
	 */
	@Test
	public void testCanGetBrennerart()
	{
		Brennerart brennerart = brennerartRepository.get(1).get();
		assertEquals(1, (int)brennerart.getId());

	}

	/**
	 * Die ersten 5 Brennerart-Objekte werden in ein Set geholt.
	 * Anschliessend wird �berpr�ft, ob das Set aus 5 Objekten besteht.
	 */
	@Test
	public void testCanGetBrennerarten()
	{

		Set<Brennerart> bList = brennerartRepository.get(m -> m.getId() <= 5);
		assertEquals(5, bList.size());
	}

	/**
	 * Bei den ersten 5 Brennerart-Objekte wird die Variable Bezeichnung ge�ndert.
	 * Die Objekte werden anschliessend wieder in die Datenbank geschrieben.
	 * Danach wird die Bezeichnung vom Objekt wieder ausgelesen. 
	 * Es wird �berpr�ft, ob die Variablen ge�ndert wurden.
	 */
	@Test
	public void testCanUpdateBrennerarten()
	{
		Brennerart brennerart = brennerartRepository.get(1).get();
		brennerart.setBezeichnung("Neue Bezeichnung 1");
		
		brennerartRepository.persist(brennerart);
		assertEquals("Neue Bezeichnung 1", brennerartRepository.get(1).get().getBezeichnung());
		
		Brennerart brennerart2 = brennerartRepository.get(2).get();
		brennerart2.setBezeichnung("Neue Bezeichnung 2");
		
		Brennerart brennerart3 = brennerartRepository.get(3).get();
		brennerart3.setBezeichnung("Neue Bezeichnung 3");
		
		brennerartRepository.persist(brennerart2, brennerart3);
		assertEquals("Neue Bezeichnung 2", brennerartRepository.get(2).get().getBezeichnung());
		assertEquals("Neue Bezeichnung 3", brennerartRepository.get(3).get().getBezeichnung());
		
		Brennerart brennerart4 = brennerartRepository.get(4).get();
		brennerart4.setBezeichnung("Neue Bezeichnung  4");
		
		Brennerart brennerart5 = brennerartRepository.get(5).get();
		brennerart5.setBezeichnung("Neue Bezeichnung  5");
		
		ArrayList<Brennerart> bListe = new ArrayList<Brennerart>();
		bListe.add(brennerart3);
		bListe.add(brennerart4);
		bListe.add(brennerart5);
	
		brennerartRepository.persist(bListe);
		
		assertEquals("Neue Bezeichnung  4", brennerartRepository.get(4).get().getBezeichnung());
		assertEquals("Neue Bezeichnung  5", brennerartRepository.get(5).get().getBezeichnung());
	}
	
	/**
	 * Die Brennerart-Objekte mit der Id gr�sser als 5 werden gel�scht. 
	 */
	@Test
	public void testCanRemoveBrennerarten()
	{
		brennerartRepository.remove(6);
		assertEquals(9, brennerartRepository.get().size());
		
		brennerartRepository.remove(brennerartRepository.get(7).get(), brennerartRepository.get(8).get());
		assertEquals(7, brennerartRepository.get().size());
		
		Set<Brennerart> bList = brennerartRepository.get(m -> m.getId() > 5 && m.getId() <= 10);
		brennerartRepository.remove(bList);
		assertEquals(0, brennerartRepository.get(m -> m.getId() > 5 && m.getId() <= 10).size());
	}
	
	/**
	 * Bean-Validation wird getestet.
	 * Es wird ein Brennerart-Objekt erstellt.
	 * Zuerst werden in die Felder ung�ltige Werte geschrieben.
	 * Danach wird die Anzahl der ung�ltigen Werte gez�hlt und Schritt f�r Schritt
	 * mit g�ltigen Werte abgef�llt, bis die Anzahl der ung�ltigen Werte auf 0 ist.
	 */
	@Test
	public void testValidateBrennerart()
	{
		Brennerart brennerart = new Brennerart();
		brennerart.setBezeichnung("Ribeye shankle pork loin hamburger, bresaola sirloin jowl beef tenderloin alcatra andouille ground round pancetta. Filet mignon turducken sirloin pork chop hamburger ball tip");

		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Brennerart>> constraintViolations = validator.validate(brennerart);
	
		assertEquals(1, constraintViolations.size());
		
		brennerart.setBezeichnung("Die neue Bezeichnung");
		constraintViolations = validator.validate(brennerart);
		assertEquals(0, constraintViolations.size());
		
		}
}
