/**
 * 
 */
package com.feukora.server.junit;

import java.util.Iterator;
import java.util.Set;

import org.junit.Test;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Person;
import com.feukora.server.models.PlzOrt;
import com.feukora.server.repositories.IRepository;
import com.feukora.server.repositories.PersonRepository;

import junit.framework.TestCase;

/**
 * @author Andrew
 *
 */
public class PersonRepositoryTest extends TestCase {

	private Person p1;
	private Person p2;
	private Person p3;
	private PlzOrt plzOrt1;
	private PlzOrt plzOrt2;
	private PlzOrt plzOrt3;
	
	private IRepository<Person> personRepository = new PersonRepository(new ConfigManager().getTestDatabaseName());
	
	
	@Test
	public void testCreatePerson(boolean mehrerePers){
		
		plzOrt1 = new PlzOrt();
		plzOrt1.setOrt("Luzern");
		plzOrt1.setPlz(6000);
		
		p1 = new Person();
		p1.setVorname("Peter");
		p1.setNachname("Meier");
		p1.setEmail("peter.meier@muster.ch");
		p1.setMobile("079 900 00 01");
		p1.setStrasse("Musterstrasse 1");
		p1.setPlzOrt(plzOrt1);
		
		if (mehrerePers == true){
			
		
		plzOrt2 = new PlzOrt();
		plzOrt2.setOrt("Luzern");
		plzOrt2.setPlz(6001);
		
		plzOrt3 = new PlzOrt();
		plzOrt3.setOrt("Z�rich");
		plzOrt3.setPlz(8000);

		
		
		p2 = new Person();
		p2.setVorname("John");
		p2.setNachname("Doe");
		p2.setEmail("john.doe@muster.ch");
		p2.setMobile("079 900 00 02");
		p2.setStrasse("Musterstrasse 2");
		p2.setPlzOrt(plzOrt2);
		
		p3 = new Person();
		p3.setVorname("Jean-Pierre");
		p3.setNachname("Molyneux");
		p3.setEmail("jean.molyneux@muster.ch");
		p3.setMobile("079 900 00 03");
		p3.setStrasse("Musterstrasse 3");
		p3.setPlzOrt(plzOrt3);
		}
		
		
		
	}
	
/**public void testCreatePerson1(String vorname, String nachname, String strasse, Integer plz, String ort, String telefon, String mobile, String email){
		
		
		plzOrt = new PlzOrt();
		
		plzOrt.setOrt(ort);
		plzOrt.setPlz(plz);

		p = new Person();
		p.setVorname(vorname);
		p.setNachname(nachname);
		p.setStrasse(strasse);
		p.setPlzOrt(plzOrt);
		p.setTelefon(telefon);
		p.setMobile(mobile);
		p.setEmail(email);
	
		
		
	}
	*/
	
	@Test
	public void testAddPerson() {
		
		
		testCreatePerson(false);

		personRepository.persist(p1);
		
		Set<Person> pList = personRepository.get();
		
		p1 = null;
		assertEquals(1, pList.size());		
		for (Iterator<Person> it = pList.iterator(); it.hasNext();){
			p1 = it.next();
			assertEquals(p1.getVorname(),"Peter");
			
		}
		
		
		
	}
	/**
	@Test
	public void testAddPersons() {
		
		

		//personRepository.persist(p1, p2, p3);
		
		Set<Person> pList = personRepository.get();
		
		// 4 weil wir in der ersten Methode schon eine Person hinzugef�gt haben
		assertEquals(4, pList.size());
	}
	
	@Test
	public void testAddPersonList() {
		
		ArrayList<Person> pList = new ArrayList<Person>();
		
		for (int i = 0; i < 20; i++) {
			
			PlzOrt plzOrt = new PlzOrt();
			plzOrt.setOrt("Luzern");
			plzOrt.setPlz(6000 + i);
			
			Person p = new Person();
			p.setVorname("R2D" + i);
			p.setNachname("Model " + i);
			p.setEmail("r2d" + 1 + "@robot.ch");
			p.setMobile("088 900 00 0" + i);
			p.setStrasse("Milkyway " + i);
			p.setPlzOrt(plzOrt);
			
			pList.add(p);
		}
		
		personRepository.persist(pList);
		
		// 24 weil wir weiter oben schon 4 Personen hinzugef�gt haben
		Set<Person> result = personRepository.get();
		assertEquals(24, result.size());
	}
	
	@Test
	public void testGetPerson() {
		
		
	
	}
	
	@Test
	public void testGetPersons() {
		
	}

	@Test
	public void testUpdatePerson() {
		
	}
	
	@Test
	public void testDeletePerson() {
		
	}
	
	@Test
	public void testDeletePersons() {
		
	}
	
	*/
}
