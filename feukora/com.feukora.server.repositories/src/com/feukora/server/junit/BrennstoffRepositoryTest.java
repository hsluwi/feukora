package com.feukora.server.junit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Brennstoff;
import com.feukora.server.repositories.IRepository;
import com.feukora.server.repositories.BrennstoffRepository;

/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 19.11.2015
 *
 *Klasse um BrennstoffRepository zu testen. 
 *
 */
public class BrennstoffRepositoryTest {

	private static IRepository<Brennstoff> brennstoffRepository = new BrennstoffRepository(new ConfigManager().getTestDatabaseName());
	private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

	/**
	 * Es werden 10 Brennstoff-Objekte generiert.
	 * Die ersten 5 Objekte werden benutzt, um die Werte der Variablen zu �berpr�fen und zu �ndern.
	 * Mit den anderen 5 Objekten wird die L�schfunktion getestet.
	 */
	@BeforeClass
	public static void init() {
		
		Brennstoff brennstoff1 = new Brennstoff();
		brennstoff1.setBezeichnung("Holz");
		
		Brennstoff brennstoff2 = new Brennstoff();
		brennstoff2.setBezeichnung("Oel");
		
		Brennstoff brennstoff3 = new Brennstoff();
		brennstoff3.setBezeichnung("Gas");
		
		Brennstoff brennstoff4 = new Brennstoff();
		brennstoff4.setBezeichnung("Kohle");
		
		Brennstoff brennstoff5 = new Brennstoff();
		brennstoff5.setBezeichnung("Benzin");
		
		Brennstoff brennstoff6 = new Brennstoff();
		brennstoff6.setBezeichnung("Erd�l");
		
		Brennstoff brennstoff7 = new Brennstoff();
		brennstoff7.setBezeichnung("BS45");
		
		Brennstoff brennstoff8 = new Brennstoff();
		brennstoff8.setBezeichnung("Brennstoff 8");
		
		Brennstoff brennstoff9 = new Brennstoff();
		brennstoff9.setBezeichnung("BS 9");
		
		Brennstoff brennstoff10 = new Brennstoff();
		brennstoff10.setBezeichnung("BrennStoff 100");
		
		brennstoffRepository.persist(brennstoff1);
		brennstoffRepository.persist(brennstoff2, brennstoff3);
		
		ArrayList<Brennstoff> bListe = new ArrayList<Brennstoff>();
		bListe.add(brennstoff4);
		bListe.add(brennstoff5);
		bListe.add(brennstoff6);
		bListe.add(brennstoff7);
		bListe.add(brennstoff8);
		bListe.add(brennstoff9);
		bListe.add(brennstoff10);
		
		brennstoffRepository.persist(bListe);
	}

	/**
	 * Die Variablen vom 1. Brennstoff-Objekt wird auf Gleichheit �berpr�ft.
	 */	
	@Test
	public void testCanGetBrennstoff()
	{
		Brennstoff brennstoff = brennstoffRepository.get(1).get();
		assertEquals(1, (int)brennstoff.getId());
		
		
	}

	/**
	 * Die ersten 5 Brennstoff-Objekte werden in ein Set geholt.
	 * Anschliessend wird �berpr�ft, ob das Set aus 5 Objekten besteht.
	 */
	@Test
	public void testCanGetBrennstoffe()
	{
		Set<Brennstoff> bList = brennstoffRepository.get(m -> m.getId() <= 5);
		assertEquals(5, bList.size());
	}

	/**
	 * Bei den ersten 5 Brennstoff-Objekte wird die Variable Bezeichnung ge�ndert.
	 * Die Objekte werden anschliessend wieder in die Datenbank geschrieben.
	 * Danach wird die Bezeichnung vom Objekt wieder ausgelesen. 
	 * Es wird �berpr�ft, ob die Variablen ge�ndert wurden.
	 */
	@Test
	public void testCanUpdateBrennstoffe()
	{
		Optional<Brennstoff> brennstoff = brennstoffRepository.get(1);
		brennstoff.get().setBezeichnung("neu Holz");
		
		brennstoffRepository.persist(brennstoff.get());
		assertEquals("neu Holz", brennstoffRepository.get(1).get().getBezeichnung());
		
		Brennstoff brennstoff2 = brennstoffRepository.get(2).get();
		brennstoff2.setBezeichnung("neu Oel");
		
		Brennstoff brennstoff3 = brennstoffRepository.get(3).get();
		brennstoff3.setBezeichnung("neu Gas");
		
		brennstoffRepository.persist(brennstoff2, brennstoff3);
		assertEquals("neu Oel", brennstoffRepository.get(2).get().getBezeichnung());
		assertEquals("neu Gas", brennstoffRepository.get(3).get().getBezeichnung());
		
		Brennstoff brennstoff4 = brennstoffRepository.get(4).get();
		brennstoff4.setBezeichnung("neu Kohle");
		
		Brennstoff brennstoff5 = brennstoffRepository.get(5).get();
		brennstoff5.setBezeichnung("neu Benzin");
		
		ArrayList<Brennstoff> bListe = new ArrayList<Brennstoff>();
		bListe.add(brennstoff3);
		bListe.add(brennstoff4);
		bListe.add(brennstoff5);

	
		brennstoffRepository.persist(bListe);
		
		assertEquals("neu Kohle", brennstoffRepository.get(4).get().getBezeichnung());
		assertEquals("neu Benzin", brennstoffRepository.get(5).get().getBezeichnung());
	}

	/**
	 * Die Brennstoff-Objekte mit der Id gr�sser als 5 werden gel�scht. 
	 */
	@Test
	public void testCanRemoveBrennstoffe()
	{
		brennstoffRepository.remove(6);
		assertEquals(9, brennstoffRepository.get().size());
		
		brennstoffRepository.remove(brennstoffRepository.get(7).get(), brennstoffRepository.get(8).get());
		assertEquals(7, brennstoffRepository.get().size());
		
		Set<Brennstoff> bList = brennstoffRepository.get(m -> m.getId() > 5 && m.getId() <= 10);
		brennstoffRepository.remove(bList);
		assertEquals(0, brennstoffRepository.get(m -> m.getId() > 5 && m.getId() <= 10).size());
	}

	/**
	 * Bean-Validation wird getestet.
	 * Es wird ein Brennstoff-Objekt erstellt.
	 * Zuerst werden in die Felder ung�ltige Werte geschrieben.
	 * Danach wird die Anzahl der ung�ltigen Werte gez�hlt und Schritt f�r Schritt
	 * mit g�ltigen Werte abgef�llt, bis die Anzahl der ung�ltigen Werte auf 0 ist.
	 */
	@Test
	public void testValidateBrennstoff()
	{
		Brennstoff brennstoff1 = new Brennstoff();
		brennstoff1.setBezeichnung("Ribeye shankle pork loin hamburger, bresaola sirloin jowl beef tenderloin alcatra andouille ground round pancetta. Filet mignon turducken sirloin pork chop hamburger ball tip");
		
		
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Brennstoff>> constraintViolations = validator.validate(brennstoff1);
	
		assertEquals(1, constraintViolations.size());
		
		brennstoff1.setBezeichnung("Gas");
		constraintViolations = validator.validate(brennstoff1);
		assertEquals(0, constraintViolations.size());
	
		
	}
}
