package com.feukora.server.junit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Kunde;
import com.feukora.server.models.PlzOrt;
import com.feukora.server.repositories.IRepository;
import com.feukora.server.repositories.KundeRepository;

/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 19.11.2015
 *
 *Klasse um KundeRepository zu testen. 
 *
 */
public class KundeRepositoryTest {

	private static IRepository<Kunde> kundenRepository = new KundeRepository(new ConfigManager().getTestDatabaseName());
	private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

	/**
	 * Es werden 10 Kunde-Objektemit PlzOrt generiert.
	 * Die ersten 5 Objekte werden benutzt, um die Werte der Variablen zu �berpr�fen und zu �ndern.
	 * Mit den anderen 5 Objekten wird die L�schfunktion getestet.
	 */
	@BeforeClass
	public static void init() {
		
	
		
		PlzOrt plzOrt1 = new PlzOrt();
		plzOrt1.setOrt("Z�rich");
		plzOrt1.setPlz(8001);
		
		PlzOrt plzOrt2 = new PlzOrt();
		plzOrt2.setOrt("Luzern");
		plzOrt2.setPlz(6005);
		
		PlzOrt plzOrt3 = new PlzOrt();
		plzOrt3.setOrt("Bern");
		plzOrt3.setPlz(3012);
		
		PlzOrt plzOrt4 = new PlzOrt();
		plzOrt4.setOrt("Lausanne");
		plzOrt4.setPlz(1045);
		
		PlzOrt plzOrt5 = new PlzOrt();
		plzOrt5.setOrt("Winterthur");
		plzOrt5.setPlz(8400);
		
		Kunde kunde1 = new Kunde();
		kunde1.setVorname("Peter");
		kunde1.setNachname("Brandenberger");
		kunde1.setEmail("p.brandenberger@gmail.ch");
		kunde1.setStrasse("Schlossgasse 42");
		kunde1.setTelefon("079 451 23 21");
		kunde1.setMobile("079 451 23 21");
		kunde1.setFirma("Brandenberger Bau AG");
		kunde1.setPlzOrt(plzOrt1);
		kunde1.setKundenArt("Hauswart");
		
		Kunde kunde2 = new Kunde();
		kunde2.setVorname("Hans");
		kunde2.setNachname("Vogel");
		kunde2.setEmail("hans.vogel@gmail.ch");
		kunde2.setStrasse("Kellerstrasse 3");
		kunde2.setTelefon("078 651 23 12");
		kunde2.setMobile("078 651 23 12");
		kunde2.setFirma("Vogel Heizungen GmbH");
		kunde2.setPlzOrt(plzOrt2);
		kunde2.setKundenArt("Verwaltung");
		
		Kunde kunde3 = new Kunde();
		kunde3.setVorname("Urs");
		kunde3.setNachname("Reuter");
		kunde3.setEmail("urs.reuter@gmail.ch");
		kunde3.setStrasse("Hohenrain 25");
		kunde3.setTelefon("076 122 45 54");
		kunde3.setMobile("076 122 45 54");
		kunde3.setFirma("Reuter Anlagen AG");
		kunde3.setPlzOrt(plzOrt3);
		kunde3.setKundenArt("Hauswart");
		
		Kunde kunde4 = new Kunde();
		kunde4.setVorname("Pierre");
		kunde4.setNachname("Depard");
		kunde4.setEmail("p.depard@gmail.com");
		kunde4.setStrasse("Burgstrasse 4");
		kunde4.setTelefon("079 222 35 35");
		kunde4.setMobile("079 222 35 35");
		kunde4.setFirma("Depard AG");
		kunde4.setPlzOrt(plzOrt4);
		kunde4.setKundenArt("Verwaltung");
		
		Kunde kunde5 = new Kunde();
		kunde5.setVorname("Edgard");
		kunde5.setNachname("Schneebeli");
		kunde5.setEmail("e.schneebeli@gmail.ch");
		kunde5.setStrasse("Hochdorfstrasse 89");
		kunde5.setTelefon("079 451 23 35");
		kunde5.setMobile("079 451 23 35");
		kunde5.setFirma("Schneebeli AG");
		kunde5.setPlzOrt(plzOrt5);
		kunde5.setKundenArt("Hauswart");
		
		Kunde kunde6 = new Kunde();
		kunde6.setVorname("Michael");
		kunde6.setNachname("Meier");
		kunde6.setEmail("m.meier@gmail.ch");
		kunde6.setStrasse("Steingasse 1");
		kunde6.setTelefon("079 222 23 44");
		kunde6.setMobile("079 222 23 44");
		kunde6.setFirma("Meier Bau AG");
		kunde6.setPlzOrt(plzOrt1);
		kunde6.setKundenArt("Hauswart");
		
		Kunde kunde7 = new Kunde();
		kunde7.setVorname("Ivo");
		kunde7.setNachname("Braunschweiler");
		kunde7.setEmail("ivo.braunschweiler@gmail.ch");
		kunde7.setStrasse("Kantonstrasse 13");
		kunde7.setTelefon("076 223 23 21");
		kunde7.setMobile("076 223 23 21");
		kunde7.setFirma("Braunschweiler AG");
		kunde7.setPlzOrt(plzOrt2);
		kunde7.setKundenArt("Hauswart");
		
		Kunde kunde8 = new Kunde();
		kunde8.setVorname("Florence");
		kunde8.setNachname("Boloise");
		kunde8.setEmail("f.boloise@gmail.ch");
		kunde8.setStrasse("Blumengasse 2b");
		kunde8.setTelefon("075 562 23 22");
		kunde8.setMobile("075 562 23 22");
		kunde8.setFirma("Brandenberger Bau AG");
		kunde8.setPlzOrt(plzOrt3);
		kunde8.setKundenArt("Verwaltung");
		
		Kunde kunde9 = new Kunde();
		kunde9.setVorname("Ursula");
		kunde9.setNachname("Lustenberger");
		kunde9.setEmail("ursula.hat@lust.ch");
		kunde9.setStrasse("Winkelriedstrasse 77");
		kunde9.setTelefon("076 432 90 98");
		kunde9.setMobile("076 432 90 98");
		kunde9.setFirma("Lustenberger Heizungen AG");
		kunde9.setPlzOrt(plzOrt4);
		kunde9.setKundenArt("Verwaltung");
		
		Kunde kunde10 = new Kunde();
		kunde10.setVorname("Rosali");
		kunde10.setNachname("Mi Amor");
		kunde10.setEmail("rosali@dirosesindfuerdimiamor.ch");
		kunde10.setStrasse("Rosengarten 1");
		kunde10.setTelefon("079 974 12 65");
		kunde10.setMobile("079 974 12 65");
		kunde10.setFirma("Lustenberger Heizungen AG");
		kunde10.setPlzOrt(plzOrt4);
		kunde10.setKundenArt("Verwaltung");
		
		kundenRepository.persist(kunde1);
		kundenRepository.persist(kunde2, kunde3);
		
		ArrayList<Kunde> kListe = new ArrayList<Kunde>();
		kListe.add(kunde4);
		kListe.add(kunde5);
		kListe.add(kunde6);
		kListe.add(kunde7);
		kListe.add(kunde8);
		kListe.add(kunde9);
		kListe.add(kunde10);
		
		kundenRepository.persist(kListe);
	}

	/**
	 * Die Variablen vom 1. Kunde-Objekt wird auf Gleichheit �berpr�ft.
	 */	
	@Test
	public void testCanGetPerson()
	{
		Optional<Kunde> kunde = kundenRepository.get(1);
		assertEquals("Peter", kunde.get().getVorname());
		assertEquals("Brandenberger", kunde.get().getNachname());
		assertEquals("Schlossgasse 42", kunde.get().getStrasse());
		assertEquals("079 451 23 21", kunde.get().getTelefon());
		assertEquals("079 451 23 21", kunde.get().getMobile());
		assertEquals("Brandenberger Bau AG", kunde.get().getFirma());
		assertEquals("Z�rich", kunde.get().getPlzOrt().getOrt());
		assertEquals(8001, (int)kunde.get().getPlzOrt().getPlz());
		assertEquals("Hauswart", kunde.get().getKundenArt());
		
	}

	/**
	 * Die ersten 5 Kunde-Objekte werden in ein Set geholt.
	 * Anschliessend wird �berpr�ft, ob das Set aus 5 Objekten besteht.
	 */
	@Test
	public void testCanGetPersons()
	{
		Set<Kunde> kList = kundenRepository.get(m -> m.getId() <= 5);
		assertEquals(5, kList.size());
	}

	/**
	 * Bei den ersten 5 Kunde-Objekte wird die Variable Email ge�ndert.
	 * Die Objekte werden anschliessend wieder in die Datenbank geschrieben.
	 * Danach wird die Email vom Objekt wieder ausgelesen. 
	 * Es wird �berpr�ft, ob die Variablen ge�ndert wurden.
	 */
	@Test
	public void testCanUpdatePersons()
	{
		Optional<Kunde> kunde = kundenRepository.get(1);
		kunde.get().setEmail("neu.email@brandenberger.ch");
		
		kundenRepository.persist(kunde.get());
		assertEquals("neu.email@brandenberger.ch", kundenRepository.get(1).get().getEmail());
		
		Kunde kunde2 = kundenRepository.get(2).get();
		kunde2.setEmail("neu.email@vogel.ch");
		
		Kunde kunde3 = kundenRepository.get(3).get();
		kunde3.setEmail("neu.email@reuter.ch");
		
		kundenRepository.persist(kunde2, kunde3);
		assertEquals("neu.email@vogel.ch", kundenRepository.get(2).get().getEmail());
		assertEquals("neu.email@reuter.ch", kundenRepository.get(3).get().getEmail());
		
		Kunde kunde4 = kundenRepository.get(4).get();
		kunde4.setEmail("neu.email@depard.ch");
		
		Kunde kunde5 = kundenRepository.get(5).get();
		kunde5.setEmail("neu.email@schneebeli.ch");
		
		ArrayList<Kunde> kListe = new ArrayList<Kunde>();
		kListe.add(kunde3);
		kListe.add(kunde4);
		kListe.add(kunde5);
	
		kundenRepository.persist(kListe);
		
		assertEquals("neu.email@depard.ch", kundenRepository.get(4).get().getEmail());
		assertEquals("neu.email@schneebeli.ch", kundenRepository.get(5).get().getEmail());
	}

	/**
	 * Die Kunde-Objekte mit der Id gr�sser als 5 werden gel�scht. 
	 */
	@Test
	public void testCanRemovePersons()
	{
		kundenRepository.remove(6);
		assertEquals(9, kundenRepository.get().size());
		
		kundenRepository.remove(kundenRepository.get(7).get(), kundenRepository.get(8).get());
		assertEquals(7, kundenRepository.get().size());
		
		Set<Kunde> kList = kundenRepository.get(m -> m.getId() > 5 && m.getId() <= 10);
		kundenRepository.remove(kList);
		assertEquals(0, kundenRepository.get(m -> m.getId() > 5 && m.getId() <= 10).size());
	}

	/**
	 * Bean-Validation wird getestet.
	 * Es wird ein Kunde-Objekt erstellt.
	 * Zuerst werden in die Felder ung�ltige Werte geschrieben.
	 * Danach wird die Anzahl der ung�ltigen Werte gez�hlt und Schritt f�r Schritt
	 * mit g�ltigen Werte abgef�llt, bis die Anzahl der ung�ltigen Werte auf 0 ist.
	 */
	@Test
	public void testValidatePerson()
	{
		Kunde kunde1 = new Kunde();
		kunde1.setVorname("Ribeye shankle pork loin hamburger, bresaola sirloin jowl beef tenderloin alcatra andouille ground round pancetta. Filet mignon turducken sirloin pork chop hamburger ball tip");
		kunde1.setNachname("Hamburger t-bone ham sirloin kevin bacon. Boudin pork belly beef, doner ground round shoulder pastrami frankfurter.");
		kunde1.setEmail("p-brandenberger-gmail-ch");
		kunde1.setStrasse("Andouille pork chop alcatra tail, kevin drumstick boudin corned beef turducken. Ground round cupim capicola biltong pastrami.");
		kunde1.setTelefon("079 451 23 21Pancetta venison pork belly frankfurter kevin bresaola, prosciutto leberkas ham. ");
		kunde1.setMobile("Ground round beef prosciutto cow shank biltong strip steak boudin rump shankle swine pork loin pig tri-tip");
		kunde1.setFirma("Andouille pork chop alcatra tail, kevin drumstick boudin corned beef turducken. Ground round cupim capicola biltong pastrami.");
		//kunde1.setPlzOrt(plzOrt1);
		
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Kunde>> constraintViolations = validator.validate(kunde1);
	
		assertEquals(9, constraintViolations.size());
		
		kunde1.setVorname("Ueli");
		constraintViolations = validator.validate(kunde1);
		assertEquals(8, constraintViolations.size());
		
		kunde1.setNachname("Maurer");
		constraintViolations = validator.validate(kunde1);
		assertEquals(7, constraintViolations.size());
		
		kunde1.setEmail("ueli.maurer@admin.ch");
		constraintViolations = validator.validate(kunde1);
		assertEquals(6, constraintViolations.size());
		
		kunde1.setStrasse("Schweizerstrasse 1");
		constraintViolations = validator.validate(kunde1);
		assertEquals(5, constraintViolations.size());
		
		kunde1.setTelefon("+41 52 222 47 08");
		constraintViolations = validator.validate(kunde1);
		assertEquals(4, constraintViolations.size());
		
		kunde1.setMobile("+41 79 100 10 10");
		constraintViolations = validator.validate(kunde1);
		assertEquals(3, constraintViolations.size());
		
		
		kunde1.setFirma("Schweizermacher AG");
		constraintViolations = validator.validate(kunde1);
		assertEquals(2, constraintViolations.size());
		
		PlzOrt plzOrt = new PlzOrt();
		plzOrt.setOrt("Bern");
		plzOrt.setPlz(3000);
		kunde1.setPlzOrt(plzOrt);	
		constraintViolations = validator.validate(kunde1);
		assertEquals(1, constraintViolations.size());
		
		
		
	}
}
