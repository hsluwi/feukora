package com.feukora.server.junit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.MitarbeiterRolle;
import com.feukora.server.repositories.IRepository;
import com.feukora.server.repositories.MitarbeiterRolleRepository;

/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 21.11.2015
 *
 *Klasse um BrennerartRepository zu testen. 
 *
 */
public class MitarbeiterRolleTest {

	private static IRepository<MitarbeiterRolle> mitarbeiterRolleRepository = new MitarbeiterRolleRepository(new ConfigManager().getTestDatabaseName());
	private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

	/**
	 * Es werden 10 MitarbeiterRolle-Objekte generiert.
	 * Die ersten 5 Objekte werden benutzt, um die Werte der Variablen zu �berpr�fen und zu �ndern.
	 * Mit den anderen 5 Objekten wird die L�schfunktion getestet.
	 */
	@BeforeClass
	public static void init() {
		
		MitarbeiterRolle miaRolle1 = new MitarbeiterRolle();
		miaRolle1.setBezeichnung("Sachbearbeiter");
		
		MitarbeiterRolle miaRolle2 = new MitarbeiterRolle();
		miaRolle2.setBezeichnung("Kontrolleur");
		
		MitarbeiterRolle miaRolle3 = new MitarbeiterRolle();
		miaRolle3.setBezeichnung("Administrator");
		
		MitarbeiterRolle miaRolle4 = new MitarbeiterRolle();
		miaRolle4.setBezeichnung("Rolle 4");
		
		MitarbeiterRolle miaRolle5 = new MitarbeiterRolle();
		miaRolle5.setBezeichnung("Rolle 5");
		
		MitarbeiterRolle miaRolle6 = new MitarbeiterRolle();
		miaRolle6.setBezeichnung("Rolle 6");
		
		MitarbeiterRolle miaRolle7 = new MitarbeiterRolle();
		miaRolle7.setBezeichnung("Rolle 7");
		
		MitarbeiterRolle miaRolle8 = new MitarbeiterRolle();
		miaRolle8.setBezeichnung("Rolle 8");
		
		MitarbeiterRolle miaRolle9 = new MitarbeiterRolle();
		miaRolle9.setBezeichnung("Rolle 9");
		
		MitarbeiterRolle miaRolle10 = new MitarbeiterRolle();
		miaRolle10.setBezeichnung("Rolle 10");
		
		mitarbeiterRolleRepository.persist(miaRolle1);
		mitarbeiterRolleRepository.persist(miaRolle2, miaRolle3);
		
		ArrayList<MitarbeiterRolle> mListe = new ArrayList<MitarbeiterRolle>();
		mListe.add(miaRolle4);
		mListe.add(miaRolle5);
		mListe.add(miaRolle6);
		mListe.add(miaRolle7);
		mListe.add(miaRolle8);
		mListe.add(miaRolle9);
		mListe.add(miaRolle10);
		
		mitarbeiterRolleRepository.persist(mListe);
	}

	/**
	 * Die Variablen vom 1. MitarbeiterRolle-Objekt wird auf Gleichheit �berpr�ft.
	 */	
	@Test
	public void testCanGetMitarbeiterRolle()
	{
		MitarbeiterRolle miaRolle = mitarbeiterRolleRepository.get(1).get();
		assertEquals(1, (int)miaRolle.getId());
		
	}

	/**
	 * Die ersten 5 MitarbeiterRolle-Objekte werden in ein Set geholt.
	 * Anschliessend wird �berpr�ft, ob das Set aus 5 Objekten besteht.
	 */
	@Test
	public void testCanGetMitarbeiterRollen()
	{
		Set<MitarbeiterRolle> mList = mitarbeiterRolleRepository.get(m -> m.getId() <= 5);
		assertEquals(5, mList.size());
	}

	/**
	 * Bei den ersten 5 MitarbeiterRolle-Objekte wird die Variable Bezeichnung ge�ndert.
	 * Die Objekte werden anschliessend wieder in die Datenbank geschrieben.
	 * Danach wird die Bezeichnung vom Objekt wieder ausgelesen. 
	 * Es wird �berpr�ft, ob die Variablen ge�ndert wurden.
	 */
	@Test
	public void testCanUpdateMitarbeiterRollen()
	{
		Optional<MitarbeiterRolle> miaRolle1 = mitarbeiterRolleRepository.get(1);
		miaRolle1.get().setBezeichnung("neue Bezeichnung 1");
		
		mitarbeiterRolleRepository.persist(miaRolle1.get());
		assertEquals("neue Bezeichnung 1", mitarbeiterRolleRepository.get(1).get().getBezeichnung());
		
		MitarbeiterRolle kunde2 = mitarbeiterRolleRepository.get(2).get();
		kunde2.setBezeichnung("neue Bezeichnung 2");
		
		MitarbeiterRolle kunde3 = mitarbeiterRolleRepository.get(3).get();
		kunde3.setBezeichnung("neue Bezeichnung 3");
		
		mitarbeiterRolleRepository.persist(kunde2, kunde3);
		assertEquals("neue Bezeichnung 2", mitarbeiterRolleRepository.get(2).get().getBezeichnung());
		assertEquals("neue Bezeichnung 3", mitarbeiterRolleRepository.get(3).get().getBezeichnung());
		
		MitarbeiterRolle kunde4 = mitarbeiterRolleRepository.get(4).get();
		kunde4.setBezeichnung("neue Bezeichnung 4");
		
		MitarbeiterRolle kunde5 = mitarbeiterRolleRepository.get(5).get();
		kunde5.setBezeichnung("neue Bezeichnung 5");
		
		ArrayList<MitarbeiterRolle> mListe = new ArrayList<MitarbeiterRolle>();
		mListe.add(kunde3);
		mListe.add(kunde4);
		mListe.add(kunde5);
	
		mitarbeiterRolleRepository.persist(mListe);
		
		assertEquals("neue Bezeichnung 4", mitarbeiterRolleRepository.get(4).get().getBezeichnung());
		assertEquals("neue Bezeichnung 5", mitarbeiterRolleRepository.get(5).get().getBezeichnung());
	}

	/**
	 * Die MitarbeiterRolle-Objekte mit der Id gr�sser als 5 werden gel�scht. 
	 */
	@Test
	public void testCanRemoveMitarbeiterRollen()
	{
		mitarbeiterRolleRepository.remove(6);
		assertEquals(9, mitarbeiterRolleRepository.get().size());
		
		mitarbeiterRolleRepository.remove(mitarbeiterRolleRepository.get(7).get(), mitarbeiterRolleRepository.get(8).get());
		assertEquals(7, mitarbeiterRolleRepository.get().size());
		
		Set<MitarbeiterRolle> mList = mitarbeiterRolleRepository.get(m -> m.getId() > 5 && m.getId() <= 10);
		mitarbeiterRolleRepository.remove(mList);
		assertEquals(0, mitarbeiterRolleRepository.get(m -> m.getId() > 5 && m.getId() <= 10).size());
	}

	/**
	 * Bean-Validation wird getestet.
	 * Es wird ein MitarbeiterRolle-Objekt erstellt.
	 * Zuerst werden in die Felder ung�ltige Werte geschrieben.
	 * Danach wird die Anzahl der ung�ltigen Werte gez�hlt und Schritt f�r Schritt
	 * mit g�ltigen Werte abgef�llt, bis die Anzahl der ung�ltigen Werte auf 0 ist.
	 */
	@Test
	public void testValidateMitarbeiterRolle()
	{
		MitarbeiterRolle miaRolle1 = new MitarbeiterRolle();
		miaRolle1.setBezeichnung("Ribeye shankle pork loin hamburger, bresaola sirloin jowl beef tenderloin alcatra andouille ground round pancetta. Filet mignon turducken sirloin pork chop hamburger ball tip");
			
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<MitarbeiterRolle>> constraintViolations = validator.validate(miaRolle1);
	
		assertEquals(1, constraintViolations.size());
		
		miaRolle1.setBezeichnung("Sachbearbeiter");
		constraintViolations = validator.validate(miaRolle1);
		assertEquals(0, constraintViolations.size());
		
		
	}
}
