package com.feukora.server.junit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Brenner;
import com.feukora.server.models.Brennerart;
import com.feukora.server.models.Brennermodell;
import com.feukora.server.repositories.IRepository;
import com.feukora.server.repositories.BrennerRepository;

/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 19.11.2015
 *
 *Klasse um BrennerRepository zu testen. 
 *
 */
public class BrennerRepositoryTest {

	private static IRepository<Brenner> brennerRepository = new BrennerRepository(new ConfigManager().getTestDatabaseName());
	private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	
	/**
	 * Es werden 10 Brenner-Objekte mit Brennerart und Brennermodell generiert.
	 * Die ersten 5 Objekte werden benutzt, um die Werte der Variablen zu �berpr�fen und zu �ndern.
	 * Mit den anderen 5 Objekten wird die L�schfunktion getestet.
	 */
	@BeforeClass
	public static void init() {
		
		Brennerart brennerart1 = new Brennerart();
		brennerart1.setBezeichnung("Brennerart 1");
		
		Brennerart brennerart2 = new Brennerart();
		brennerart2.setBezeichnung("Brennerart 2");
				
		Brennermodell brennermodell1 = new Brennermodell();
		brennermodell1.setBezeichnung("Brennermodell 1");
		brennermodell1.setFeuerungswaermeleistung(200);
		brennermodell1.setHersteller("Burny");
		brennermodell1.setBrennerart(brennerart1);
		
		Brennermodell brennermodell2 = new Brennermodell();
		brennermodell2.setBezeichnung("Brennermodell 2");
		brennermodell2.setFeuerungswaermeleistung(200);
		brennermodell2.setHersteller("Hello");
		brennermodell2.setBrennerart(brennerart2);
		
		Brenner brenner1 = new Brenner();
		brenner1.setBrennermodell(brennermodell1);
		brenner1.setBaujahr(1998);
		brenner1.setZulassungsNr("1574GK445G");
		
		Brenner brenner2 = new Brenner();
		brenner2.setBrennermodell(brennermodell2);
		brenner2.setBaujahr(2007);
		brenner2.setZulassungsNr("45555");
		
		Brenner brenner3 = new Brenner();
		brenner3.setBrennermodell(brennermodell1);
		brenner3.setBaujahr(2009);
		brenner3.setZulassungsNr("adDUe12");
		
		Brenner brenner4 = new Brenner();
		brenner4.setBrennermodell(brennermodell1);
		brenner4.setBaujahr(2014);
		brenner4.setZulassungsNr("458hg");
		
		Brenner brenner5 = new Brenner();
		brenner5.setBrennermodell(brennermodell1);
		brenner5.setBaujahr(2008);
		brenner5.setZulassungsNr("1574GK445G");
		
		Brenner brenner6 = new Brenner();
		brenner6.setBrennermodell(brennermodell1);
		brenner6.setBaujahr(1999);
		brenner6.setZulassungsNr("AB45EJFff");
		
		Brenner brenner7 = new Brenner();
		brenner7.setBrennermodell(brennermodell2);
		brenner7.setBaujahr(1998);
		brenner7.setZulassungsNr("45ADJr8F");
		
		Brenner brenner8 = new Brenner();
		brenner8.setBrennermodell(brennermodell1);
		brenner8.setBaujahr(1998);
		brenner8.setZulassungsNr("fg56tzg");
		
		Brenner brenner9 = new Brenner();
		brenner9.setBrennermodell(brennermodell1);
		brenner9.setBaujahr(1998);
		brenner9.setZulassungsNr("adf4566gg");
		
		Brenner brenner10 = new Brenner();
		brenner10.setBrennermodell(brennermodell2);
		brenner10.setBaujahr(1997);
		brenner10.setZulassungsNr("45df458d");
		
		brennerRepository.persist(brenner1);
		brennerRepository.persist(brenner2, brenner3);
		
		ArrayList<Brenner> bListe = new ArrayList<Brenner>();
		bListe.add(brenner4);
		bListe.add(brenner5);
		bListe.add(brenner6);
		bListe.add(brenner7);
		bListe.add(brenner8);
		bListe.add(brenner9);
		bListe.add(brenner10);
		
		brennerRepository.persist(bListe);
	}

	/**
	 * Die Variablen vom 1. Brenner-Objekt wird auf Gleichheit �berpr�ft.
	 */	
	@Test
	public void testCanGetBrenner()
	{
		Optional<Brenner> brenner = brennerRepository.get(1);
		assertEquals("Brennermodell 1", brenner.get().getBrennermodell().getBezeichnung());
		assertEquals("Brennerart 1", brenner.get().getBrennermodell().getBrennerart().getBezeichnung());
		assertEquals(1998, (int)brenner.get().getBaujahr());

	}

	/**
	 * Die ersten 5 Brenner-Objekte werden in ein Set geholt.
	 * Anschliessend wird �berpr�ft, ob das Set aus 5 Objekten besteht.
	 */
	@Test
	public void testCanGetBrenners()
	{
		Set<Brenner> bList = brennerRepository.get(m -> m.getId() <= 5);
		assertEquals(5, bList.size());
	}

	/**
	 * Bei den ersten 5 Brenner-Objekte wird die Variable ZulassungsNr ge�ndert.
	 * Die Objekte werden anschliessend wieder in die Datenbank geschrieben.
	 * Danach wird die ZulassungsNr vom Objekt wieder ausgelesen. 
	 * Es wird �berpr�ft, ob die Variablen ge�ndert wurden.
	 */
	@Test
	public void testCanUpdateBrenners()
	{
		Optional<Brenner> brenner1 = brennerRepository.get(1);
		brenner1.get().setZulassungsNr("neueNummer123");
		
		brennerRepository.persist(brenner1.get());
		assertEquals("neueNummer123", brennerRepository.get(1).get().getZulassungsNr());
		
		Brenner brenner2 = brennerRepository.get(2).get();
		brenner2.setZulassungsNr("neueNummer45");
		
		Brenner brenner3 = brennerRepository.get(3).get();
		brenner3.setZulassungsNr("NN45");
		
		brennerRepository.persist(brenner2, brenner3);
		assertEquals("neueNummer45", brennerRepository.get(2).get().getZulassungsNr());
		assertEquals("NN45", brennerRepository.get(3).get().getZulassungsNr());
		
		Brenner brenner4 = brennerRepository.get(4).get();
		brenner4.setZulassungsNr("ZulassungsnummerNeu754");
		
		Brenner brenner5 = brennerRepository.get(5).get();
		brenner5.setZulassungsNr("NummerTest33");
		
		ArrayList<Brenner> bListe = new ArrayList<Brenner>();
		bListe.add(brenner3);
		bListe.add(brenner4);
		bListe.add(brenner5);
	
		brennerRepository.persist(bListe);
		
		assertEquals("ZulassungsnummerNeu754", brennerRepository.get(4).get().getZulassungsNr());
		assertEquals("NummerTest33", brennerRepository.get(5).get().getZulassungsNr());
	}

	/**
	 * Die Brenner-Objekte mit der Id gr�sser als 5 werden gel�scht. 
	 */
	@Test
	public void testCanRemoveBrenners()
	{
		brennerRepository.remove(6);
		assertEquals(9, brennerRepository.get().size());
		
		brennerRepository.remove(brennerRepository.get(7).get(), brennerRepository.get(8).get());
		assertEquals(7, brennerRepository.get().size());
		
		Set<Brenner> bList = brennerRepository.get(m -> m.getId() > 5 && m.getId() <= 10);
		brennerRepository.remove(bList);
		assertEquals(0, brennerRepository.get(m -> m.getId() > 5 && m.getId() <= 10).size());
	}

	/**
	 * Bean-Validation wird getestet.
	 * Es wird ein Brenner-Objekt erstellt.
	 * Zuerst wird in die Felder ung�ltige Werte geschrieben.
	 * Danach wird die Anzahl der ung�ltigen Werte gez�hlt und Schritt f�r Schritt
	 * mit g�ltigen Werte abgef�llt, bis die Anzahl der ung�ltigen Werte auf 0 ist.
	 */
	@Test
	public void testValidateBrenner()
	{
		
		Brenner brenner = new Brenner();
		//brenner.setBrennermodell(brennermodell1);
		brenner.setBaujahr(1800);
		brenner.setZulassungsNr("1574GK445G adf  45af45af4d  54adf4  54adf4afd Andouille pork chop alcatra tail, kevin drumstick boudi Andouille pork chop alcatra tail, kevin drumstick boudi Andouille pork chop alcat");
		
		
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Brenner>> constraintViolations = validator.validate(brenner);
	
		assertEquals(3, constraintViolations.size());
		
		brenner.setBaujahr(1990);
		constraintViolations = validator.validate(brenner);
		assertEquals(2, constraintViolations.size());
		
		brenner.setZulassungsNr("45DHJ4584");
		constraintViolations = validator.validate(brenner);
		assertEquals(1, constraintViolations.size());
		
		Brennerart brennerart = new Brennerart();
		brennerart.setBezeichnung("Brennerart");
		Brennermodell brennermodell = new Brennermodell();
		brennermodell.setBezeichnung("Brennermodell");
		brennermodell.setHersteller("Hersteller");
		brennermodell.setBrennerart(brennerart);
		brenner.setBrennermodell(brennermodell);
		constraintViolations = validator.validate(brenner);
		assertEquals(0, constraintViolations.size());
		
	}
}
