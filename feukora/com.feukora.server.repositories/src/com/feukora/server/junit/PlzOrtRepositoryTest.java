package com.feukora.server.junit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.PlzOrt;
import com.feukora.server.repositories.IRepository;
import com.feukora.server.repositories.PlzOrtRepository;


/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 21.11.2015
 *
 *Klasse um PlzOrtRepository zu testen. 
 *
 */
public class PlzOrtRepositoryTest {

	private static IRepository<PlzOrt> plzOrtRepository = new PlzOrtRepository(new ConfigManager().getTestDatabaseName());
	private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

	/**
	 * Es werden 10 PlzOrt-Objekte generiert.
	 * Die ersten 5 Objekte werden benutzt, um die Werte der Variablen zu �berpr�fen und zu �ndern.
	 * Mit den anderen 5 Objekten wird die L�schfunktion getestet.
	 */
	@BeforeClass
	public static void init() {
		
		PlzOrt plzOrt1 = new PlzOrt();
		plzOrt1.setOrt("Z�rich");
		plzOrt1.setPlz(8001);
		
		PlzOrt plzOrt2 = new PlzOrt();
		plzOrt2.setOrt("Luzern");
		plzOrt2.setPlz(6005);
		
		PlzOrt plzOrt3 = new PlzOrt();
		plzOrt3.setOrt("Bern");
		plzOrt3.setPlz(3012);
		
		PlzOrt plzOrt4 = new PlzOrt();
		plzOrt4.setOrt("Lausanne");
		plzOrt4.setPlz(1045);
		
		PlzOrt plzOrt5 = new PlzOrt();
		plzOrt5.setOrt("Winterthur");
		plzOrt5.setPlz(8400);
		
		PlzOrt plzOrt6 = new PlzOrt();
		plzOrt6.setOrt("Rothenburg");
		plzOrt6.setPlz(6023);
		
		PlzOrt plzOrt7 = new PlzOrt();
		plzOrt7.setOrt("Kriens");
		plzOrt7.setPlz(6010);
		
		PlzOrt plzOrt8 = new PlzOrt();
		plzOrt8.setOrt("Grossdietwil");
		plzOrt8.setPlz(6146);
		
		PlzOrt plzOrt9 = new PlzOrt();
		plzOrt9.setOrt("Sursee");
		plzOrt9.setPlz(6210);
		
		PlzOrt plzOrt10 = new PlzOrt();
		plzOrt10.setOrt("Emmen");
		plzOrt10.setPlz(6032);
		
		plzOrtRepository.persist(plzOrt1);
		plzOrtRepository.persist(plzOrt2, plzOrt3);
		
		ArrayList<PlzOrt> pListe = new ArrayList<PlzOrt>();
		pListe.add(plzOrt4);
		pListe.add(plzOrt5);
		pListe.add(plzOrt6);
		pListe.add(plzOrt7);
		pListe.add(plzOrt8);
		pListe.add(plzOrt9);
		pListe.add(plzOrt10);
		
		plzOrtRepository.persist(pListe);
	}

	/**
	 * Die Variablen vom 1. PlzOrt-Objekt wird auf Gleichheit �berpr�ft.
	 */	
	@Test
	public void testCanGetPlzOrt()
	{
		Optional<PlzOrt> plzOrt = plzOrtRepository.get(1);
		assertEquals("Z�rich", plzOrt.get().getOrt());
		
		
	}

	/**
	 * Die ersten 5 PlzOrt-Objekte werden in ein Set geholt.
	 * Anschliessend wird �berpr�ft, ob das Set aus 5 Objekten besteht.
	 */
	@Test
	public void testCanGetPlzOrte()
	{
		Set<PlzOrt> pListe = plzOrtRepository.get(m -> m.getId() <= 5);
		assertEquals(5, pListe.size());
	}

	/**
	 * Bei den ersten 5 PlzOrt-Objekte wird die Variable Plz ge�ndert.
	 * Die Objekte werden anschliessend wieder in die Datenbank geschrieben.
	 * Danach wird Plz vom Objekt wieder ausgelesen. 
	 * Es wird �berpr�ft, ob die Variablen ge�ndert wurden.
	 */
	@Test
	public void testCanUpdatePlzOrte()
	{
		Optional<PlzOrt> plzOrt = plzOrtRepository.get(1);
		plzOrt.get().setPlz(8001);
		
		plzOrtRepository.persist(plzOrt.get());
		assertEquals(8001, (int)plzOrtRepository.get(1).get().getPlz());
		
		PlzOrt plzOrt2 = plzOrtRepository.get(2).get();
		plzOrt2.setPlz(6002);
		
		PlzOrt plzOrt3 = plzOrtRepository.get(3).get();
		plzOrt3.setPlz(3010);
		
		plzOrtRepository.persist(plzOrt2, plzOrt3);
		assertEquals(6002, (int)plzOrtRepository.get(2).get().getPlz());
		assertEquals(3010, (int)plzOrtRepository.get(3).get().getPlz());
		
		PlzOrt plzOrt4 = plzOrtRepository.get(4).get();
		plzOrt4.setPlz(1040);
		
		PlzOrt plzOrt5 = plzOrtRepository.get(5).get();
		plzOrt5.setPlz(8401);
		
		ArrayList<PlzOrt> pListe = new ArrayList<PlzOrt>();
		pListe.add(plzOrt3);
		pListe.add(plzOrt4);
		pListe.add(plzOrt5);

	
		plzOrtRepository.persist(pListe);
		
		assertEquals(1040, (int)plzOrtRepository.get(4).get().getPlz());
		assertEquals(8401, (int)plzOrtRepository.get(5).get().getPlz());
	}

	/**
	 * Die PlzOrt-Objekte mit der Id gr�sser als 5 werden gel�scht. 
	 */
	@Test
	public void testCanRemovePlzOrte()
	{
		plzOrtRepository.remove(6);
		assertEquals(9, plzOrtRepository.get().size());
		
		plzOrtRepository.remove(plzOrtRepository.get(7).get(), plzOrtRepository.get(8).get());
		assertEquals(7, plzOrtRepository.get().size());
		
		Set<PlzOrt> pListe = plzOrtRepository.get(m -> m.getId() > 5 && m.getId() <= 10);
		plzOrtRepository.remove(pListe);
		assertEquals(0, plzOrtRepository.get(m -> m.getId() > 5 && m.getId() <= 10).size());
	}

	/**
	 * Bean-Validation wird getestet.
	 * Es wird ein PlzOrt-Objekt erstellt.
	 * Zuerst werden in die Felder ung�ltige Werte geschrieben.
	 * Danach wird die Anzahl der ung�ltigen Werte gez�hlt und Schritt f�r Schritt
	 * mit g�ltigen Werte abgef�llt, bis die Anzahl der ung�ltigen Werte auf 0 ist.
	 */
	@Test
	public void testValidatePlzOrt()
	{
		PlzOrt plzOrt1 = new PlzOrt();
		plzOrt1.setOrt("Ribeye shankle pork loin hamburger, bresaola sirloin jowl beef tenderloin alcatra andouille ground round pancetta. Filet mignon turducken sirloin pork chop hamburger ball tip");
		plzOrt1.setPlz(512023);
		
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<PlzOrt>> constraintViolations = validator.validate(plzOrt1);
	
		assertEquals(2, constraintViolations.size());
		
		plzOrt1.setOrt("Rothenburg");
		constraintViolations = validator.validate(plzOrt1);
		assertEquals(1, constraintViolations.size());
	
		plzOrt1.setPlz(6023);
		constraintViolations = validator.validate(plzOrt1);
		assertEquals(0, constraintViolations.size());
		
	}
}
