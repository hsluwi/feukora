package com.feukora.server.junit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Anlagenstandort;
import com.feukora.server.models.Brenner;
import com.feukora.server.models.Brennerart;
import com.feukora.server.models.Brennermodell;
import com.feukora.server.models.Brennstoff;
import com.feukora.server.models.Gemeinde;
import com.feukora.server.models.Kunde;
import com.feukora.server.models.PlzOrt;
import com.feukora.server.models.Waermeerzeuger;
import com.feukora.server.models.Waermeerzeugermodell;
import com.feukora.server.repositories.IRepository;
import com.feukora.server.repositories.KundeRepository;
import com.feukora.server.repositories.PlzOrtRepository;
import com.feukora.server.repositories.AnlagenstandortRepository;
import com.feukora.server.repositories.BrennerRepository;
import com.feukora.server.repositories.GemeindeRepository;


/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 19.11.2015
 *
 *Klasse um AnlagenstandortRepository zu testen. 
 *
 */
public class AnlagenstandortRepositoryTest {

	
	private static IRepository<Anlagenstandort> anlagenstandortRepository = new AnlagenstandortRepository(new ConfigManager().getTestDatabaseName());
	private static IRepository<Kunde> kundenRepository = new KundeRepository(new ConfigManager().getTestDatabaseName());
	private static IRepository<Gemeinde> gemeindeRepository = new GemeindeRepository(new ConfigManager().getTestDatabaseName());
	private static IRepository<PlzOrt> plzOrtRepository = new PlzOrtRepository(new ConfigManager().getTestDatabaseName());
	private static IRepository<Brenner> brennerRepository = new BrennerRepository(new ConfigManager().getTestDatabaseName());
	private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	
	/**
	 * Es werden 10 Anlagenstandorte-Objekte mit Gemeinde, Kunde und PlzOrt, Brenner und Waermeerzeuger generiert.
	 * Die ersten 5 Objekte werden benutzt, um die Werte der Variablen zu �berpr�fen und zu �ndern.
	 * Mit den anderen 5 Objekten wird die L�schfunktion getestet.
	 */
	@BeforeClass
	public static void init() {
		

		
		PlzOrt plzOrt1 = new PlzOrt();
		plzOrt1.setOrt("Dietikon");
		plzOrt1.setPlz(8953);
		
		PlzOrt plzOrt2 = new PlzOrt();
		plzOrt2.setOrt("Rothenburg");
		plzOrt2.setPlz(6023);
		
		plzOrtRepository.persist(plzOrt1, plzOrt2);

		Gemeinde gemeinde1 = new Gemeinde();
		gemeinde1.setBezeichnung("Dietikon");
		gemeinde1.setKanton("Z�rich");
		
		Gemeinde gemeinde2 = new Gemeinde();
		gemeinde2.setBezeichnung("Rothenburg");
		gemeinde2.setKanton("Luzern");

		gemeindeRepository.persist(gemeinde1, gemeinde2);

		Kunde kunde1 = new Kunde();
		kunde1.setVorname("Peter");
		kunde1.setNachname("Brandenberger");
		kunde1.setEmail("p.brandenberger@gmail.ch");
		kunde1.setStrasse("Schlossgasse 42");
		kunde1.setTelefon("079 451 23 21");
		kunde1.setMobile("079 451 23 21");
		kunde1.setFirma("Brandenberger Bau AG");
		kunde1.setPlzOrt(plzOrt1);
		kunde1.setKundenArt("Hauswart");
		
		Kunde kunde2 = new Kunde();
		kunde2.setVorname("Hans");
		kunde2.setNachname("Vogel");
		kunde2.setEmail("hans.vogel@gmail.ch");
		kunde2.setStrasse("Kellerstrasse 3");
		kunde2.setTelefon("078 651 23 12");
		kunde2.setMobile("078 651 23 12");
		kunde2.setFirma("Vogel Heizungen GmbH");
		kunde2.setPlzOrt(plzOrt2);
		kunde2.setKundenArt("Verwaltung");
		
		Kunde kunde3 = new Kunde();
		kunde3.setVorname("Urs");
		kunde3.setNachname("Reuter");
		kunde3.setEmail("urs.reuter@gmail.ch");
		kunde3.setStrasse("Hohenrain 25");
		kunde3.setTelefon("076 122 45 54");
		kunde3.setMobile("076 122 45 54");
		kunde3.setFirma("Reuter Anlagen AG");
		kunde3.setPlzOrt(plzOrt1);
		kunde3.setKundenArt("Hauswart");
		
		Kunde kunde4 = new Kunde();
		kunde4.setVorname("Pierre");
		kunde4.setNachname("Depard");
		kunde4.setEmail("p.depard@gmail.com");
		kunde4.setStrasse("Burgstrasse 4");
		kunde4.setTelefon("079 222 35 35");
		kunde4.setMobile("079 222 35 35");
		kunde4.setFirma("Depard AG");
		kunde4.setPlzOrt(plzOrt1);
		kunde4.setKundenArt("Verwaltung");
		
		Kunde kunde5 = new Kunde();
		kunde5.setVorname("Edgard");
		kunde5.setNachname("Schneebeli");
		kunde5.setEmail("e.schneebeli@gmail.ch");
		kunde5.setStrasse("Hochdorfstrasse 89");
		kunde5.setTelefon("079 451 23 35");
		kunde5.setMobile("079 451 23 35");
		kunde5.setFirma("Schneebeli AG");
		kunde5.setPlzOrt(plzOrt2);
		kunde5.setKundenArt("Hauswart");
		
		kundenRepository.persist(kunde1, kunde2, kunde3, kunde4, kunde5);
		
		Brennerart brennerart1 = new Brennerart();
		brennerart1.setBezeichnung("Brennerart 1");
		
		Brennerart brennerart2 = new Brennerart();
		brennerart2.setBezeichnung("Brennerart 2");
				
		Brennermodell brennermodell1 = new Brennermodell();
		brennermodell1.setBezeichnung("Brennermodell 1");
		brennermodell1.setFeuerungswaermeleistung(200);
		brennermodell1.setHersteller("Burny");
		brennermodell1.setBrennerart(brennerart1);
		
		Brennermodell brennermodell2 = new Brennermodell();
		brennermodell2.setBezeichnung("Brennermodell 2");
		brennermodell2.setFeuerungswaermeleistung(200);
		brennermodell2.setHersteller("Hello");
		brennermodell2.setBrennerart(brennerart2);
		
		Brenner brenner1 = new Brenner();
		brenner1.setBrennermodell(brennermodell1);
		brenner1.setBaujahr(1998);
		brenner1.setZulassungsNr("1574GK445G");
		
		Brenner brenner2 = new Brenner();
		brenner2.setBrennermodell(brennermodell2);
		brenner2.setBaujahr(2007);
		brenner2.setZulassungsNr("45555");
		
		Brenner brenner3 = new Brenner();
		brenner3.setBrennermodell(brennermodell1);
		brenner3.setBaujahr(2009);
		brenner3.setZulassungsNr("adDUe12");
		
		Brenner brenner4 = new Brenner();
		brenner4.setBrennermodell(brennermodell1);
		brenner4.setBaujahr(2014);
		brenner4.setZulassungsNr("458hg");
		
		Brenner brenner5 = new Brenner();
		brenner5.setBrennermodell(brennermodell1);
		brenner5.setBaujahr(2008);
		brenner5.setZulassungsNr("1574GK445G");
		
		brennerRepository.persist(brenner1, brenner2, brenner3, brenner4, brenner5);
		
		Brennstoff brennstoff1 = new Brennstoff();
		brennstoff1.setBezeichnung("Holz");
		
		Brennstoff brennstoff2 = new Brennstoff();
		brennstoff2.setBezeichnung("Oel");
					
		Waermeerzeugermodell waermeerzeugerm1 = new Waermeerzeugermodell();
		waermeerzeugerm1.setBezeichnung("Model 1");
		waermeerzeugerm1.setHersteller("Arnold GmbH");
		waermeerzeugerm1.setBrennstoff(brennstoff1);
		
		Waermeerzeugermodell waermeerzeugerm2 = new Waermeerzeugermodell();
		waermeerzeugerm2.setBezeichnung("Model 2");
		waermeerzeugerm2.setHersteller("Brenn AG");
		waermeerzeugerm2.setBrennstoff(brennstoff2);
		
		Waermeerzeuger waermeerzeuger1 = new Waermeerzeuger();
		waermeerzeuger1.setWaermeerzeugermodell(waermeerzeugerm1);
		waermeerzeuger1.setBaujahr(1998);
		waermeerzeuger1.setZulassungsNr("1574GK445G");
		
		Waermeerzeuger waermeerzeuger2 = new Waermeerzeuger();
		waermeerzeuger2.setWaermeerzeugermodell(waermeerzeugerm1);
		waermeerzeuger2.setBaujahr(2007);
		waermeerzeuger2.setZulassungsNr("45555");
		
		Waermeerzeuger waermeerzeuger3 = new Waermeerzeuger();
		waermeerzeuger3.setWaermeerzeugermodell(waermeerzeugerm2);
		waermeerzeuger3.setBaujahr(2009);
		waermeerzeuger3.setZulassungsNr("adDUe12");
		
		Waermeerzeuger waermeerzeuger4 = new Waermeerzeuger();
		waermeerzeuger4.setWaermeerzeugermodell(waermeerzeugerm1);
		waermeerzeuger4.setBaujahr(2014);
		waermeerzeuger4.setZulassungsNr("458hg");
		
		Waermeerzeuger waermeerzeuger5 = new Waermeerzeuger();
		waermeerzeuger5.setWaermeerzeugermodell(waermeerzeugerm2);
		waermeerzeuger5.setBaujahr(2008);
		waermeerzeuger5.setZulassungsNr("1574GK445G");
		
		Anlagenstandort anlagenst1 = new Anlagenstandort();
		anlagenst1.setGemeinde(gemeinde1);
		anlagenst1.setKunde(kunde1);
		anlagenst1.setHauswart(kunde2);
		anlagenst1.setPlzOrt(plzOrt1);
		anlagenst1.setStrasse("Felsenegg 5");
		anlagenst1.setBrenner(brenner1);
		
		Anlagenstandort anlagenst2 = new Anlagenstandort();
		anlagenst2.setGemeinde(gemeinde2);
		anlagenst2.setKunde(kunde3);
		anlagenst2.setHauswart(kunde4);
		anlagenst2.setPlzOrt(plzOrt2);
		anlagenst2.setStrasse("Kirchweg 5");
		anlagenst2.setBrenner(brenner2);
		
		Anlagenstandort anlagenst3 = new Anlagenstandort();
		anlagenst3.setGemeinde(gemeinde1);
		anlagenst3.setKunde(kunde5);
		anlagenst3.setHauswart(kunde1);
		anlagenst3.setPlzOrt(plzOrt1);
		anlagenst3.setStrasse("Kantonsstrasse 55");
		anlagenst3.setBrenner(brenner3);
		
		Anlagenstandort anlagenst4 = new Anlagenstandort();
		anlagenst4.setGemeinde(gemeinde1);
		anlagenst4.setKunde(kunde1);
		anlagenst4.setHauswart(kunde2);
		anlagenst4.setPlzOrt(plzOrt1);
		anlagenst4.setStrasse("Hallwilerweg 81");
		anlagenst4.setBrenner(brenner4);
		
		Anlagenstandort anlagenst5 = new Anlagenstandort();
		anlagenst5.setGemeinde(gemeinde2);
		anlagenst5.setKunde(kunde1);
		anlagenst5.setHauswart(kunde1);
		anlagenst5.setPlzOrt(plzOrt2);
		anlagenst5.setStrasse("Felsenegg 5");
		anlagenst5.setBrenner(brenner5);
		
		Anlagenstandort anlagenst6 = new Anlagenstandort();
		anlagenst6.setGemeinde(gemeinde2);
		anlagenst6.setKunde(kunde4);
		anlagenst6.setHauswart(kunde2);
		anlagenst6.setPlzOrt(plzOrt2);
		anlagenst6.setStrasse("Neumoosstrasse 2");
		anlagenst6.setWaermeerzeuger(waermeerzeuger1);
		
		Anlagenstandort anlagenst7 = new Anlagenstandort();
		anlagenst7.setGemeinde(gemeinde1);
		anlagenst7.setKunde(kunde4);
		anlagenst7.setHauswart(kunde3);
		anlagenst7.setPlzOrt(plzOrt1);
		anlagenst7.setStrasse("Eschenstrasse 14");
		anlagenst7.setWaermeerzeuger(waermeerzeuger2);
		
		Anlagenstandort anlagenst8 = new Anlagenstandort();
		anlagenst8.setGemeinde(gemeinde1);
		anlagenst8.setKunde(kunde1);
		anlagenst8.setHauswart(kunde5);
		anlagenst8.setPlzOrt(plzOrt1);
		anlagenst8.setStrasse("Zentralstrasse 5");
		anlagenst8.setWaermeerzeuger(waermeerzeuger3);
		
		Anlagenstandort anlagenst9 = new Anlagenstandort();
		anlagenst9.setGemeinde(gemeinde1);
		anlagenst9.setKunde(kunde1);
		anlagenst9.setHauswart(kunde2);
		anlagenst9.setPlzOrt(plzOrt1);
		anlagenst9.setStrasse("Moseggstrasse 75");
		anlagenst9.setWaermeerzeuger(waermeerzeuger4);
		
		Anlagenstandort anlagenst10 = new Anlagenstandort();
		anlagenst10.setGemeinde(gemeinde2);
		anlagenst10.setKunde(kunde5);
		anlagenst10.setHauswart(kunde2);
		anlagenst10.setPlzOrt(plzOrt2);
		anlagenst10.setStrasse("Senegalweg 32");
		anlagenst10.setWaermeerzeuger(waermeerzeuger5);
		
		anlagenstandortRepository.persist(anlagenst1);
		anlagenstandortRepository.persist(anlagenst2, anlagenst3);
		
		ArrayList<Anlagenstandort> aListe = new ArrayList<Anlagenstandort>();
		aListe.add(anlagenst4);
		aListe.add(anlagenst5);
		aListe.add(anlagenst6);
		aListe.add(anlagenst7);
		aListe.add(anlagenst8);
		aListe.add(anlagenst9);
		aListe.add(anlagenst10);
		
		anlagenstandortRepository.persist(aListe);
	}
	
	/**
	 * Die Variablen vom 1. Anlagenstandort-Objekt wird auf Gleichheit �berpr�ft.
	 */	
	@Test
	public void testCanGetAnlagenstandort()
	{
		Optional<Anlagenstandort> anlagenst = anlagenstandortRepository.get(1);
		assertEquals("Dietikon", anlagenst.get().getGemeinde().getBezeichnung());
		assertEquals("Z�rich", anlagenst.get().getGemeinde().getKanton());
		assertEquals("Brandenberger", anlagenst.get().getKunde().getNachname());
		assertEquals(8953, (int)anlagenst.get().getPlzOrt().getPlz());
		assertEquals("1574GK445G", anlagenst.get().getBrenner().getZulassungsNr());
		assertEquals("Felsenegg 5", anlagenst.get().getStrasse());
		
		
	}
	

	/**
	 * Die ersten 5 Anlagenstandort-Objekte werden in ein Set geholt.
	 * Anschliessend wird �berpr�ft, ob das Set aus 5 Objekten besteht.
	 */
	@Test
	public void testCanGetAnlagenstandorte()
	{
		Set<Anlagenstandort> aList = anlagenstandortRepository.get(m -> m.getId() <= 5);
		assertEquals(5, aList.size());
	}
	
	/**
	 * Bei den ersten 5 Anlagenstandort-Objekte werden Variablen ge�ndert.
	 * Die Objekte werden anschliessend wieder in die Datenbank geschrieben.
	 * Danach werden die zuvor ge�nderten Variablen vom Objekt wieder ausgelesen. 
	 * Es wird �berpr�ft, ob die Variablen ge�ndert wurden.
	 */
	@Test
	public void testCanUpdateAnlagenstandorte()
	{
		Optional<Anlagenstandort> anlagenst = anlagenstandortRepository.get(1);
		anlagenst.get().setHauswart(kundenRepository.get(3).get());
		
		anlagenstandortRepository.persist(anlagenst.get());
		assertEquals("Reuter", anlagenstandortRepository.get(1).get().getHauswart().getNachname());
		assertEquals("Urs", anlagenstandortRepository.get(1).get().getHauswart().getVorname());
		
		Anlagenstandort anlagenst2 = anlagenstandortRepository.get(2).get();
		anlagenst2.setStrasse("Neuestrasse 5");
		
		Anlagenstandort anlagenst3 = anlagenstandortRepository.get(3).get();
		anlagenst3.setStrasse("Geaenderte-Strasse 4");
		
		anlagenstandortRepository.persist(anlagenst2, anlagenst3);
		assertEquals("Neuestrasse 5", anlagenstandortRepository.get(2).get().getStrasse());
		assertEquals("Geaenderte-Strasse 4", anlagenstandortRepository.get(3).get().getStrasse());
		
		Anlagenstandort anlagenst4 = anlagenstandortRepository.get(4).get();
		anlagenst4.setHauswart(kundenRepository.get(4).get());
		
		Anlagenstandort anlagenst5 = anlagenstandortRepository.get(5).get();
		anlagenst5.setHauswart(kundenRepository.get(5).get());
		
		ArrayList<Anlagenstandort> aListe = new ArrayList<Anlagenstandort>();
		aListe.add(anlagenst3);
		aListe.add(anlagenst4);
		aListe.add(anlagenst5);
	
		anlagenstandortRepository.persist(aListe);
		
		assertEquals("Depard", anlagenstandortRepository.get(4).get().getHauswart().getNachname());
		assertEquals("Pierre", anlagenstandortRepository.get(4).get().getHauswart().getVorname());
		
		assertEquals("Schneebeli", anlagenstandortRepository.get(5).get().getHauswart().getNachname());
		assertEquals("Edgard", anlagenstandortRepository.get(5).get().getHauswart().getVorname());
		
	}
	
	/**
	 * Die Anlagenstandort-Objekte mit der Id gr�sser als 5 werden gel�scht. 
	 */
	@Test
	public void testCanRemoveAnlagenstandorte()
	{
		anlagenstandortRepository.remove(6);
		assertEquals(9, anlagenstandortRepository.get().size());
		
		anlagenstandortRepository.remove(anlagenstandortRepository.get(7).get(), anlagenstandortRepository.get(8).get());
		assertEquals(7, anlagenstandortRepository.get().size());
		
		Set<Anlagenstandort> aList = anlagenstandortRepository.get(m -> m.getId() > 5 && m.getId() <= 10);
		anlagenstandortRepository.remove(aList);
		assertEquals(0, anlagenstandortRepository.get(m -> m.getId() > 5 && m.getId() <= 10).size());
	}
	
	/**
	 * Bean-Validation wird getestet.
	 * Es wird ein Anlagenstandort-Objekt erstellt.
	 * Zuerst werden in die Felder ung�ltige Werte geschrieben.
	 * Danach wird die Anzahl der ung�ltigen Werte gez�hlt und Schritt f�r Schritt
	 * mit g�ltigen Werte abgef�llt, bis die Anzahl der ung�ltigen Werte auf 0 ist.
	 */
	@Test
	public void testValidateAnlagenstandort()
	{
		Anlagenstandort anlagenst1 = new Anlagenstandort();
		anlagenst1.setStrasse("Eschenstrasse dies ist eine viel zu lange Strasse dies ist eine viel zu lange Strasse dies ist eine viel zu lange Strasse ");
		//anlagenst1.setGemeinde(gemeinde1);
		//anlagenst1.setKunde(kunde4);
		//anlagenst1.setPlzOrt(plzOrt1);
		//anlagenst1.setBrenner(brenner1);

		
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Anlagenstandort>> constraintViolations = validator.validate(anlagenst1);
		constraintViolations.forEach(m -> System.out.println(m.getMessage()));
		assertEquals(4, constraintViolations.size());
		
		anlagenst1.setStrasse("Hallwilerweg 57");
		constraintViolations = validator.validate(anlagenst1);
		assertEquals(3, constraintViolations.size());
		
		anlagenst1.setGemeinde(gemeindeRepository.get(1).get());
		constraintViolations = validator.validate(anlagenst1);
		assertEquals(2, constraintViolations.size());
		
		anlagenst1.setKunde(kundenRepository.get(3).get());
		constraintViolations = validator.validate(anlagenst1);
		assertEquals(1, constraintViolations.size());
		
		anlagenst1.setPlzOrt(plzOrtRepository.get(1).get());
		constraintViolations = validator.validate(anlagenst1);
		assertEquals(0, constraintViolations.size());
	}
}
