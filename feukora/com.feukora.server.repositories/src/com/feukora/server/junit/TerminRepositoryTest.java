package com.feukora.server.junit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

import com.feukora.server.models.Termin;
import com.feukora.server.models.Waermeerzeuger;
import com.feukora.server.models.Waermeerzeugermodell;
import com.feukora.server.models.Kontrolle;
import com.feukora.server.models.Kunde;
import com.feukora.server.models.Mitarbeiter;
import com.feukora.server.models.MitarbeiterRolle;
import com.feukora.server.models.PlzOrt;
import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Anlagenstandort;
import com.feukora.server.models.Brenner;
import com.feukora.server.models.Brennerart;
import com.feukora.server.models.Brennermodell;
import com.feukora.server.models.Brennstoff;
import com.feukora.server.models.Gemeinde;
import com.feukora.server.repositories.IRepository;
import com.feukora.server.repositories.TerminRepository;
/**
 * 
 * @author Philipp Anderhub
 *
 */
public class TerminRepositoryTest  {

	private static IRepository<Termin> terminRepository = new TerminRepository(new ConfigManager().getTestDatabaseName());
	private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	

	
	// Generiere 10 Termine
	// Nur die ersten 5 werden geupdatet
	// Nur die Termine zwischen 6 und 10 werden gel�scht
	@BeforeClass
	public static void init() {
		
		Gemeinde gemeinde1 = new Gemeinde();
		gemeinde1.setBezeichnung("Adliswil");
		gemeinde1.setKanton("Z�rich");
		
		Gemeinde gemeinde2 = new Gemeinde();
		gemeinde2.setBezeichnung("Adligenswil");
		gemeinde2.setKanton("Luzern");
		

		
		Brennstoff brennstoff1 = new Brennstoff();
		brennstoff1.setBezeichnung("Holz");
		
		Brennerart brennerArt1 = new Brennerart();
		brennerArt1.setBezeichnung("Holzkessel");
		
		Waermeerzeugermodell waermeerzeugermodell1 = new Waermeerzeugermodell();
		waermeerzeugermodell1.setBezeichnung("Monitec 2000");
		waermeerzeugermodell1.setBrennstoff(brennstoff1);
		waermeerzeugermodell1.setHersteller("Monikas Heizungen");
		
		Waermeerzeuger waermeerzeuger1 = new Waermeerzeuger();
		waermeerzeuger1.setBaujahr(2008);
		waermeerzeuger1.setWaermeerzeugermodell(waermeerzeugermodell1);
		waermeerzeuger1.setZulassungsNr("CH-39128");
		
		Waermeerzeuger waermeerzeuger2 = new Waermeerzeuger();
		waermeerzeuger2.setBaujahr(2008);
		waermeerzeuger2.setWaermeerzeugermodell(waermeerzeugermodell1);
		waermeerzeuger2.setZulassungsNr("CH-39129");
		
		Waermeerzeuger waermeerzeuger3 = new Waermeerzeuger();
		waermeerzeuger3.setBaujahr(2008);
		waermeerzeuger3.setWaermeerzeugermodell(waermeerzeugermodell1);
		waermeerzeuger3.setZulassungsNr("CH-3913B");
		
		Waermeerzeuger waermeerzeuger4 = new Waermeerzeuger();
		waermeerzeuger4.setBaujahr(2008);
		waermeerzeuger4.setWaermeerzeugermodell(waermeerzeugermodell1);
		waermeerzeuger4.setZulassungsNr("CH-39131");
		
		Waermeerzeuger waermeerzeuger5 = new Waermeerzeuger();
		waermeerzeuger5.setBaujahr(2008);
		waermeerzeuger5.setWaermeerzeugermodell(waermeerzeugermodell1);
		waermeerzeuger5.setZulassungsNr("CH-39132");
		
		Brennermodell brennermodell1 = new Brennermodell();
		brennermodell1.setBezeichnung("Heat 150");
		brennermodell1.setBrennerart(brennerArt1);
		brennermodell1.setFeuerungswaermeleistung(2000);
		brennermodell1.setHersteller("immerWarm (r)");
		
		Brenner brenner1 = new Brenner();
		brenner1.setBaujahr(2009);
		brenner1.setBrennermodell(brennermodell1);
		brenner1.setZulassungsNr("CH-3892");
		
		Brenner brenner2 = new Brenner();
		brenner2.setBaujahr(2009);
		brenner2.setBrennermodell(brennermodell1);
		brenner2.setZulassungsNr("CH-3893");
		
		Brenner brenner3 = new Brenner();
		brenner3.setBaujahr(2009);
		brenner3.setBrennermodell(brennermodell1);
		brenner3.setZulassungsNr("CH-3894");
		
		Brenner brenner4 = new Brenner();
		brenner4.setBaujahr(2009);
		brenner4.setBrennermodell(brennermodell1);
		brenner4.setZulassungsNr("CH-3895");
		
		Brenner brenner5 = new Brenner();
		brenner5.setBaujahr(2009);
		brenner5.setBrennermodell(brennermodell1);
		brenner5.setZulassungsNr("CH-3896");
		
		
	
		PlzOrt plzOrt1 = new PlzOrt();
		plzOrt1.setOrt("Z�rich");
		plzOrt1.setPlz(8001);
		
		PlzOrt plzOrt2 = new PlzOrt();
		plzOrt2.setOrt("Luzern");
		plzOrt2.setPlz(6005);
		
		MitarbeiterRolle mitarbeiterRolle1 = new MitarbeiterRolle();
		mitarbeiterRolle1.setBezeichnung("Kontrolleur");

		Mitarbeiter mitarbeiter1 = new Mitarbeiter();
		mitarbeiter1.setVorname("Francesco");
		mitarbeiter1.setNachname("Sabotelli");
		mitarbeiter1.setStrasse("Moosstrasse 23");
		mitarbeiter1.setPlzOrt(plzOrt2);
		mitarbeiter1.setTelefon("041 201 03 84");
		mitarbeiter1.setMobile("075 832 02 03");
		mitarbeiter1.setEmail("f.sabotelli@sabotec.ch");
		mitarbeiter1.setUserName("fsabotelli");
		mitarbeiter1.setPasswort("ichbinenganzehash");
		mitarbeiter1.setRolle(mitarbeiterRolle1);
	
		Mitarbeiter mitarbeiter2 = new Mitarbeiter();
		mitarbeiter2.setVorname("Nunathananthanan");
		mitarbeiter2.setNachname("Burathean");
		mitarbeiter2.setStrasse("Oerlikonerweg 42");
		mitarbeiter2.setPlzOrt(plzOrt1);
		mitarbeiter2.setTelefon("043 234 42 24");
		mitarbeiter2.setMobile("076 331 44 64");
		mitarbeiter2.setEmail("burathean@thermo-service.ch");
		mitarbeiter2.setUserName("nburathean");
		mitarbeiter2.setPasswort("ichbinenganzehash");
		mitarbeiter2.setRolle(mitarbeiterRolle1);
	
		Kunde kunde1 = new Kunde();
		kunde1.setVorname("Peter");
		kunde1.setNachname("Brandenberger");
		kunde1.setEmail("p.brandenberger@gmail.ch");
		kunde1.setStrasse("Schlossgasse 42");
		kunde1.setTelefon("079 451 23 21");
		kunde1.setMobile("079 451 23 21");
		kunde1.setFirma("Brandenberger Bau AG");
		kunde1.setPlzOrt(plzOrt1);
		kunde1.setKundenArt("Hauswart");
	
		Kunde kunde2 = new Kunde();
		kunde2.setVorname("Hans");
		kunde2.setNachname("Vogel");
		kunde2.setEmail("hans.vogel@gmail.ch");
		kunde2.setStrasse("Kellerstrasse 3");
		kunde2.setTelefon("078 651 23 12");
		kunde2.setMobile("078 651 23 12");
		kunde2.setFirma("Vogel Heizungen GmbH");
		kunde2.setPlzOrt(plzOrt2);
		kunde2.setKundenArt("Verwaltung");
	
		Anlagenstandort anlagenstandort1 = new Anlagenstandort();
		anlagenstandort1.setGemeinde(gemeinde1);
		anlagenstandort1.setHauswart(kunde1);
		anlagenstandort1.setKunde(kunde2);
		anlagenstandort1.setPlzOrt(plzOrt1);
		anlagenstandort1.setStrasse("Grundstrasse 20");
		anlagenstandort1.setBrenner(brenner1);
		anlagenstandort1.setWaermeerzeuger(waermeerzeuger1);
		
		Anlagenstandort anlagenstandort2 = new Anlagenstandort();
		anlagenstandort2.setGemeinde(gemeinde2);
		anlagenstandort2.setHauswart(kunde1);
		anlagenstandort2.setKunde(kunde1);
		anlagenstandort2.setPlzOrt(plzOrt2);
		anlagenstandort2.setStrasse("Neuhofstrasse 220");
		anlagenstandort2.setBrenner(brenner1);
		anlagenstandort2.setWaermeerzeuger(waermeerzeuger2);
		
		Anlagenstandort anlagenstandort3 = new Anlagenstandort();
		anlagenstandort3.setGemeinde(gemeinde1);
		anlagenstandort3.setHauswart(kunde2);
		anlagenstandort3.setKunde(kunde2);
		anlagenstandort3.setPlzOrt(plzOrt1);
		anlagenstandort3.setStrasse("Blauturmpfad 32a");
		anlagenstandort3.setBrenner(brenner1);
		anlagenstandort3.setWaermeerzeuger(waermeerzeuger1);
	
		Anlagenstandort anlagenstandort4 = new Anlagenstandort();
		anlagenstandort4.setGemeinde(gemeinde2);
		anlagenstandort4.setHauswart(kunde2);
		anlagenstandort4.setKunde(kunde1);
		anlagenstandort4.setPlzOrt(plzOrt2);
		anlagenstandort4.setStrasse("Maihofstrasse 1");
		anlagenstandort4.setBrenner(brenner1);
		anlagenstandort4.setWaermeerzeuger(waermeerzeuger4);
		
		Anlagenstandort anlagenstandort5 = new Anlagenstandort();
		anlagenstandort5.setGemeinde(gemeinde1);
		anlagenstandort5.setHauswart(kunde1);
		anlagenstandort5.setKunde(kunde2);
		anlagenstandort5.setPlzOrt(plzOrt1);
		anlagenstandort5.setStrasse("Paradeplatzsicht 6");
		anlagenstandort5.setBrenner(brenner1);
		anlagenstandort5.setWaermeerzeuger(waermeerzeuger5);

		Kontrolle kontrolle1 = new Kontrolle();
		kontrolle1.setBemerkungen("");
		kontrolle1.setBestanden(true);
		kontrolle1.setEinregulierungMoeglich(false);
		kontrolle1.setFailAbgasverlust(false);
		kontrolle1.setFailCo(false);
		kontrolle1.setFailNo(false);
		kontrolle1.setFailOel(false);
		kontrolle1.setFailRusszahl(false);
		kontrolle1.setKenntnisnahmeKunde(true);
		kontrolle1.setKontrollart("Routinekontrolle");
		
		
		Termin termin1 = new Termin();
		termin1.setAnlagenstandort(anlagenstandort1);
		termin1.setDatumZeit(new GregorianCalendar(2015, 12, 28, 10, 00));
		termin1.setMitarbeiter(mitarbeiter1);
		
		Termin termin2 = new Termin();
		termin2.setAnlagenstandort(anlagenstandort2);
		termin2.setDatumZeit(new GregorianCalendar(2016, 1, 4, 10, 00));
		termin2.setMitarbeiter(mitarbeiter2);
		
		Termin termin3 = new Termin();
		termin3.setAnlagenstandort(anlagenstandort3);
		termin3.setDatumZeit(new GregorianCalendar(2016, 1, 5, 10, 00));
		termin3.setMitarbeiter(mitarbeiter1);
		
		Termin termin4 = new Termin();
		termin4.setAnlagenstandort(anlagenstandort4);
		termin4.setDatumZeit(new GregorianCalendar(2016, 1, 6, 10, 00));
		termin4.setMitarbeiter(mitarbeiter2);
		
		Termin termin5 = new Termin();
		termin5.setAnlagenstandort(anlagenstandort4);
		termin5.setDatumZeit(new GregorianCalendar(2016, 10, 4, 10, 00));
		termin5.setMitarbeiter(mitarbeiter1);
		
		Termin termin6 = new Termin();
		termin6.setAnlagenstandort(anlagenstandort1);
		termin6.setDatumZeit(new GregorianCalendar(2016, 11, 4, 10, 00));
		termin6.setMitarbeiter(mitarbeiter2);

		Termin termin7 = new Termin();
		termin7.setAnlagenstandort(anlagenstandort2);
		termin7.setDatumZeit(new GregorianCalendar(2016, 12, 4, 10, 00));
		termin7.setMitarbeiter(mitarbeiter1);

		Termin termin8 = new Termin();
		termin8.setAnlagenstandort(anlagenstandort3);
		termin8.setDatumZeit(new GregorianCalendar(2016, 13, 4, 10, 00));
		termin8.setMitarbeiter(mitarbeiter2);
		
		Termin termin9 = new Termin();
		termin9.setAnlagenstandort(anlagenstandort4);
		termin9.setDatumZeit(new GregorianCalendar(2016, 21, 4, 10, 00));
		termin9.setMitarbeiter(mitarbeiter1);
		
		Termin termin10 = new Termin();
		termin10.setAnlagenstandort(anlagenstandort5);
		termin10.setDatumZeit(new GregorianCalendar(2016, 31, 4, 10, 00));
		termin10.setMitarbeiter(mitarbeiter2);
		
		terminRepository.persist(termin1);
		terminRepository.persist(termin2, termin3);
		
		ArrayList<Termin> tListe = new ArrayList<Termin>();
		tListe.add(termin4);
		tListe.add(termin5);
		tListe.add(termin6);
		tListe.add(termin7);
		tListe.add(termin8);
		tListe.add(termin9);
		tListe.add(termin10);
		
		terminRepository.persist(tListe);
	}
		
	@Test
	public void testGetTermin()
	{
		Optional<Termin> termin = terminRepository.get(1);
		assertEquals("Adliswil", termin.get().getAnlagenstandort().getGemeinde().getBezeichnung());
		assertEquals("Peter", termin.get().getAnlagenstandort().getHauswart().getVorname());
		assertEquals("Hans", termin.get().getAnlagenstandort().getKunde().getVorname());
		assertEquals("Z�rich", termin.get().getAnlagenstandort().getPlzOrt().getOrt());
		assertEquals("Grundstrasse 20", termin.get().getAnlagenstandort().getStrasse());
		assertEquals("Francesco", termin.get().getMitarbeiter().getVorname());
		assertEquals("CH-3892", termin.get().getAnlagenstandort().getBrenner().getZulassungsNr());
		assertEquals("Heat 150", termin.get().getAnlagenstandort().getBrenner().getBrennermodell().getBezeichnung());
		assertEquals("Holzkessel", termin.get().getAnlagenstandort().getBrenner().getBrennermodell().getBrennerart().getBezeichnung());
		assertEquals("Monitec 2000", termin.get().getAnlagenstandort().getWaermeerzeuger().getWaermeerzeugermodell().getBezeichnung());
		assertEquals("CH-39128", termin.get().getAnlagenstandort().getWaermeerzeuger().getZulassungsNr());
		assertEquals("Holz", termin.get().getAnlagenstandort().getWaermeerzeuger().getWaermeerzeugermodell().getBrennstoff().getBezeichnung());
				
	}

	
	@Test
	public void testGetTermine()
	{
		// Hol alle Termine die ID kleiner oder gleich 5 haben. Also die ersten 5.
		Set<Termin> tList = terminRepository.get(m -> m.getId() <= 5);
		assertEquals(5, tList.size());
	}
	
	
	@Test
	public void testUpdateTermin()
	{
		Termin termin1 = terminRepository.get(1).get();
		termin1.setDatumZeit(new GregorianCalendar(2016,02,03,8,00));
		
		terminRepository.persist(termin1);
		assertEquals(new GregorianCalendar(2016,02,03,8,00), terminRepository.get(1).get().getDatumZeit());
		
		Termin termin2 = terminRepository.get(2).get();
		termin2.setDatumZeit(new GregorianCalendar(2016,02,04,8,00));
		
		Termin termin3 = terminRepository.get(3).get();
		termin3.setDatumZeit(new GregorianCalendar(2016,02,05,8,00));
		
		terminRepository.persist(termin2, termin3);
		assertEquals(new GregorianCalendar(2016,02,04,8,00), terminRepository.get(2).get().getDatumZeit());
		assertEquals(new GregorianCalendar(2016,02,05,8,00), terminRepository.get(3).get().getDatumZeit());
		
		Termin termin4 = terminRepository.get(4).get();
		termin4.setDatumZeit(new GregorianCalendar(2016,02,06,8,00));
		
		Termin termin5 = terminRepository.get(5).get();
		termin5.setDatumZeit(new GregorianCalendar(2016,02,07,8,00));
		
		
		ArrayList<Termin> tListe = new ArrayList<Termin>();
		tListe.add(termin4);
		tListe.add(termin5);
	
		terminRepository.persist(tListe);
		
		assertEquals(new GregorianCalendar(2016,02,06,8,00), terminRepository.get(4).get().getDatumZeit());
		assertEquals(new GregorianCalendar(2016,02,07,8,00), terminRepository.get(5).get().getDatumZeit());

	}
	
	@Test
	public void testRemoveTermin()
	{
		terminRepository.remove(6);
		assertEquals(9, terminRepository.get().size());
		
		terminRepository.remove(terminRepository.get(7).get(), terminRepository.get(8).get());
		assertEquals(7, terminRepository.get().size());
		
		Set<Termin> tList = terminRepository.get(m -> m.getId() > 5 && m.getId() <= 10);
		terminRepository.remove(tList);
		assertEquals(0, terminRepository.get(m -> m.getId() > 5 && m.getId() <= 10).size());
	}
	
	@Test
	public void testValidateTermin()
	{
		Termin termin1 = new Termin();
		termin1.setAnlagenstandort(null);
		termin1.setMitarbeiter(null);
		termin1.setDatumZeit(null);
		
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Termin>> constraintViolations = validator.validate(termin1);
	
		assertEquals(3, constraintViolations.size());
		
		termin1.setDatumZeit(new GregorianCalendar(2016,02,06,8,00));
		constraintViolations = validator.validate(termin1);
		assertEquals(2, constraintViolations.size());
		
	}
}
