package com.feukora.server.junit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Gemeinde;
import com.feukora.server.repositories.IRepository;
import com.feukora.server.repositories.GemeindeRepository;
/**
 * 
 * @author Philipp Anderhub
 *
 */
public class GemeindeRepositoryTest {

	private static IRepository<Gemeinde> gemeindeRepository = new GemeindeRepository(new ConfigManager().getTestDatabaseName());
	private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	
	// Generiere 10 Gemeinden
	// Nur die ersten 5 werden geupdatet
	// Nur die Gemeinden zwischen 6 und 10 werden gel�scht
	@BeforeClass
	public static void init() {
		
		Gemeinde gemeinde1 = new Gemeinde();
		gemeinde1.setBezeichnung("Risch");
		gemeinde1.setKanton("Zug");
		
		Gemeinde gemeinde2 = new Gemeinde();
		gemeinde2.setBezeichnung("Luzern");
		gemeinde2.setKanton("Luzern");
		
		Gemeinde gemeinde3 = new Gemeinde();
		gemeinde3.setBezeichnung("Meierskappel");
		gemeinde3.setKanton("Luzern");
	
		Gemeinde gemeinde4 = new Gemeinde();
		gemeinde4.setBezeichnung("Chame");
		gemeinde4.setKanton("Zug");
		
		Gemeinde gemeinde5 = new Gemeinde();
		gemeinde5.setBezeichnung("Steihausen");
		gemeinde5.setKanton("Zug");
		
		Gemeinde gemeinde6 = new Gemeinde();
		gemeinde6.setBezeichnung("Nonnenhausen");
		gemeinde6.setKanton("Schwyz");
		
		Gemeinde gemeinde7 = new Gemeinde();
		gemeinde7.setBezeichnung("R�bikon");
		gemeinde7.setKanton("Uri");
		
		Gemeinde gemeinde8 = new Gemeinde();
		gemeinde8.setBezeichnung("Littau");
		gemeinde8.setKanton("Luzern");
		
		Gemeinde gemeinde9 = new Gemeinde();
		gemeinde9.setBezeichnung("Hochdorf");
		gemeinde9.setKanton("Luzern");
		
		Gemeinde gemeinde10 = new Gemeinde();
		gemeinde10.setBezeichnung("M�nsingen");
		gemeinde10.setKanton("Bern");
		
		gemeindeRepository.persist(gemeinde1);
		gemeindeRepository.persist(gemeinde2, gemeinde3);
		
		ArrayList<Gemeinde> gListe = new ArrayList<Gemeinde>();
		gListe.add(gemeinde4);
		gListe.add(gemeinde5);
		gListe.add(gemeinde6);
		gListe.add(gemeinde7);
		gListe.add(gemeinde8);
		gListe.add(gemeinde9);
		gListe.add(gemeinde10);
		
		gemeindeRepository.persist(gListe);
	}
		
	@Test
	public void testGetGemeinde()
	{
		Optional<Gemeinde> gemeinde = gemeindeRepository.get(1);
		assertEquals("Risch", gemeinde.get().getBezeichnung());
		assertEquals("Zug", gemeinde.get().getKanton());	
	}

	
	@Test
	public void testGetGemeinden()
	{
		// Hol die ersten 5 Gemeinden
		Set<Gemeinde> kList = gemeindeRepository.get(m -> m.getId() <= 5);
		assertEquals(5, kList.size());
	}
	
	
	@Test
	public void testUpdateGemeinde()
	{
		Gemeinde gemeinde1 = gemeindeRepository.get(1).get();
		gemeinde1.setBezeichnung("Risch-Rotkreuz");
		
		gemeindeRepository.persist(gemeinde1);
		assertEquals("Risch-Rotkreuz", gemeindeRepository.get(1).get().getBezeichnung());
		
		Gemeinde gemeinde2 = gemeindeRepository.get(2).get();
		gemeinde2.setBezeichnung("Luzern City");
		
		Gemeinde gemeinde3 = gemeindeRepository.get(3).get();
		gemeinde3.setBezeichnung("Meierskappel-Udligenswil");
		
		gemeindeRepository.persist(gemeinde2, gemeinde3);
		assertEquals("Luzern City", gemeindeRepository.get(2).get().getBezeichnung());
		assertEquals("Meierskappel-Udligenswil", gemeindeRepository.get(3).get().getBezeichnung());
		
		Gemeinde gemeinde4 = gemeindeRepository.get(4).get();
		gemeinde4.setBezeichnung("Cham");
		
		Gemeinde gemeinde5 = gemeindeRepository.get(5).get();
		gemeinde5.setBezeichnung("Steinhausen");
		
		ArrayList<Gemeinde> gListe = new ArrayList<Gemeinde>();
		gListe.add(gemeinde4);
		gListe.add(gemeinde5);
	
		gemeindeRepository.persist(gListe);
		
		assertEquals("Cham", gemeindeRepository.get(4).get().getBezeichnung());
		assertEquals("Steinhausen", gemeindeRepository.get(5).get().getBezeichnung());
	}
	
	@Test
	public void testRemoveGemeinde()
	{
		gemeindeRepository.remove(6);
		assertEquals(9, gemeindeRepository.get().size());
		
		gemeindeRepository.remove(gemeindeRepository.get(7).get(), gemeindeRepository.get(8).get());
		assertEquals(7, gemeindeRepository.get().size());
		
		Set<Gemeinde> kList = gemeindeRepository.get(m -> m.getId() > 5 && m.getId() <= 10);
		gemeindeRepository.remove(kList);
		assertEquals(0, gemeindeRepository.get(m -> m.getId() > 5 && m.getId() <= 10).size());
	}
	
	@Test
	public void testValidateGemeinde()
	{
		Gemeinde gemeinde1 = new Gemeinde();
		gemeinde1.setBezeichnung("Gemeinde aus der Fusion von Risch-Meierskappel-Cham-Steinhausen-Merenschwand-Dietwil");
		gemeinde1.setKanton("Der ultimative Superduperkanton, welcher gar nicht wirklich existiert. Das k�mmer aber niemanden.");
		
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Gemeinde>> constraintViolations = validator.validate(gemeinde1);
	
		assertEquals(2, constraintViolations.size());
		
		gemeinde1.setBezeichnung("Zug");
		constraintViolations = validator.validate(gemeinde1);
		assertEquals(1, constraintViolations.size());
		
		gemeinde1.setKanton("Zug");
		constraintViolations = validator.validate(gemeinde1);
		assertEquals(0, constraintViolations.size());
				
	}
}
