package com.feukora.server.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
/**
 * 
 * @author Nadine Lang
 * @version 1.0
  *
 *Klasse um alle vorhandenen JUnit Test auf einmal zu testen. 
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ AnlagenstandortRepositoryTest.class, BrennerartRepositoryTest.class, BrennermodellRepositoryTest.class,
		BrennerRepositoryTest.class, BrennstoffRepositoryTest.class, GemeindeRepositoryTest.class,
		KontrolleRepositoryTest.class, KundeRepositoryTest.class, MessergebnisRepositoryTest.class,
		MitarbeiterRolleTest.class, MitarbeiterTest.class, PersonRepositoryTest.class, PlzOrtRepositoryTest.class,
		TerminRepositoryTest.class, WaermeerzeugermodellRepositoryTest.class, WaermeerzeugerRepositoryTest.class })
public class AllTests {

}
