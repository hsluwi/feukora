/**
 * 
 */
package com.feukora.server.repositories;

import com.feukora.server.models.Brenner;

/**
 * Data Access f�r Brenner. �ber den Konstruktor wird das generische Repository definiert
 * @author Andrew
 *
 */
public class BrennerRepository extends Repository<Brenner> {

	/**
	 * Konstruktor welche das generische Repository definiert. 
	 * Typ und PersistenceUnit Name werden durch diesen Konstruktor definiert.
	 * @param persistenceUnitName PersistenceUnit Name
	 */
	public BrennerRepository(String persistenceUnitName) {
		super(Brenner.class, persistenceUnitName);
	}

}
