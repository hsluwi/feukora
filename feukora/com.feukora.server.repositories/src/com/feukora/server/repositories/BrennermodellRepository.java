/**
 * 
 */
package com.feukora.server.repositories;

import com.feukora.server.models.Brennermodell;

/**
 * Data Access f�r Brennermodell. �ber den Konstruktor wird das generische Repository definiert
 * @author Andrew
 *
 */
public class BrennermodellRepository extends Repository<Brennermodell> {

	/**
	 * Konstruktor welche das generische Repository definiert. 
	 * Typ und PersistenceUnit Name werden durch diesen Konstruktor definiert.
	 * @param persistenceUnitName PersistenceUnit Name
	 */
	public BrennermodellRepository(String persistenceUnitName) {
		super(Brennermodell.class, persistenceUnitName);
	}

}
