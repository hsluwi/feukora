/**
 * 
 */
package com.feukora.server.repositories;

import com.feukora.server.models.Kontrolle;

/**
 * Data Access f�r Kontrolle. �ber den Konstruktor wird das generische Repository definiert
 * @author Andrew
 *
 */
public class KontrolleRepository extends Repository<Kontrolle> {

	/**
	 * Konstruktor welche das generische Repository definiert. 
	 * Typ und PersistenceUnit Name werden durch diesen Konstruktor definiert.
	 * @param persistenceUnitName PersistenceUnit Name
	 */
	public KontrolleRepository(String persistenceUnitName) {
		super(Kontrolle.class, persistenceUnitName);
	}

}
