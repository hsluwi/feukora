/**
 * 
 */
package com.feukora.server.repositories;

import com.feukora.server.models.PlzOrt;

/**
 * Data Access f�r PlzOrt. �ber den Konstruktor wird das generische Repository definiert
 * @author Andrew
 *
 */
public class PlzOrtRepository extends Repository<PlzOrt>{

	/**
	 * Konstruktor welche das generische Repository definiert. 
	 * Typ und PersistenceUnit Name werden durch diesen Konstruktor definiert.
	 * @param persistenceUnitName PersistenceUnit Name
	 */
	public PlzOrtRepository(String persistenceUnitName) {
		super(PlzOrt.class, persistenceUnitName);
	}
}
