/**
 * 
 */
package com.feukora.server.repositories;

import com.feukora.server.models.Anlagenstandort;

/**
 * Data Access f�r Anlagenstandort. �ber den Konstruktor wird das generische Repository definiert
 * @author Andrew
 *
 */
public class AnlagenstandortRepository extends Repository<Anlagenstandort>{

	/**
	 * Konstruktor welche das generische Repository definiert. 
	 * Typ und PersistenceUnit Name werden durch diesen Konstruktor definiert.
	 * @param persistenceUnitName PersistenceUnit Name
	 */
	public AnlagenstandortRepository(String persistenceUnitName) {
		super(Anlagenstandort.class, persistenceUnitName);
	}
}
