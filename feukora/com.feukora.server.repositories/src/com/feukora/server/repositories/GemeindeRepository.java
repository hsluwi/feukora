/**
 * 
 */
package com.feukora.server.repositories;

import com.feukora.server.models.Gemeinde;

/**
 * Data Access f�r Gemeinde. �ber den Konstruktor wird das generische Repository definiert
 * @author Andrew
 *
 */
public class GemeindeRepository extends Repository<Gemeinde> {
	
	/**
	 * Konstruktor welche das generische Repository definiert. 
	 * Typ und PersistenceUnit Name werden durch diesen Konstruktor definiert.
	 * @param persistenceUnitName PersistenceUnit Name
	 */
	public GemeindeRepository(String persistenceUnitName) {
		super(Gemeinde.class, persistenceUnitName);
	}

}
