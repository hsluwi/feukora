/**
 * 
 */
package com.feukora.server.repositories;

import com.feukora.server.models.Person;

/**
 * Data Access f�r Person. �ber den Konstruktor wird das generische Repository definiert
 * @author Andrew
 *
 */
public class PersonRepository extends Repository<Person> {

	/**
	 * Konstruktor welche das generische Repository definiert. 
	 * Typ und PersistenceUnit Name werden durch diesen Konstruktor definiert.
	 * @param persistenceUnitName PersistenceUnit Name
	 */
	public PersonRepository(String persistenceUnitName) {
		super(Person.class, persistenceUnitName);
	}

}
