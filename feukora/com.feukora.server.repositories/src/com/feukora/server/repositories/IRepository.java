package com.feukora.server.repositories;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.feukora.server.models.FeukoraObj;

/**
 * Interface f�r das generische Repository. 
 * Dieses Interface beinhaltet Default Implementationen
 * @author Andrew
 *
 */
public interface IRepository<T extends FeukoraObj> {
	
	/**
	 * Holt eine spezifisches Entity �ber die ID von der Datenbank
	 * @param id ID des Entity
	 * @return Entity
	 */
	default Optional<T> get(Integer id) { 
		 return get()
	                .stream()
	                .filter(entity -> entity.getId().equals(id))
	                .findAny();
	}
	
	/**
	 * Holt eine Liste von Entit�ten von der Datenbank, welches auf das �bergebene Predicate passen.
	 * @param predicate filter zb. m -> m.getName() == 'Test'
	 * @return Liste von Entities
	 */
	default Set<T> get(Predicate<T> predicate) {
		return get()
				.stream()
				.filter(predicate)
				.collect(Collectors.toSet());
	}

	/**
	 * Holt alle Entit�ten von der Datenbank
	 * @return
	 */
    Set<T> get();

    /**
     * Speichert oder aktualisiert ein Entit�t in der Datenbank
     * @param entity Entit�t
     * @return Entit�t mit einer ID
     */
    T persist(T entity);

    /**
     * Speichert oder aktualisiert mehrere Entit�ten in der Datenbank. Die Parameteranzahl ist dynamisch
     * @param entities Entit�ten (dynamische Parameteranzahl)
     */
    default void persist(T... entities) {
    	 persist(Arrays.asList(entities));
    }

    /**
     * Speichert  eine Liste von Entit�ten in der Datenbank
     * @param entities Liste von Entit�ten
     */
    default void persist(Collection<T> entities) {
    	entities.forEach(this::persist);
    }

    /**
     * L�scht ein Entit�t von der Datenbank
     * @param entity Entit�t
     */
    void remove(T entity);

    /**
     * L�scht mehrere Entit�ten in der Datenbank. Parameteranzahl ist dynamisch
     * @param entities mehrere Entit�ten
     */
    default void remove(T... entities) {
    	remove(Arrays.asList(entities));
    }

    /**
     * L�scht eine Liste von Entit�ten in der Datenbank
     * @param entities
     */
    default void remove(Collection<T> entities) {
    	 entities.forEach(this::remove);
    }
    
    /**
     * L�scht eine Entit�t in der Datenbank �ber die ID
     * @param id Entit�t ID
     */
    default void remove(Integer id) {
    	remove(entity -> entity.getId().equals(id));
    }
  
    /**
     * L�scht Entit�ten in der Datenbank,  welches auf das �bergebene Predicate passen. 
     * @param predicate filter zb. m -> m.getName() == 'Test'
     */
    default void remove(Predicate<T> predicate) {
    	get(predicate).forEach(this::remove);
    }
}
