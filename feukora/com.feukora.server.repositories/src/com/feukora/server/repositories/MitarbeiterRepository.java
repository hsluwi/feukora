/**
 * 
 */
package com.feukora.server.repositories;

import com.feukora.server.models.Mitarbeiter;

/**
 * Data Access f�r Mitarbeiter. �ber den Konstruktor wird das generische Repository definiert
 * @author Andrew
 *
 */
public class MitarbeiterRepository extends Repository<Mitarbeiter> {

	/**
	 * Konstruktor welche das generische Repository definiert. 
	 * Typ und PersistenceUnit Name werden durch diesen Konstruktor definiert.
	 * @param persistenceUnitName PersistenceUnit Name
	 */
	public MitarbeiterRepository(String persistenceUnitName) {
		super(Mitarbeiter.class, persistenceUnitName);
	}

}
