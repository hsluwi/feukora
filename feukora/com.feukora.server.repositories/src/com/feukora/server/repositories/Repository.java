/**
 * 
 */
package com.feukora.server.repositories;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.FeukoraObj;


/**
 * Generisches Repository. Data Access Layer mit CRUD Methoden
 * @author Andrew
 *
 */
public class Repository<T extends FeukoraObj> implements IRepository<T> {

    private EntityManagerFactory emf;
    private ConfigManager configManager;

    private Class<T> type;

    public Repository(Class<T> type, String persistenceUnitName) {
        this.type = type;
        configManager = new ConfigManager();
        emf = Persistence.createEntityManagerFactory(persistenceUnitName, configManager.getDatabaseSettings());
    }

    /**
     * Holt alle Entit�ten von der Datenbanm
     * @return Liste von Entit�ten
     */
    @Override
    public Set<T> get() {
        List<T> resultList = run(entityManager -> {
            final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            final CriteriaQuery<T> criteria = criteriaBuilder.createQuery(type);

            final Root<T> root = criteria.from(type);
            criteria.select(root);

            final TypedQuery<T> query = entityManager.createQuery(criteria);
            return query.getResultList();
        });

        return new HashSet<>(resultList);
    }

    /**
     * Speichert eine Entit�t in die Datenbank. 
     * Der Prozess ist in einer Transaktion
     * @param Entit�t objekt
     * @return Entit�t objekt
     */
    @Override
    public T persist(T entity) {
        return runInTransaction(entityManager -> {
            T e = entityManager.merge(entity);
            entityManager.flush();
            return e;
        });
    }

    /**
     * L�scht eine Entit�t von der Datenbank.
     * Der Prozess ist in einer Transaktion.
     */
    @Override
    public void remove(T entity) {
        runInTransaction(entityManager -> {
            final T managedEntity = entityManager.find(type, entity.getId());
            if (managedEntity != null) {
                entityManager.remove(managedEntity);
            }
        });
    }

    
    
    /**
     * Methode die ein Funktion zur�ckgibt die ein EntityManager beinhaltet
     * @param function
     * @return Funktion
     */
    private <R> R run(Function<EntityManager, R> function) {
        final EntityManager entityManager = emf.createEntityManager();
        try {
            return function.apply(entityManager);
        } finally {
            entityManager.close();
        }
    }

    /**
     * Methode die ein EntityManager nimmt und den Prozess in eine Transaktionprozess umh�llt
     * @param function
     * @return ResultSet
     */
    private <R> R runInTransaction(Function<EntityManager, R> function) {
        return run(entityManager -> {
            entityManager.getTransaction().begin();

            final R result = function.apply(entityManager);

            entityManager.getTransaction().commit();

            return result;
        });
    }

    /**
     * Registriert das EntityManager in die Funktion f�r den Transaktionsprozess
     * @param function
     */
    private void runInTransaction(Consumer<EntityManager> function) {
        runInTransaction(entityManager -> {
            function.accept(entityManager);
            return null;
        });
    }
}
