/**
 * 
 */
package com.feukora.server.repositories;

import com.feukora.server.models.Brennstoff;

/**
 * Data Access f�r Brennstoff. �ber den Konstruktor wird das generische Repository definiert
 * @author Andrew
 *
 */
public class BrennstoffRepository extends Repository<Brennstoff> {

	/**
	 * Konstruktor welche das generische Repository definiert. 
	 * Typ und PersistenceUnit Name werden durch diesen Konstruktor definiert.
	 * @param persistenceUnitName PersistenceUnit Name
	 */
	public BrennstoffRepository(String persistenceUnitName) {
		super(Brennstoff.class, persistenceUnitName);
	}
}
