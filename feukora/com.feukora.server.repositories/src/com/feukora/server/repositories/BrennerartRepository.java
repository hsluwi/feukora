/**
 * 
 */
package com.feukora.server.repositories;

import com.feukora.server.models.Brennerart;

/**
 * Data Access f�r Brennerart. �ber den Konstruktor wird das generische Repository definiert
 * @author Andrew
 *
 */
public class BrennerartRepository extends Repository<Brennerart> {

	/**
	 * Konstruktor welche das generische Repository definiert. 
	 * Typ und PersistenceUnit Name werden durch diesen Konstruktor definiert.
	 * @param persistenceUnitName PersistenceUnit Name
	 */
	public BrennerartRepository( String persistenceUnitName) {
		super(Brennerart.class, persistenceUnitName);
	}

}
