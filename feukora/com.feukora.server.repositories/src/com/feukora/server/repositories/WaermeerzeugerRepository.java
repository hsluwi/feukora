/**
 * 
 */
package com.feukora.server.repositories;

import com.feukora.server.models.Waermeerzeuger;

/**
 * Data Access f�r Waermeerzeuger. �ber den Konstruktor wird das generische Repository definiert
 * @author Andrew
 *
 */
public class WaermeerzeugerRepository extends Repository<Waermeerzeuger> {

	/**
	 * Konstruktor welche das generische Repository definiert. 
	 * Typ und PersistenceUnit Name werden durch diesen Konstruktor definiert.
	 * @param persistenceUnitName PersistenceUnit Name
	 */
	public WaermeerzeugerRepository(String persistenceUnitName) {
		super(Waermeerzeuger.class, persistenceUnitName);
	}

}
