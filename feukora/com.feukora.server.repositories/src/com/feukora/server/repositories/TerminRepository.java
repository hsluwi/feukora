/**
 * 
 */
package com.feukora.server.repositories;

import com.feukora.server.models.Termin;

/**
 * Data Access f�r Termin. �ber den Konstruktor wird das generische Repository definiert
 * @author Andrew
 *
 */
public class TerminRepository extends Repository<Termin> {

	/**
	 * Konstruktor welche das generische Repository definiert. 
	 * Typ und PersistenceUnit Name werden durch diesen Konstruktor definiert.
	 * @param persistenceUnitName PersistenceUnit Name
	 */
	public TerminRepository(String persistenceUnitName) {
		super(Termin.class, persistenceUnitName);
	}

}
