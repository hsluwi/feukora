/**
 * 
 */
package com.feukora.server.repositories;

import com.feukora.server.models.Messergebnis;

/**
 * Data Access f�r Messergebnis. �ber den Konstruktor wird das generische Repository definiert
 * @author Andrew
 *
 */
public class MessergebnisRepository extends Repository<Messergebnis> {

	/**
	 * Konstruktor welche das generische Repository definiert. 
	 * Typ und PersistenceUnit Name werden durch diesen Konstruktor definiert.
	 * @param persistenceUnitName PersistenceUnit Name
	 */
	public MessergebnisRepository(String persistenceUnitName) {
		super(Messergebnis.class, persistenceUnitName);
	}

}
