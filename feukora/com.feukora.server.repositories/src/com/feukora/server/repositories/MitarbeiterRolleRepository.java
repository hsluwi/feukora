/**
 * 
 */
package com.feukora.server.repositories;

import com.feukora.server.models.MitarbeiterRolle;

/**
 * Data Access f�r MitarbeiterRolle. �ber den Konstruktor wird das generische Repository definiert
 * @author Andrew
 *
 */
public class MitarbeiterRolleRepository extends Repository<MitarbeiterRolle> {

	/**
	 * Konstruktor welche das generische Repository definiert. 
	 * Typ und PersistenceUnit Name werden durch diesen Konstruktor definiert.
	 * @param persistenceUnitName PersistenceUnit Name
	 */
	public MitarbeiterRolleRepository(String persistenceUnitName) {
		super(MitarbeiterRolle.class, persistenceUnitName);
	}

}
