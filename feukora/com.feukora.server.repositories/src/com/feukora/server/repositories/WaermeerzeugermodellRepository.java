/**
 * 
 */
package com.feukora.server.repositories;

import com.feukora.server.models.Waermeerzeugermodell;

/**
 * Data Access f�r Waermeerzeugermodell. �ber den Konstruktor wird das generische Repository definiert
 * @author Andrew
 *
 */
public class WaermeerzeugermodellRepository extends Repository<Waermeerzeugermodell> {

	/**
	 * Konstruktor welche das generische Repository definiert. 
	 * Typ und PersistenceUnit Name werden durch diesen Konstruktor definiert.
	 * @param persistenceUnitName PersistenceUnit Name
	 */
	public WaermeerzeugermodellRepository(String persistenceUnitName) {
		super(Waermeerzeugermodell.class, persistenceUnitName);
	}

}
