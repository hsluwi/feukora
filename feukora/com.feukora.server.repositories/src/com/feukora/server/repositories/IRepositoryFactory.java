package com.feukora.server.repositories;

import com.feukora.server.models.Anlagenstandort;
import com.feukora.server.models.Brenner;
import com.feukora.server.models.Brennerart;
import com.feukora.server.models.Brennermodell;
import com.feukora.server.models.Brennstoff;
import com.feukora.server.models.Gemeinde;
import com.feukora.server.models.Kontrolle;
import com.feukora.server.models.Kunde;
import com.feukora.server.models.Messergebnis;
import com.feukora.server.models.Mitarbeiter;
import com.feukora.server.models.MitarbeiterRolle;
import com.feukora.server.models.Person;
import com.feukora.server.models.PlzOrt;
import com.feukora.server.models.Termin;
import com.feukora.server.models.Waermeerzeuger;
import com.feukora.server.models.Waermeerzeugermodell;

/**
 * Ein Interface die zust�ndig ist die jeweiligen Repositories (Data Access Objects) zu liefen.
 * @author Andrew
 *
 */
public interface IRepositoryFactory {

	/**
	 * Gibt ein MitarbeiterRepository zur�ck
	 * @return the _mitarbeiterRepository
	 */
	IRepository<Mitarbeiter> get_mitarbeiterRepository();
	
	/**
	 * Gibt ein AnlagenstandortRepository zur�ck
	 * @return the _anlagaenstandortRepository
	 */
	IRepository<Anlagenstandort> get_anlagenstandortRepository();
	
	/**
	 * Gibt ein BrennstoffRepository zur�ck
	 * @return the _brennstoffRepository
	 */
	IRepository<Brennstoff> get_brennstoffRepository();
	
	/**
	 * Gibt ein WaermeerzeugerModellRepository zur�ck
	 * @return the _WaermeerzeugermodellRepository
	 */
	IRepository<Waermeerzeugermodell> get_waermeerzeugermodellRepository();

	/**
	 * Gibt ein BrennermodellRepository zur�ck
	 * @return the _brennermodellRepository
	 */
	IRepository<Brennermodell> get_brennermodellRepository();
	
	/**
	 * Gibt ein BrennerartRepository zur�ck
	 * @return the _brennermodellRepository
	 */
	IRepository<Brennerart> get_brennerartRepository();
	
	/**
	 * Gibt ein KundeRepository zur�ck
	 * @return the _kundeRepository
	 */
	IRepository<Kunde> get_kundeRepository();
	
	
	/**
	 * Gibt ein BrennerRepository zur�ck
	 * @return the _brennerRepository
	 */
	IRepository<Brenner> get_brennerRepository();
	
	/**
	 * Gibt ein MitarbeiterRolleRepository zur�ck
	 * @return the _mitarbeiterRolleRepository
	 */
	IRepository<MitarbeiterRolle> get_mitarbeiterRolleRepository();

	/**
	 * Gibt ein KontrolleRepository zur�ck
	 * @return the _kontrolleRepository
	 */
	IRepository<Kontrolle> get_kontrolleRepository();

	/**
	 * Gibt ein GemeindeRepository zur�ck
	 * @return the _gemeindeRepository
	 */
	IRepository<Gemeinde> get_gemeindeRepository();
	
	/**
	 * Gibt ein PlzOrtRepository zur�ck
	 * @return the _plzOrtRepository
	 */
	IRepository<PlzOrt> get_plzOrtRepository();
	
	/**
	 * Gibt ein MessergebnisRepository zur�ck
	 * @return the _messergebnisRepository
	 */
	IRepository<Messergebnis> get_messergebnisRepository();

	/**
	 * Gibt ein TerminRepository zur�ck
	 * @return the _terminRepositoryRepository
	 */
	IRepository<Termin> get_terminRepository();

	/**
	 * Gibt ein WaermeerzeugerRepository zur�ck
	 * @return the _waermeerzeugerRepository
	 */
	IRepository<Waermeerzeuger> get_waermeerzeugerRepository();

	/**
	 * Gibt ein PersonRepository zur�ck
	 * @return the _personRepository
	 */
	IRepository<Person> get_personRepository();

}