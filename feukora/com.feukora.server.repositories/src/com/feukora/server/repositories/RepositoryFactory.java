/**
 * 
 */
package com.feukora.server.repositories;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.*;

/**
 * Eine Klasse die zust�ndig ist die jeweiligen Repositories (Data Access Objects) zu liefern.
 * @author Andrew
 *
 */
public class RepositoryFactory implements IRepositoryFactory {

	private String persistenceName;
	private static final Logger log = LogManager.getLogger();
	
	/**
	 * Konstrukor mit dem mann die Persistence Unit definieren kann
	 * @param persistenceName
	 */
	public RepositoryFactory(String persistenceName) {
		this.persistenceName = persistenceName;
		
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			log.error(e.getMessage());
		}
	}
	
	/**
	 * Standart Konstruktor. 
	 * Nimmt die standart definierte PersistenceUnit vom communications.properties file
	 */
	public RepositoryFactory() {
		persistenceName = new ConfigManager().getDatabaseName();
		try {
			Class.forName(new ConfigManager().getDatabaseDriver());
		} catch (ClassNotFoundException e) {
			log.error(e.getMessage());
		}
	}
	

	/**
	 * Gibt ein MitarbeiterRepository zur�ck
	 * @return the _mitarbeiterRepository
	 */
	@Override
	public IRepository<Mitarbeiter> get_mitarbeiterRepository() {
		return new MitarbeiterRepository(this.persistenceName);
	}

	/**
	 * Gibt ein KundeRepository zur�ck
	 * @return the _kundeRepository
	 */
	@Override
	public IRepository<Kunde> get_kundeRepository() {
		return new KundeRepository(this.persistenceName);
	}

	/**
	 * Gibt ein PlzOrtRepository zur�ck
	 * @return the _plzOrtRepository
	 */
	public IRepository<PlzOrt> get_plzOrtRepository(){
		return new PlzOrtRepository(this.persistenceName);
	}
	
	/**
	 * Gibt ein BrennerRepository zur�ck
	 * @return the _brennerRepository
	 */
	@Override
	public IRepository<Brenner> get_brennerRepository() {
		return new BrennerRepository(this.persistenceName);
	}

	/**
	 * Gibt ein KontrolleRepository zur�ck
	 * @return the _kontrolleRepository
	 */
	@Override
	public IRepository<Kontrolle> get_kontrolleRepository() {
		return new KontrolleRepository(this.persistenceName);
	}

	/**
	 * Gibt ein MessergebnisRepository zur�ck
	 * @return the _messergebnisRepository
	 */
	@Override
	public IRepository<Messergebnis> get_messergebnisRepository() {
		return new MessergebnisRepository(this.persistenceName);
	}

	/**
	 * Gibt ein TerminRepository zur�ck
	 * @return the _terminRepositoryRepository
	 */
	@Override
	public IRepository<Termin> get_terminRepository() {
		return new TerminRepository(this.persistenceName);
	}

	/**
	 * Gibt ein WaermeerzeugerRepository zur�ck
	 * @return the _waermeerzeugerRepository
	 */
	@Override
	public IRepository<Waermeerzeuger> get_waermeerzeugerRepository() {
		return new WaermeerzeugerRepository(this.persistenceName);
	}

	/**
	 * Gibt ein PersonRepository zur�ck
	 * @return the _personRepository
	 */
	@Override
	public IRepository<Person> get_personRepository() {
		return new PersonRepository(this.persistenceName);
	}

	/**
	 * Gibt ein MitarbeiterRolleRepository zur�ck
	 * @return the _mitarbeiterRolleRepository
	 */
	public IRepository<MitarbeiterRolle> get_mitarbeiterRolleRepository() {
		return new MitarbeiterRolleRepository(this.persistenceName);
	}

	/**
	 * Gibt ein AnlagenstandortRepository zur�ck
	 * @return the _anlagaenstandortRepository
	 */
	@Override
	public IRepository<Anlagenstandort> get_anlagenstandortRepository() {
		return new AnlagenstandortRepository(this.persistenceName);
	}

	/**
	 * Gibt ein BrennermodellRepository zur�ck
	 * @return the _brennermodellRepository
	 */
	@Override
	public IRepository<Brennermodell> get_brennermodellRepository() {
		return new BrennermodellRepository(this.persistenceName);
	}

	/**
	 * Gibt ein BrennerartRepository zur�ck
	 * @return the _brennermodellRepository
	 */
	@Override
	public IRepository<Brennerart> get_brennerartRepository() {
		return new BrennerartRepository(this.persistenceName);
	}

	/**
	 * Gibt ein BrennstoffRepository zur�ck
	 * @return the _brennstoffRepository
	 */
	@Override
	public IRepository<Brennstoff> get_brennstoffRepository() {
		return new BrennstoffRepository(this.persistenceName);
	}

	/**
	 * Gibt ein WaermeerzeugerModellRepository zur�ck
	 * @return the _WaermeerzeugermodellRepository
	 */
	@Override
	public IRepository<Waermeerzeugermodell> get_waermeerzeugermodellRepository() {
		return new WaermeerzeugermodellRepository(this.persistenceName);
	}

	/**
	 * Gibt ein GemeindeRepository zur�ck
	 * @return the _gemeindeRepository
	 */
	@Override
	public IRepository<Gemeinde> get_gemeindeRepository() {
		return new GemeindeRepository(this.persistenceName);
	}
}
