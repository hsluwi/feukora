/**
 * 
 */
package com.feukora.server.repositories;

import com.feukora.server.models.Kunde;

/**
 * Data Access f�r Kunden. �ber den Konstruktor wird das generische Repository definiert
 * @author Andrew
 *
 */
public class KundeRepository extends Repository<Kunde> {

	/**
	 * Konstruktor welche das generische Repository definiert. 
	 * Typ und PersistenceUnit Name werden durch diesen Konstruktor definiert.
	 * @param persistenceUnitName PersistenceUnit Name
	 */
	public KundeRepository(String persistenceUnitName) {
		super(Kunde.class, persistenceUnitName);
	}

}
