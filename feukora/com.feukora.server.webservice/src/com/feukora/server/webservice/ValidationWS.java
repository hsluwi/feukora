package com.feukora.server.webservice;

import java.rmi.RemoteException;
import java.util.Collection;
import javax.jws.WebService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Anlagenstandort;
import com.feukora.server.models.Brenner;
import com.feukora.server.models.Brennerart;
import com.feukora.server.models.Brennermodell;
import com.feukora.server.models.Brennstoff;
import com.feukora.server.models.Gemeinde;
import com.feukora.server.models.Kontrolle;
import com.feukora.server.models.Kunde;
import com.feukora.server.models.Messergebnis;
import com.feukora.server.models.Mitarbeiter;
import com.feukora.server.models.MitarbeiterRolle;
import com.feukora.server.models.PlzOrt;
import com.feukora.server.models.Termin;
import com.feukora.server.models.Waermeerzeuger;
import com.feukora.server.models.Waermeerzeugermodell;
import com.feukora.server.rmi.IValidationController;
import com.feukora.server.webservice.util.RmiManager;

/**
 * 
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 * 
 * Diese Klasse stellt die Webservices f�r "Validation" zur Verf�gung. 
 * Die Daten werden per RMI von der Businesslogik bezogen oder an die Businesslogik weitergeleitet. 
 */
@WebService
public class ValidationWS implements IValidationController {
	
	private static final Logger logger = LogManager.getLogger();
	
	private IValidationController getValidationController () {
		IValidationController vc = (IValidationController) new RmiManager().getValidationRMI();
		return vc;
	}

	@Override
	public Collection<String> validateAnlagenstandort(Anlagenstandort anlagenstandort) throws RemoteException {
		try {
			return getValidationController().validateAnlagenstandort(anlagenstandort);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Collection<String> validateBrennerart(Brennerart brennerart) throws RemoteException {
		try {
			return getValidationController().validateBrennerart(brennerart);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Collection<String> validateBrenner(Brenner brenner) throws RemoteException {
		try {
			return getValidationController().validateBrenner(brenner);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Collection<String> validateBrennermodell(Brennermodell brennermodell) throws RemoteException {
		try {
			return getValidationController().validateBrennermodell(brennermodell);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Collection<String> validateBrennstoff(Brennstoff brennstoff) throws RemoteException {
		try {
			return getValidationController().validateBrennstoff(brennstoff);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Collection<String> validateGemeinde(Gemeinde gemeinde) throws RemoteException {
		try {
			return getValidationController().validateGemeinde(gemeinde);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Collection<String> validateKontrolle(Kontrolle kontrolle) throws RemoteException {
		try {
			return getValidationController().validateKontrolle(kontrolle);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Collection<String> validateKunde(Kunde kunde) throws RemoteException {
		try {
			return getValidationController().validateKunde(kunde);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Collection<String> validateMessergebnis(Messergebnis messergebnis) throws RemoteException {
		try {
			return getValidationController().validateMessergebnis(messergebnis);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Collection<String> validateMitarbeiter(Mitarbeiter mitarbeiter) throws RemoteException {
		try {
			return getValidationController().validateMitarbeiter(mitarbeiter);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Collection<String> validateMitarbeiterRolle(MitarbeiterRolle mitarbeiterRolle) throws RemoteException {
		try {
			return getValidationController().validateMitarbeiterRolle(mitarbeiterRolle);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Collection<String> validatePlzOrt(PlzOrt plzOrt) throws RemoteException {
		try {
			return getValidationController().validatePlzOrt(plzOrt);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Collection<String> validateTermin(Termin termin) throws RemoteException {
		try {
			return getValidationController().validateTermin(termin);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Collection<String> validateWaermeerzeuger(Waermeerzeuger waermeerzeuger) throws RemoteException {
		try {
			return getValidationController().validateWaermeerzeuger(waermeerzeuger);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	@Override
	public Collection<String> validateWaermeerzeugermodell(Waermeerzeugermodell waermeerzeugermodell)
			throws RemoteException {
		try {
			return getValidationController().validateWaermeerzeugermodell(waermeerzeugermodell);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

}
