package com.feukora.server.webservice;

import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Brenner;
import com.feukora.server.rmi.IBrennerController;
import com.feukora.server.webservice.util.RmiManager;
/**
 * 
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 * 
 * Diese Klasse stellt die Webservices f�r "Brenner" zur Verf�gung. 
 * Die Daten werden per RMI von der Businesslogik bezogen oder an die Businesslogik weitergeleitet. 
 */
@WebService
public class BrennerWS implements IBrennerController {
	
	private static final Logger logger = LogManager.getLogger();
	
	private IBrennerController getBrennerController() {
		IBrennerController bmc = (IBrennerController) new RmiManager().getBrennerRMI();
		return bmc;
	}

	/**
	 * Gibt ein Set von "Brenner" zur�ck. Bei Misserfolg wird "null" zur�ckgegeben.
	 * @return Set Brenner
	 */
	public Set<Brenner> getListBrenner() {
		try {
			IBrennerController bc = getBrennerController();
			return bc.getListBrenner();
		} catch (Exception e) {
			logger.error("Brenner konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein einzelnes Objekt "Brenner" zur�ck, wenn eine "id" �bergeben wird.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param Brenner ID
	 * @return Brenner objekt
	 */
	public Brenner getBrenner(Integer id) {
		try {
			IBrennerController bc = getBrennerController();
			return bc.getBrenner(id);
		} catch (Exception e) {
			logger.error("Brenner konnte nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein einzelnes Objekt "Brenner" durch die �bergabe des Objekts.
	 * Bei Erfolg wird das gespeicherte Objekt zur�ckgegeben, bei Misserfolg "null".
	 * @param Brenner objekt
	 * @return Brenner objekt
	 */
	public Brenner saveBrenner(Brenner brenner) {
		try {
			IBrennerController bc = getBrennerController();
			return bc.saveBrenner(brenner);
		} catch (Exception e) {
			logger.error("Brenner konnte nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Objekte "Brenner" durch die �bergabe mehrerer Objekte.
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param mehrere Brenner
	 * @return true oder false
	 */
	public boolean saveMultipleBrenner(Brenner... brennerList) {
		try {
			IBrennerController bc = getBrennerController();
			return bc.saveMultipleBrenner(brennerList);
		} catch (Exception e) {
			logger.error("Brenner konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert mehrere Objekte "Brenner" durch die �bergabe von einer Collection
	 * mit dem entsprechenden Objekt. Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Brennerliste
	 * @return true oder false
	 */
	public boolean saveListBrenner(Collection<Brenner> brennerListe) {
		try {
			IBrennerController bc = getBrennerController();
			return bc.saveListBrenner(brennerListe);
		} catch (Exception e) {
			logger.error("Brenner konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht einen "Brenner" durch die �bergabe einer "id".
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Brenner ID
	 * @return true oder false
	 */
	public boolean removeBrenner(Integer id) {
		try {
			IBrennerController bc = getBrennerController();
			return bc.removeBrenner(id);
		} catch (Exception e) {
			logger.error("Brenner konnte nicht entfernt werden:");
			logger.error(e.getMessage());
		}
		return false;
	}

}
