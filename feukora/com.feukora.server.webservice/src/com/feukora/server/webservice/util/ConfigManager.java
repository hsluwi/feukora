package com.feukora.server.webservice.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Klasse um die Config File auszulesen
 * @author Andrew
 */
public class ConfigManager {
	
	private final String rmiUrl;
	private final String rmiPort;
	
	
	private final String propertiesPath;
	
	public ConfigManager() {
		this.propertiesPath = "config.properties";
		
		Properties properties = new Properties();
		ClassLoader cLoader = this.getClass().getClassLoader();
        try {
        	InputStream is = cLoader.getResourceAsStream(propertiesPath);
        	properties.load(is);
		} catch (IOException e) {
			System.out.println("Configfile nicht geladen.");
		} 
        this.rmiUrl = properties.getProperty("rmiUrl"); 
        this.rmiPort = properties.getProperty("rmiPort");
        System.out.println("Properties werden geladen:");
        System.out.println("RMI URL: " + rmiUrl);
        System.out.println("RMI Port: " + rmiPort);
	}

	/**
	 * @return the rmiUrl
	 */
	public String getRmiUrl() {
		return rmiUrl;
	}

	/**
	 * @return the rmiPort
	 */
	public String getRmiPort() {
		return rmiPort;
	}
	
}
