package com.feukora.server.webservice.util;

import java.rmi.Naming;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.rmi.*;

/**
 * Klasse um sich mit dem RMI-Server zu verbinden
 * @author Andrew
 *
 */
public class RmiManager {

	private ConnectionManager connectionManager;
	private static final Logger log = LogManager.getLogger();
	
	public RmiManager() {
		connectionManager = new ConnectionManager();
	}
	
	/**
	 * Gibt die Klassen vom RMI-Server zur�ck
	 * @param rmiString Interface Klassen
	 * @return
	 */
	private Object getRmiClient(String rmiString) {
		try {
			String url = connectionManager.getRemoteConnection(rmiString);
			return Naming.lookup(url);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	public IAnlagenstandortController getAnlagenstandortRMI() {
		return (IAnlagenstandortController) getRmiClient("IAnlagenstandortController");
	}
	
	public IBrennerartController getBrennerartRMI() {
		return (IBrennerartController) getRmiClient("IBrennerartController");
	}
	
	public IBrennerController getBrennerRMI() {
		return (IBrennerController) getRmiClient("IBrennerController");
	}
	
	public ISetupController getSetupRMI() {
		return (ISetupController) getRmiClient("ISetupController");
	}
	
	public IBrennermodellController getBrennermodellRMI() {
		return (IBrennermodellController) getRmiClient("IBrennermodellController");
	}
	
	public IBrennstoffController getBrennerstoffRMI() {
		return (IBrennstoffController) getRmiClient("IBrennstoffController");
	}
	
	public IGemeindeController getGemeindeRMI() {
		return (IGemeindeController) getRmiClient("IGemeindeController");
	}
	
	public IKontrolleController getKontrolleRMI() {
		return (IKontrolleController) getRmiClient("IKontrolleController");
	}
	
	public IKundeController getKundeRMI() {
		return (IKundeController) getRmiClient("IKundeController");
	}
	
	public IMessergebnisController getMessergebnisRMI() {
		return (IMessergebnisController) getRmiClient("IMessergebnisController");
	}
	
	public IMitarbeiterController getMitarbeiterRMI() {
		return (IMitarbeiterController) getRmiClient("IMitarbeiterController");
	}
	
	public IMitarbeiterRolleController getMitabeiterRolleRMI() {
		return (IMitarbeiterRolleController) getRmiClient("IMitarbeiterRolleController");
	}
	
	public IPlzOrtController getPlzOrtRMI() {
		return (IPlzOrtController) getRmiClient("IPlzOrtController");
	}
	
	public ITerminController getTerminRMI() {
		return (ITerminController) getRmiClient("ITerminController");
	}
	
	public IWaermeerzeugerController getWaermeerzeugerRMI() {
		return (IWaermeerzeugerController) getRmiClient("IWaermeerzeugerController");
	}
	
	public IWaermeerzeugermodellController getWaermeerzeugermodellRMI() {
		return (IWaermeerzeugermodellController) getRmiClient("IWaermeerzeugermodellController");
	}
	
	public IValidationController getValidationRMI() {
		return (IValidationController) getRmiClient("IValidationController");
	} 
}
