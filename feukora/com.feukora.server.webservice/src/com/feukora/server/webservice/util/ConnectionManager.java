package com.feukora.server.webservice.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Klasse f�r die f�r die Connections zust�ndig ist.
 * Sie gibt URL der jeweiligen Services zur�ck.
 * @author Andrew
 */
public class ConnectionManager {
	
	private ConfigManager configManager;
	private static final Logger log = LogManager.getLogger();
	
	
	public ConnectionManager() {
		configManager = new ConfigManager();
		
	}
	
	/**
	 * Gibt die RMI URL zur�ck
	 * @param remoteString
	 * @return RMI Url
	 */
	public String getRemoteConnection(String remoteString) {
		try {
			
			return "rmi://" + configManager.getRmiUrl() + 
					":" + configManager.getRmiPort() + "/" + remoteString;
		} catch (Exception ex) {
			log.error(ex.getMessage());
		}
		return null;
	}
}
