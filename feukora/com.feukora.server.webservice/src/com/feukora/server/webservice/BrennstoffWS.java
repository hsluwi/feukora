package com.feukora.server.webservice;

import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Brennstoff;
import com.feukora.server.rmi.IBrennstoffController;
import com.feukora.server.webservice.util.RmiManager;
/**
 * 
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 * 
 * Diese Klasse stellt die Webservices f�r "Brennstoff" zur Verf�gung. 
 * Die Daten werden per RMI von der Businesslogik bezogen oder an die Businesslogik weitergeleitet. 
 */
@WebService
public class BrennstoffWS implements IBrennstoffController {
	
	private static final Logger logger = LogManager.getLogger();
	
	private IBrennstoffController getBrennerstoffController() {
		IBrennstoffController bmc = (IBrennstoffController) new RmiManager().getBrennerstoffRMI();
		return bmc;
	}
	
	/**
	 * Gibt ein Set von "Brennstoff" zur�ck. Bei Misserfolg wird "null" zur�ckgegeben.
	 * @return Set Brennstoff
	 */
	public Set<Brennstoff> getListBrennstoff() {
		try {
			IBrennstoffController bsc = getBrennerstoffController();
			return bsc.getListBrennstoff();
		} catch (Exception e) {
			logger.error("Brennstoffe konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein einzelnes Objekt "Brennstoff" zur�ck, wenn eine "id" �bergeben wird.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param Brennstoff ID
	 * @return Brennstoff objekt
	 */
	public Brennstoff getBrennstoff(Integer id) {
		try {
			IBrennstoffController bsc = getBrennerstoffController();
			return bsc.getBrennstoff(id);
		} catch (Exception e) {
			logger.error("Brennstoff konnte nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein einzelnes Objekt "Brennstoff" durch die �bergabe des Objekts.
	 * Bei Erfolg wird das gespeicherte Objekt zur�ckgegeben, bei Misserfolg "null".
	 * @param Brennstoff objekt
	 * @return Brennstoff objekt
	 */	
	public Brennstoff saveBrennstoff(Brennstoff brennstoff) {
		try {
			IBrennstoffController bsc = getBrennerstoffController();
			return bsc.saveBrennstoff(brennstoff);
		} catch (Exception e) {
			logger.error("Brennstoff konnte nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Objekte "Brennstoff" durch die �bergabe mehrerer Objekte.
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Set Brennstoff
	 * @return true oder false
	 */
	public boolean saveMultipleBrennstoff(Brennstoff... brennstoffList) {
		try {
			IBrennstoffController bsc = getBrennerstoffController();
			return bsc.saveMultipleBrennstoff(brennstoffList);
		} catch (Exception e) {
			logger.error("Brennstoffe konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}

	/**
	 * Speichert mehrere Objekte "Brennstoff" durch die �bergabe von einer Collection
	 * mit dem entsprechenden Objekt. Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Colletion Brennstoff
	 * @return true oder false
	 */
	public boolean saveListBrennstoff(Collection<Brennstoff> brennstoffListe) {
		try {
			IBrennstoffController bsc = getBrennerstoffController();
			return bsc.saveListBrennstoff(brennstoffListe);
		} catch (Exception e) {
			logger.error("Brennstoffe konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht einen "Brennstoff" durch die �bergabe einer "id".
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Brennstoff ID
	 * @return true oder false
	 */
	public boolean removeBrennstoff(Integer id) {
		try {
			IBrennstoffController bsc = getBrennerstoffController();
			return bsc.removeBrennstoff(id);
		} catch (Exception e) {
			logger.error("Brennstoff konnte nicht entfernt werden:");
			logger.error(e.getMessage());
		}
		return false;
	}

}
