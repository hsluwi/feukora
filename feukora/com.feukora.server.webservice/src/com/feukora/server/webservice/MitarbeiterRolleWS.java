package com.feukora.server.webservice;

import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.MitarbeiterRolle;
import com.feukora.server.rmi.IMitarbeiterRolleController;
import com.feukora.server.webservice.util.RmiManager;
/**
 * 
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 * 
 * Diese Klasse stellt die Webservices f�r "MitarbeiterRolle" zur Verf�gung. 
 * Die Daten werden per RMI von der Businesslogik bezogen oder an die Businesslogik weitergeleitet. 
 */
@WebService
public class MitarbeiterRolleWS implements IMitarbeiterRolleController {

	private static final Logger logger = LogManager.getLogger();
	
	private IMitarbeiterRolleController getMessergebnisRolleController() {
		IMitarbeiterRolleController mc = (IMitarbeiterRolleController) new RmiManager().getMitabeiterRolleRMI();
		return mc;
	}
	
	/**
	 * Gibt ein Set von "MitarbeiterRolle" zur�ck. Bei Misserfolg wird "null" zur�ckgegeben.
	 * @return Set MitarbeiterRolle
	 */
	public Set<MitarbeiterRolle> getListMitarbeiterRolle() {
		try {
			IMitarbeiterRolleController mrc = getMessergebnisRolleController();
			return mrc.getListMitarbeiterRolle();
		} catch (Exception e) {
			logger.error("MitarbeiterRollen konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein einzelnes Objekt "MitarbeiterRolle" zur�ck, wenn eine "id" �bergeben wird.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param MitarbeiterRolle ID
	 * @return MitarbeiterRolle objekt
	 */
	public MitarbeiterRolle getMitarbeiterRolle(Integer id) {
		try {
			IMitarbeiterRolleController mrc = getMessergebnisRolleController();
			return mrc.getMitarbeiterRolle(id);
		} catch (Exception e) {
			logger.error("MitarbeiterRolle konnte nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein einzelnes Objekt "MitarbeiterRolle" durch die �bergabe des Objekts.
	 * Bei Erfolg wird das gespeicherte Objekt zur�ckgegeben, bei Misserfolg "null".
	 * @param MitarbeiterRolle objekt
	 * @return MitarbeiterRolle objekt
	 */
	public MitarbeiterRolle saveMitarbeiterRolle(MitarbeiterRolle mitarbeiterRolle) {
		try {
			IMitarbeiterRolleController mrc = getMessergebnisRolleController();
			return mrc.saveMitarbeiterRolle(mitarbeiterRolle);
		} catch (Exception e) {
			logger.error("MitarbeiterRolle konnte nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Objekte "MitarbeiterRolle" durch die �bergabe mehrerer Objekte.
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param mehrere MitarbeiterRollen
	 * @return true oder false
	 */
	public boolean saveMultipleMitarbeiterRolle(MitarbeiterRolle... mitarbeiterRolleList) {
		try {
			IMitarbeiterRolleController mrc = getMessergebnisRolleController();
			return mrc.saveMultipleMitarbeiterRolle(mitarbeiterRolleList);
		} catch (Exception e) {
			logger.error("MitarbeiterRollen konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert mehrere Objekte "MitarbeiterRolle" durch die �bergabe von einer Collection
	 * mit dem entsprechenden Objekt. Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Collection MitarbeiterRolle
	 * @return true oder false
	 */
	public boolean saveListMitarbeiterRolle(Collection<MitarbeiterRolle> mitarbeiterRolleListe) {
		try {
			IMitarbeiterRolleController mrc = getMessergebnisRolleController();
			return mrc.saveListMitarbeiterRolle(mitarbeiterRolleListe);
		} catch (Exception e) {
			logger.error("MitarbeiterRollen konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht einen "MitarbeiterRolle" durch die �bergabe einer "id".
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param MitarbeiterRolle ID
	 * @return true oder false
	 */
	public boolean removeMitarbeiterRolle(Integer id) {
		try {
			IMitarbeiterRolleController mrc = getMessergebnisRolleController();
			return mrc.removeMitarbeiterRolle(id);
		} catch (Exception e) {
			logger.error("MitarbeiterRolle konnte nicht entfernt werden:");
			logger.error(e.getMessage());
		}
		return false;
	}

}
