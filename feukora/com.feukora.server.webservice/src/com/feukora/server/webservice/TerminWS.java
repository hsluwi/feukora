package com.feukora.server.webservice;

import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Set;

import javax.jws.WebService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Termin;
import com.feukora.server.rmi.ITerminController;
import com.feukora.server.webservice.util.RmiManager;
/**
 * 
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 * 
 * Diese Klasse stellt die Webservices f�r "Termin" zur Verf�gung. 
 * Die Daten werden per RMI von der Businesslogik bezogen oder an die Businesslogik weitergeleitet. 
 */
@WebService
public class TerminWS implements ITerminController{
	
	private static final Logger logger = LogManager.getLogger();
	
	private ITerminController getTerminController() {
		ITerminController tc = (ITerminController) new RmiManager().getTerminRMI();
		return tc;
	}

	/**
	 * Gibt ein Set von "Termin" zur�ck. Bei Misserfolg wird "null" zur�ckgegeben.
	 * @return Terminliste
	 */
	@Override
	public Set<Termin> getListTermin() {
		try {
			ITerminController tc = getTerminController();
			return tc.getListTermin();
		} catch (Exception e) {
			logger.error("Termine konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * Gibt ein Set von "Termin" nach "userId" zur�ck. Dazu wird die "userId" �bergeben.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param Mitarbeiter ID
	 * @return Termine vom Mitarbeiter
	 */
	@Override
	public Set<Termin> getListTerminByUserId(Integer userId) {
		try {
			ITerminController tc = getTerminController();
			return tc.getListTerminByUserId(userId);
		} catch (Exception e) {
			logger.error("Termine konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * Gibt ein einzelnes Objekt "Termin" zur�ck, wenn eine "id" �bergeben wird.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param Termin ID
	 * @return Termin objekt
	 */
	@Override
	public Termin getTermin(Integer id) {
		try {
			ITerminController tc = getTerminController();
			return tc.getTermin(id);
		} catch (Exception e) {
			logger.error("Termin konnte nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * Gibt ein Set von "Termin" einer Woche zur�ck. Dazu wird "date" und "id" �bergeben.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param Datum und Mitarbeiter ID
	 * @return Terminliste
	 */
	@Override
	public Set<Termin> getTerminWeek(GregorianCalendar date) {
		try {
			ITerminController tc = getTerminController();
			return tc.getTerminWeek(date);
		} catch (Exception e) {
			logger.error("Termine konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein Set von "Termin" von einem User in  einer Woche zur�ck. Dazu wird "date" und "id" �bergeben.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 */
	@Override
	public Set<Termin> getTerminWeekByUser(GregorianCalendar date, Integer id) {
		try {
			ITerminController tc = getTerminController();
			return tc.getTerminWeek(date);
		} catch (Exception e) {
			logger.error("Termine konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * Speichert ein einzelnes Objekt "Termin" durch die �bergabe des Objekts.
	 * Bei Erfolg wird das gespeicherte Objekt zur�ckgegeben, bei Misserfolg "null".
	 * @param Termin objekt
	 * @return Termin objekt
	 */
	@Override
	public Termin saveTermin(Termin termin) {
		try {
			ITerminController tc = getTerminController();
			return tc.saveTermin(termin);
		} catch (Exception e) {
			logger.error("Termin konnte nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * Speichert mehrere Objekte "Termin" durch die �bergabe mehrerer Objekte.
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param mehrere Termine
	 * @return true oder false
	 */
	@Override
	public boolean saveMultipleTermin(Termin... termine) {
		try {
			ITerminController tc = getTerminController();
			return tc.saveMultipleTermin(termine);
		} catch (Exception e) {
			logger.error("Termine konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}

	/**
	 * Speichert mehrere Objekte "Termin" durch die �bergabe von einer Collection
	 * mit dem entsprechenden Objekt. Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Terminliste
	 * @return true oder false
	 */
	@Override
	public boolean saveListTermin(Collection<Termin> terminListe) {
		try {
			ITerminController tc = getTerminController();
			return tc.saveListTermin(terminListe);
		} catch (Exception e) {
			logger.error("Termine konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}

	/**
	 * L�scht einen "Termin" durch die �bergabe einer "id".
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Termin ID
	 * @return true oder false
	 */
	@Override
	public boolean removeTermin(Integer id) {
		try {
			ITerminController tc = getTerminController();
			return tc.removeTermin(id);
		} catch (Exception e) {
			logger.error("Termin konnte nicht entfernt werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	

}
