package com.feukora.server.webservice;

import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Gemeinde;
import com.feukora.server.rmi.IGemeindeController;
import com.feukora.server.webservice.util.RmiManager;

/**
 * 
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 * 
 * Diese Klasse stellt die Webservices f�r "Gemeinde" zur Verf�gung. 
 * Die Daten werden per RMI von der Businesslogik bezogen oder an die Businesslogik weitergeleitet. 
 */
@WebService
public class GemeindeWS implements IGemeindeController {

	//Logger Definition: added by Philipp Anderhub
	private static final Logger logger = LogManager.getLogger();
	
	private IGemeindeController getGemeindeController() {
		IGemeindeController gc = (IGemeindeController) new RmiManager().getGemeindeRMI();
		return gc;
	}
	
	/**
	 * Gibt ein Set von "Gemeinde" zur�ck. Bei Misserfolg wird "null" zur�ckgegeben.
	 * @return Set Gemeinde
	 */
	public Set<Gemeinde> getListGemeinde() {
		try {
			IGemeindeController gc = getGemeindeController();
			return gc.getListGemeinde();
		} catch (Exception e) {
			logger.error("Gemeinden konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein einzelnes Objekt "Gemeinde" zur�ck, wenn eine "id" �bergeben wird.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param Gemeinde ID
	 * @return Gemeinde objekt
	 */
	public Gemeinde getGemeinde(Integer id) {
		try {
			IGemeindeController gc = getGemeindeController();
			return gc.getGemeinde(id);
		} catch (Exception e) {
			logger.error("Gemeinde konnte nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein einzelnes Objekt "Gemeinde" durch die �bergabe des Objekts.
	 * Bei Erfolg wird das gespeicherte Objekt zur�ckgegeben, bei Misserfolg "null".
	 * @param Gemeinde objekt
	 * @return Gemeinde objekt
	 */
	public Gemeinde saveGemeinde(Gemeinde gemeinde) {
		try {
			IGemeindeController gc = getGemeindeController();
			return gc.saveGemeinde(gemeinde);
		} catch (Exception e) {
			logger.error("Gemeinde konnte nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Objekte "Gemeinde" durch die �bergabe mehrerer Objekte.
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param mehrere Gemeinde
	 * @return true oder false
	 */
	public boolean saveMultipleGemeinde(Gemeinde... gemeindeList) {
		try {
			IGemeindeController gc = getGemeindeController();
			return gc.saveMultipleGemeinde(gemeindeList);
		} catch (Exception e) {
			logger.error("Gemeinden konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert mehrere Objekte "Gemeinde" durch die �bergabe von einer Collection
	 * mit dem entsprechenden Objekt. Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Gemeindeliste
	 * @return true oder false
	 */
	public boolean saveListGemeinde(Collection<Gemeinde> gemeindeListe) {
		try {
			IGemeindeController gc = getGemeindeController();
			return gc.saveListGemeinde(gemeindeListe);
		} catch (Exception e) {
			logger.error("Gemeinden konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht einen "Gemeinde" durch die �bergabe einer "id".
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Gemeinde ID
	 * @return true oder false
	 */
	public boolean removeGemeinde(Integer id) {
		try {
			IGemeindeController gc = getGemeindeController();
			return gc.removeGemeinde(id);
		} catch (Exception e) {
			logger.error("Gemeinde konnte nicht entfernt werden:");
			logger.error(e.getMessage());
		}
		return false;
	}

}
