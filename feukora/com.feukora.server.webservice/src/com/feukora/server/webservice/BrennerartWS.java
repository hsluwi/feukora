package com.feukora.server.webservice;

import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Brennerart;
import com.feukora.server.rmi.IBrennerartController;
import com.feukora.server.webservice.util.RmiManager;
/**
 * 
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 * 
 * Diese Klasse stellt die Webservices f�r "Brennerart" zur Verf�gung. 
 * Die Daten werden per RMI von der Businesslogik bezogen oder an die Businesslogik weitergeleitet. 
 */
@WebService
public class BrennerartWS implements IBrennerartController {
	
		private static final Logger logger = LogManager.getLogger();
		
	private IBrennerartController getBrennerartController () {
		IBrennerartController bac = (IBrennerartController) new RmiManager().getBrennerartRMI();
		return bac;
	}
	
	/**
	 * Gibt ein Set von "Brennerart" zur�ck. Bei Misserfolg wird "null" zur�ckgegeben.
	 * @return Set Brennerart 
	 */
	public Set<Brennerart> getListBrennerart() {
		try {
			IBrennerartController bac = getBrennerartController();
			return bac.getListBrennerart();
		} catch (Exception e) {
			logger.error("Brennerarten konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein einzelnes Objekt "Brennerart" zur�ck, wenn eine "id" �bergeben wird.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param Brennerart ID
	 * @param Brennerart objekt
	 */
	public Brennerart getBrennerart(Integer id) {
		try {
			IBrennerartController bac = getBrennerartController();
			return bac.getBrennerart(id);
		} catch (Exception e) {
			logger.error("Brennerart konnte nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein einzelnes Objekt "Brennerart" durch die �bergabe des Objekts.
	 * Bei Erfolg wird das gespeicherte Objekt zur�ckgegeben, bei Misserfolg "null".
	 * @param Brennerart objekt
	 * @return Brennerart objekt
	 */
	public Brennerart saveBrennerart(Brennerart brennerart) {
		try {
			IBrennerartController bac = getBrennerartController();
			return bac.saveBrennerart(brennerart);
		} catch (Exception e) {
			logger.error("Brennerart konnte nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Objekte "Brennerart" durch die �bergabe mehrerer Objekte.
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param mehrere Brennerarten
	 * @return true oder false
	 */
	public boolean saveMultipleBrennerart(Brennerart... brennerart) {
		try {
			IBrennerartController bac = getBrennerartController();
			return bac.saveMultipleBrennerart(brennerart);
		} catch (Exception e) {
			logger.error("Brennerarten konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert mehrere Objekte "Brennerart" durch die �bergabe von einer Collection
	 * mit dem entsprechenden Objekt. Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Collection Brennerart
	 * @return true oder false
	 */
	public boolean saveListBrennerart(Collection<Brennerart> brennerartListe) {
		try {
			IBrennerartController bac = getBrennerartController();
			return bac.saveListBrennerart(brennerartListe);
		} catch (Exception e) {
			logger.error("Brennerarten konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht eine "Brennerart" durch die �bergabe einer "id".
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Brennerart ID
	 * @return true oder false
	 */
	public boolean removeBrennerart(Integer id) {
		try {
			IBrennerartController bac = getBrennerartController();
			return bac.removeBrennerart(id);
		} catch (Exception e) {
			logger.error("Brennerart konnte nicht entfernt werden:");
			logger.error(e.getMessage());
		}
		return false;
	}

}
