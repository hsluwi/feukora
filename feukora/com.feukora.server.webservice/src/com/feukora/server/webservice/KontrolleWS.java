package com.feukora.server.webservice;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Kontrolle;
import com.feukora.server.rmi.IKontrolleController;
import com.feukora.server.rmi.IMessergebnisController;
import com.feukora.server.webservice.util.RmiManager;

/**
 * 
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 * 
 * Diese Klasse stellt die Webservices f�r "Kontrolle" zur Verf�gung. 
 * Die Daten werden per RMI von der Businesslogik bezogen oder an die Businesslogik weitergeleitet. 
 */
@WebService
public class KontrolleWS implements IKontrolleController {
	
		private static final Logger logger = LogManager.getLogger();
		
	private IKontrolleController getKontrolleController() {
		IKontrolleController kc = (IKontrolleController) new RmiManager().getKontrolleRMI();
		return kc;
	}
	

	/**
	 * Gibt ein Set von "Kontrolle" zur�ck. Bei Misserfolg wird "null" zur�ckgegeben.
	 * @return Kontrolleliste
	 */
	public Set<Kontrolle> getListKontrolle() {
		try {
			IKontrolleController kc = getKontrolleController();
			return kc.getListKontrolle();
		} catch (Exception e) {
			logger.error("Kontrollen konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein einzelnes Objekt "Kontrolle" zur�ck, wenn eine "id" �bergeben wird.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param Kontrolle ID
	 * @return Kontrolle objekt
	 */
	public Kontrolle getKontrolle(Integer id) {
		try {
			IKontrolleController kc = getKontrolleController();
			return kc.getKontrolle(id);
		} catch (Exception e) {
			logger.error("Kontrolle konnte nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein einzelnes Objekt "Kontrolle" durch die �bergabe des Objekts.
	 * Bei Erfolg wird das gespeicherte Objekt zur�ckgegeben, bei Misserfolg "null".
	 * @param Kontrolle objekt
	 * @return Kontrolle objekt
	 */
	public Kontrolle saveKontrolle(Kontrolle kontrolle) {
		try {
			IKontrolleController kc = getKontrolleController();
			return kc.saveKontrolle(kontrolle);
		} catch (Exception e) {
			logger.error("Kontrolle konnte nicht gesepeichert werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Objekte "Kontrolle" durch die �bergabe mehrerer Objekte.
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param mehrere Kontrolle
	 * @return true oder false
	 */
	public boolean saveMultipleKontrolle(Kontrolle... kontrolleList) {
		try {
			IKontrolleController kc = getKontrolleController();
			return kc.saveMultipleKontrolle(kontrolleList);
		} catch (Exception e) {
			logger.error("Kontrollen konnten nicht gesepeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	

	/**
	 * Speichert mehrere Objekte "Kontrolle" durch die �bergabe von einer Collection
	 * mit dem entsprechenden Objekt. Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Kontrolleliste
	 * @return true oder false
	 */
	public boolean saveListKontrolle(Collection<Kontrolle> kontrolleListe) {
		try {
			IKontrolleController kc = getKontrolleController();
			return kc.saveListKontrolle(kontrolleListe);
		} catch (Exception e) {
			logger.error("Kontrollen konnten nicht gesepeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht eine "Kontrolle" durch die �bergabe einer "id".
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Kontrolle ID
	 * @return true oder false
	 */
	public boolean removeKontrolle(Integer id) {
		try {
			IKontrolleController kc = getKontrolleController();
			return kc.removeKontrolle(id);
		} catch (Exception e) {
			logger.error("Kontrolle konnte nicht entfernt werden:");
			logger.error(e.getMessage());
		}
		return false;
	}

	/**
	 * Gibt Set Kontrolle nach �bergebener Termin-ID zur�ck.
	 * Im Fehlerfall wird null zur�ckgegeben.
	 * @param Termin-ID
	 * @return Kontrolleliste
	 */
	@Override
	public Set<Kontrolle> getKontrolleByTerminId(Integer terminId) throws RemoteException {
		try {
			IKontrolleController mc = getKontrolleController();
			return mc.getKontrolleByTerminId(terminId);
		} catch (Exception e) {
			logger.error("Messergebnisse konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}

}
