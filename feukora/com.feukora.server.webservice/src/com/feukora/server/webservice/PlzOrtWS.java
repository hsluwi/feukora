package com.feukora.server.webservice;

import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.PlzOrt;
import com.feukora.server.rmi.IPlzOrtController;
import com.feukora.server.webservice.util.RmiManager;
/**
 * 
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 * 
 * Diese Klasse stellt die Webservices f�r "PlzOrt" zur Verf�gung. 
 * Die Daten werden per RMI von der Businesslogik bezogen oder an die Businesslogik weitergeleitet. 
 */
@WebService
public class PlzOrtWS implements IPlzOrtController {
	
	private static final Logger logger = LogManager.getLogger();
	
	private IPlzOrtController getPlzOrtController() {
		IPlzOrtController poc = (IPlzOrtController) new RmiManager().getPlzOrtRMI();
		return poc;
	}
	
	/**
	 * Gibt ein Set von "PlzOrt" zur�ck. Bei Misserfolg wird "null" zur�ckgegeben.
	 * @return Set PlzOrt
	 */
	public Set<PlzOrt> getListPlzOrt() {
		try {
			IPlzOrtController poc = getPlzOrtController();
			return poc.getListPlzOrt();
		} catch (Exception e) {
			logger.error("PlzOrt-Objekte konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	

	/**
	 * Gibt ein einzelnes Objekt "PlzOrt" zur�ck, wenn eine "id" �bergeben wird.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param PlzOrt ID
	 * @return PlzOrt objekt
	 */
	public PlzOrt getPlzOrt(Integer id) {
		try {
			IPlzOrtController poc = getPlzOrtController();
			return poc.getPlzOrt(id);
		} catch (Exception e) {
			logger.error("PlzOrt konnte nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein Set von Objekten "PlzOrt" zur�ck, wenn eine "plz" �bergeben wird.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param Postleitzahl
	 * @return Set PlzOrt
	 */
	public Set<PlzOrt> getPlzOrtByPlz(Integer plz){
		try {
			IPlzOrtController poc = getPlzOrtController();
			return poc.getPlzOrtByPlz(plz);
		} catch (Exception e) {
			logger.error("PlzOrt-Objekte konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein einzelnes Objekt "PlzOrt" durch die �bergabe des Objekts.
	 * Bei Erfolg wird das gespeicherte Objekt zur�ckgegeben, bei Misserfolg "null".
	 * @param PlzOrt objekt
	 * @return PlzOrt obekt
	 */
	public PlzOrt savePlzOrt(PlzOrt plzOrt) {
		try {
			IPlzOrtController poc = getPlzOrtController();
			return poc.savePlzOrt(plzOrt);
		} catch (Exception e) {
			logger.error("PlzOrt konnte nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Objekte "PlzOrt" durch die �bergabe mehrerer Objekte.
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param mehrere PlzOrte
	 * @return true oder false
	 */
	public boolean saveMultiplePlzOrt(PlzOrt... plzOrte) {
		try {
			IPlzOrtController poc = getPlzOrtController();
			return poc.saveMultiplePlzOrt(plzOrte);
		} catch (Exception e) {
			logger.error("PlzOrt-Objekte konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert mehrere Objekte "PlzOrt" durch die �bergabe von einer Collection
	 * mit dem entsprechenden Objekt. Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param PlzOrte-Liste
	 * @return true oder false
	 */
	public boolean saveListPlzOrt(Collection<PlzOrt> plzOrtListe) {
		try {
			IPlzOrtController poc = getPlzOrtController();
			return poc.saveListPlzOrt(plzOrtListe);
		} catch (Exception e) {
			logger.error("PlzOrt-Objekte konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht einen "PlzOrt" durch die �bergabe einer "id".
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param PlzOrt ID
	 * @return true oder false
	 */
	public boolean removePlzOrt(Integer id) {
		try {
			IPlzOrtController poc = getPlzOrtController();
			return poc.removePlzOrt(id);
		} catch (Exception e) {
			logger.error("PlzOrt konnte nicht entfernt werden:");
			logger.error(e.getMessage());
		}
		return false;
	}

}
