package com.feukora.server.webservice;

import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Anlagenstandort;
import com.feukora.server.rmi.IAnlagenstandortController;
import com.feukora.server.webservice.util.RmiManager;

/**
 * 
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 * 
 * Diese Klasse stellt die Webservices f�r "Anlagenstandort" zur Verf�gung. 
 * Die Daten werden per RMI von der Businesslogik bezogen oder an die Businesslogik weitergeleitet. 
 */
@WebService
public class AnlagenstandortWS implements IAnlagenstandortController{
	
	private static final Logger logger = LogManager.getLogger();
	
	private IAnlagenstandortController getAnlagestandortController () {
		IAnlagenstandortController ac = (IAnlagenstandortController) new RmiManager().getAnlagenstandortRMI();
		return ac;
	}
	
	/**
	 * Gibt ein Set von "Anlagenstandort" zur�ck. Bei Misserfolg wird "null" zur�ckgegeben.
	 * @return Set Anlagenstandort
	 */
	@Override	
	public Set<Anlagenstandort> getListAnlagenstandort() {
		try {
			IAnlagenstandortController ac = getAnlagestandortController();
			return ac.getListAnlagenstandort();
		} catch (Exception e) {
			logger.error("Anlagenstandorte konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * Gibt ein einzelnes Objekt "Anlagenstandort" zur�ck, wenn eine "id" �bergeben wird.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param Anlagestandort id
	 * @return Anlagestandort objekt
	 */
	@Override
	public Anlagenstandort getAnlagenstandort(Integer id) {
		try {
			IAnlagenstandortController ac = getAnlagestandortController();
			return ac.getAnlagenstandort(id);
		} catch (Exception e) {
			logger.error("Anlagenstandort konnte nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * Speichert ein einzelnes Objekt "Anlagenstandort" durch die �bergabe des Objekts.
	 * Bei Erfolg wird das gespeicherte Objekt zur�ckgegeben, bei Misserfolg "null".
	 * @param Anlagestandort objekt
	 * @return Anlagestandort objekt
	 */
	@Override
	public Anlagenstandort saveAnlagenstandort(Anlagenstandort ao) {
		try {
			IAnlagenstandortController ac = getAnlagestandortController();
			return ac.saveAnlagenstandort(ao);
		} catch (Exception e) {
			logger.error("Anlagenstandort konnte nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * Speichert mehrere Objekte "Anlagenstandort" durch die �bergabe mehrerer Objekte.
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param mehrere Anlagestandorte
	 * @return true oder false
	 */
	@Override
	public boolean saveMultipleAnlagenstandort(Anlagenstandort... aos) {
		try {
			IAnlagenstandortController ac = getAnlagestandortController();
			return ac.saveMultipleAnlagenstandort(aos);
		} catch (Exception e) {
			logger.error("Anlagenstandorte konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}

	/**
	 * Speichert mehrere Objekte "Anlagenstandort" durch die �bergabe von einer Collection
	 * mit dem entsprechenden Objekt. Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Anlagestandortliste
	 * @return true oder false
	 */
	@Override
	public boolean saveListAnlagenstandort(Collection<Anlagenstandort> aoList) {
		try {
			IAnlagenstandortController ac = getAnlagestandortController();
			return ac.saveListAnlagenstandort(aoList);
		} catch (Exception e) {
			logger.error("Anlagenstandorte konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}

	/**
	 * L�scht einen "Anlagenstandort" durch die �bergabe einer "id".
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Anlagestandort ID
	 * @return true oder false
	 */
	@Override
	public boolean removeAnlagenstandort(Integer id) {
		try {
			IAnlagenstandortController ac = getAnlagestandortController();
			return ac.removeAnlagenstandort(id);
		} catch (Exception e) {
			logger.error("Anlagenstandort konnte nicht entfernt werden:");
			logger.error(e.getMessage());
		}
		return false;
	}	
}
