package com.feukora.server.webservice;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Waermeerzeugermodell;
import com.feukora.server.rmi.IWaermeerzeugermodellController;
import com.feukora.server.webservice.util.RmiManager;

/**
 * 
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 * 
 * Diese Klasse stellt die Webservices f�r "Waermeerzeugermodell" zur Verf�gung. 
 * Die Daten werden per RMI von der Businesslogik bezogen oder an die Businesslogik weitergeleitet. 
 */
@WebService
public class WaermeerzeugermodellWS implements IWaermeerzeugermodellController {
	
	private static final Logger logger = LogManager.getLogger();
	
	private IWaermeerzeugermodellController getWaermeerzeugermodellController() {
		IWaermeerzeugermodellController tc = (IWaermeerzeugermodellController) new RmiManager().getWaermeerzeugermodellRMI();
		return tc;
	}

	/**
	 * Gibt ein Set von "Waermeerzeugermodell" zur�ck. Bei Misserfolg wird "null" zur�ckgegeben.
	 * @return Set Waermeerzeuger
	 */	
	public Set<Waermeerzeugermodell> getListWaermeerzeugermodell() {
		try {
			IWaermeerzeugermodellController wec = getWaermeerzeugermodellController();
			return wec.getListWaermeerzeugermodell();
		} catch (Exception e) {
			logger.error("Waermeerzeugermodelle konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein einzelnes Objekt "Waermeerzeugermodell" zur�ck, wenn eine "id" �bergeben wird.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param Waermeerzeugermodell ID
	 * @return Waermeerzeugermodell objekt
	 */	
	public Waermeerzeugermodell getWaermeerzeugermodell(Integer id) {
		try {
			IWaermeerzeugermodellController wec = getWaermeerzeugermodellController();
			return wec.getWaermeerzeugermodell(id);
		} catch (Exception e) {
			logger.error("Waermeerzeugermodell konnte nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein einzelnes Objekt "Waermeerzeugermodell" durch die �bergabe des Objekts.
	 * Bei Erfolg wird das gespeicherte Objekt zur�ckgegeben, bei Misserfolg "null".
	 * @param Waermeerzeugermodell objekt
	 * @return Waermeerzeugermodell objekt
	 */
	public Waermeerzeugermodell saveWaermeerzeugermodell(Waermeerzeugermodell waermeerzeugermodell) {
		try {
			IWaermeerzeugermodellController wec = getWaermeerzeugermodellController();
			return wec.saveWaermeerzeugermodell(waermeerzeugermodell);
		} catch (Exception e) {
			logger.error("Waermeerzeugermodelle konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Objekte "Waermeerzeugermodell" durch die �bergabe mehrerer Objekte.
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param mehrere Waermeerzeugermodell
	 * @return true oder false
	 */
	public boolean saveMultipleWaermeerzeugermodell(Waermeerzeugermodell... waermeerzeugermodelle) {
		try {
			IWaermeerzeugermodellController wec = getWaermeerzeugermodellController();
			return wec.saveMultipleWaermeerzeugermodell(waermeerzeugermodelle);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Speichert mehrere Objekte "Waermeerzeugermodell" durch die �bergabe von einer Collection
	 * mit dem entsprechenden Objekt. Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Waermeerzeugermodellliste
	 * @return true oder false
	 */
	public boolean saveListWaermeerzeugermodell(Collection<Waermeerzeugermodell> waermeerzeugermodellListe) {
		try {
			IWaermeerzeugermodellController wec = getWaermeerzeugermodellController();
			return wec.saveListWaermeerzeugermodell(waermeerzeugermodellListe);
		} catch (Exception e) {
			logger.error("Waermeerzeugermodelle konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}

	/**
	 * L�scht einen "Waermeerzeugermodell" durch die �bergabe einer "id".
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Waermeerzeugermodell ID
	 * @return true oder false
	 */
	public boolean removeWaermeerzeugermodell(Integer id) {
		try {
			IWaermeerzeugermodellController wec = getWaermeerzeugermodellController();
			return wec.removeWaermeerzeugermodell(id);
		} catch (Exception e) {
			logger.error("Waermeerzeugermodell konnte nicht entfernt werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
}
