package com.feukora.server.webservice;

import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Waermeerzeuger;
import com.feukora.server.rmi.IWaermeerzeugerController;
import com.feukora.server.webservice.util.RmiManager;
/**
 * 
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 * 
 * Diese Klasse stellt die Webservices f�r "Waermeerzeuger" zur Verf�gung. 
 * Die Daten werden per RMI von der Businesslogik bezogen oder an die Businesslogik weitergeleitet. 
 */
@WebService
public class WaermeerzeugerWS implements IWaermeerzeugerController {
	
	private static final Logger logger = LogManager.getLogger();
	
	private IWaermeerzeugerController getWaermeerzeugerController() {
		IWaermeerzeugerController tc = (IWaermeerzeugerController) new RmiManager().getWaermeerzeugerRMI();
		return tc;
	}
	
	/**
	 * Gibt ein Set von "Waermeerzeuger" zur�ck. Bei Misserfolg wird "null" zur�ckgegeben.
	 * @return Set Waermeerzeuger
	 */
	public Set<Waermeerzeuger> getListWaermeerzeuger() {
		try {
			IWaermeerzeugerController wec = getWaermeerzeugerController();
			return wec.getListWaermeerzeuger();
		} catch (Exception e) {
			logger.error("Waermeerzeuger konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein einzelnes Objekt "Waermererzeuger" zur�ck, wenn eine "id" �bergeben wird.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param Waermeerzeuger ID
	 * @return Waermeerzeuger objekt
	 */
	public Waermeerzeuger getWaermeerzeuger(Integer id) {
		try {
			IWaermeerzeugerController wec = getWaermeerzeugerController();
			return wec.getWaermeerzeuger(id);
		} catch (Exception e) {
			logger.error("Waermeerzeuger konnte nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein einzelnes Objekt "Waermeerzeuger" durch die �bergabe des Objekts.
	 * Bei Erfolg wird das gespeicherte Objekt zur�ckgegeben, bei Misserfolg "null".
	 * @param Waermeerzeuger objekt
	 * @return Waermeerzeuger objekt
	 */
	public Waermeerzeuger saveWaermeerzeuger(Waermeerzeuger waermeerzeuger) {
		try {
			IWaermeerzeugerController wec = getWaermeerzeugerController();
			return wec.saveWaermeerzeuger(waermeerzeuger);
		} catch (Exception e) {
			logger.error("Waermeerzeuger konnte nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Objekte "Waermeerzeuger" durch die �bergabe mehrerer Objekte.
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param mehrere Waermeerzeuger
	 * @return true oder false
	 */
	public boolean saveMultipleWaermeerzeuger(Waermeerzeuger... waermeerzeugerList) {
		try {
			IWaermeerzeugerController wec = getWaermeerzeugerController();
			return wec.saveMultipleWaermeerzeuger(waermeerzeugerList);
		} catch (Exception e) {
			logger.error("Waermeerzeuger konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert mehrere Objekte "Waermeerzeuger" durch die �bergabe von einer Collection
	 * mit dem entsprechenden Objekt. Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Waermeerzeugerliste
	 * @return true oder false
	 */
	public boolean saveListWaermeerzeuger(Collection<Waermeerzeuger> waermeerzeugerListe) {
		try {
			IWaermeerzeugerController wec = getWaermeerzeugerController();
			return wec.saveListWaermeerzeuger(waermeerzeugerListe);
		} catch (Exception e) {
			logger.error("Waermeerzeuger konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht einen "Waermerzeuger" durch die �bergabe einer "id".
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Waermerzeuger ID
	 * @return true oder false
	 */
	public boolean removeWaermeerzeuger(Integer id) {
		try {
			IWaermeerzeugerController wec = getWaermeerzeugerController();
			return wec.removeWaermeerzeuger(id);
		} catch (Exception e) {
			logger.error("Waermeerzeuger konnte nicht entfernt werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
}
