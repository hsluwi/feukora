package com.feukora.server.webservice;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.jws.WebService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Mitarbeiter;
import com.feukora.server.rmi.IMitarbeiterController;
import com.feukora.server.webservice.util.AES;
import com.feukora.server.webservice.util.RmiManager;

/**
 * 
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 * 
 * Diese Klasse stellt die Webservices f�r "Mitarbeiter" zur Verf�gung. 
 * Die Daten werden per RMI von der Businesslogik bezogen oder an die Businesslogik weitergeleitet. 
 */
@WebService
public class MitarbeiterWS implements IMitarbeiterController {

	private static final Logger logger = LogManager.getLogger();
	
	private IMitarbeiterController getMitarbeiterController() {
		IMitarbeiterController mc = (IMitarbeiterController) new RmiManager().getMitarbeiterRMI();
		
		return mc;
	}
	
	private String encrypt(String str) {
		final String encryptionKey = "MZygpewJsCpRrfOr";
		AES aes = new AES(encryptionKey);
		try {
			return aes.encrypt(str);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}
	
	private String decrypt(String str) {
		final String encryptionKey = "MZygpewJsCpRrfOr";
		AES aes = new AES(encryptionKey);
		try {
			return aes.decrypt(str);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein Set von "Mitarbeiter" zur�ck. Bei Misserfolg wird "null" zur�ckgegeben.
	 * @return Mitarbeiterliste
	 */
	public Set<Mitarbeiter> getListMitarbeiter() {
		try {
			IMitarbeiterController mc = getMitarbeiterController();
			Collection<Mitarbeiter> mList =  mc.getListMitarbeiter();
			for (Mitarbeiter mitarbeiter : mList) {
				mitarbeiter.setPasswort(decrypt(mitarbeiter.getPasswort()));
			}
			return new HashSet<Mitarbeiter>(mList);
		} catch (Exception e) {
			logger.error("Mitarbeiter konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein einzelnes Objekt "Mitarbeiter" zur�ck, wenn eine "id" �bergeben wird.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param Mitarbeiter ID
	 * @return Mitarbeiter objekt
	 */
	public Mitarbeiter getMitarbeiter(Integer id) {
		try {
			IMitarbeiterController mc = getMitarbeiterController();
			Mitarbeiter ma = mc.getMitarbeiter(id);
			ma.setPasswort(decrypt(ma.getPasswort()));
			return ma;
		} catch (Exception e) {
			logger.error("Mitarbeiter konnte nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein einzelnes Objekt "Mitarbeiter" durch die �bergabe des Objekts.
	 * Bei Erfolg wird das gespeicherte Objekt zur�ckgegeben, bei Misserfolg "null".
	 * @param Mitarbeiter objekt
	 * @return Mitarbeiter objekt
	 */
	public Mitarbeiter saveMitarbeiter(Mitarbeiter mitarbeiter) {
		try {
			//Passwort encryption
			mitarbeiter.setPasswort(encrypt(mitarbeiter.getPasswort()));
			IMitarbeiterController mc = getMitarbeiterController();
			return mc.saveMitarbeiter(mitarbeiter);
		} catch (Exception e) {
			logger.error("Mitarbeiter konnte nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Objekte "Mitarbeiter" durch die �bergabe mehrerer Objekte.
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param mehrere Mitarbeiter
	 * @return true oder false
	 */
	public boolean saveMultipleMitarbeiter(Mitarbeiter... mitarbeiterList) {
		try {
			IMitarbeiterController mc = getMitarbeiterController();
			return mc.saveMultipleMitarbeiter(mitarbeiterList);
		} catch (Exception e) {
			logger.error("Mitarbeiter konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert mehrere Objekte "Mitarbeiter" durch die �bergabe von einer Collection
	 * mit dem entsprechenden Objekt. Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Mitarbeiterliste
	 * @return true oder false
	 */
	public boolean saveListMitarbeiter(Collection<Mitarbeiter> mitarbeiterListe) {
		try {
			IMitarbeiterController mc = getMitarbeiterController();
			return mc.saveListMitarbeiter(mitarbeiterListe);
		} catch (Exception e) {
			logger.error("Mitarbeiter konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht einen "Mitarbeiter" durch die �bergabe einer "id".
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Mitarbeiter ID
	 * @return true oder false
	 */
	public boolean removeMitarbeiter(Integer id) {
		try {
			IMitarbeiterController mc = getMitarbeiterController();
			return mc.removeMitarbeiter(id);
		} catch (Exception e) {
			logger.error("Mitarbeiter konnte nicht entfernt werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Pr�ft durch die �bergabe eines "username" ob dieser verf�gbar ist. Falls er verf�gbar ist, wird "true" zur�ckgegeben.
	 * Im Fehlerfall oder wenn der Name nicht mehr verf�gbar ist, wird "false" zur�ckgegeben.
	 * @param Username
	 * @return true oder false
	 */
	public boolean usernameAvailable(String username){
		try {
			IMitarbeiterController mc = getMitarbeiterController();
			return mc.usernameAvailable(username);
		} catch (Exception e) {
			logger.error("�berpr�fung nach Verf�gbarkeit des Username ist fehlgeschlagen:");
			logger.error(e.getMessage());
		}
		return false;
	}

	/**
	 * Gibt ein einzelnes Objekt "Mitarbeiter" zur�ck, wenn eine "username" und "passwort"-Kombination
	 * �bergeben wird, welche existiert. Andernfalls oder im Fehlerfall wird null zur�ckgegeben.
	 * @param Username und Passwort
	 * @return Mitarbeiter objekt
	 */
	@Override
	public Mitarbeiter getMitarbeiterLogin(String username, String passwort) throws RemoteException {
		try {
			IMitarbeiterController mc = getMitarbeiterController();
			
			//Passwort encryption
			return mc.getMitarbeiterLogin(username, encrypt(passwort));
		} catch (Exception e) {
			logger.error("Mitarbeiterlogin konnte nicht gepr�ft werden:");
			logger.error(e.getMessage());
		}
		return null;
	}

}
