package com.feukora.server.webservice;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Messergebnis;
import com.feukora.server.rmi.IMessergebnisController;
import com.feukora.server.webservice.util.RmiManager;
/**
 * 
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 * 
 * Diese Klasse stellt die Webservices f�r "Messergebnis" zur Verf�gung. 
 * Die Daten werden per RMI von der Businesslogik bezogen oder an die Businesslogik weitergeleitet. 
 */
@WebService
public class MessergebnisWS implements IMessergebnisController {

	private static final Logger logger = LogManager.getLogger();
	
	private IMessergebnisController getMessergebnisController() {
		IMessergebnisController mc = (IMessergebnisController) new RmiManager().getMessergebnisRMI();
		return mc;
	}

	/**
	 * Gibt ein Set von "Messergebnis" zur�ck. Bei Misserfolg wird "null" zur�ckgegeben.
	 * @return Messergebnisliste
	 */
	public Set<Messergebnis> getListMessergebnis() {
		try {
			IMessergebnisController mc = getMessergebnisController();
			return mc.getListMessergebnis();
		} catch (Exception e) {
			logger.error("Messergebnisse konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * Gibt ein einzelnes Objekt "Messergebnis" zur�ck, wenn eine "id" �bergeben wird.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param Messergebnis ID
	 * @return Messergebnis objekt
	 */
	public Messergebnis getMessergebnis(Integer id) {
		try {
			IMessergebnisController mc = getMessergebnisController();
			return mc.getMessergebnis(id);
		} catch (Exception e) {
			logger.error("Messergebnis konnte nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	

	/**
	 * Speichert ein einzelnes Objekt "Messergebnis" durch die �bergabe des Objekts.
	 * Bei Erfolg wird das gespeicherte Objekt zur�ckgegeben, bei Misserfolg "null".
	 * @param Messergebnis objekt
	 * @return Messergebnis objekt
	 */
	public Messergebnis saveMessergebnis(Messergebnis messergebnis) {
		try {
			IMessergebnisController mc = getMessergebnisController();
			return mc.saveMessergebnis(messergebnis);
		} catch (Exception e) {
			logger.error("Messergebnis konnte nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Objekte "Messergebnis" durch die �bergabe mehrerer Objekte.
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param mehrere Messergebnisse
	 * @return true oder false
	 */
	public boolean saveMultipleMessergebnis(Messergebnis... messergebnisList) {
		try {
			IMessergebnisController mc = getMessergebnisController();
			return mc.saveMultipleMessergebnis(messergebnisList);
		} catch (Exception e) {
			logger.error("Messergebnisse konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert mehrere Objekte "Messergebnis" durch die �bergabe von einer Collection
	 * mit dem entsprechenden Objekt. Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Messergebnisliste
	 * @return true oder false
	 */
	public boolean saveListMessergebnis(Collection<Messergebnis> messergebnisListe) {
		try {
			IMessergebnisController mc = getMessergebnisController();
			return mc.saveListMessergebnis(messergebnisListe);
		} catch (Exception e) {
			logger.error("Messergebnisse konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht ein "Messergebnis" durch die �bergabe einer "id".
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Messergebnis ID
	 * @return true oder false
	 */	
	public boolean removeMessergebnis(Integer id) {
		try {
			IMessergebnisController mc = getMessergebnisController();
			return mc.removeMessergebnis(id);
		} catch (Exception e) {
			logger.error("Messergebnis konnte nicht entfernt werden:");
			logger.error(e.getMessage());
		}
		return false;
	}

	/**
	 * Gibt alle Messergebnisse mit einer �bergebenen Kontrolle-ID zur�ck
	 * @param Integer kontrolleId
	 * @return Set Messergebnis
	 * 
	 */
	@Override
	public Set<Messergebnis> getMessergebnisByKontrolleId(Integer kontrolleId) throws RemoteException {
		try {
			IMessergebnisController mc = getMessergebnisController();
			return mc.getMessergebnisByKontrolleId(kontrolleId);
		} catch (Exception e) {
			logger.error("Messergebnisse konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
}
