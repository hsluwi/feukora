package com.feukora.server.webservice;

import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Kunde;
import com.feukora.server.rmi.IKundeController;
import com.feukora.server.webservice.util.RmiManager;
/**
 * 
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 * 
 * Diese Klasse stellt die Webservices f�r "Kunde" zur Verf�gung. 
 * Die Daten werden per RMI von der Businesslogik bezogen oder an die Businesslogik weitergeleitet. 
 */
@WebService
public class KundeWS implements IKundeController{

	private static final Logger logger = LogManager.getLogger();
	
	private IKundeController getKontrolleController() {
		IKundeController kc = (IKundeController) new RmiManager().getKundeRMI();
		return kc;
	}	
	
	/**
	 * Gibt ein Set von "Kunde" zur�ck. Bei Misserfolg wird "null" zur�ckgegeben.
	 * @return Set Kunde
	 */
	public Set<Kunde> getListKunde() {
		try {
			IKundeController kc = getKontrolleController();
			return kc.getListKunde();
		} catch (Exception e) {
			logger.error("Kunden konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein einzelnes Objekt "Kunde" zur�ck, wenn eine "id" �bergeben wird.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param Kunden ID
	 * @return Kunden objekt
	 */
	public Kunde getKunde(Integer id) {
		try {
			IKundeController kc = getKontrolleController();
			return kc.getKunde(id);
		} catch (Exception e) {
			logger.error("Kunde konnte nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein einzelnes Objekt "Kunde" durch die �bergabe des Objekts.
	 * Bei Erfolg wird das gespeicherte Objekt zur�ckgegeben, bei Misserfolg "null".
	 * @param Kunde objekt
	 * @return Kunde objekt
	 */
	public Kunde saveKunde(Kunde kunde) {
		try {
			IKundeController kc = getKontrolleController();
			return kc.saveKunde(kunde);
		} catch (Exception e) {
			logger.error("Kunde konnte nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Objekte "Kunde" durch die �bergabe mehrerer Objekte.
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param mehrere Kunden
	 * @return true oder false
	 */
	public boolean saveMultipleKunde(Kunde... kundeList) {
		try {
			IKundeController kc = getKontrolleController();
			return kc.saveMultipleKunde(kundeList);
		} catch (Exception e) {
			logger.error("Kunden konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}

	/**
	 * Speichert mehrere Objekte "Kunde" durch die �bergabe von einer Collection
	 * mit dem entsprechenden Objekt. Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Kundenliste
	 * @return true oder false
	 */
	public boolean saveListKunde(Collection<Kunde> kundeListe) {
		try {
			IKundeController kc = getKontrolleController();
			return kc.saveListKunde(kundeListe);
		} catch (Exception e) {
			logger.error("Kunden konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht ein "Kunde" durch die �bergabe einer "id".
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Kunde ID
	 * @return true oder false
	 */
	public boolean removeKunde(Integer id) {
		try {
			IKundeController kc = getKontrolleController();
			return kc.removeKunde(id);
		} catch (Exception e) {
			logger.error("Kunde konnte nicht entfernt werden:");
			logger.error(e.getMessage());
		}
		return false;
	}

}
