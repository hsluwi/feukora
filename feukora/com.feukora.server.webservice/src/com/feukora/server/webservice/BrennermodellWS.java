package com.feukora.server.webservice;

import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Brennermodell;
import com.feukora.server.rmi.IBrennermodellController;
import com.feukora.server.webservice.util.RmiManager;
/**
 * 
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 * 
 * Diese Klasse stellt die Webservices f�r "Brennermodell" zur Verf�gung. 
 * Die Daten werden per RMI von der Businesslogik bezogen oder an die Businesslogik weitergeleitet. 
 */
@WebService
public class BrennermodellWS implements IBrennermodellController {
	
	private static final Logger logger = LogManager.getLogger();
		
	private IBrennermodellController getBrennermodellController () {
		IBrennermodellController bmc = (IBrennermodellController) new RmiManager().getBrennermodellRMI();
		return bmc;
	}
	
	/**
	 * Gibt ein Set von "Brennermodell" zur�ck. Bei Misserfolg wird "null" zur�ckgegeben.
	 * @return Set Brennermodell
	 */
	public Set<Brennermodell> getListBrennermodell() {
		try {
			IBrennermodellController bmc = getBrennermodellController();
			return bmc.getListBrennermodell();
		} catch (Exception e) {
			logger.error("Brennermodelle konnten nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein einzelnes Objekt "Brennermodell" zur�ck, wenn eine "id" �bergeben wird.
	 * Bei Misserfolg wird "null" zur�ckgegeben.
	 * @param Brennermodell ID
	 * @return Brennermodell objekt
	 */
	public Brennermodell getBrennermodell(Integer id) {
		try {
			IBrennermodellController bmc = getBrennermodellController();
			return bmc.getBrennermodell(id);
		} catch (Exception e) {
			logger.error("Brennermodell konnte nicht geladen werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein einzelnes Objekt "Brennermodell" durch die �bergabe des Objekts.
	 * Bei Erfolg wird das gespeicherte Objekt zur�ckgegeben, bei Misserfolg "null".
	 * @param Brennermodell objekt
	 * @return Brennermodell objekt
	 */
	public Brennermodell saveBrennermodell(Brennermodell brennermodell) {
		try {
			IBrennermodellController bmc = getBrennermodellController();
			return bmc.saveBrennermodell(brennermodell);
		} catch (Exception e) {
			logger.error("Brennermodell konnte nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Objekte "Brennermodell" durch die �bergabe mehrerer Objekte.
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param mehrere Brennermodelle
	 * @return true oder false
	 */
	public boolean saveMultipleBrennermodell(Brennermodell... brennermodelle) {
		try {
			IBrennermodellController bmc = getBrennermodellController();
			return bmc.saveMultipleBrennermodell(brennermodelle);
		} catch (Exception e) {
			logger.error("Brennermodelle konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert mehrere Objekte "Brennermodell" durch die �bergabe von einer Collection
	 * mit dem entsprechenden Objekt. Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Collection Brennermodell
	 * @return true oder false
	 */
	public boolean saveListBrennermodell(Collection<Brennermodell> brennermodellListe) {
		try {
			IBrennermodellController bmc = getBrennermodellController();
			return bmc.saveListBrennermodell(brennermodellListe);
		} catch (Exception e) {
			logger.error("Brennermodelle konnten nicht gespeichert werden:");
			logger.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht ein "Brennermodell" durch die �bergabe einer "id".
	 * Bei Erfolg wird "true", bei Misserfolg "false" zur�ckgegeben.
	 * @param Brennermodell ID
	 * @return true oder false
	 */
	public boolean removeBrennermodell(Integer id) {
		try {
			IBrennermodellController bmc = getBrennermodellController();
			return bmc.removeBrennermodell(id);
		} catch (Exception e) {
			logger.error("Brennermodell konnte nicht entfernt werden:");
			logger.error(e.getMessage());
		}
		return false;
	}

}
