/**
 * 
 */
package com.feukora.server.models;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.feukora.server.models.interfaces.IPlzOrt;

/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 31.10.2015
 *
 *Klasse um PlzOrt-Objekte zu erzeugen. Die Klasse erbt von der Klasse FeukoraObj
 *und implementiert Serializable und IPlzOrt.
 *
 */
@Entity
public class PlzOrt extends FeukoraObj implements Serializable, IPlzOrt {

	private static final long serialVersionUID = -523031685344314801L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull(message = "Der Ort darf nicht leer sein.")
	@Size(min=1, max=50, message="Ort darf maximal {max} Zeichen lang sein.")
	private String ort;
	
	@Min(1000)
	@Max(9999)
	private Integer plz;

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPlzOrt#getOrt()
	 */
	@Override
	public String getOrt() {
		return ort;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPlzOrt#setOrt(java.lang.String)
	 */
	@Override
	public void setOrt(String ort) {
		this.ort = ort;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPlzOrt#getPlz()
	 */
	@Override
	public Integer getPlz() {
		return plz;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPlzOrt#setPlz(java.lang.Integer)
	 */
	@Override
	public void setPlz(Integer plz) {
		this.plz = plz;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPlzOrt#getId()
	 */
	@Override
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	
}
