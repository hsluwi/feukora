package com.feukora.server.models;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.GregorianCalendar;
import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.eclipse.persistence.annotations.CascadeOnDelete;

import com.feukora.server.models.interfaces.ITermin;
@Entity
public class Termin extends FeukoraObj implements Serializable, ITermin {
	
	/**
	 * @author Philipp Anderhub
	 * @version 1.0
	 * @since 1.0
	 * Model f�r Termin. Erzeugt alle Datenbankelemente und Beziehungen.
	 */
	
	private static final long serialVersionUID = -8707581245384489906L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	
	@ManyToOne
	@CascadeOnDelete
	@Valid
	@NotNull(message="Standort darf nicht leer sein.")
	private Anlagenstandort anlagenstandort;
	
    @Temporal(TemporalType.TIMESTAMP)
    @Valid
    @NotNull(message="Datum darf nicht leer sein.")
    private GregorianCalendar datumZeit;
    
    private Integer einsatz;
    
    @ManyToOne
    @CascadeOnDelete
    @Valid
    @NotNull(message="Mitarbeiter darf nicht leer sein")
    private Mitarbeiter mitarbeiter;

	/* (non-Javadoc)
	 * @see com.feukora.server.models.ITermin#getId()
	 */
	@Override
	public Integer getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.ITermin#setId(java.lang.Integer)
	 */
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.ITermin#getAnlagenstandort()
	 */
	@Override
	public Anlagenstandort getAnlagenstandort() {
		return anlagenstandort;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.ITermin#setAnlagenstandort(com.feukora.server.models.Anlagenstandort)
	 */
	@Override
	public void setAnlagenstandort(Anlagenstandort anlagenstandort) {
		this.anlagenstandort = anlagenstandort;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.ITermin#getDatumZeit()
	 */
	@Override
	public GregorianCalendar getDatumZeit() {
		return datumZeit;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.ITermin#setDatumZeit(java.util.GregorianCalendar)
	 */
	@Override
	public void setDatumZeit(GregorianCalendar datumZeit) {
		this.datumZeit = datumZeit;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.ITermin#getEinsatz()
	 */
	@Override
	public Integer getEinsatz() {
		return einsatz;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.ITermin#setEinsatz(java.lang.Integer)
	 */
	@Override
	public void setEinsatz(Integer einsatz) {
		this.einsatz = einsatz;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.ITermin#getMitarbeiter()
	 */
	@Override
	public Mitarbeiter getMitarbeiter() {
		return mitarbeiter;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.ITermin#setMitarbeiter(com.feukora.server.models.Mitarbeiter)
	 */
	@Override
	public void setMitarbeiter(Mitarbeiter mitarbeiter) {
		this.mitarbeiter = mitarbeiter;
	}
    
    
	
	
    
}
