package com.feukora.server.models;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;

import com.feukora.server.models.interfaces.IFeukoraObj;

@Entity
@MappedSuperclass
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class FeukoraObj implements Serializable, IFeukoraObj {

	public FeukoraObj() {
		
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IFeukoraObj#setId(java.lang.Integer)
	 */
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IFeukoraObj#getId()
	 */
	@Override
	public Integer getId() {
        return id;
    }


	

}
