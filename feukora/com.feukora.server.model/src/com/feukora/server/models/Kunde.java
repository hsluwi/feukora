/**
 * 
 */
package com.feukora.server.models;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.feukora.server.models.interfaces.IKunde;

/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 31.10.2015
 *
 *Klasse um Kunde-Objekte zu erzeugen. Die Klasse erbt von der Klasse FeukoraObj
 *und implementiert Serializable und IKunde.
 *
 */
@Entity
public class Kunde extends Person implements Serializable, IKunde {
	
	private static final long serialVersionUID = 2815628430437339374L;

	@Size(max=50, message="Firma darf maximal {max} Zeichen lang sein.")
	private String firma;

	@NotNull(message="Kundenart darf nicht leer sein.")
	@Valid
	private String kundenArt;
	
	private Person person;

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKunde#getFirma()
	 */
	@Override
	public String getFirma() {
		return firma;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKunde#setFirma(java.lang.String)
	 */
	@Override
	public void setFirma(String firma) {
		this.firma = firma;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKunde#getKundenArt()
	 */
	@Override
	public String getKundenArt() {
		return kundenArt;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKunde#setKundenArt(java.lang.String)
	 */
	@Override
	public void setKundenArt(String kundenArt) {
		this.kundenArt = kundenArt;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKunde#getPerson()
	 */
	@Override
	public Person getPerson() {
		return person;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKunde#setPerson(com.feukora.server.models.Person)
	 */
	@Override
	public void setPerson(Person person) {
		this.person = person;
	}
	
	
}
