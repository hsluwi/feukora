package com.feukora.server.models.interfaces;

import java.rmi.Remote;

public interface IFeukoraObj extends Remote {

	void setId(Integer id);

	Integer getId();

}