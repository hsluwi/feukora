package com.feukora.server.models.interfaces;

import java.rmi.Remote;

import com.feukora.server.models.Brennerart;

/**
 * 
 * @author Andrew
 *
 */
public interface IBrennermodell extends Remote {

	Integer getId();

	String getBezeichnung();

	void setBezeichnung(String bezeichnung);

	Integer getFeuerungswaermeleistung();

	void setFeuerungswaermeleistung(Integer feuerungswaermeleistung);

	String getHersteller();

	void setHersteller(String hersteller);

	Brennerart getBrennerart();

	void setBrennerart(Brennerart brennerart);

}