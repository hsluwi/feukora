package com.feukora.server.models.interfaces;

import java.rmi.Remote;

import com.feukora.server.models.MitarbeiterRolle;
import com.feukora.server.models.Person;

public interface IMitarbeiter extends Remote {

	String getUserName();

	void setUserName(String userName);

	String getPasswort();

	void setPasswort(String passwort);

	Person getPerson();

	void setPerson(Person person);

	MitarbeiterRolle getRolle();

	void setRolle(MitarbeiterRolle rolle);

}