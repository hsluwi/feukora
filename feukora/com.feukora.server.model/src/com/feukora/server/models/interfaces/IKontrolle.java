package com.feukora.server.models.interfaces;

import java.rmi.Remote;

import com.feukora.server.models.Termin;

public interface IKontrolle extends Remote {

	Termin getTermin();

	void setTermin(Termin termin);

	void setId(Integer id);

	Integer getId();

	String getBemerkungen();

	void setBemerkungen(String bemerkungen);

	Boolean getEinregulierungMoeglich();

	void setEinregulierungMoeglich(Boolean einregulierungMoeglich);

	Boolean getFailAbgasverlust();

	void setFailAbgasverlust(Boolean failAbgasverlust);

	Boolean getFailCo();

	void setFailCo(Boolean failCo);

	Boolean getFailNo();

	void setFailNo(Boolean failNo);

	Boolean getFailOel();

	void setFailOel(Boolean failOel);

	Boolean getFailRusszahl();

	void setFailRusszahl(Boolean failRusszahl);

	Boolean getKenntnisnahmeKunde();

	void setKenntnisnahmeKunde(Boolean kenntnisnahmeKunde);

	String getKontrollart();

	void setKontrollart(String kontrollart);

	/**
	 * @return the bestanden
	 */
	Boolean getBestanden();

	/**
	 * @param bestanden the bestanden to set
	 */
	void setBestanden(Boolean bestanden);

}