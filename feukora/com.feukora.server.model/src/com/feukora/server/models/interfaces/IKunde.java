package com.feukora.server.models.interfaces;

import java.rmi.Remote;

import com.feukora.server.models.Person;

public interface IKunde extends Remote {

	String getFirma();

	void setFirma(String firma);

	String getKundenArt();

	void setKundenArt(String kundenArt);

	Person getPerson();

	void setPerson(Person person);

}