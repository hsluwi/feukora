package com.feukora.server.models.interfaces;

import java.rmi.Remote;

import com.feukora.server.models.PlzOrt;

public interface IPerson extends Remote {

	String getNachname();

	void setNachname(String nachname);

	String getVorname();

	void setVorname(String vorname);

	String getStrasse();

	void setStrasse(String strasse);

	PlzOrt getPlzOrt();

	void setPlzOrt(PlzOrt plzOrt);

	String getTelefon();

	void setTelefon(String telefon);

	String getMobile();

	void setMobile(String mobile);

	String getEmail();

	void setEmail(String email);

}