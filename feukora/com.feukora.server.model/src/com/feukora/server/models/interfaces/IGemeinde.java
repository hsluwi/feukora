package com.feukora.server.models.interfaces;

import java.rmi.Remote;

public interface IGemeinde extends Remote {

	String getBezeichnung();

	void setBezeichnung(String bezeichnung);

	String getKanton();

	void setKanton(String kanton);

	Integer getId();

}