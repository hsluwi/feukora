package com.feukora.server.models.interfaces;

import com.feukora.server.models.Brenner;
import com.feukora.server.models.Gemeinde;
import com.feukora.server.models.Kunde;
import com.feukora.server.models.PlzOrt;
import com.feukora.server.models.Waermeerzeuger;

public interface IAnlagenstandort {

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getId()
	 */
	Integer getId();

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getMyId()
	 */
	Integer getMyId();

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getGemeinde()
	 */
	Gemeinde getGemeinde();

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#setGemeinde(com.feukora.server.models.Gemeinde)
	 */
	void setGemeinde(Gemeinde gemeinde);

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getKunde()
	 */
	Kunde getKunde();

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#setKunde(com.feukora.server.models.Kunde)
	 */
	void setKunde(Kunde kunde);

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getHauswart()
	 */
	Kunde getHauswart();

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#setHauswart(com.feukora.server.models.Kunde)
	 */
	void setHauswart(Kunde hauswart);

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getPlzOrt()
	 */
	PlzOrt getPlzOrt();

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#setPlzOrt(com.feukora.server.models.PlzOrt)
	 */
	void setPlzOrt(PlzOrt plzOrt);

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getStrasse()
	 */
	String getStrasse();

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#setStrasse(java.lang.String)
	 */
	void setStrasse(String strasse);

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getBrenner()
	 */
	Brenner getBrenner();

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#setBrenner(com.feukora.server.models.Brenner)
	 */
	void setBrenner(Brenner brenner);

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getWaermeerzeuger()
	 */
	Waermeerzeuger getWaermeerzeuger();

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#setWaermeerzeuger(com.feukora.server.models.Waermeerzeuger)
	 */
	void setWaermeerzeuger(Waermeerzeuger waermeerzeuger);

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#setMyId(java.lang.Integer)
	 */
	void setMyId(Integer id);

}