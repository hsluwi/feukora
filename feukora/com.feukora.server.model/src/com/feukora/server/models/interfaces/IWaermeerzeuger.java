package com.feukora.server.models.interfaces;

import java.rmi.Remote;

import com.feukora.server.models.Waermeerzeugermodell;

public interface IWaermeerzeuger extends Remote {

	Integer getId();

	Waermeerzeugermodell getWaermeerzeugermodell();

	void setWaermeerzeugermodell(Waermeerzeugermodell waermeerzeugermodell);

	Integer getBaujahr();

	void setBaujahr(Integer baujahr);

	String getZulassungsNr();

	void setZulassungsNr(String zulassungsNr);

}