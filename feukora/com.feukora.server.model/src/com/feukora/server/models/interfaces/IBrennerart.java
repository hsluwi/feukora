package com.feukora.server.models.interfaces;

import java.rmi.Remote;

/**
 * 
 * @author Andrew
 *
 */
public interface IBrennerart extends Remote {

	Integer getId();

	String getBezeichnung();

	void setBezeichnung(String bezeichnung);

}