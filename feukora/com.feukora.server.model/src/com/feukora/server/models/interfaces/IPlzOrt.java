package com.feukora.server.models.interfaces;

import java.rmi.Remote;

public interface IPlzOrt extends Remote {

	String getOrt();

	void setOrt(String ort);

	Integer getPlz();

	void setPlz(Integer plz);

	Integer getId();

}