package com.feukora.server.models.interfaces;

import java.rmi.Remote;
import java.util.GregorianCalendar;

import com.feukora.server.models.Anlagenstandort;
import com.feukora.server.models.Mitarbeiter;

public interface ITermin extends Remote {

	Integer getId();

	void setId(Integer id);

	Anlagenstandort getAnlagenstandort();

	void setAnlagenstandort(Anlagenstandort anlagenstandort);

	GregorianCalendar getDatumZeit();

	void setDatumZeit(GregorianCalendar datumZeit);

	Integer getEinsatz();

	void setEinsatz(Integer einsatz);

	Mitarbeiter getMitarbeiter();

	void setMitarbeiter(Mitarbeiter mitarbeiter);

}