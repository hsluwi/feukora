package com.feukora.server.models.interfaces;

import java.rmi.Remote;

public interface IMitarbeiterRolle extends Remote {

	String getBezeichnung();

	void setBezeichnung(String bezeichnung);

	Integer getId();

}