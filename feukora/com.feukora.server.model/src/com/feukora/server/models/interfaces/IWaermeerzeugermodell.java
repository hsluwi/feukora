package com.feukora.server.models.interfaces;

import java.rmi.Remote;

import com.feukora.server.models.Brennstoff;

public interface IWaermeerzeugermodell extends Remote {

	Integer getId();

	String getBezeichnung();

	void setBezeichnung(String bezeichnung);

	String getHersteller();

	void setHersteller(String hersteller);

	Brennstoff getBrennstoff();

	void setBrennstoff(Brennstoff brennstoff);

}