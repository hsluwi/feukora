package com.feukora.server.models.interfaces;

import java.rmi.Remote;

import com.feukora.server.models.Brennermodell;

/**
 * 
 * @author Andrew
 *
 */
public interface IBrenner extends Remote {

	Integer getId();

	Brennermodell getBrennermodell();

	void setBrennermodell(Brennermodell brennermodell);

	Integer getBaujahr();

	void setBaujahr(Integer baujahr);

	String getZulassungsNr();

	void setZulassungsNr(String zulassungsNr);

}