package com.feukora.server.models.interfaces;

import java.rmi.Remote;

import com.feukora.server.models.Kontrolle;

public interface IMessergebnis extends Remote{

	Integer getId();

	void setId(Integer id);

	Integer getStufe();

	void setStufe(Integer stufe);

	Integer getMessungNr();

	void setMessungNr(Integer messungNr);

	Double getAbgastemperatur();

	void setAbgastemperatur(Double abgastemperatur);

	Double getAbgasverluste();

	void setAbgasverluste(Double abgasverluste);

	Double getCo3PVol();

	void setCo3PVol(Double co3pVol);

	Double getNox3PVol();

	void setNox3PVol(Double nox3pVol);

	Double getO2Gehalt();

	void setO2Gehalt(Double o2Gehalt);

	Boolean getOelanteile();

	void setOelanteile(Boolean oelanteile);

	Double getRusszahl();

	void setRusszahl(Double russzahl);

	Double getVerbrennLuftTemp();

	void setVerbrennLuftTemp(Double verbrennLuftTemp);

	Double getWaermeErzTemp();

	void setWaermeErzTemp(Double waermeErzTemp);

	Kontrolle getKontrolle();

	void setKontrolle(Kontrolle kontrolle);

}