/**
 * 
 */
package com.feukora.server.models;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.feukora.server.models.interfaces.IPerson;

/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 31.10.2015
 *
 *Klasse um Person-Objekte zu erzeugen. Die Klasse erbt von der Klasse FeukoraObj
 *und implementiert Serializable und IPerson.
 *
 */
@Entity
public class Person extends FeukoraObj implements Serializable, IPerson {

	private static final long serialVersionUID = -1982472154225702158L;

	@NotNull(message = "Nachname darf nicht leer sein.")
	@NotBlank(message = "Nachname darf nicht leer sein.")
	@Size(max=50, message="Der Nachname darf maximal {max} Zeichen lang sein.")
	private String nachname;
	
	@NotNull(message = "Vorname darf nicht leer sein.")
	@NotBlank(message = "Vorname darf nicht leer sein.")
	@Size(max=50, message="Der Vorname darf maximal {max} Zeichen lang sein.")
	private String vorname;
	
	@NotNull(message = "Die Strasse darf nicht leer sein.")
	@NotBlank(message = "Die Strasse darf nicht leer sein.")
	@Size(max=60, message="Strasse darf maximal {max} Zeichen haben.")
	private String strasse;
	
	@NotNull(message="PLZ/Ort darf nicht leer sein.")
	@Valid
	private PlzOrt plzOrt;
	
	@NotNull(message = "Die Telefonnummer darf nicht leer sein.")
	@Size(max=20, message="Telefonnummer darf maximal {max} Zeichen haben.")
	@NotBlank(message = "Die Telefonnummer darf nicht leer sein.")
	private String telefon;
	
	@NotNull(message = "Die Handynummer darf nicht leer sein")
	@NotBlank(message = "Die Handynummer darf nicht leer sein")
	@Size(max=20, message="Handynummer darf maximal {max} Zeichen haben.")
	private String mobile;
	
	@Size(max=60, message="E-Mail-Adresse darf maximal {max} Zeichen haben.")
	@NotNull(message = "Die E-Mail-Adresse darf nicht leer sein.")
	@NotBlank(message = "Die E-Mail-Adresse darf nicht leer sein.")
	@Email(message="Ung�ltige E-Mail-Adresse.")
	private String email;

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPerson#getNachname()
	 */
	@Override
	public String getNachname() {
		return nachname;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPerson#setNachname(java.lang.String)
	 */
	@Override
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPerson#getVorname()
	 */
	@Override
	public String getVorname() {
		return vorname;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPerson#setVorname(java.lang.String)
	 */
	@Override
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPerson#getStrasse()
	 */
	@Override
	public String getStrasse() {
		return strasse;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPerson#setStrasse(java.lang.String)
	 */
	@Override
	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPerson#getPlzOrt()
	 */
	@Override
	public PlzOrt getPlzOrt() {
		return plzOrt;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPerson#setPlzOrt(com.feukora.server.models.PlzOrt)
	 */
	@Override
	public void setPlzOrt(PlzOrt plzOrt) {
		this.plzOrt = plzOrt;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPerson#getTelefon()
	 */
	@Override
	public String getTelefon() {
		return telefon;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPerson#setTelefon(java.lang.String)
	 */
	@Override
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPerson#getMobile()
	 */
	@Override
	public String getMobile() {
		return mobile;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPerson#setMobile(java.lang.String)
	 */
	@Override
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPerson#getEmail()
	 */
	@Override
	public String getEmail() {
		return email;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IPerson#setEmail(java.lang.String)
	 */
	@Override
	public void setEmail(String email) {
		this.email = email;
	}	
}
