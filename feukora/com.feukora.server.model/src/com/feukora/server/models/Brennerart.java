package com.feukora.server.models;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.feukora.server.models.interfaces.IBrennerart;


/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 31.10.2015
 *
 *Klasse um Brennerart-Objekte zu erzeugen. Die Klasse erbt von der Klasse FeukoraObj
 *und implementiert Serializable und IBrennerart.
 *
 */
@Entity
public class Brennerart extends FeukoraObj implements Serializable, IBrennerart {

	private static final long serialVersionUID = 5334434913046176963L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull(message = "Bezeichnung darf nicht leer sein.")
	@NotBlank(message = "Bezeichnung darf nicht leer sein.")
	@Size(max=50, message="Bezeichnung darf maximal {max} Zeichen lang sein.")
	private String bezeichnung;

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrennerart#getId()
	 */
	@Override
	public Integer getId() {
		return id;
	}


	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrennerart#getBezeichnung()
	 */
	@Override
	public String getBezeichnung() {
		return bezeichnung;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrennerart#setBezeichnung(java.lang.String)
	 */
	@Override
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}


	public void setId(Integer id) {
		this.id = id;
	}


}
