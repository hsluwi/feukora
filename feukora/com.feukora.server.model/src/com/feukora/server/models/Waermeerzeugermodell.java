package com.feukora.server.models;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.eclipse.persistence.annotations.CascadeOnDelete;

import com.feukora.server.models.interfaces.IWaermeerzeugermodell;


/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 31.10.2015
 *
 *Klasse um Waermeerzeugermodell-Objekte zu erzeugen. Die Klasse erbt von der Klasse FeukoraObj
 *und implementiert Serializable und IWaermeerzeugermodell.
 *Die Klasse besitzt eine @ManyToOne Beziehung zu der Klasse Brennstoff.
 *
 */
@Entity
public class Waermeerzeugermodell extends FeukoraObj implements Serializable, IWaermeerzeugermodell{

	private static final long serialVersionUID = -1518937977355888174L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull(message = "Bezeichnung darf nicht leer sein.")
	@Size(max=50, message="Die Bezeichnung darf maximal {max} Zeichen lang sein.")
	private String bezeichnung;
	
	@NotNull(message = "Hersteller darf nicht leer sein.")
	@Size(max=50, message="Hersteller darf maximal {max} Zeichen lang sein.")
	private String hersteller;
	
	@ManyToOne
	@CascadeOnDelete
	@Valid
	@NotNull(message="Brennstoff darf nicht leer sein.")
	private Brennstoff brennstoff;
	
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IWaermeerzeugermodell#getId()
	 */
	@Override
	public Integer getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IWaermeerzeugermodell#getBezeichnung()
	 */
	@Override
	public String getBezeichnung() {
		return bezeichnung;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IWaermeerzeugermodell#setBezeichnung(java.lang.String)
	 */
	@Override
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IWaermeerzeugermodell#getHersteller()
	 */
	@Override
	public String getHersteller() {
		return hersteller;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IWaermeerzeugermodell#setHersteller(java.lang.String)
	 */
	@Override
	public void setHersteller(String hersteller) {
		this.hersteller = hersteller;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IWaermeerzeugermodell#getBrennstoff()
	 */
	@Override
	public Brennstoff getBrennstoff() {
		return brennstoff;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IWaermeerzeugermodell#setBrennstoff(com.feukora.server.models.Brennstoff)
	 */
	@Override
	public void setBrennstoff(Brennstoff brennstoff) {
		this.brennstoff = brennstoff;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	
	

}
