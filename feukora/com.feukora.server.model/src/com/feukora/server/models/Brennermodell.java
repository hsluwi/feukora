package com.feukora.server.models;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.eclipse.persistence.annotations.CascadeOnDelete;
import org.hibernate.validator.constraints.NotBlank;

import com.feukora.server.models.interfaces.IBrennermodell;


/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 31.10.2015
 *
 *Klasse um Brennermodell-Objekte zu erzeugen. Die Klasse erbt von der Klasse FeukoraObj
 *und implementiert Serializable und IBrennermodell.
 *Die Klasse besitzt eine @ManyToOne Beziehung zu der Klasse Brennerart.
 *
 */
@Entity
public class Brennermodell extends FeukoraObj implements Serializable, IBrennermodell{

	private static final long serialVersionUID = 727995994306891758L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@NotBlank(message = "Bezeichnung darf nicht leer sein.")
	@NotNull(message = "Bezeichnung darf nicht leer sein.")
	@Size(max=50, message="Die Bezeichnung darf maximal {max} Zeichen lang sein.")
	private String bezeichnung;
	
	@Min(1)
	@Max(4000)
	private Integer feuerungswaermeleistung;
	
	@NotBlank(message = "Hersteller darf nicht leer sein.")
	@NotNull(message = "Hersteller darf nicht leer sein.")
	@Size(min=1, max=50, message="Hersteller darf maximal {max} Zeichen lang sein.")
	private String hersteller;
	
	@ManyToOne
	@CascadeOnDelete
	@NotNull
	private Brennerart brennerart;
	

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrennermodell#getId()
	 */
	@Override
	public Integer getId() {
		return id;
	}


	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrennermodell#getBezeichnung()
	 */
	@Override
	public String getBezeichnung() {
		return bezeichnung;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrennermodell#setBezeichnung(java.lang.String)
	 */
	@Override
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrennermodell#getFeuerungswaermeleistung()
	 */
	@Override
	public Integer getFeuerungswaermeleistung() {
		return feuerungswaermeleistung;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrennermodell#setFeuerungswaermeleistung(java.lang.Integer)
	 */
	@Override
	public void setFeuerungswaermeleistung(Integer feuerungswaermeleistung) {
		this.feuerungswaermeleistung = feuerungswaermeleistung;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrennermodell#getHersteller()
	 */
	@Override
	public String getHersteller() {
		return hersteller;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrennermodell#setHersteller(java.lang.String)
	 */
	@Override
	public void setHersteller(String hersteller) {
		this.hersteller = hersteller;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrennermodell#getBrennerart()
	 */
	@Override
	public Brennerart getBrennerart() {
		return brennerart;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrennermodell#setBrennerart(com.feukora.server.models.Brennerart)
	 */
	@Override
	public void setBrennerart(Brennerart brennerart) {
		this.brennerart = brennerart;
	}


	public void setId(Integer id) {
		this.id = id;
	}


}
