package com.feukora.server.models;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.feukora.server.models.interfaces.IBrennstoff;


/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 31.10.2015
 *
 *Klasse um Brennstoff-Objekte zu erzeugen. Die Klasse erbt von der Klasse FeukoraObj
 *und implementiert Serializable und IBrennstoff.
 *
 */
@Entity
public class Brennstoff extends FeukoraObj implements Serializable, IBrennstoff{

	private static final long serialVersionUID = -3011515758460230274L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@NotBlank(message = "Bezeichnung darf nicht leer sein.")
	@NotNull(message = "Bezeichnung darf nicht leer sein.")
	@Size(max=50, message="Bezeichnung darf maximal {max} Zeichen lang sein.")
	private String bezeichnung;

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrennstoff#getId()
	 */
	@Override
	public Integer getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrennstoff#getBezeichnung()
	 */
	@Override
	public String getBezeichnung() {
		return bezeichnung;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrennstoff#setBezeichnung(java.lang.String)
	 */
	@Override
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	
}
