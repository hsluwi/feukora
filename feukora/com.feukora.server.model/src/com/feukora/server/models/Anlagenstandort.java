package com.feukora.server.models;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.eclipse.persistence.annotations.CascadeOnDelete;
import org.hibernate.validator.constraints.NotBlank;

import com.feukora.server.models.interfaces.IAnlagenstandort;
@Entity
public class Anlagenstandort extends FeukoraObj implements Serializable, IAnlagenstandort {

	/**
	 * @author Philipp Anderhub
	 * @version 1.0
	 * @since 1.0
	 * Model f�r Anlagenstandort. Erzeugt alle Datenbankelemente und Beziehungen.
	 */
	private static final long serialVersionUID = 4453767150842879386L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@ManyToOne
	@CascadeOnDelete
	@NotNull(message="Eine Gemeinde muss zugewiesen werden.")
	@Valid
	private Gemeinde gemeinde;
	@ManyToOne
	@CascadeOnDelete
	@NotNull(message="Ein Kunde muss zugewiesen werden.")
	@Valid
	private Kunde kunde;
	@ManyToOne
	@CascadeOnDelete
	@Valid
	private Kunde hauswart;
	@ManyToOne
	@CascadeOnDelete
	@NotNull(message = "PLZ/Ort muss zugewiesen werden.")
	@Valid
	private PlzOrt plzOrt;
	
	@NotNull(message = "Strasse darf nicht leer sein.")
	@NotBlank(message = "Strasse darf nicht leer sein.")
	@Size(max=60, message="Strasse darf maximal {max} Zeichen haben.")
	private String strasse;
	
	@OneToOne
	@CascadeOnDelete
	@Valid
	private Brenner brenner;
	
	@OneToOne
	@CascadeOnDelete
	@Valid
	private Waermeerzeuger Waermeerzeuger;
	
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getId()
	 */
	@Override
	public Integer getId() {
		return id;
	}	
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getMyId()
	 */
	@Override
	public Integer getMyId() {
		return id;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getGemeinde()
	 */
	@Override
	public Gemeinde getGemeinde() {
		return gemeinde;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#setGemeinde(com.feukora.server.models.Gemeinde)
	 */
	@Override
	public void setGemeinde(Gemeinde gemeinde) {
		this.gemeinde = gemeinde;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getKunde()
	 */
	@Override
	public Kunde getKunde() {
		return kunde;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#setKunde(com.feukora.server.models.Kunde)
	 */
	@Override
	public void setKunde(Kunde kunde) {
		this.kunde = kunde;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getHauswart()
	 */
	@Override
	public Kunde getHauswart() {
		return hauswart;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#setHauswart(com.feukora.server.models.Kunde)
	 */
	@Override
	public void setHauswart(Kunde hauswart) {
		this.hauswart = hauswart;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getPlzOrt()
	 */
	@Override
	public PlzOrt getPlzOrt() {
		return plzOrt;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#setPlzOrt(com.feukora.server.models.PlzOrt)
	 */
	@Override
	public void setPlzOrt(PlzOrt plzOrt) {
		this.plzOrt = plzOrt;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getStrasse()
	 */
	@Override
	public String getStrasse() {
		return strasse;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#setStrasse(java.lang.String)
	 */
	@Override
	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getBrenner()
	 */
	@Override
	public Brenner getBrenner() {
		return brenner;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#setBrenner(com.feukora.server.models.Brenner)
	 */
	@Override
	public void setBrenner(Brenner brenner) {
		this.brenner = brenner;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#getWaermeerzeuger()
	 */
	@Override
	public Waermeerzeuger getWaermeerzeuger() {
		return Waermeerzeuger;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#setWaermeerzeuger(com.feukora.server.models.Waermeerzeuger)
	 */
	@Override
	public void setWaermeerzeuger(Waermeerzeuger waermeerzeuger) {
		Waermeerzeuger = waermeerzeuger;
	}	
	
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IAnlagestandort#setMyId(java.lang.Integer)
	 */
	@Override
	public void setMyId(Integer id){
		this.id = id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
}


