/**
 * 
 */
package com.feukora.server.models;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.feukora.server.models.interfaces.IMitarbeiter;

/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 31.10.2015
 *
 *Klasse um Mitarbeiter-Objekte zu erzeugen. Die Klasse erbt von der Klasse FeukoraObj
 *und implementiert Serializable und IMitarbeiter.
 *
 */
@Entity
public class Mitarbeiter extends Person implements Serializable, IMitarbeiter {

	private static final long serialVersionUID = 4102692985367803845L;

	@NotNull
	@Size(min=3, max=20, message="Benutzername muss zwischen ${min} und ${max} Zeichen lang sein.")
	//@Pattern(regexp="^[a-zA-Z0-9]$", message="Der Benutzername darf keine Sonderzeichen haben.")
	private String userName;
	
	@NotNull
	@Size(min=6, max=100, message="Passwort muss zwischen {min} und {max} Zeichen lang sein.")
	private String passwort;
	
	@Valid
	private Person person;
	
	@NotNull(message = "Mitarbeiter muss eine Rolle haben.")
	@Valid
	private MitarbeiterRolle rolle;

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMitarbeiter#getUserName()
	 */
	@Override
	public String getUserName() {
		return userName;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMitarbeiter#setUserName(java.lang.String)
	 */
	@Override
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMitarbeiter#getPasswort()
	 */
	@Override
	public String getPasswort() {
		return passwort;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMitarbeiter#setPasswort(java.lang.String)
	 */
	@Override
	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMitarbeiter#getPerson()
	 */
	@Override
	public Person getPerson() {
		return person;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMitarbeiter#setPerson(com.feukora.server.models.Person)
	 */
	@Override
	public void setPerson(Person person) {
		this.person = person;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMitarbeiter#getRolle()
	 */
	@Override
	public MitarbeiterRolle getRolle() {
		return rolle;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMitarbeiter#setRolle(com.feukora.server.models.MitarbeiterRolle)
	 */
	@Override
	public void setRolle(MitarbeiterRolle rolle) {
		this.rolle = rolle;
	}
	
	
}
