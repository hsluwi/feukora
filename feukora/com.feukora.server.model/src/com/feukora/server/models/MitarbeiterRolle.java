package com.feukora.server.models;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.feukora.server.models.interfaces.IMitarbeiterRolle;

/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 31.10.2015
 *
 *Klasse um MitarbeiterRolle-Objekte zu erzeugen. Die Klasse erbt von der Klasse FeukoraObj
 *und implementiert Serializable und IMitarbeiterRolle.
 *
 */
@Entity
public class MitarbeiterRolle extends FeukoraObj implements Serializable, IMitarbeiterRolle {

	private static final long serialVersionUID = 9164617778272594411L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull(message = "Bezeichnung darf nicht leer sein.")
	@Size(min=1, max=50, message="Die Bezeichnung darf maximal {max} Zeichen lang sein.")
	private String bezeichnung;
	
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMitarbeiterRolle#getBezeichnung()
	 */
	@Override
	public String getBezeichnung() {
		return bezeichnung;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMitarbeiterRolle#setBezeichnung(java.lang.String)
	 */
	@Override
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMitarbeiterRolle#getId()
	 */
	@Override
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
}
