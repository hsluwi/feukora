package com.feukora.server.models;
import java.io.Serializable;
import java.rmi.RemoteException;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.eclipse.persistence.annotations.CascadeOnDelete;

import com.feukora.server.models.interfaces.IMessergebnis;
@Entity
public class Messergebnis extends FeukoraObj implements Serializable, IMessergebnis{

	/**
	 * @author Philipp Anderhub
	 * @version 1.0
	 * @since 1.0
	 * Model f�r Messergebnis. Erzeugt alle Datenbankelemente und Beziehungen.
	 */
	private static final long serialVersionUID = 6140858439641395744L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@NotNull private Integer stufe;
	@NotNull private Integer messungNr;
	@NotNull private Double abgastemperatur;
	@NotNull private Double abgasverluste;
	@NotNull private Double co3PVol;
	@NotNull private Double nox3PVol;
	@NotNull private Double o2Gehalt;
	@NotNull private Boolean oelanteile;
	@NotNull private Double Russzahl;
	@NotNull private Double verbrennLuftTemp;
	@NotNull private Double waermeErzTemp;
	@ManyToOne
	@CascadeOnDelete
	@Valid
	@NotNull(message="Kontrolle darf nicht leer sein.")
	
	private Kontrolle kontrolle;
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#getId()
	 */
	@Override
	public Integer getId() {
		return id;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#setId(java.lang.Integer)
	 */
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#getStufe()
	 */
	@Override
	public Integer getStufe() {
		return stufe;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#setStufe(java.lang.Integer)
	 */
	@Override
	public void setStufe(Integer stufe) {
		this.stufe = stufe;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#getMessungNr()
	 */
	@Override
	public Integer getMessungNr() {
		return messungNr;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#setMessungNr(java.lang.Integer)
	 */
	@Override
	public void setMessungNr(Integer messungNr) {
		this.messungNr = messungNr;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#getAbgastemperatur()
	 */
	@Override
	public Double getAbgastemperatur() {
		return abgastemperatur;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#setAbgastemperatur(java.lang.Double)
	 */
	@Override
	public void setAbgastemperatur(Double abgastemperatur) {
		this.abgastemperatur = abgastemperatur;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#getAbgasverluste()
	 */
	@Override
	public Double getAbgasverluste() {
		return abgasverluste;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#setAbgasverluste(java.lang.Double)
	 */
	@Override
	public void setAbgasverluste(Double abgasverluste) {
		this.abgasverluste = abgasverluste;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#getCo3PVol()
	 */
	@Override
	public Double getCo3PVol() {
		return co3PVol;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#setCo3PVol(java.lang.Double)
	 */
	@Override
	public void setCo3PVol(Double co3pVol) {
		co3PVol = co3pVol;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#getNox3PVol()
	 */
	@Override
	public Double getNox3PVol() {
		return nox3PVol;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#setNox3PVol(java.lang.Double)
	 */
	@Override
	public void setNox3PVol(Double nox3pVol) {
		nox3PVol = nox3pVol;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#getO2Gehalt()
	 */
	@Override
	public Double getO2Gehalt() {
		return o2Gehalt;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#setO2Gehalt(java.lang.Double)
	 */
	@Override
	public void setO2Gehalt(Double o2Gehalt) {
		this.o2Gehalt = o2Gehalt;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#getOelanteile()
	 */
	@Override
	public Boolean getOelanteile() {
		return oelanteile;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#setOelanteile(java.lang.Boolean)
	 */
	@Override
	public void setOelanteile(Boolean oelanteile) {
		this.oelanteile = oelanteile;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#getRusszahl()
	 */
	@Override
	public Double getRusszahl() {
		return Russzahl;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#setRusszahl(java.lang.Double)
	 */
	@Override
	public void setRusszahl(Double russzahl) {
		Russzahl = russzahl;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#getVerbrennLuftTemp()
	 */
	@Override
	public Double getVerbrennLuftTemp() {
		return verbrennLuftTemp;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#setVerbrennLuftTemp(java.lang.Double)
	 */
	@Override
	public void setVerbrennLuftTemp(Double verbrennLuftTemp) {
		this.verbrennLuftTemp = verbrennLuftTemp;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#getWaermeErzTemp()
	 */
	@Override
	public Double getWaermeErzTemp() {
		return waermeErzTemp;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#setWaermeErzTemp(java.lang.Double)
	 */
	@Override
	public void setWaermeErzTemp(Double waermeErzTemp) {
		this.waermeErzTemp = waermeErzTemp;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#getKontrolle()
	 */
	@Override
	public Kontrolle getKontrolle() {
		return kontrolle;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IMessergebnis#setKontrolle(com.feukora.server.models.Kontrolle)
	 */
	@Override
	public void setKontrolle(Kontrolle kontrolle) {
		this.kontrolle = kontrolle;
	}
	
}
