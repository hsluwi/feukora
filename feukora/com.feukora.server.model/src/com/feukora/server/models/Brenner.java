package com.feukora.server.models;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.eclipse.persistence.annotations.CascadeOnDelete;

import com.feukora.server.models.interfaces.IBrenner;


/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 31.10.2015
 *
 *Klasse um Brenner-Objekte zu erzeugen. Die Klasse erbt von der Klasse FeukoraObj
 *und implementiert Serializable und IBrenner.
 *Die Klasse besitzt eine @ManyToOne Beziehung zu der Klasse Brennermodell.
 *
 */
@Entity
public class Brenner extends FeukoraObj implements Serializable, IBrenner{

	private static final long serialVersionUID = 5978938058562106560L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@CascadeOnDelete
	@NotNull
	@Valid
	private Brennermodell brennermodell;
	
	@Min(1900)
	@Max(2100)
	private Integer baujahr;
	

	@Size(max=50, message="Zulassungsnummer darf maximal {max} Zeichen haben.")
	private String zulassungsNr;

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrenner#getId()
	 */
	
	@Override
	public Integer getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrenner#getBrennermodell()
	 */
	@Override
	public Brennermodell getBrennermodell() {
		return brennermodell;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrenner#setBrennermodell(com.feukora.server.models.Brennermodell)
	 */
	@Override
	public void setBrennermodell(Brennermodell brennermodell) {
		this.brennermodell = brennermodell;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrenner#getBaujahr()
	 */
	@Override
	public Integer getBaujahr() {
		return baujahr;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrenner#setBaujahr(java.lang.Integer)
	 */
	@Override
	public void setBaujahr(Integer baujahr) {
		this.baujahr = baujahr;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrenner#getZulassungsNr()
	 */
	@Override
	public String getZulassungsNr() {
		return zulassungsNr;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IBrenner#setZulassungsNr(java.lang.String)
	 */
	@Override
	public void setZulassungsNr(String zulassungsNr) {
		this.zulassungsNr = zulassungsNr;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
}
