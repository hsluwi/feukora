package com.feukora.server.models;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.eclipse.persistence.annotations.CascadeOnDelete;

import com.feukora.server.models.interfaces.IKontrolle;

@Entity
public class Kontrolle extends FeukoraObj implements Serializable, IKontrolle {

	/**
	 * @author Philipp Anderhub
	 * @version 1.0
	 * @since 1.0
	 * Model f�r Kontrolle. Erzeugt alle Datenbankelemente und Beziehungen.
	 */
	private static final long serialVersionUID = -8956836683353906200L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Size(max=200, message="Bemerkung darf maximal {max} Zeichen lang sein.")
	private String bemerkungen;
	private Boolean einregulierungMoeglich;
	@NotNull private Boolean failAbgasverlust;
	@NotNull private Boolean failCo;
	@NotNull private Boolean failNo;
	@NotNull private Boolean failOel;
	@NotNull private Boolean failRusszahl;
	@NotNull private Boolean kenntnisnahmeKunde;
	@NotNull private Boolean bestanden;
    @OneToOne
    @CascadeOnDelete
    @Valid
    @NotNull(message="Termin darf nicht leer sein.")
    private Termin termin;	
	@NotNull
	private String kontrollart;
	
	
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#getTermin()
	 */
	@Override
	public Termin getTermin() {
		return termin;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#setTermin(com.feukora.server.models.Termin)
	 */
	@Override
	public void setTermin(Termin termin) {
		this.termin = termin;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#setId(java.lang.Integer)
	 */
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#getId()
	 */
	@Override
	public Integer getId() {
		return id;
	}
	
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#getBemerkungen()
	 */
	@Override
	public String getBemerkungen() {
		return bemerkungen;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#setBemerkungen(java.lang.String)
	 */
	@Override
	public void setBemerkungen(String bemerkungen) {
		this.bemerkungen = bemerkungen;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#getEinregulierungMoeglich()
	 */
	@Override
	public Boolean getEinregulierungMoeglich() {
		return einregulierungMoeglich;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#setEinregulierungMoeglich(java.lang.Boolean)
	 */
	@Override
	public void setEinregulierungMoeglich(Boolean einregulierungMoeglich) {
		this.einregulierungMoeglich = einregulierungMoeglich;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#getFailAbgasverlust()
	 */
	@Override
	public Boolean getFailAbgasverlust() {
		return failAbgasverlust;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#setFailAbgasverlust(java.lang.Boolean)
	 */
	@Override
	public void setFailAbgasverlust(Boolean failAbgasverlust) {
		this.failAbgasverlust = failAbgasverlust;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#getFailCo()
	 */
	@Override
	public Boolean getFailCo() {
		return failCo;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#setFailCo(java.lang.Boolean)
	 */
	@Override
	public void setFailCo(Boolean failCo) {
		this.failCo = failCo;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#getFailNo()
	 */
	@Override
	public Boolean getFailNo() {
		return failNo;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#setFailNo(java.lang.Boolean)
	 */
	@Override
	public void setFailNo(Boolean failNo) {
		this.failNo = failNo;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#getFailOel()
	 */
	@Override
	public Boolean getFailOel() {
		return failOel;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#setFailOel(java.lang.Boolean)
	 */
	@Override
	public void setFailOel(Boolean failOel) {
		this.failOel = failOel;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#getFailRusszahl()
	 */
	@Override
	public Boolean getFailRusszahl() {
		return failRusszahl;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#setFailRusszahl(java.lang.Boolean)
	 */
	@Override
	public void setFailRusszahl(Boolean failRusszahl) {
		this.failRusszahl = failRusszahl;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#getKenntnisnahmeKunde()
	 */
	@Override
	public Boolean getKenntnisnahmeKunde() {
		return kenntnisnahmeKunde;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#setKenntnisnahmeKunde(java.lang.Boolean)
	 */
	@Override
	public void setKenntnisnahmeKunde(Boolean kenntnisnahmeKunde) {
		this.kenntnisnahmeKunde = kenntnisnahmeKunde;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#getKontrollart()
	 */
	@Override
	public String getKontrollart() {
		return kontrollart;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#setKontrollart(java.lang.String)
	 */
	@Override
	public void setKontrollart(String kontrollart) {
		this.kontrollart = kontrollart;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#getBestanden()
	 */
	@Override
	public Boolean getBestanden() {
		return bestanden;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IKontrolle#setBestanden(java.lang.Boolean)
	 */
	@Override
	public void setBestanden(Boolean bestanden) {
		this.bestanden = bestanden;
	}
	
	
	
	
}
