package com.feukora.server.models;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.eclipse.persistence.annotations.CascadeOnDelete;

import com.feukora.server.models.interfaces.IWaermeerzeuger;


/**
 * 
 * @author Nadine Lang
 * @version 1.0
 * @since 31.10.2015
 *
 *Klasse um Waermeerzeuger-Objekte zu erzeugen. Die Klasse erbt von der Klasse FeukoraObj
 *und implementiert Serializable und IWaermeerzeuger.
 *Die Klasse besitzt eine @ManyToOne Beziehung zu der Klasse Waermeerzeugermodell.
 *
 */
@Entity
public class Waermeerzeuger extends FeukoraObj implements Serializable, IWaermeerzeuger{

	private static final long serialVersionUID = 5029933674684786066L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@CascadeOnDelete
	@NotNull
	@Valid
	private Waermeerzeugermodell waermeerzeugermodell;
	
	@Min(1900)
	@Max(2100)
	private Integer baujahr;
		
	@Size(max=50, message="Zulassungsnummer darf maximal {max} Zeichen lang sein.")
	private String zulassungsNr;
	

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IWaermeerzeuger#getId()
	 */
	@Override
	public Integer getId() {
		return id;
	}


	/* (non-Javadoc)
	 * @see com.feukora.server.models.IWaermeerzeuger#getWaermeerzeugermodell()
	 */
	@Override
	public Waermeerzeugermodell getWaermeerzeugermodell() {
		return waermeerzeugermodell;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IWaermeerzeuger#setWaermeerzeugermodell(com.feukora.server.models.Waermeerzeugermodell)
	 */
	@Override
	public void setWaermeerzeugermodell(Waermeerzeugermodell waermeerzeugermodell) {
		this.waermeerzeugermodell = waermeerzeugermodell;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IWaermeerzeuger#getBaujahr()
	 */
	@Override
	public Integer getBaujahr() {
		return baujahr;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IWaermeerzeuger#setBaujahr(java.lang.Integer)
	 */
	@Override
	public void setBaujahr(Integer baujahr) {
		this.baujahr = baujahr;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IWaermeerzeuger#getZulassungsNr()
	 */
	@Override
	public String getZulassungsNr() {
		return zulassungsNr;
	}

	/* (non-Javadoc)
	 * @see com.feukora.server.models.IWaermeerzeuger#setZulassungsNr(java.lang.String)
	 */
	@Override
	public void setZulassungsNr(String zulassungsNr) {
		this.zulassungsNr = zulassungsNr;
	}


	public void setId(Integer id) {
		this.id = id;
	}

	
	
	
}
