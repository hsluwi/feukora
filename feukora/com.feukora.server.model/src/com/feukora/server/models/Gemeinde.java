package com.feukora.server.models;

import java.io.Serializable;
import java.rmi.RemoteException;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.feukora.server.models.interfaces.IGemeinde;
@Entity
public class Gemeinde extends FeukoraObj implements Serializable, IGemeinde{
	
	/**
	 * @author Philipp Anderhub
	 * @version 1.0
	 * @since 1.0
	 * Model f�r Gemeinde. Erzeugt alle Datenbankelemente und Beziehungen.
	 */
	private static final long serialVersionUID = -5265853375276783282L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@NotBlank(message = "Bezeichnung darf nicht leer sein.")
	@NotNull(message = "Bezeichnung darf nicht leer sein.")
	@Size(max=50, message="Bezeichnung darf maximal {max} Zeichen lang sein.")
	private String bezeichnung;
	
	@NotBlank(message = "Kanton darf nicht leer sein.")
	@NotNull(message = "Kanton darf nicht leer sein.")
	@Size(max=50, message="Kanton darf maximal {max} Zeichen lang sein.")
	private String kanton;
	
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IGemeinde#getBezeichnung()
	 */
	@Override
	public String getBezeichnung() {
		return bezeichnung;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IGemeinde#setBezeichnung(java.lang.String)
	 */
	@Override
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IGemeinde#getKanton()
	 */
	@Override
	public String getKanton() {
		return kanton;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IGemeinde#setKanton(java.lang.String)
	 */
	@Override
	public void setKanton(String kanton) {
		this.kanton = kanton;
	}
	/* (non-Javadoc)
	 * @see com.feukora.server.models.IGemeinde#getId()
	 */
	@Override
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
}
