package com.feukora.server.webservice;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import com.feukora.server.models.Kontrolle;

@WebService
public interface IKontrolleController extends Remote {
		
		public Set<Kontrolle> getListKontrolle() throws RemoteException;
		
		
		public Set<Kontrolle> getKontrolleByTerminId(Integer TerminId) throws RemoteException;
		
		
		public Kontrolle getKontrolle(Integer id) throws RemoteException;
		
		
		public Kontrolle saveKontrolle(Kontrolle object) throws RemoteException;
		
		
		public boolean saveMultipleKontrolle(Kontrolle... object) throws RemoteException;
		

		public boolean saveListKontrolle(Collection<Kontrolle> objectList) throws RemoteException;


		public boolean removeKontrolle(Integer id) throws RemoteException;
	}
