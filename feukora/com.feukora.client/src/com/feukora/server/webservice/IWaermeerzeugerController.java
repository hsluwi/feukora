package com.feukora.server.webservice;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import com.feukora.server.models.Waermeerzeuger;

@WebService
public interface IWaermeerzeugerController extends Remote {
		
		public Set<Waermeerzeuger> getListWaermeerzeuger() throws RemoteException;
		
		
		public Waermeerzeuger getWaermeerzeuger(Integer id) throws RemoteException;
		
		
		public Waermeerzeuger saveWaermeerzeuger(Waermeerzeuger object) throws RemoteException;
		
		
		public boolean saveMultipleWaermeerzeuger(Waermeerzeuger... object) throws RemoteException;
		

		public boolean saveListWaermeerzeuger(Collection<Waermeerzeuger> objectList) throws RemoteException;


		public boolean removeWaermeerzeuger(Integer id) throws RemoteException;
	}
