package com.feukora.server.webservice;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import com.feukora.server.models.PlzOrt;

@WebService
public interface IPlzOrtController extends Remote {
		
		public Set<PlzOrt> getListPlzOrt() throws RemoteException;
		
		
		public PlzOrt getPlzOrt(Integer id) throws RemoteException;
		
		
		public PlzOrt savePlzOrt(PlzOrt object) throws RemoteException;
		
		
		public boolean saveMultiplePlzOrt(PlzOrt... object) throws RemoteException;
		

		public boolean saveListPlzOrt(Collection<PlzOrt> objectList) throws RemoteException;


		public boolean removePlzOrt(Integer id) throws RemoteException;
		
		
		public Set<PlzOrt> getPlzOrtByPlz(Integer plz) throws RemoteException;
	}
