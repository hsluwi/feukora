package com.feukora.server.webservice;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import com.feukora.server.models.Mitarbeiter;

@WebService
public interface IMitarbeiterController extends Remote {
		
		public Set<Mitarbeiter> getListMitarbeiter() throws RemoteException;
		
		
		public Mitarbeiter getMitarbeiter(Integer id) throws RemoteException;
		
		
		public Mitarbeiter saveMitarbeiter(Mitarbeiter object) throws RemoteException;
		
		
		public boolean saveMultipleMitarbeiter(Mitarbeiter... object) throws RemoteException;
		

		public boolean saveListMitarbeiter(Collection<Mitarbeiter> objectList) throws RemoteException;


		public boolean removeMitarbeiter(Integer id) throws RemoteException;
		
		
		public boolean usernameAvailable(String username) throws RemoteException;
		
		
		public Mitarbeiter getMitarbeiterLogin(String username, String passwort) throws RemoteException;
	}
