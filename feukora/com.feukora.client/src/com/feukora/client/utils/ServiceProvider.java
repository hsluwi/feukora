/**
 * 
 */
package com.feukora.client.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.client.config.Config;
import com.feukora.server.webservice.IAnlagenstandortController;
import com.feukora.server.webservice.IBrennerController;
import com.feukora.server.webservice.IBrennerartController;
import com.feukora.server.webservice.IBrennermodellController;
import com.feukora.server.webservice.IBrennstoffController;
import com.feukora.server.webservice.IGemeindeController;
import com.feukora.server.webservice.IKontrolleController;
import com.feukora.server.webservice.IKundeController;
import com.feukora.server.webservice.IMessergebnisController;
import com.feukora.server.webservice.IMitarbeiterController;
import com.feukora.server.webservice.IMitarbeiterRolleController;
import com.feukora.server.webservice.IPlzOrtController;
import com.feukora.server.webservice.ITerminController;
import com.feukora.server.webservice.IValidationController;
import com.feukora.server.webservice.IWaermeerzeugerController;
import com.feukora.server.webservice.IWaermeerzeugermodellController;



/**
 * Stellt die Verbindung zum WebService her und gibt die jeweiligen
 * WebServiceController zur�ck
 * @author Andrew
 *
 */
public class ServiceProvider {
	
	private static final Logger log = LogManager.getLogger();
	
	private Config config;
	private String wsdlserviceUrl;
	
	public ServiceProvider() {
		config = new Config();
		wsdlserviceUrl = "http://" + 
						config.getWsdlurl() + ":" + 
						config.getWsdlport() + "/" +
						config.getWebserviceProjekt() + "/";
	}

	/**
	 * Registriert das InterfaceController auf dem Server
	 * @param serviceName z.b KundeWS
	 * @param interfaceClass z.b IKundeController
	 * @return Ein WebServiceController
	 * @throws RemoteException
	 * @throws MalformedURLException
	 */
	public <T> T getController(String serviceName, Class<T> interfaceClass)  {
		try {
			QName webServiceUrl = 
					new QName("http://" + config.getWebserviceName() + "/",
							serviceName + "Service");
			
			URL wsdlURL = new URL(wsdlserviceUrl + serviceName + "?wsdl");
			
			Service service = Service.create(wsdlURL, webServiceUrl);
			T proxy = 
					(T)service.getPort(new QName("http://" + config.getWebserviceName() 
					+ "/", serviceName + "Port"), interfaceClass);

			return proxy;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Registriert den IAnlagenstandortController auf dem Server
	 * @return IAnlagenstandortController WebService Klasse 
	 */
	public IAnlagenstandortController getAnlagenstandortService() {
		return getController("AnlagenstandortWS", IAnlagenstandortController.class);
	}
	
	/**
	 * Registriert den IBrennerartController auf dem Server
	 * @return IBrennerartController WebService Klasse 
	 */
	public IBrennerartController getBrennerartService() {
		return getController("BrennerartWS", IBrennerartController.class);
	}
	
	/**
	 * Registriert den IBrennermodellController auf dem Server
	 * @return IBrennermodellController WebService Klasse 
	 */
	public IBrennermodellController getBrennermodellService() {
		return getController("BrennermodellWS", IBrennermodellController.class);
	}
	
	/**
	 * Registriert den IBrennerController auf dem Server
	 * @return IBrennerController WebService Klasse 
	 */
	public IBrennerController getBrennerService() {
		return getController("BrennerWS", IBrennerController.class);
	}
	
	/**
	 * Registriert den IBrennerController auf dem Server
	 * @return IBrennerController WebService Klasse 
	 */
	public IValidationController getValidationService() {
		return getController("ValidationWS", IValidationController.class);
	}
	
	/**
	 * Registriert den IBrennstoffController auf dem Server
	 * @return IBrennstoffController WebService Klasse 
	 */
	public IBrennstoffController getBrennstoffService() {
		return getController("BrennstoffWS", IBrennstoffController.class);
	}
	
	/**
	 * Registriert den IGemeindeController auf dem Server
	 * @return IGemeindeController WebService Klasse 
	 */
	public IGemeindeController getGemeindeService() {
		return getController("GemeindeWS", IGemeindeController.class);
	}
	
	/**
	 * Registriert den IKontrolleController auf dem Server
	 * @return IKontrolleController WebService Klasse 
	 */
	public IKontrolleController getKontrolleService() {
		return getController("KontrolleWS", IKontrolleController.class);
	}
	
	/**
	 * Registriert den IKundenController auf dem Server
	 * @return IKundeController WebService Klasse 
	 */
	public IKundeController getKundeService() {
		return getController("KundeWS", IKundeController.class);
	}
	
	/**
	 * Registriert den IMessergebnisController auf dem Server
	 * @return IMessergebnisController WebService Klasse 
	 */
	public IMessergebnisController getMessergebnisService() {
		return getController("MessergebnisWS", IMessergebnisController.class);
	}
	
	/**
	 * Registriert den IMitarbeiterController auf dem Server
	 * @return IMitarbeiterController WebService Klasse 
	 */
	public IMitarbeiterController getMitarbeiterService() {
		return getController("MitarbeiterWS", IMitarbeiterController.class);
	}
	
	/**
	 * Registriert den IMitarbeiterRolleController auf dem Server
	 * @return IMitarbeiterRolleController WebService Klasse 
	 */
	public IMitarbeiterRolleController getMitarbeiterRolleService() {
		return getController("MitarbeiterRolleWS", IMitarbeiterRolleController.class);
	}
	
	/**
	 * Registriert den IPlzOrtController auf dem Server
	 * @return IPlzOrtController WebService Klasse 
	 */
	public IPlzOrtController getPlzOrtService() {
		return getController("PlzOrtWS", IPlzOrtController.class);
	}
	
	/**
	 * Registriert den IPlzOrtController auf dem Server
	 * @return IPlzOrtController WebService Klasse 
	 */
	public ITerminController getTerminService() {
		return getController("TerminWS", ITerminController.class);
	}
	
	/**
	 * Registriert den IPlzOrtController auf dem Server
	 * @return IPlzOrtController WebService Klasse 
	 */
	public IWaermeerzeugerController getWaermeerzeugerService() {
		return getController("WaermeerzeugerWS", IWaermeerzeugerController.class);
	}
	
	/**
	 * Registriert den IPlzOrtController auf dem Server
	 * @return IPlzOrtController WebService Klasse 
	 */
	public IWaermeerzeugermodellController getWaermeerzeugermodellService() {
		return getController("WaermeerzeugermodellWS", IWaermeerzeugermodellController.class);
	}
	
}
