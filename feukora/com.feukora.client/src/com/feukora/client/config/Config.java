package com.feukora.client.config;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Klasse um die Config File auszulesen
 * @author Andrew
 */
public class Config {

	private final String wsdlurl;
	
	private final String wsdlport;
	
	private final String webserviceName;
	
	private final String webserviceProjekt;
	
	private final String propertiesPath;
	
	/**
	 * Konstruktor der die Configfile ausliest und die Variablen setzt
	 */
	public Config() {
		this.propertiesPath = "config.properties";
		
		Properties properties = new Properties();
		ClassLoader cLoader = this.getClass().getClassLoader();
		try (FileInputStream fis = new FileInputStream(propertiesPath)) {
			properties.load(fis);
		} catch(Exception e){
			System.err.println(e.getMessage());
		}
      
        this.wsdlport = properties.getProperty("wsdlport"); 
        this.wsdlurl = properties.getProperty("wsdlurl"); 
        this.webserviceName = properties.getProperty("webserviceName"); 
        this.webserviceProjekt = properties.getProperty("webserviceProjekt");
	}

	/**
	 * @return the wsdlport
	 */
	public String getWsdlport() {
		return wsdlport;
	}

	/**
	 * @return the wsdlurl
	 */
	public String getWsdlurl() {
		return wsdlurl;
	}

	/**
	 * @return the webserviceName
	 */
	public String getWebserviceName() {
		return webserviceName;
	}

	/**
	 * @return the webserviceProjekt
	 */
	public String getWebserviceProjekt() {
		return webserviceProjekt;
	}
}
