package com.feukora.client.controller;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.feukora.server.models.Waermeerzeugermodell;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * 
 * @author Sven Müller
 * @version 2.0
 * @since 30.11.2015
 * 
 * Controller Klasse zum anzeigen und verwalten aller erfassten Waermeerzeugermodellen
 *
 */
public class WaermeerzeugerModellController extends MainController implements Initializable {
	@FXML private GridPane grdWaermeerzeuger;
	@FXML private Button btnBearbeiten;
	@FXML private Button btnNeu;
	@FXML private Button btnWaehle;
	@FXML protected TextField txtSuche;
	@FXML private TextField txtWaermeerzeugerID;
	@FXML private TextField txtHersteller;
	@FXML private TextField txtTyp;
	@FXML private TextField txtBrennstoff;
	@FXML TableView<Waermeerzeugermodell> tblWaermeerzeugermodell;
	@FXML TableColumn<Waermeerzeugermodell, String> colHersteller;
	@FXML TableColumn<Waermeerzeugermodell, String> colModell;
	
	private WaermeerzeugerVerwaltenController weVeCo;
	protected Waermeerzeugermodell clickedWaermeerzeugerModell;
	
	private ObservableList<Waermeerzeugermodell> olWaermeerzeugerModell;
	
	public WaermeerzeugerModellController(WaermeerzeugerVerwaltenController weVeCo){
		super();
		this.weVeCo = weVeCo;
	}
	
	/**
	 * 
	 * @author Sven Müller
	 * @since 2.0
	 * @param Standardparameter von implementierter Initializable Klasse
	 * 
	 * Lade alle Wärmeerzeugerdaten aus dem MainController
	 *
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		loadSpecificData(this);
	}
	
	/**
	 * 
	 * @author Sven Müller
	 * @since 2.0
	 * @param waermeerzeugerModell Liste sämtlicher Wärmeerzeugermodelle
	 * 
	 * Wird durch MainController.loadSpecificData aufgerufen und übergibt die Daten so
	 * an diesen Controller und Tabelle wir neu eingelesen
	 *
	 */
	public void updateTable(List<Waermeerzeugermodell> waermeerzeugerModell){
		
		this.olWaermeerzeugerModell = FXCollections.observableArrayList(waermeerzeugerModell);
		this.tblWaermeerzeugermodell.setItems(this.olWaermeerzeugerModell);
	
	
	FilteredList<Waermeerzeugermodell> filteredData = new FilteredList<Waermeerzeugermodell>(olWaermeerzeugerModell, p -> true);
	 
	txtSuche.textProperty().addListener((observable, alterWert, neuerWert) -> {
            filteredData.setPredicate(weModell -> {
                // Wenn nichts eingegeben ist, zeige alle Wärmeerzeugermodelle an
                if (neuerWert == null || neuerWert.isEmpty()) {
                    return true;
                }
             // Vergleich von Hersteller und Modell
                String lowerCaseFilter = neuerWert.toLowerCase();
                if (weModell.getHersteller().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter = Hersteller
                } else if (weModell.getBezeichnung().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter = Modellname
                }
                return false; // Filter ungleich Hersteller/Modellname
            });
        });
	 
	 
	 SortedList<Waermeerzeugermodell> sortedData = new SortedList<>(filteredData);
	 
	 sortedData.comparatorProperty().bind(tblWaermeerzeugermodell.comparatorProperty());
	 tblWaermeerzeugermodell.setItems(sortedData); 
	}	
	
	/**
	 * 
	 * @author Sven Müller
	 * @since 1.0
	 * 
	 * Zeigt gewähltes Modell in den Textfelder an um mehr Details zu sehen
	 *
	 */
	@FXML
	public void getWaermeerzeugerDetails(){
		this.btnWaehle.setDisable(false);
		this.btnBearbeiten.setDisable(false);
		clickedWaermeerzeugerModell = tblWaermeerzeugermodell.getSelectionModel().getSelectedItem();
		int kundID = clickedWaermeerzeugerModell.getId().intValue();
		txtWaermeerzeugerID.setText(String.valueOf(kundID));
		txtBrennstoff.setText(clickedWaermeerzeugerModell.getBrennstoff().getBezeichnung());
		txtHersteller.setText(clickedWaermeerzeugerModell.getHersteller());
		txtTyp.setText(clickedWaermeerzeugerModell.getBezeichnung());	
	}
	
	/**
	 * 
	 * @author Sven Müller
	 * @since 1.0
	 * @throws IOException
	 * 
	 * Öffnet neues GUI Fenster "Wärmeerzeuger Modell bearbeiten" und übergibt dabei 
	 * sich selber (WaermeerzeugerModellController)an den aufzurufen Konstruktor WaermeerzeugerModellVerwalten
	 *
	 */
	@FXML
	public void btnBearbeitenClicked() {
		Parent root;

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/WaermeerzeugerModellVerwalten.fxml"));
			WaermeerzeugerModellVerwaltenController controller = new WaermeerzeugerModellVerwaltenController(this);
			fxmlLoader.setController(controller);
			
			root = fxmlLoader.load();

			Stage stage = new Stage();
			stage.setTitle("Wärmeerzeugermodell bearbeiten");
			stage.setScene(new Scene(root));
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @author Sven Müller
	 * @since 1.0
	 * @throws IOException
	 * 
	 * Öffnet neues GUI Fenster "Wärmeerzeuger Modell erfassen" und übergibt dabei 
	 * sich selber (WaermeerzeugerModellController)an den aufzurufen Konstruktor WaermeerzeugerModellVerwalten
	 *
	 */
	@FXML
	public void btnNeuClicked() {
		Parent root;
		clickedWaermeerzeugerModell = null;

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/WaermeerzeugerModellVerwalten.fxml"));
			
			WaermeerzeugerModellVerwaltenController controller = new WaermeerzeugerModellVerwaltenController(this);
			fxmlLoader.setController(controller);
			root = fxmlLoader.load();
			
			Stage stage = new Stage();
			stage.setTitle("Wärmeerezeugermodell erfassen");
			stage.setScene(new Scene(root));
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/**
	 * 
	 * @author Sven Müller
	 * @since 2.0
	 * 
	 * Gibt das gewählte Modell zurück and den WärmeerzeugerVerwaltenController
	 *
	 */
	@FXML
	public void btnWaehleClicked(ActionEvent event){
		if (txtWaermeerzeugerID.getText() == null || txtWaermeerzeugerID.getText() == "" ){
			Alert alert = new Alert(AlertType.INFORMATION, "Sie haben keinen Wärmeerzeuger gewählt.");
			alert.show();
			}else{
				weVeCo.objWaermeerzeugerModell = clickedWaermeerzeugerModell;
				weVeCo.reloadAuswahl();
				close(event);
		}
	}
	
	/**
	 * 
	 * @author Sven Müller
	 * @since 1.0
	 * 
	 * Schliesst das Fenster
	 *
	 */
	private void close(ActionEvent e) {
		Node node = (Node) e.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		stage.close();
	}
}
