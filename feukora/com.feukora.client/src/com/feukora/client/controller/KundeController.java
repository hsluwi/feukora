package com.feukora.client.controller;

import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Kunde;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * Kundekontroller f�r die View Kunde.fxml
 * 
 * @author Marco Hostettler
 * @version 3.0
 * @since 28.11.2015
 * 
 *
 */
public class KundeController extends MainController implements Initializable {
	@FXML
	private GridPane grdKunde;
	@FXML
	private Button btnBearbeiten;
	@FXML
	private Button btnNeu;
	@FXML
	private Button btnLoeschen;
	@FXML
	protected TextField txtSuche;
	@FXML
	private TextField txtKundenID;
	@FXML
	private TextField txtKundenart;
	@FXML
	private TextField txtFirma;
	@FXML
	private TextField txtVorname;
	@FXML
	private TextField txtNachname;
	@FXML
	private TextField txtStrasse;
	@FXML
	private TextField txtPLZ;
	@FXML
	private TextField txtOrt;
	@FXML
	private TextField txtTelefon;
	@FXML
	private TextField txtMobil;
	@FXML
	private TextField txtMail;
	@FXML
	protected TableView<Kunde> tblKunden;
	@FXML
	private TableColumn<Kunde, String> colNummer;
	@FXML
	TableColumn<Kunde, String> colOrt;
	@FXML
	TableColumn<Kunde, String> colNachname;

	protected ObservableList<Kunde> olKunden;
	private static final Logger logger = LogManager.getLogger();
	protected Kunde clickedKunde;
	MainController mainCo;

	public static Stage secondStage;

	public KundeController(MainController mainCo) {
		super();
		this.mainCo = mainCo;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		setCellValueFactory();
		updateData(mainCo.kunden);

	}

	/**
	 * Konfiguration f�r die Tabellenbef�llung, f�r colOrt da diese nicht direkt
	 * vom Kunden-Objekt kommen - FXML limitation
	 */
	private void setCellValueFactory() {
		// CellValueFactory f�r colOrt, da diese nicht direkt vom Kunde-Objekt
		// kommt. FXML limitation
		colOrt.setCellValueFactory(
				new Callback<TableColumn.CellDataFeatures<Kunde, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TableColumn.CellDataFeatures<Kunde, String> p) {
						return new SimpleObjectProperty<String>(p.getValue().getPlzOrt().getOrt());
					}
				});
	}

	/**
	 * Setzen der Daten in die Tabelle. Zus�tzliche Filterfunktion auf txtSuche
	 * implementiert
	 * 
	 * @author Marco Hostettler
	 * @param kunden
	 *            - welche im MainController geladen worden sind
	 * 
	 * 
	 */
	public void updateData(List<Kunde> kunden) {
		this.olKunden = FXCollections.observableArrayList(kunden);
		tblKunden.setItems(this.olKunden);

		FilteredList<Kunde> filteredData = new FilteredList<>(olKunden, p -> true);

		txtSuche.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(kunde -> {
				// TxtSuche = leer , Zeige alle Daten an
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}

				String lowerCaseFilter = newValue.toLowerCase();
				if (String.valueOf(kunde.getId()) != null
						&& String.valueOf(kunde.getId()).toLowerCase().contains(lowerCaseFilter)) {
					return true; // Filter = Kunden ID

				} else if (kunde.getPlzOrt().getOrt() != null
						&& kunde.getPlzOrt().getOrt().toLowerCase().contains(lowerCaseFilter)) {
					return true; // Filter = Ort
				} else if (kunde.getNachname() != null && kunde.getNachname().toLowerCase().contains(lowerCaseFilter)) {
					return true; // Filter = Nachname
				}
				return false; // Filter !=
			});
		});
		SortedList<Kunde> sortedData = new SortedList<>(filteredData);

		sortedData.comparatorProperty().bind(tblKunden.comparatorProperty());
		tblKunden.setItems(sortedData);
	}

	/**
	 * Abf�llung der Daten des ausgew�hlten kunden
	 */
	@FXML
	public void getKundeDetails() {
		this.btnBearbeiten.setDisable(false);
		this.btnLoeschen.setDisable(false);
		this.clickedKunde = tblKunden.getSelectionModel().getSelectedItem();
		int kundID = clickedKunde.getId().intValue();
		this.txtKundenID.setText(String.valueOf(kundID));
		this.txtKundenart.setText(clickedKunde.getKundenArt());
		this.txtFirma.setText(clickedKunde.getFirma());
		this.txtVorname.setText(clickedKunde.getVorname());
		this.txtNachname.setText(clickedKunde.getNachname());
		this.txtStrasse.setText(clickedKunde.getStrasse());
		int plz = clickedKunde.getPlzOrt().getPlz();
		this.txtPLZ.setText(String.valueOf(plz));
		this.txtOrt.setText(clickedKunde.getPlzOrt().getOrt());
		this.txtTelefon.setText(clickedKunde.getTelefon());
		this.txtMobil.setText(clickedKunde.getMobile());
		this.txtMail.setText(clickedKunde.getEmail());
	}

	@FXML
	public void btnBearbeitenClicked() {
		Parent root;

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/KundeVerwalten.fxml"));
			KundeVerwaltenController controller = new KundeVerwaltenController(this);
			fxmlLoader.setController(controller);

			root = fxmlLoader.load();

			Stage stage = new Stage();
			stage.initModality(Modality.WINDOW_MODAL);
			stage.initOwner(mainCo.mainstage.getScene().getWindow());
			stage.setTitle("Kunde bearbeiten");
			stage.setScene(new Scene(root));
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	public void btnNeuClicked() {
		Parent root;
		clickedKunde = null;

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/KundeVerwalten.fxml"));
			KundeVerwaltenController controller = new KundeVerwaltenController(this);
			fxmlLoader.setController(controller);
			root = fxmlLoader.load();

			Stage stage = new Stage();
			stage.initModality(Modality.WINDOW_MODAL);
			stage.initOwner(mainCo.mainstage.getScene().getWindow());
			stage.setTitle("Kunde erfassen");
			stage.setScene(new Scene(root));
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	public void btnLoeschenClicked() {

		Alert alert = new Alert(AlertType.CONFIRMATION, "Wollen Sie diesen Kunden endg�ltig l�schen?");
		alert.showAndWait().ifPresent(response -> {
			if (response == ButtonType.OK) {
				kundeLoeschen();
			}

		});
	}

	/**
	 * L�scht den Kunden vom Server und loggt das entsprechende Ergebniss
	 * 
	 * @author Marco Hostettler
	 * @version 3.0
	 * @since 1.0
	 */
	private void kundeLoeschen() {
		try {
			if (serviceProvider.getKundeService().removeKunde(clickedKunde.getId())) {
				Alert alert = new Alert(AlertType.INFORMATION, "Kunde wurde erfolgreich gel�scht.");
				alert.show();
				mainCo.loadSpecificData(this);
				txtSuche.clear();
				logger.info(
						"Kunde " + clickedKunde.getVorname() + " " + clickedKunde.getNachname() + " wurde gel�scht.");
			} else {
				Alert alert = new Alert(AlertType.ERROR, "Kontrolleur konnte nicht gel�scht werden");
				alert.show();
				logger.error("Kunde " + clickedKunde.getVorname() + " " + clickedKunde.getNachname()
						+ "  konnte nicht gel�scht werden.");
			}
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}

	}

}