package com.feukora.client.controller;

import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Mitarbeiter;
import com.feukora.server.models.MitarbeiterRolle;
import com.feukora.server.models.PlzOrt;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
/**
 * Kontroller Klasse f�r die View KontrolleurVerwalten.fxml
 * @author Marco Hostettler
 * @version 4.0
 * @since 8.12.2015
 *
 */
public class KontrolleurVerwaltenController extends MainController implements Initializable {
	@FXML
	private TextField txtVorname;
	@FXML
	private TextField txtNachname;
	@FXML
	private TextField txtStrasse;
	@FXML
	private TextField txtTelefon;
	@FXML
	private TextField txtEmail;
	@FXML
	private TextField txtUsername;
	@FXML
	private TextField txtPlz;
	@FXML
	private TextField txtMobile;
	@FXML
	private ComboBox<String> cbOrt;
	@FXML
	private Label lblMitarbeiterID;
	@FXML
	private Label lblTitel;
	@FXML
	private PasswordField pwdPasswort1;
	@FXML
	private PasswordField pwdPasswort2;

	private static final Logger logger = LogManager.getLogger();
	protected KontrolleurController koCo;
	private List<PlzOrt> plzOrt;
	private ObservableList<String> olPlzOrt = FXCollections.observableArrayList();
	private Mitarbeiter mitarbeiter;
	private PlzOrt obPlzOrt;

	public KontrolleurVerwaltenController(KontrolleurController koCo) {
		super();
		this.koCo = koCo;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		if (koCo.clickedMitarbeiter != null) {
			this.lblTitel.setText("Kontrolleur bearbeiten");
			this.mitarbeiter = koCo.clickedMitarbeiter;
			int mitarbeiterID = mitarbeiter.getId().intValue();
			this.lblMitarbeiterID.setText(String.valueOf(mitarbeiterID));
			this.txtVorname.setText(mitarbeiter.getVorname());
			this.txtNachname.setText(mitarbeiter.getNachname());
			this.txtStrasse.setText(mitarbeiter.getStrasse());
			this.txtMobile.setText(mitarbeiter.getMobile());
			this.txtTelefon.setText(mitarbeiter.getTelefon());
			this.txtEmail.setText(mitarbeiter.getEmail());
			this.txtUsername.setText(mitarbeiter.getUserName());
			int plz = mitarbeiter.getPlzOrt().getPlz();
			this.txtPlz.setText(String.valueOf(plz));
			this.cbOrt.setValue(mitarbeiter.getPlzOrt().getOrt());
			this.pwdPasswort1.setText(mitarbeiter.getPasswort());
			this.pwdPasswort2.setText(mitarbeiter.getPasswort());
			obPlzOrt = mitarbeiter.getPlzOrt();
		}
		else{
			txtUsername.setDisable(false);
		}
	}

	public void btnAbbrechenClicked(ActionEvent event) {
		close(event);

	}

	private void close(ActionEvent e) {
		Node node = (Node) e.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		stage.close();

	}

	/**
	 * "On Key Typed" FXML Methode. Sobald 4 Stellen eingegeben werden, wird
	 * beim Server nach den Ortschaften mit diesen 4 Zahlen als Plz nachgefragt.
	 * Diese stehen dann im CbOrt zur Auswahl
	 * 
	 * @author Marco Hostettler
	 * @version 1.0
	 * @since 1.12.2015
	 */
	@FXML
	public void txtPlzChanged() {
		cbOrt.getItems().clear();
		if (txtPlz.getText().length() == 4) {

			try {
				this.plzOrt = new ArrayList<PlzOrt>(serviceProvider.getPlzOrtService().getPlzOrtByPlz(Integer.parseInt(txtPlz.getText())));
			} catch (NumberFormatException e) {
				logger.error(e.getMessage());
			} catch (RemoteException e) {
				logger.error(e.getMessage());
			}

			plzOrt.forEach(p -> olPlzOrt.add(p.getOrt()));
			this.cbOrt.setItems(olPlzOrt);
			this.cbOrt.setValue("Bitte w�hlen Sie...");

		}

	}

	/**
	 * "On Action" FXML Methode. Das PlzOrt-Objekt erstellen aus der Auswahl des
	 * cbOrt.
	 * 
	 * @author Marco Hostettler
	 * @version 1.0
	 * @since 1.12.2015
	 */
	@FXML
	void cbOrtChanged() {
		for (PlzOrt p : plzOrt) {
			if (p.getOrt().equals(cbOrt.getSelectionModel().getSelectedItem())) {
				obPlzOrt = p;
			}
		}

	}
	/**
	 * Speicher das Kontrolleur Objekt in der Datenbank
	 * @param event - ActionEvent
	 */
	public void btnSpeichernClicked(ActionEvent event) {
		MitarbeiterRolle rolle = new MitarbeiterRolle();
		rolle.setId(2);
		rolle.setBezeichnung("Kontrolleur");

		// Mitarbeiter Objekt erstellen wenn Mitarbeiter noch nicht existiert
		if (lblMitarbeiterID.getText().equals("")) {
			mitarbeiter = new Mitarbeiter();
			mitarbeiter.setRolle(rolle);
		}

		mitarbeiter.setVorname(txtVorname.getText());
		mitarbeiter.setNachname(txtNachname.getText());
		mitarbeiter.setStrasse(txtStrasse.getText());
		mitarbeiter.setTelefon(txtTelefon.getText());
		mitarbeiter.setMobile(txtMobile.getText());
		mitarbeiter.setEmail(txtEmail.getText());
		mitarbeiter.setUserName(txtVorname.getText());
		mitarbeiter.setPasswort(pwdPasswort2.getText());
		mitarbeiter.setPlzOrt(obPlzOrt);

		// Objekt validieren
		String errors = validate(mitarbeiter);

		if (!pwdPasswort1.getText().equals(pwdPasswort2.getText()) || pwdPasswort1.getText().length() < 6) {
			errors += " Passw�rter stimmen nicht �berein. Das Passwort muss mindestens 6 Zeichen haben";
		}
		// Error Ausgabe von Validierung
		if (errors != "") {
			Alert alert = new Alert(AlertType.WARNING, errors);
			alert.show();
			logger.error("Daten unvollst�ndig/Fehlerhaft. Keine weiterleitung an Server");
		}

		// Wenn keine Fehler dann speichern
		else {
			try {
				if ((serviceProvider.getMitarbeiterService().saveMitarbeiter(mitarbeiter)) == null) {
					Alert alert = new Alert(AlertType.ERROR, "Kontrolleur konnte nicht gespeichert werden");
					alert.show();
					logger.error("Kontrolleur konnte nicht in Datenbank gespeichert werden.");
				} else {
					Alert alert = new Alert(AlertType.INFORMATION, "Kontrolleur wurde erfolgreich gespeichert");
					alert.showAndWait().ifPresent(response -> {
						if (response == ButtonType.OK) {
							close(event);

						}
					});
					// Reload Daten vom Server, sowie Reload der TableView
					loadSpecificData(this);

				}
			} catch (RemoteException e) {
				logger.error(e.getMessage());
			}

		}

	};

}
