package com.feukora.client.controller;

import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Brenner;
import com.feukora.server.models.Brennermodell;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
/**
 * 
 * @author Sven M�ller
 * @version 3.0
 * @since 09.12.2015
 *
 * Diese Controller Klasse verwaltet einen spezifischen physischen Brenner,
 * welcher einer Anlagen zugeh�ren muss.
 *
 */
public class BrennerVerwaltenController extends MainController implements Initializable {
	@FXML private Button tbnAbbrechen;
	@FXML private AnchorPane anpBrennerModellErfassen;
	@FXML private Label lblHerstellerModell;
	@FXML private Label lblBrennerart;
	@FXML private TextField txtBaujahr;

	private Brenner objBrenner;
	protected Brennermodell objBrennerModell;

	private static final Logger logger = LogManager.getLogger();
	
	private AnlagenVerwaltenController anVeCo;
	
	//Zum �ffnen und Laden des W�rmeerzeuger Fenster
	public static BrennerModellController waermeModellController;
	 
	/**
	 * 
	 * @author Sven M�ller
	 * @since 2.0
	 * @param anVeCo  Anlagen Verwalten Controller Objekt der Klasse AnlagenVerwaltenController
	 *
	 * Der Konstruktor wird in der Klasse AnlagenVerwaltenController verwendet. 
	 * Durch den Konstruktor wird das selektierte Anlagenobjekt �bergeben.
	 */
	BrennerVerwaltenController(AnlagenVerwaltenController anVeCo){
		super();
		this.anVeCo = anVeCo;
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * 
	 * @param Standardparameter von implementierter Initializable Klasse
	 * 
	 * Pr�ft ob bereits ein W�rmeerzeugermodell der Anlage zugewiesen wurde
	 * und bef�hlt das GUI entsprechend.
	 *
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		if (anVeCo.objAnlagenstandort.getBrenner() != null){
			this.objBrenner = anVeCo.objAnlagenstandort.getBrenner();
			lblHerstellerModell.setText(objBrenner.getBrennermodell().getHersteller() + " " + 
										objBrenner.getBrennermodell().getBezeichnung());
			lblBrennerart.setText(objBrenner.getBrennermodell().getBrennerart().getBezeichnung());
			txtBaujahr.setText(Integer.toString(objBrenner.getBaujahr()));
			txtBaujahr.setDisable(true);
		}
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * @throws IOException
	 * 
	 * �ffnet neues GUI Fenster "Brenner Modell" und �bergibt dabei 
	 * sich selber (BrennerController)an den aufzurufen Konstruktor BrennerModell
	 *
	 */
	@FXML
	public void btnWaehleModellClicked (ActionEvent event){
		Parent root;
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/BrennerModell.fxml"));
			BrennerModellController controller = new BrennerModellController(this);
			fxmlLoader.setController(controller);
			root = fxmlLoader.load();
			
			Stage stage = new Stage();
			stage.setTitle("Brennermodell w�hlen");
			stage.setScene(new Scene(root));
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 2.0
	 * 
	 * Wird durch dem BrennerModellController aufgerufen, damit der selektierte Wert
	 * hier angezeigt wird
	 */
	@FXML
	public void reloadAuswahl(){
		lblHerstellerModell.setText(objBrennerModell.getHersteller() + "  " + objBrennerModell.getBezeichnung());
		lblBrennerart.setText(objBrennerModell.getBrennerart().getBezeichnung());
		txtBaujahr.setDisable(false);
		txtBaujahr.setText("");
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 2.0
	 * @param Standardparameter von implementierter Initializable Klasse
	 * 
	 * Ruft den Webservice zur Persistierung des Brenners auf
	 * Gibt entsprechende Meldung aus
	 * Updatet das Fenster Anlagen Verwalten (AnlagenVerwaltenConroller) mit dem neuen Wert 
	 * Schliesst dieses Fenster (BrennerVerwaltenController)
	 */
	@FXML
	public void btnSpeichernClicked(ActionEvent event) {
		
		objBrenner = new Brenner();
		//Objekt Brenner mit Link zum W�rmeerzeugermodell erstellen
		objBrenner.setBrennermodell(objBrennerModell);
		int bj = Integer.parseInt(txtBaujahr.getText());
		objBrenner.setBaujahr(bj);
		
		//Objekt validieren
		String errors = "";
//				validate(objBrenner);
				
		//Wenn Validierung ok...
		if (errors != "") {
			Alert alert = new Alert(AlertType.WARNING, errors);
			alert.show();
			logger.error("Daten unvollst�ndig/Fehlerhaft. Keine weiterleitung an Server");

		}else{
			//Objekt f�r Datenbankr�ckgabe, wenn erfolgreich gespeichert
			Brenner returnbrenner = new Brenner();
			//Webserviceaufruf zum Persistieren
			try {
				if((returnbrenner = serviceProvider.getBrennerService().saveBrenner(objBrenner)) == null){
							
					Alert alert = new Alert(AlertType.ERROR, "Brenner konnte nicht gespeichert werden");
					alert.show();
					logger.error("Brenner konnte nicht in Datenbank gespeichert werden.");
					
				} else {
					anVeCo.objAnlagenstandort.setBrenner(returnbrenner);
					anVeCo.reloadBrenner();
					Alert alert = new Alert(AlertType.INFORMATION, "Brenner wurde erfolgreich gespeichert");
					alert.showAndWait().ifPresent(response -> {
						if (response == ButtonType.OK) {
							close(event);
						}
					});
				}
			} catch (RemoteException e) {
				logger.error(e.getMessage());
			}
		}
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * 
	 * Beim Klicken des Abbrechen Button wird das Fenster geschlossen
	 */
	@FXML
	public void btnAbbrechenClicked(ActionEvent event){
		close(event);
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * 
	 * Schliesst das Fenster
	 */
	private void close(ActionEvent e) {
		Node node = (Node) e.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		stage.close();
	}
}
