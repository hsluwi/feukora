package com.feukora.client.controller;

import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Anlagenstandort;
import com.feukora.server.models.Brenner;
import com.feukora.server.models.Gemeinde;
import com.feukora.server.models.Kunde;
import com.feukora.server.models.PlzOrt;
import com.feukora.server.models.Waermeerzeuger;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
/**
 * 
 * @author Sven M�ller
 * @version 2.0
 * @since 10.12.2015
 * 
 * Diese Controller Klasse verwaltet Anlagen der View AnlagenVerwalten.fxml
 *
 */
public class AnlagenVerwaltenController extends RapportController implements Initializable {
	@FXML private Button tbnAbbrechen;
	@FXML private AnchorPane anlageErfassen;
	@FXML private Label lblAnlageID;
	@FXML private Label lblViewTitel;
	@FXML private TextField txtStrasse;
	@FXML private TextField txtPlz;
	@FXML private TextField txtKanton;
	@FXML private TextField txtGemeinde;
	@FXML private ComboBox<String> cbKunde;
	@FXML private ComboBox<String> cbOrt;
	@FXML private Label lblIdWaermeerzeuger;
	@FXML private Label lblWaermeerzeuger;
	@FXML private Label lblIdBrenner;
	@FXML private Label lblBrenner;
	
	private static final Logger logger = LogManager.getLogger();
	private ObservableList<String> olKunde = FXCollections.observableArrayList();
	private List<PlzOrt> plzOrt;
	private boolean anlageBearbeiten = false;
	private boolean nurBrWeBearbeiten = false;
	private boolean reload = false;
	private List<Kunde> kundenliste;
	private Kunde objKunde;
	protected AnlagenController anCo;
	private RapportController raCo;
	private Gemeinde objGemeinde;
	private PlzOrt objplzOrt;
	protected Anlagenstandort objAnlagenstandort;
	private ObservableList<String> olPlzOrt = FXCollections.observableArrayList();
	protected Waermeerzeuger objWaermeerzeuger;
	protected Brenner objBrenner;
	
	/**
	 * Konstruktor f�r den Aufruf von AnlagenVerwaltenController. Setzen des Controllers sowie Bearbeitungsmodus
	 * @author Marco Hostettler
	 * @param anCo - Anlagen Controller Objekt der Klasse AnlagenController
	 * @version 2.0
	 * 
	 */
	public AnlagenVerwaltenController(AnlagenController anCo){
		super();
		this.anCo = anCo;
		this.reload = true;
		if(anCo.clickedAnlage != null) {
			this.anlageBearbeiten = true;
			this.objAnlagenstandort = anCo.clickedAnlage;	
		}
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 2.0
	 * @param raCo  Rapport Controller Objekt der Klasse RapportController
	 *
	 * Der Konstruktor wird in der Klasse RapportController verwendet. 
	 * Durch den Konstruktor wird der zu bearbeitende Rapport �bergeben.
	 */
	public AnlagenVerwaltenController(RapportController raCo) {
		super();
		this.raCo = raCo;

		if (raCo.objAnlagenstandort != null){
			this.nurBrWeBearbeiten = true;
			this.objAnlagenstandort = raCo.objAnlagenstandort;
		}
	}
	
	/**
	 * 
	 * @author Marco Hostettler & Sven M�ller
	 * @since 2.0
	 * @param Standardparameter von implementierter Initializable Klasse
	 *
	 * Beim Laden des Fenster wird gepr�ft ob dieses aus dem Anlagen oder Rapport GUI ge�ffnet
	 * worden ist und dabei entsprechend die Felderauswahl angepasst
	 * ODER
	 * Ob eine neue Anlage erstellt werden soll
	 * 
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		
		// Wenn eine Anlage gew�hlt wurde (Bestehende Anlage bearbeiten)
		if (anlageBearbeiten || nurBrWeBearbeiten){
			String waermeerzeugerID = String.valueOf(objAnlagenstandort.getWaermeerzeuger().getId().intValue());
			String hersteller = objAnlagenstandort.getWaermeerzeuger().getWaermeerzeugermodell().getHersteller();
			String modell = objAnlagenstandort.getWaermeerzeuger().getWaermeerzeugermodell().getBezeichnung();
			String baujahr = String.valueOf(objAnlagenstandort.getWaermeerzeuger().getBaujahr().intValue());
			String BrennerID = String.valueOf(objAnlagenstandort.getBrenner().getId().intValue());
			String bhersteller = objAnlagenstandort.getBrenner().getBrennermodell().getHersteller();
			String bmodell = objAnlagenstandort.getBrenner().getBrennermodell().getBezeichnung();
			String bbaujahr = String.valueOf(objAnlagenstandort.getBrenner().getBaujahr().intValue());
			
			this.lblViewTitel.setText("Anlage bearbeiten");
			
			
			this.txtKanton.setText(objAnlagenstandort.getGemeinde().getKanton());
			this.txtGemeinde.setText(objAnlagenstandort.getGemeinde().getBezeichnung());
			this.cbKunde.setValue(objAnlagenstandort.getKunde().getNachname());
			int aID = objAnlagenstandort.getId().intValue();
			this.lblAnlageID.setText(String.valueOf(aID));
			this.txtStrasse.setText(objAnlagenstandort.getStrasse());
			int plz = objAnlagenstandort.getPlzOrt().getPlz().intValue();
			this.txtPlz.setText(String.valueOf(plz));
			this.cbOrt.setValue(objAnlagenstandort.getPlzOrt().getOrt());
			this.lblIdWaermeerzeuger.setText(waermeerzeugerID);
			this.lblWaermeerzeuger.setText(hersteller + " " + modell + " (" + baujahr + ")");
			this.lblIdBrenner.setText(BrennerID);
			this.lblBrenner.setText(bhersteller + " " + bmodell + " (" + bbaujahr + ")");
			this.objKunde = objAnlagenstandort.getKunde();
			this.objGemeinde = objAnlagenstandort.getGemeinde();
			this.objplzOrt = objAnlagenstandort.getPlzOrt();
				
		} else {
			this.objAnlagenstandort = new Anlagenstandort();
			this.objGemeinde = new Gemeinde();
			this.cbKunde.setDisable(false);
			this.txtStrasse.setDisable(false);
			this.kundenliste = anCo.mainCo.kunden;
			kundenliste.forEach(k -> olKunde.add(k.getNachname()));
			this.cbKunde.setItems(olKunde);
			this.cbKunde.setValue("Bitte w�hlen Sie...");
		}
				


	}
	/**
	 * "On Action" FXML Methode. Das Kunde-Objekt erstellen aus der Auswahl des cbKunde.
	 * @author Marco Hostettler
	 * @version 1.0
	 * @since 10.12.2015
	 */
	@FXML
	void cbKundeChanged() {
		for (Kunde k : kundenliste) {
			if (k.getNachname().equals(cbKunde.getSelectionModel().getSelectedItem())) {
				objKunde = k;
			}
		}
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * 
	 * Wird durch dem WaermeerzeugerController aufgerufen, damit der selektierte Wert �bergeben und
	 * hier angezeigt wird
	 */
	public void reloadWaermeerzeuger() {
		
		String waermeerzeugerID = String.valueOf(objAnlagenstandort.getWaermeerzeuger().getId().intValue());
		String hersteller = objAnlagenstandort.getWaermeerzeuger().getWaermeerzeugermodell().getHersteller();
		String modell = objAnlagenstandort.getWaermeerzeuger().getWaermeerzeugermodell().getBezeichnung();
		String baujahr = String.valueOf(objAnlagenstandort.getWaermeerzeuger().getBaujahr().intValue());
		
		lblIdWaermeerzeuger.setText(waermeerzeugerID);
		lblWaermeerzeuger.setText(hersteller + " " + modell + " (" + baujahr + ")");
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * 
	 * Wird durch dem BrennerController aufgerufen, damit der selektierte Wert �bergeben und
	 * hier angezeigt wird
	 */
	public void reloadBrenner() {
		
		String BrennerID = String.valueOf(objAnlagenstandort.getBrenner().getId().intValue());
		String hersteller = objAnlagenstandort.getBrenner().getBrennermodell().getHersteller();
		String modell = objAnlagenstandort.getBrenner().getBrennermodell().getBezeichnung();
		String baujahr = String.valueOf(objAnlagenstandort.getBrenner().getBaujahr().intValue());
		
		lblIdBrenner.setText(BrennerID);
		lblBrenner.setText(hersteller + " " + modell + " (" + baujahr + ")");
	}

	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * @throws IOException
	 * 
	 * �ffnet neues GUI Fenster "W�rmeerzeuger erfassen" und �bergibt dabei 
	 * sich selber (AnlagenVerwaltenController)an den aufzurufen Konstruktor WaermeerzeugerVerwalten
	 *
	 */
	@FXML
	public void btnNeuWaermeerzeugerClicked(ActionEvent event){
		Parent root;

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/WaermeerzeugerVerwalten.fxml"));			
			WaermeerzeugerVerwaltenController controller = new WaermeerzeugerVerwaltenController(this);
			fxmlLoader.setController(controller);
			root = fxmlLoader.load();
		
			Stage stage = new Stage();
			stage.setTitle("W�rmeerzeuger  hinzuf�gen");
			stage.setScene(new Scene(root));
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	/**
	 * "On Key Typed" FXML Methode. Sobald 4 Stellen eingegeben werden, wird beim Server nach den Ortschaften mit diesen 4 Zahlen als Plz nachgefragt.
	 * Diese stehen dann im CbOrt zur Auswahl
	 * @author Marco Hostettler
	 * @version 1.0
	 * @since 10.12.2015
	 */
	@FXML
	public void txtPlzChanged() {
		cbOrt.getItems().clear();
		if (txtPlz.getText().length() == 4) {

			try {
				this.plzOrt = new ArrayList<PlzOrt>(serviceProvider.getPlzOrtService().getPlzOrtByPlz(Integer.parseInt(txtPlz.getText())));
			} catch (NumberFormatException e) {
				logger.error(e.getMessage());
			} catch (RemoteException e) {
				logger.error(e.getMessage());
			}

			plzOrt.forEach(p -> olPlzOrt.add(p.getOrt()));
			this.cbOrt.setItems(olPlzOrt);
			this.cbOrt.setValue("Bitte w�hlen Sie...");

		}

	}
	/**
	 * "On Action" FXML Methode. Das PlzOrt-Objekt erstellen aus der Auswahl des cbOrt.
	 * @author Marco Hostettler
	 * @version 1.0
	 * @since 10.12.2015
	 */
	@FXML
	void cbOrtChanged() {
		for (PlzOrt p : plzOrt) {
			if (p.getOrt().equals(cbOrt.getSelectionModel().getSelectedItem())) {
				objplzOrt = p;
			}
		}

	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * @throws IOException
	 * 
	 * �ffnet neues GUI Fenster "Brenner erfassen" und �bergibt dabei 
	 * sich selber (AnlagenVerwaltenController)an den aufzurufen Konstruktor BrennerVerwalten
	 *
	 */
	@FXML
	public void btnNeuBrennerClicked(ActionEvent event){
		Parent root;

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/BrennerVerwalten.fxml"));
			BrennerVerwaltenController controller = new BrennerVerwaltenController(this);
			fxmlLoader.setController(controller);
			root = fxmlLoader.load();
			
			Stage stage = new Stage();
			stage.setTitle("Brenner erfassen");
			stage.setScene(new Scene(root));
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @author Marco Hostettler & Sven M�ller
	 * @since 2.0
	 * @param Standardparameter von implementierter Initializable Klasse
	 * 
	 * Stellt Objekt f�r Persistierung zusammen
	 * Ruft den Webservice zur Persistierung der Anlage auf
	 * Gibt entsprechende Meldung aus
	 * Updatet das enstprechene Fenster Rapport oder Anlage mit dem neuen Wert 
	 */
	@FXML public void btnSpeichernClicked (ActionEvent event){
		// Objekt Gemeinde erstellen
		
		objGemeinde.setBezeichnung(txtGemeinde.getText());
		objGemeinde.setKanton(txtKanton.getText());
		
		//Restliche Anlagenstandort-Objekte setzen
		objAnlagenstandort.setKunde(objKunde);
		objAnlagenstandort.setGemeinde(objGemeinde);
		objAnlagenstandort.setHauswart(objKunde);
		objAnlagenstandort.setPlzOrt(objplzOrt);
		//Restliche Anlagenstandort-Daten setzen
		objAnlagenstandort.setStrasse(txtStrasse.getText());
	
		String errors = validate(objAnlagenstandort);
		

		if (errors != "") {
			Alert alert = new Alert(AlertType.WARNING, errors);
			alert.show();
			logger.error("Daten unvollst�ndig/Fehlerhaft. Keine weiterleitung an Server");
		}else{
			// Wenn keine Fehler dann speichern	
			Anlagenstandort returnAnlagenstandort = new Anlagenstandort();
			//Objekt speichern----------------------------		
			try {
				if((returnAnlagenstandort = serviceProvider.getAnlagenstandortService().saveAnlagenstandort(objAnlagenstandort)) == null){
					
					Alert alert = new Alert(AlertType.ERROR, "Anlage konnte nicht gespeichert werden");
					alert.show();
					logger.error("Anlage konnte nicht in Datenbank gespeichert werden.");
					
				}else {
					Alert alert = new Alert(AlertType.INFORMATION, "Anlage wurde erfolgreich gespeichert");
					alert.showAndWait().ifPresent(response -> {
						if (response == ButtonType.OK) {
							close(event);
						}
					});
					if(nurBrWeBearbeiten){
						raCo.objAnlagenstandort = returnAnlagenstandort;
						raCo.reloadAnlagenstandortAuswahl();
					}
					if(reload){
						loadSpecificData(this);
					}
				}
			} catch (RemoteException e) {
				logger.error(e.getMessage());
			}
		}		
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * 
	 * Beim Klicken des Abbrechen Button wird das Fenster geschlossen
	 */
	@FXML
	public void btnAbbrechenClicked(ActionEvent event){
		close(event);
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * 
	 * Schliesst das Fenster
	 */
	private void close(ActionEvent e) {
		Node node = (Node) e.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		stage.close();
	}	
}
