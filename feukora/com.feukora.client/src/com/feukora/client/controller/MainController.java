package com.feukora.client.controller;

import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.client.utils.ServiceProvider;
import com.feukora.server.models.Anlagenstandort;
import com.feukora.server.models.Brenner;
import com.feukora.server.models.Brennerart;
import com.feukora.server.models.Brennermodell;
import com.feukora.server.models.Brennstoff;
import com.feukora.server.models.Gemeinde;
import com.feukora.server.models.Kontrolle;
import com.feukora.server.models.Kunde;
import com.feukora.server.models.Messergebnis;
import com.feukora.server.models.Mitarbeiter;
import com.feukora.server.models.MitarbeiterRolle;
import com.feukora.server.models.PlzOrt;
import com.feukora.server.models.Termin;
import com.feukora.server.models.Waermeerzeuger;
import com.feukora.server.models.Waermeerzeugermodell;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Controller Klasse welche die Hauptsteuerung der Applikation �bernimmt. Steuert die verschiedenen Tabs.
 * Auch verantwortlich f�r die initiale Ladung der Daten vom Server, sowie die Aktualisierung nach einer �nderung 
 * (loadSpecificData)
 * @author Marco Hostettler
 * @version 6.0
 * @since 12.12.2015
 *
 *
 */

public class MainController implements Initializable {
	@FXML
	public Pane myAnchorPane;
	@FXML
	public VBox vBox;
	@FXML
	TabPane tabPane;
	// Tabs
	@FXML
	public Tab tabWelcome;
	@FXML
	public Tab tabAnlage;
	@FXML
	public Tab tabKontrolleur;
	@FXML
	public Tab tabKunden;
	@FXML
	public Tab tabWaermeerzeuger;
	@FXML
	public Tab tabBrenner;
	@FXML
	public Tab tabTermin;
	@FXML
	public Tab tabRapport;
	@FXML
	public Tab tabMitarbeiter;
	@FXML
	public Label lblWelcome;

	private boolean tabSelection = false;
	private String role;
	private static MainController mainController;
	protected KundeController kcontroller;
	private KontrolleurVerwaltenController koVeController;
	private KundeVerwaltenController kuVeController;
	private MitarbeiterVerwaltenController mVeController;
	private KontrolleurController kocontroller;
	private MitarbeiterController mcontroller;
	private EinsatzplanController econtroller;
	protected EinsatzplanVerwaltenController evecontroller;
	private AnlagenController acontroller;
	private AnlagenVerwaltenController aveController;

	private WaermeerzeugerModellController weMoController;
	private BrennerModellController beMoController;
	private Validator validator;
	private ValidatorFactory validatorFactory;
	protected Stage mainstage;

	protected ServiceProvider serviceProvider;
	// F�r Objektvalidierung
	private static ValidatorFactory objValFactory = Validation.buildDefaultValidatorFactory();

	protected List<Kunde> kunden;
	protected List<Mitarbeiter> mitarbeiter;
	protected Mitarbeiter loggedInMitarbeiter;

	protected List<Anlagenstandort> anlagenListe;
	List<Brennerart> brennerartListe;
	List<Brennstoff> brennstoffListe;
	List<Waermeerzeuger> waermeerzeugerListe;
	protected List<Waermeerzeugermodell> waermeerzeugerModellListe;
	List<Brenner> brennerListe;
	List<Brennermodell> brennerModellListe;
	List<Termin> termine;
	// Logger
	private static final Logger logger = LogManager.getLogger();

	public MainController() {
		serviceProvider = new ServiceProvider();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		mainController = this;
		loadData();

	}
	/**
	 * Methode um die Applikation f�r die jeweiligen Benutzer vorzubereiten. 
	 * Umsetzung des Rollenkonzeptes basierend der rolle des eingew�hlten Benutzers (loggedInMitarbeiter.getRole)
	 * 	
	 */
	private void hideTabs() {
		
		switch (role) {
		case "Sachbearbeiter":

			try {
				tabPane.getTabs().remove(tabMitarbeiter);
			} catch (Exception e1) {
				logger.error(role + " Rolle; Tabs konnten nicht ausgeblendet werden");
				e1.printStackTrace();
			}
			break;
		case "Kontrolleur":
			try {
				tabPane.getTabs().remove(tabKunden);
				tabPane.getTabs().remove(tabMitarbeiter);
				tabPane.getTabs().remove(tabKontrolleur);
			} catch (Exception e1) {
				logger.error(role + " Rolle; Tabs konnten nicht ausgeblendet werden");
				e1.printStackTrace();
			}
			break;
		case "Administrator":
			try {
				tabPane.getTabs().remove(tabAnlage);
				tabPane.getTabs().remove(tabKontrolleur);
				tabPane.getTabs().remove(tabKunden);
				tabPane.getTabs().remove(tabTermin);

			} catch (Exception e) {
				logger.error(role + " Rolle; Tabs konnten nicht ausgeblendet werden");
				e.printStackTrace();
			}
			break;
		}

	}

	/**
	 * L�dt initial beim �ffnen der Applikation die ben�tigten Daten vom Server
	 * @versin 3.0
	 */
	public void loadData() {

		try {
			this.kunden = new ArrayList<Kunde>(serviceProvider.getKundeService().getListKunde());

			this.mitarbeiter = new ArrayList<Mitarbeiter>(serviceProvider.getMitarbeiterService().getListMitarbeiter());

			this.anlagenListe = new ArrayList<Anlagenstandort>(serviceProvider.getAnlagenstandortService().getListAnlagenstandort());

			this.brennstoffListe = new ArrayList<Brennstoff>(serviceProvider.getBrennstoffService().getListBrennstoff());

			this.waermeerzeugerModellListe = new ArrayList<Waermeerzeugermodell>(serviceProvider
					.getWaermeerzeugermodellService().getListWaermeerzeugermodell());

			this.waermeerzeugerListe = new ArrayList<Waermeerzeuger>(serviceProvider.getWaermeerzeugerService().getListWaermeerzeuger());

			this.brennerModellListe = new ArrayList<Brennermodell>(serviceProvider.getBrennermodellService().getListBrennermodell());

			this.termine = new ArrayList<Termin>(serviceProvider.getTerminService().getListTermin());

		} catch (Exception e) {
			logger.error("Die Daten konnten nicht vom Server geladen werden" + e);
			Alert alert = new Alert(AlertType.ERROR,
					"Die FEUKORA-Daten konnten nicht geladen werden. Bitte wenden Sie sich an Ihren Systemadministraotr");
			alert.show();
		}

	}

	/**
	 * Methode um Daten von einem spezifischen Objekt nach dessen Modifikation zu laden. Ggibt diese dem jeweiligen Kontroller Objekt zur�ck
	 * zu laden
	 * 
	 * @param Object
	 *            von jeweiligen Controllerklasse
	 * @return List von spezifischen Objekten
	 * @version 5.0
	 */
	public List<?> loadSpecificData(Object o) {
		try {
			String className = o.getClass().getSimpleName();
			switch (className) {

			case "KundeVerwaltenController":
				mainController.kunden = new ArrayList<Kunde>(serviceProvider.getKundeService().getListKunde());
				mainController.kuVeController = (KundeVerwaltenController) o;
				mainController.kuVeController.kuCo.updateData(mainController.kunden);
				break;
			case "KundeController":
				mainController.kunden = new ArrayList<Kunde>(serviceProvider.getKundeService().getListKunde());
				mainController.kcontroller = (KundeController) o;
				mainController.kcontroller.updateData(mainController.kunden);
				break;
			case "KontrolleurVerwaltenController":
				mainController.mitarbeiter = new ArrayList<Mitarbeiter>(serviceProvider.getMitarbeiterService().getListMitarbeiter());
				mainController.koVeController = (KontrolleurVerwaltenController) o;
				mainController.koVeController.koCo.updateData(mainController.mitarbeiter);
				break;
			case "KontrolleurController":
				mainController.mitarbeiter = new ArrayList<Mitarbeiter>(serviceProvider.getMitarbeiterService().getListMitarbeiter());
				mainController.kocontroller = (KontrolleurController) o;
				mainController.kocontroller.updateData(mainController.mitarbeiter);
				break;
			case "MitarbeiterVerwaltenController":
				mainController.mitarbeiter = new ArrayList<Mitarbeiter>(serviceProvider.getMitarbeiterService().getListMitarbeiter());
				mainController.mVeController = (MitarbeiterVerwaltenController) o;
				mainController.mVeController.miCo.updateData(mainController.mitarbeiter);
				break;
			case "MitarbeiterController":
				mainController.mitarbeiter = new ArrayList<Mitarbeiter>(serviceProvider.getMitarbeiterService().getListMitarbeiter());
				mainController.mcontroller = (MitarbeiterController) o;
				mainController.mcontroller.updateData(mainController.mitarbeiter);
				break;
			case "WaermeerzeugerModellController":
				mainController.waermeerzeugerModellListe = new ArrayList<Waermeerzeugermodell>(
						serviceProvider.getWaermeerzeugermodellService().getListWaermeerzeugermodell());
				mainController.weMoController = (WaermeerzeugerModellController) o;
				mainController.weMoController.updateTable(mainController.waermeerzeugerModellListe);
				break;
			case "BrennerModellController":
				mainController.brennerModellListe = new ArrayList<Brennermodell>(serviceProvider.getBrennermodellService().getListBrennermodell());
				mainController.beMoController = (BrennerModellController) o;
				mainController.beMoController.updateTable(mainController.brennerModellListe);
				break;
			case "AnlagenVerwaltenController":
				mainController.anlagenListe = new ArrayList<Anlagenstandort>(serviceProvider.getAnlagenstandortService().getListAnlagenstandort());
				mainController.aveController = (AnlagenVerwaltenController) o;
				mainController.aveController.anCo.updateData(mainController.anlagenListe);
				break;
			case "AnlagenController":
				mainController.anlagenListe = new ArrayList<Anlagenstandort>(serviceProvider.getAnlagenstandortService().getListAnlagenstandort());
				mainController.acontroller = (AnlagenController) o;
				mainController.acontroller.updateData(mainController.anlagenListe);
				break;
			

			}
		} catch (Exception e) {
			logger.error("Konnte die spezifischen Daten nicht vom Server laden: " + e);
			Alert alert = new Alert(AlertType.ERROR,
					"Die FEUKORA-Daten konnten nicht aktualisiert werden. Bitte wenden Sie sich an Ihren Systemadministraotr");
			alert.show();

		}
		return null;
	}


	/**
	 * Setzt den eingew�hlten Mitarbeiter
	 * @param m - Mitarbeiter Objekt welches vom LoginController �bergeben wird
	 */
	public void	setLoggedInMitarbeiter(Mitarbeiter m) {
		this.loggedInMitarbeiter = m;
		this.role = loggedInMitarbeiter.getRolle().getBezeichnung();
		hideTabs();
		}

	/**
	 * Methode um die Objekte zu validieren, bevor sie in DB auf dem Server
	 * gespeichert werden
	 * 
	 * @author Marco Hostettler
		 * @param Object - Spezifisches Objekt welches gespeichert werden soll
	 *            
	 * @return error liste
	 */
	public String validate(Anlagenstandort object) {
		String errors = "";
		Collection<String> violations;
		try {
			violations = serviceProvider.getValidationService().validateAnlagenstandort(object);
			for (String error : violations) {
				errors += error + System.getProperty("line.separator");
			}
			return errors;
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		return errors;
	}

	public String validate(Brennerart object) {
		String errors = "";
		Collection<String> violations;
		try {
			violations = serviceProvider.getValidationService().validateBrennerart(object);
			for (String error : violations) {
				errors += error + System.getProperty("line.separator");
			}
			return errors;
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		return errors;
	}

	public String validate(Brenner object) {
		String errors = "";
		Collection<String> violations;
		try {
			violations = serviceProvider.getValidationService().validateBrenner(object);
			for (String error : violations) {
				errors += error + System.getProperty("line.separator");
			}
			return errors;
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		return errors;
	}

	public String validate(Brennermodell object) {
		String errors = "";
		Collection<String> violations;
		try {
			violations = serviceProvider.getValidationService().validateBrennermodell(object);
			for (String error : violations) {
				errors += error + System.getProperty("line.separator");
			}
			return errors;
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		return errors;
	}

	public String validate(Brennstoff object) {
		String errors = "";
		Collection<String> violations;
		try {
			violations = serviceProvider.getValidationService().validateBrennstoff(object);
			for (String error : violations) {
				errors += error + System.getProperty("line.separator");
			}
			return errors;
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		return errors;
	}

	public String validate(Gemeinde object) {
		String errors = "";
		Collection<String> violations;
		try {
			violations = serviceProvider.getValidationService().validateGemeinde(object);
			for (String error : violations) {
				errors += error + System.getProperty("line.separator");
			}
			return errors;
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		return errors;
	}

	public String validate(Kontrolle object) {
		String errors = "";
		Collection<String> violations;
		try {
			violations = serviceProvider.getValidationService().validateKontrolle(object);
			for (String error : violations) {
				errors += error + System.getProperty("line.separator");
			}
			return errors;
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		return errors;
	}

	public String validate(Kunde object) {
		String errors = "";
		Collection<String> violations;
		try {
			violations = serviceProvider.getValidationService().validateKunde(object);
			for (String error : violations) {
				errors += error + System.getProperty("line.separator");
			}
			return errors;
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		return errors;
	}

	public String validate(Messergebnis object) {
		String errors = "";
		Collection<String> violations;
		try {
			violations = serviceProvider.getValidationService().validateMessergebnis(object);
			for (String error : violations) {
				errors += error + System.getProperty("line.separator");
			}
			return errors;
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		return errors;
	}

	public String validate(Mitarbeiter object) {
		String errors = "";
		Collection<String> violations;
		try {
			violations = serviceProvider.getValidationService().validateMitarbeiter(object);
			for (String error : violations) {
				errors += error + System.getProperty("line.separator");
			}
			return errors;
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		return errors;
	}

	public String validate(MitarbeiterRolle object) {
		String errors = "";
		Collection<String> violations;
		try {
			violations = serviceProvider.getValidationService().validateMitarbeiterRolle(object);
			for (String error : violations) {
				errors += error + System.getProperty("line.separator");
			}
			return errors;
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		return errors;
	}

	public String validate(PlzOrt object) {
		String errors = "";
		Collection<String> violations;
		try {
			violations = serviceProvider.getValidationService().validatePlzOrt(object);
			for (String error : violations) {
				errors += error + System.getProperty("line.separator");
			}
			return errors;
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		return errors;
	}

	public String validate(Termin object) {
		String errors = "";
		Collection<String> violations;
		try {
			violations = serviceProvider.getValidationService().validateTermin(object);
			for (String error : violations) {
				errors += error + System.getProperty("line.separator");
			}
			return errors;
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		return errors;
	}

	public String validate(Waermeerzeuger object) {
		String errors = "";
		Collection<String> violations;
		try {
			violations = serviceProvider.getValidationService().validateWaermeerzeuger(object);
			for (String error : violations) {
				errors += error + System.getProperty("line.separator");
			}
			return errors;
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		return errors;
	}

	public String validate(Waermeerzeugermodell object) {
		String errors = "";
		Collection<String> violations;
		try {
			violations = serviceProvider.getValidationService().validateWaermeerzeugermodell(object);
			for (String error : violations) {
				errors += error + System.getProperty("line.separator");
			}
			return errors;
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		return errors;
	}

	/**
	 * Reagiert auf tabMitarbeiter und l�dt die entsprechende View
	 * 
	 * @param event
	 */
	@FXML
	void tabMitarbeiterSelected(Event event) {
		String ressource = null;
		if (checkTab()) {

			try {
				ressource = "/com/feukora/client/views/Mitarbeiter.fxml";
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(ressource));

				this.mcontroller = new MitarbeiterController(this);
				fxmlLoader.setController(mcontroller);

				Pane myPane = fxmlLoader.load();
				myAnchorPane.getChildren().clear();
				myAnchorPane.getChildren().setAll(myPane);

			} catch (IOException e) {
				logger.error("Die view: '" + ressource + " konnte nicht geladen werden");
				
			}

		}
	}

	/**
	 * Reagiert auf tabAnlagen und l�dt die entsprechende View
	 * 
	 * @param event
	 */
	@FXML
	void tabAnlageSelected(Event event) {
		String ressource = null;
		if (checkTab()) {
			ressource = "/com/feukora/client/views/Anlagen.fxml";

			try {

				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(ressource));

				this.acontroller = new AnlagenController(this);
				fxmlLoader.setController(this.acontroller);

				Pane myPane = fxmlLoader.load();
				myAnchorPane.getChildren().clear();
				myAnchorPane.getChildren().setAll(myPane);

			} catch (IOException e) {
				logger.error("Die view: '" + ressource + " konnte nicht geladen werden");
			
			}

		}
	}

	@FXML
	void tabWelcomeSelected(Event event) {
		tabPane.getTabs().remove(tabWelcome);
		
	}
	/**
	 * Reagiert auf tabKontrolleur und l�dt die entsprechende View
	 * 
	 * @param event
	 */
	@FXML
	void tabKontrolleurSelected(Event event) {
		String ressource = null;
		if (checkTab()) {

			ressource = "/com/feukora/client/views/Kontrolleur.fxml";
			
			try {

				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(ressource));

				this.kocontroller = new KontrolleurController(this);
				fxmlLoader.setController(kocontroller);

				Pane myPane = fxmlLoader.load();
				myAnchorPane.getChildren().clear();
				myAnchorPane.getChildren().setAll(myPane);

			} catch (IOException e) {
				logger.error("Die view: '" + ressource + " konnte nicht geladen werden");
				
			}

		}

	}

	/**
	 * Reagiert auf tabKunden und l�dt die entsprechende View
	 * 
	 * @param event
	 */
	@FXML
	void tabKundendatenSelected(Event event) {
		String ressource = null;
		if (checkTab()) {
			ressource = "/com/feukora/client/views/Kunde.fxml";
			try {

				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(ressource));

				this.kcontroller = new KundeController(this);
				fxmlLoader.setController(kcontroller);

				Pane myPane = fxmlLoader.load();
				myAnchorPane.getChildren().clear();
				myAnchorPane.getChildren().setAll(myPane);

			} catch (IOException e) {
				logger.error("Die view: '" + ressource + " konnte nicht geladen werden");
				
			}

		}
	}

	/**
	 * Reagiert auf tabTermin und l�dt die entsprechende View
	 * @param event
	 */
	@FXML
	void tabTerminSelected(Event event) {

		String ressource = null;
		if (checkTab()) {

			try {

				ressource = "/com/feukora/client/views/Einsatzplan S.fxml";
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(ressource));

				this.econtroller = new EinsatzplanController(this);
				fxmlLoader.setController(econtroller);

				Pane myPane = fxmlLoader.load();
				myAnchorPane.getChildren().clear();
				myAnchorPane.getChildren().setAll(myPane);

			} catch (IOException e) {
				logger.error("Die view: '" + ressource + " konnte nicht geladen werden");
				
			}

		}
	}

	private boolean checkTab() {
		tabSelection = !tabSelection;
		return tabSelection;
	}

	public void setStage(Stage stage) {
		this.mainstage = stage;

	}

}
