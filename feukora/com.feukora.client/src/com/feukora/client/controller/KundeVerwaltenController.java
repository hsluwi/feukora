package com.feukora.client.controller;

import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Kunde;
import com.feukora.server.models.PlzOrt;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Kontroller Klasse f�r die View KundeVerwalten.fxml
 * @author Marco Hostettler
 * @version 3.0
 * @since 28.11.2015
 *
 */
public class KundeVerwaltenController extends MainController implements Initializable {
	@FXML
	private Button tbnAbbrechen;
	@FXML
	private Label lblKundenID;
	@FXML
	private Label lblViewTitel;
	@FXML
	private AnchorPane anpKundenErfassen;

	@FXML
	private TextField txtFirma;
	@FXML
	private TextField txtVorname;
	@FXML
	private TextField txtNachname;
	@FXML
	private TextField txtStrasse;
	@FXML
	private TextField txtPlz;
	@FXML
	private TextField txtOrt;
	@FXML
	private TextField txtTelefon;
	@FXML
	private TextField txtMobil;
	@FXML
	private TextField txtMail;
	@FXML
	private ComboBox<String> cbKundenart;
	@FXML
	private ComboBox<String> cbOrt;
	private ObservableList<String> kundenartDaten = FXCollections.observableArrayList("Haushalt", "Verwaltung");
	private List<PlzOrt> plzOrt;
	private ObservableList<String> olPlzOrt = FXCollections.observableArrayList();
	private Kunde kunde;
	private PlzOrt obPlzOrt;
	protected KundeController kuCo;

	private static final Logger logger = LogManager.getLogger();

	public KundeVerwaltenController(KundeController kuCo) {
		super();
		this.kuCo = kuCo;

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// Initialisiere ComboBox kundenartDaten
		cbKundenart.setItems(kundenartDaten);
		if (kuCo.clickedKunde != null) {
			this.lblViewTitel.setText("Kunde bearbeiten");
			this.kunde = kuCo.clickedKunde;

			int kundID = kunde.getId().intValue();
			this.lblKundenID.setText(String.valueOf(kundID));
			this.cbKundenart.setValue(kunde.getKundenArt());
			this.txtFirma.setText(kunde.getFirma());
			this.txtVorname.setText(kunde.getVorname());
			this.txtNachname.setText(kunde.getNachname());
			this.txtStrasse.setText(kunde.getStrasse());
			int plz = kunde.getPlzOrt().getPlz();
			this.txtPlz.setText(String.valueOf(plz));
			this.cbOrt.setValue(kunde.getPlzOrt().getOrt());
			this.txtTelefon.setText(kunde.getTelefon());
			this.txtMobil.setText(kunde.getMobile());
			this.txtMail.setText(kunde.getEmail());
			this.obPlzOrt = kunde.getPlzOrt();
		} else {
			this.kunde = new Kunde();
		}

	}

	@FXML
	public void btnAbbrechenClicked(ActionEvent event) {

		close(event);

	}

	/**
	 * "On Key Typed" FXML Methode. Sobald 4 Stellen eingegeben werden, wird
	 * beim Server nach den Ortschaften mit diesen 4 Zahlen als Plz nachgefragt.
	 * Diese stehen dann im CbOrt zur Auswahl
	 * 
	 * @author Marco Hostettler
	 * @version 1.0
	 * @since 1.12.2015
	 */
	@FXML
	public void txtPlzChanged() {
		cbOrt.getItems().clear();
		if (txtPlz.getText().length() == 4) {

			try {
				this.plzOrt = new ArrayList<PlzOrt>(serviceProvider.getPlzOrtService().getPlzOrtByPlz(Integer.parseInt(txtPlz.getText())));
			} catch (NumberFormatException e) {
				logger.error(e.getMessage());
			} catch (RemoteException e) {
				logger.error(e.getMessage());
			}

			plzOrt.forEach(p -> olPlzOrt.add(p.getOrt()));
			this.cbOrt.setItems(olPlzOrt);
			this.cbOrt.setValue("Bitte w�hlen Sie...");

		}

	}

	/**
	 * "On Action" FXML Methode. Das PlzOrt-Objekt erstellen aus der Auswahl des
	 * cbOrt.
	 * 
	 * @author Marco Hostettler
	 * @version 1.0
	 * @since 1.12.2015
	 */
	@FXML
	void cbOrtChanged() {
		for (PlzOrt p : plzOrt) {
			if (p.getOrt().equals(cbOrt.getSelectionModel().getSelectedItem())) {
				obPlzOrt = p;
			}
		}

	}

	private void close(ActionEvent e) {
		Node node = (Node) e.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		stage.close();

	}
	/**
	 * Methode welche das Kunden-Objekt in der DB speichert
	 * @param event
	 */
	@FXML
	public void btnSpeichernClicked(ActionEvent event) {

		this.kunde.setFirma(txtFirma.getText());
		this.kunde.setVorname(txtVorname.getText());
		this.kunde.setNachname(txtNachname.getText());
		this.kunde.setStrasse(txtStrasse.getText());
		this.kunde.setTelefon(txtTelefon.getText());
		this.kunde.setMobile(txtMobil.getText());
		this.kunde.setEmail(txtMail.getText());
		this.kunde.setKundenArt(cbKundenart.getValue());
		this.kunde.setPlzOrt(obPlzOrt);

		// Objekt validieren
		String errors = validate(kunde);
		// Ausgabe der Validierungs Fehler
		if (errors != "") {
			Alert alert = new Alert(AlertType.WARNING, errors);
			alert.show();
			logger.error("Daten unvollst�ndig/Fehlerhaft. Keine weiterleitung an Server");
		}

		// Wenn keine Fehler dann speichern
		else {
			Kunde returnKunde = new Kunde();
			try {
				if ((returnKunde = serviceProvider.getKundeService().saveKunde(kunde)) == null) {

					Alert alert = new Alert(AlertType.ERROR, "Kunde konnte nicht gespeichert werden");
					alert.show();
					logger.error("Kunde konnte nicht in Datenbank gespeichert werden.");
				}

				else {
					Alert alert = new Alert(AlertType.INFORMATION, "Kunde wurde erfolgreich gespeichert");
					alert.showAndWait().ifPresent(response -> {
						if (response == ButtonType.OK) {
							close(event);
						}
					});

					// Reload Daten vom Server, sowie Reload der TableView

					loadSpecificData(this);
					kuCo.txtSuche.clear();
					kuCo.txtSuche.setText(returnKunde.getNachname());
					kuCo.tblKunden.getSelectionModel().selectFirst();

				}
			} catch (RemoteException e) {
				logger.error(e.getMessage());
			}

		}

	};

}
