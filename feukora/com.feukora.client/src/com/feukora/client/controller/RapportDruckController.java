package com.feukora.client.controller;

import java.net.URL;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Anlagenstandort;
import com.feukora.server.models.Kontrolle;
import com.feukora.server.models.Messergebnis;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;

/**
 * 
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 * 
 * Zeigt die Druckansicht des Feuerungsrapports an.
 *
 */
public class RapportDruckController extends MainController implements Initializable {
	
	@FXML private Label lblKontrollart;
	@FXML private Label lblAnlagenstandort;
	@FXML private Label lblGemeinde;
	@FXML private Label lblKunde;
	@FXML private Label lblHauswart;
	@FXML private Label lblWaermeerzeugerZulassung;
	@FXML private Label lblWEBaujahr;
	@FXML private Label lblWEmodell;
	@FXML private Label lblBrennstoff;
	@FXML private Label lblBrennerZulassung;
	@FXML private Label lblBRBaujahr;
	@FXML private Label lblBrennermodell;
	@FXML private Label lblBrennerart;
	@FXML private Label lblLeistung;
	@FXML private Label lblKontrolleur;
	@FXML private Label lblRusszal11;
	@FXML private Label lblRusszal12;
	@FXML private Label lblRusszahl21;
	@FXML private Label lblRusszahl22;
	@FXML private Label lblCO11;
	@FXML private Label lblCO12;
	@FXML private Label lblCO21;
	@FXML private Label lblCO22;
	@FXML private Label lblNo11;
	@FXML private Label lblNo12;
	@FXML private Label lblNo21;
	@FXML private Label lblNo22;
	@FXML private Label lblAbgastemperatur11;
	@FXML private Label lblAbgastemperatur12;
	@FXML private Label lblAbgastemperatur21;
	@FXML private Label lblAbgastemperatur22;
	@FXML private Label lblWaermeerzeugertem11;
	@FXML private Label lblWaermeerzeugertem12;
	@FXML private Label lblWaermeerzeugertem21;
	@FXML private Label lblWaermeerzeugertem22;
	@FXML private Label lblVerbrennungsllufttem11;
	@FXML private Label lblVerbrennungsllufttem12;
	@FXML private Label lblVerbrennungsllufttem21;
	@FXML private Label lblVerbrennungsllufttem22;
	@FXML private Label lblO2Gehaltin11;
	@FXML private Label lblO2Gehaltin12;
	@FXML private Label lblO2Gehaltin21;
	@FXML private Label lblO2Gehaltin22;
	@FXML private Label lblAbgasverluste11;
	@FXML private Label lblAbgasverluste12;
	@FXML private Label lblAbgasverluste21;
	@FXML private Label lblAbgasverluste22;
	
	@FXML private ToggleGroup kontrollart;
	@FXML private ToggleGroup m1Stufe1;
	@FXML private ToggleGroup m2Stufe1;
	@FXML private ToggleGroup m1Stufe2;
	@FXML private ToggleGroup m2Stufe2;
	@FXML private ToggleGroup beurteilung;
	@FXML private ToggleGroup wVorgehen;

	@FXML private RadioButton rbtnRoutinekontrolle;
	@FXML private RadioButton rbtnAbnahmekontrolle;
	@FXML private RadioButton rbtnJa11;
	@FXML private RadioButton rbtnNein11;
	@FXML private RadioButton rbtnJa12;
	@FXML private RadioButton rbtnNein12;
	@FXML private RadioButton rbtnJa21;
	@FXML private RadioButton rbtnNein21;
	@FXML private RadioButton rbtnJa22;
	@FXML private RadioButton rbtnNein22;
	@FXML private RadioButton rbtnBeurteilung1;
	@FXML private RadioButton rbtnBeurteilung2;
	@FXML private RadioButton rbtnWeiteresVorgehen1;
	@FXML private RadioButton rbtnWeiteresVorgehen2;
	

	@FXML private Button btnDrucken;
	@FXML private Button btnAbbrechen;
	
	@FXML private CheckBox cbRusszahlen;
	@FXML private CheckBox cbOelanteile;
	@FXML private CheckBox cbCO;
	@FXML private CheckBox cbNO2;
	@FXML private CheckBox cbAbgasverluste;
	@FXML private CheckBox cbBestaetigung; 

	@FXML private TextArea txtBemerkung;
	
	@FXML private AnchorPane mainPane;
	
	private String errors = null;
	
	private int messdurchlauf = 0;
	
	private Kontrolle objKontrolle;
	protected Anlagenstandort objAnlagenstandort;
	
	private static final Logger logger = LogManager.getLogger();

	public RapportDruckController() {
		super();
	}
	/**
	 * 
	 * 
	 * @param Kontrolle objKontrolle
	 *
	 * Kontrollobjekt welches dargestellt wird, muss �bergeben werden.
	 *
	 */
	public RapportDruckController(Kontrolle objKontrolle) {
		this.objKontrolle = objKontrolle;
	}

	/**
	 * 
	 * @param Standardparameter von implementierter Initializable Klasse
	 * 
	 * Setzt die Hintergrundfarbe und ruft loadData() zum Laden der Daten auf
	 *
	 */
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// Hintergrund weiss setzen f�r Ausdruck
	    mainPane.setStyle("-fx-background-color: #ffffff;");
	    loadData();
	
	}

	/**
	 * 
	 * Beim Klicken des Abbrechen-Buttons wird das Fenster geschlossen
	 * @param ActionEvent event
	 *
	 */
	@FXML
	public void btnAbbrechenClicked(ActionEvent event) {
		
		close(event);
		
	}
	
	
	/**
	 * 
	 * Schliesst das Fenster
	 *
	 */
	private void close(ActionEvent e) {
		
		Node node = (Node) e.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		stage.close();	
	}

	

	
	/**
	 * Druckt den Feuerungsrapport beim Klick auf den Drucken-Button
	 * @param event
	 */
	@FXML
	public void btnDruckenClicked(ActionEvent event) { 
		
		
		    Printer printer = Printer.getDefaultPrinter();
			    PageLayout pageLayout = printer.createPageLayout(Paper.A4, PageOrientation.PORTRAIT, Printer.MarginType.HARDWARE_MINIMUM);
			    PrinterJob printerJob = PrinterJob.createPrinterJob();
			    double scaleX
			        = pageLayout.getPrintableWidth() / mainPane.getBoundsInParent().getWidth();
			    double scaleY
			        = pageLayout.getPrintableHeight() / mainPane.getBoundsInParent().getHeight();
			    Scale scale = new Scale(scaleX, scaleY);

			    btnAbbrechen.setVisible(false);
			    btnDrucken.setVisible(false);
			    
			    mainPane.getTransforms().add(scale);
			    printerJob.getJobSettings().setJobName("Rapport FEUKORA");
			    if (printerJob != null && printerJob.showPrintDialog(mainPane.getScene().getWindow())) {
			      if (printerJob.printPage(pageLayout, mainPane)) {
			    	  printerJob.endJob();
			      }
			    }
			    mainPane.getTransforms().remove(scale);
			    btnAbbrechen.setVisible(true);
			    btnDrucken.setVisible(true);
			  }
	
	/**
	 * L�dt das Kontrolle-Objekt in die entsprechenden Felder des Rapports
	 */
	public void loadData(){
		lblKontrollart.setText(objKontrolle.getKontrollart());
		lblAnlagenstandort.setText(objKontrolle.getTermin().getAnlagenstandort().getStrasse()
				+ ", " +objKontrolle.getTermin().getAnlagenstandort().getPlzOrt().getPlz() + " " +
				objKontrolle.getTermin().getAnlagenstandort().getPlzOrt().getOrt());
		lblGemeinde.setText(objKontrolle.getTermin().getAnlagenstandort().getGemeinde().getBezeichnung() + 
				" (" + objKontrolle.getTermin().getAnlagenstandort().getGemeinde().getKanton() + ")");
		lblKunde.setText(objKontrolle.getTermin().getAnlagenstandort().getKunde().getNachname() + " " +
				objKontrolle.getTermin().getAnlagenstandort().getKunde().getVorname() + ", " +
				objKontrolle.getTermin().getAnlagenstandort().getKunde().getFirma() + ", " +
				objKontrolle.getTermin().getAnlagenstandort().getKunde().getStrasse() + ", " +
				objKontrolle.getTermin().getAnlagenstandort().getKunde().getPlzOrt().getPlz() + " " +
				objKontrolle.getTermin().getAnlagenstandort().getKunde().getPlzOrt().getOrt());
		lblHauswart.setText(objKontrolle.getTermin().getAnlagenstandort().getHauswart().getNachname() + " " +
				objKontrolle.getTermin().getAnlagenstandort().getHauswart().getVorname() + ", " +
				objKontrolle.getTermin().getAnlagenstandort().getHauswart().getFirma() + ", " +
				objKontrolle.getTermin().getAnlagenstandort().getHauswart().getStrasse() + ", " +
				objKontrolle.getTermin().getAnlagenstandort().getHauswart().getPlzOrt().getPlz() + " " +
				objKontrolle.getTermin().getAnlagenstandort().getHauswart().getPlzOrt().getOrt());
		lblWaermeerzeugerZulassung.setText(objKontrolle.getTermin().getAnlagenstandort().getWaermeerzeuger().
				getZulassungsNr());
		lblWEBaujahr.setText(objKontrolle.getTermin().getAnlagenstandort().getWaermeerzeuger().getBaujahr().
				toString());
		lblWEmodell.setText(objKontrolle.getTermin().getAnlagenstandort().getWaermeerzeuger().
				getWaermeerzeugermodell().getHersteller() + " "	+ objKontrolle.getTermin().getAnlagenstandort().
				getWaermeerzeuger().getWaermeerzeugermodell().getBezeichnung());
		lblBrennerZulassung.setText(objKontrolle.getTermin().getAnlagenstandort().getBrenner().getZulassungsNr());
		lblBRBaujahr.setText(objKontrolle.getTermin().getAnlagenstandort().getBrenner().getBaujahr().toString());
		lblBrennermodell.setText(objKontrolle.getTermin().getAnlagenstandort().getBrenner().getBrennermodell().
				getHersteller() + " "	+ objKontrolle.getTermin().getAnlagenstandort().
				getBrenner().getBrennermodell().getBezeichnung());
		lblBrennstoff.setText(objKontrolle.getTermin().getAnlagenstandort().getWaermeerzeuger().
				getWaermeerzeugermodell().getBrennstoff().getBezeichnung());
		lblBrennerart.setText(objKontrolle.getTermin().getAnlagenstandort().getBrenner().getBrennermodell().
				getBrennerart().getBezeichnung());
		lblLeistung.setText(objKontrolle.getTermin().getAnlagenstandort().getBrenner().getBrennermodell().
				getFeuerungswaermeleistung().toString() + " kW");
		lblKontrolleur.setText(objKontrolle.getTermin().getMitarbeiter().getNachname() + " " + 
				objKontrolle.getTermin().getMitarbeiter().getVorname());
		
		messdurchlauf = 0;
		System.out.println(objKontrolle.getId());
		try {
			Collection<Messergebnis> q = serviceProvider.getMessergebnisService().getMessergebnisByKontrolleId(objKontrolle.getId());
			serviceProvider.getMessergebnisService().getMessergebnisByKontrolleId(objKontrolle.getId()).forEach(x -> {
				messdurchlauf++;
				switch (messdurchlauf) {
				case 1: 		lblRusszal11.setText(x.getRusszahl().toString());
								lblCO11.setText(x.getCo3PVol().toString());
								lblNo11.setText(x.getNox3PVol().toString());
								lblAbgastemperatur11.setText(x.getAbgastemperatur().toString());
								lblWaermeerzeugertem11.setText(x.getWaermeErzTemp().toString());
								lblVerbrennungsllufttem11.setText(x.getVerbrennLuftTemp().toString());
								lblO2Gehaltin11.setText(x.getO2Gehalt().toString());
								lblAbgasverluste11.setText(x.getAbgasverluste().toString());
								if (x.getOelanteile()) {
									rbtnJa11.setSelected(true);
									rbtnNein11.setSelected(false);
								}
								else {
									rbtnNein11.setSelected(true);
									rbtnJa11.setSelected(false);
								}
								break;
				case 2:
						 		lblRusszal12.setText(x.getRusszahl().toString());
								lblCO12.setText(x.getCo3PVol().toString());
								lblNo12.setText(x.getNox3PVol().toString());
								lblAbgastemperatur12.setText(x.getAbgastemperatur().toString());
								lblWaermeerzeugertem12.setText(x.getWaermeErzTemp().toString());
								lblVerbrennungsllufttem12.setText(x.getVerbrennLuftTemp().toString());
								lblO2Gehaltin12.setText(x.getO2Gehalt().toString());
								lblAbgasverluste12.setText(x.getAbgasverluste().toString());
								if (x.getOelanteile()) {
									rbtnJa12.setSelected(true);
									rbtnNein12.setSelected(false);
								}
								else {
									rbtnNein12.setSelected(true);
									rbtnJa12.setSelected(false);
								}
								break;
					
				case 3: 		lblRusszahl21.setText(x.getRusszahl().toString());
								lblCO21.setText(x.getCo3PVol().toString());
								lblNo21.setText(x.getNox3PVol().toString());
								lblAbgastemperatur21.setText(x.getAbgastemperatur().toString());
								lblWaermeerzeugertem21.setText(x.getWaermeErzTemp().toString());
								lblVerbrennungsllufttem21.setText(x.getVerbrennLuftTemp().toString());
								lblO2Gehaltin21.setText(x.getO2Gehalt().toString());
								lblAbgasverluste21.setText(x.getAbgasverluste().toString());
								if (x.getOelanteile()) {
									rbtnJa21.setSelected(true);
									rbtnNein21.setSelected(false);
								}
								else {
									rbtnNein21.setSelected(true);
									rbtnJa21.setSelected(false);
								}
								break;
				case 4: 		lblRusszahl22.setText(x.getRusszahl().toString());
								lblCO22.setText(x.getCo3PVol().toString());
								lblNo22.setText(x.getNox3PVol().toString());
								lblAbgastemperatur22.setText(x.getAbgastemperatur().toString());
								lblWaermeerzeugertem22.setText(x.getWaermeErzTemp().toString());
								lblVerbrennungsllufttem22.setText(x.getVerbrennLuftTemp().toString());
								lblO2Gehaltin22.setText(x.getO2Gehalt().toString());
								lblAbgasverluste22.setText(x.getAbgasverluste().toString());
								if (x.getOelanteile()) {
									rbtnJa22.setSelected(true);
									rbtnNein22.setSelected(false);
								}
								else {
									rbtnNein22.setSelected(true);
									rbtnJa22.setSelected(false);
								}
								break;
				}
			});
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		
		if (objKontrolle.getBestanden()) {
			rbtnBeurteilung1.setSelected(true);
			rbtnBeurteilung2.setSelected(false);
		}else{
			rbtnBeurteilung1.setSelected(false);
			rbtnBeurteilung2.setSelected(true);
			cbRusszahlen.setSelected(objKontrolle.getFailRusszahl());
			cbCO.setSelected(objKontrolle.getFailCo());
			cbAbgasverluste.setSelected(objKontrolle.getFailAbgasverlust());
			cbOelanteile.setSelected(objKontrolle.getFailOel());
			cbNO2.setSelected(objKontrolle.getFailNo());
			if (objKontrolle.getEinregulierungMoeglich()){
				rbtnWeiteresVorgehen1.setSelected(true);
				rbtnWeiteresVorgehen2.setSelected(false);
			}else {
				rbtnWeiteresVorgehen1.setSelected(false);
				rbtnWeiteresVorgehen2.setSelected(true);
			}
		}
		
		cbBestaetigung.setSelected(objKontrolle.getKenntnisnahmeKunde());
		txtBemerkung.setText(objKontrolle.getBemerkungen());
		lblKontrolleur.setText(objKontrolle.getTermin().getMitarbeiter().getNachname() + " " + 
				objKontrolle.getTermin().getMitarbeiter().getVorname());
		
	}
		
	
}
	

