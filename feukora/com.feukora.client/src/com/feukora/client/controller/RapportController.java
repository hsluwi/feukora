package com.feukora.client.controller;

import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Anlagenstandort;
import com.feukora.server.models.Brenner;
import com.feukora.server.models.Kontrolle;
import com.feukora.server.models.Messergebnis;
import com.feukora.server.models.Termin;
import com.feukora.server.models.Waermeerzeuger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * 
 * @author Nadine Lang
 * @version 1.0
 *
 */
public class RapportController extends MainController implements Initializable {
	@FXML private ToggleGroup kontrollart;
	@FXML private ToggleGroup m1Stufe1;
	@FXML private ToggleGroup m2Stufe1;
	@FXML private ToggleGroup m1Stufe2;
	@FXML private ToggleGroup m2Stufe2;
	@FXML private ToggleGroup beurteilung;
	@FXML private ToggleGroup wVorgehen;

	@FXML private RadioButton rbtnRoutinekontrolle;
	@FXML private RadioButton rbtnAbnahmekontrolle;
	@FXML private RadioButton rbtnJa11;
	@FXML private RadioButton rbtnNein11;
	@FXML private RadioButton rbtnJa12;
	@FXML private RadioButton rbtnNein12;
	@FXML private RadioButton rbtnJa21;
	@FXML private RadioButton rbtnNein21;
	@FXML private RadioButton rbtnJa22;
	@FXML private RadioButton rbtnNein22;
	@FXML private RadioButton rbtnBeurteilung1;
	@FXML private RadioButton rbtnBeurteilung2;
	@FXML private RadioButton rbtnWeiteresVorgehen1;
	@FXML private RadioButton rbtnWeiteresVorgehen2;
	
	@FXML private Label lblZulassungsNrWaermeer;
	@FXML private Label lblZulassungsNrBrenner;
	@FXML private Label lblWeiteresVorgehen1;
	@FXML private Label lblAnlagenstandort;
	@FXML private Label lblWaermeerzeuger;
	@FXML private Label lblBrenner;
	
	@FXML private TextField txtRusszal11;
	@FXML private TextField txtRusszal12;
	@FXML private TextField txtRusszahl21;
	@FXML private TextField txtRusszahl22;
	@FXML private TextField txtCO11;
	@FXML private TextField txtCO12;
	@FXML private TextField txtCO21;
	@FXML private TextField txtCO22;
	@FXML private TextField txtNo11;
	@FXML private TextField txtNo12;
	@FXML private TextField txtNo21;
	@FXML private TextField txtNo22;
	@FXML private TextField txtAbgastemperatur11;
	@FXML private TextField txtAbgastemperatur12;
	@FXML private TextField txtAbgastemperatur21;
	@FXML private TextField txtAbgastemperatur22;
	@FXML private TextField txtWaermeerzeugertem11;
	@FXML private TextField txtWaermeerzeugertem12;
	@FXML private TextField txtWaermeerzeugertem21;
	@FXML private TextField txtWaermeerzeugertem22;
	@FXML private TextField txtVerbrennungsllufttem11;
	@FXML private TextField txtVerbrennungsllufttem12;
	@FXML private TextField txtVerbrennungsllufttem21;
	@FXML private TextField txtVerbrennungsllufttem22;
	@FXML private TextField txtO2Gehaltin11;
	@FXML private TextField txtO2Gehaltin12;
	@FXML private TextField txtO2Gehaltin21;
	@FXML private TextField txtO2Gehaltin22;
	@FXML private TextField txtAbgasverluste11;
	@FXML private TextField txtAbgasverluste12;
	@FXML private TextField txtAbgasverluste21;
	@FXML private TextField txtAbgasverluste22;
	@FXML private TextField txtZulassungsNrBrenner;
	@FXML private TextField txtZulassungsNrWaermeer;
	
	@FXML private Button btnSpeichernUndSchliessen;
	@FXML private Button btnAendernNeu;
	@FXML private Button btnAbbrechen;
	
	@FXML private CheckBox cbRusszahlen;
	@FXML private CheckBox cbOelanteile;
	@FXML private CheckBox cbCO;
	@FXML private CheckBox cbNO2;
	@FXML private CheckBox cbAbgasverluste;
	@FXML private CheckBox cbBestaetigung;  
	
	@FXML private TextArea txtBemerkung;
	
	@FXML private AnchorPane mainPane;
	
	private String errors = null;
	
	private Alert alrtPrint = new Alert(AlertType.CONFIRMATION);
	
	private Termin objTermin;
	protected Anlagenstandort objAnlagenstandort;
	
	private static final Logger logger = LogManager.getLogger();

	public RapportController() {
		super();
	}
	/**
	 * 
	 * @author Nadine Lang
	 * @version 1.0
	 * 
	 * @param markierterTermin  markierte Termin von der Klasse EinsatzplanController
	 *
	 *Der Konstruktor wird in der Klasse EinsatzplanController verwendet. 
	 *F�r den markierten Termin kann ein Rapport erstellt werden. 
	 *Durch den Konstruktor wird der ausgew�hlte Termin �bergeben und in ein Termin Objekt gespeichert.
	 */
	public RapportController(Termin markierterTermin) {
		objTermin = markierterTermin;
	}

	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * 
	 * @param Standardparameter von implementierter Initializable Klasse
	 * 
	 * Pr�ft ob bereits Anlagenstandort vorhanden ist und l�sst diesen laden
	 *
	 */
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
				
		//Setze Bemerkungsfeld initial
		txtBemerkung.setText("");
		
		
		if(objTermin.getAnlagenstandort()!= null){
			//�ber Terminobjekt den Anlagenstandort auslesen
			objAnlagenstandort = objTermin.getAnlagenstandort();
			reloadAnlagenstandortAuswahl();
			
		}
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 2.0
	 * 
	 * L�dt Daten des Anlagenstandorts in die GUI Elemente
	 *
	 */
	
	public void reloadAnlagenstandortAuswahl(){
		
		//Standortdaten
		String strasse = objAnlagenstandort.getStrasse();
		int intPlz = objAnlagenstandort.getPlzOrt().getPlz().intValue();
		String plz = String.valueOf(intPlz);
		String ort = objAnlagenstandort.getPlzOrt().getOrt();
		lblAnlagenstandort.setText(strasse + "   /   " + plz + " " + ort);
		
		//W�rmeerzeugerdaten
		String modell = objAnlagenstandort.getWaermeerzeuger().getWaermeerzeugermodell().getBezeichnung();
		int intBaujahr = objAnlagenstandort.getWaermeerzeuger().getBaujahr().intValue();
		String baujahr = String.valueOf(intBaujahr);
		String nummer = objAnlagenstandort.getWaermeerzeuger().getZulassungsNr();
		lblWaermeerzeuger.setText(modell + " (" + baujahr + ")");
		txtZulassungsNrWaermeer.setText(nummer);
		
		//Brennerdaten
		modell = objAnlagenstandort.getBrenner().getBrennermodell().getBezeichnung();
		intBaujahr = objAnlagenstandort.getBrenner().getBaujahr().intValue();
		baujahr = String.valueOf(intBaujahr);
		nummer = objAnlagenstandort.getBrenner().getZulassungsNr();
		lblBrenner.setText(modell + " (" + baujahr + ")");
		txtZulassungsNrBrenner.setText(nummer);
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * @throws IOException
	 * 
	 * �ffnet neues GUI Fenster "Anlage verwalten" und �bergibt dabei sich selber (RapportController)
	 * an den aufzurufen Konstruktor AnlagenVerwaltenController
	 *
	 */
	@FXML
	public void btnAnlagenstandortClicked() {
		Parent root;

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/AnlageVerwalten.fxml"));
			AnlagenVerwaltenController racontroller = new AnlagenVerwaltenController(this);
			fxmlLoader.setController(racontroller);

			root = fxmlLoader.load();

			Stage stage = new Stage();
			stage.setTitle("Anlage bearbeiten");
			stage.setScene(new Scene(root));
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Anlage Bearbeiten View konnte nicht geladen werden" + e);
		}
		
		
	}

	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * 
	 * Beim Klicken des Abbrechen Button wird das Fenster geschlossen
	 *
	 */
	@FXML
	public void btnAbbrechenClicked(ActionEvent event) {
		
		close(event);
		
	}
	
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * 
	 * Schliesst das Fenster
	 *
	 */
	private void close(ActionEvent e) {		
		Node node = (Node) e.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		stage.close();	
	}

	/**
	 * 
	 * @author Nadine Lang
	 * @version 1.0
	 * 
	 * Methode blendet die Zulassungsnummern vom Brenner und vom Waermeerzeuger ein,
	 * wenn auf den Radio-Button Abnahmekontrolle geklickt wird.
	 *
	 */
	@FXML
	public void rbtnAbnahmekontrolleClicked(ActionEvent event) {
		if(rbtnAbnahmekontrolle.isSelected()){
		lblZulassungsNrWaermeer.setDisable(false);
		txtZulassungsNrWaermeer.setDisable(false);
		lblZulassungsNrBrenner.setDisable(false);
		txtZulassungsNrBrenner.setDisable(false);
	
		}
	}
	
	/**
	 * 
	 * @author Nadine Lang
	 * @version 1.0
	 * 
	 * Methode blendet die Zulassungsnummern vom Brenner und vom Waermeerzeuger aus,
	 * wenn auf den Radio-Button Routinekontrolle geklickt wird.
	 *
	 */
	@FXML
	public void rbtnRoutinekontrolleClicked(ActionEvent event) { 
		if(rbtnRoutinekontrolle.isSelected()){
		lblZulassungsNrWaermeer.setDisable(true);
		txtZulassungsNrWaermeer.setDisable(true);
		lblZulassungsNrBrenner.setDisable(true);
		txtZulassungsNrBrenner.setDisable(true);
		}
	}

	/**
	 * 
	 * @author Nadine Lang
	 * @version 1.0
	 * 
	 * Methode blendet die Radio-Buttons f�r das weitere Vorgehen und die Checkboxen f�r die Beanstandungen aus,
	 * wenn auf den Radio-Button rbtnBeurteilung1 geklickt wird.
	 *
	 */
	@FXML
	public void rbtnBeurteilung1Clicked(ActionEvent event) { 
		if(rbtnBeurteilung1.isSelected()){
			rbtnWeiteresVorgehen1.setSelected(false);
			rbtnWeiteresVorgehen1.setDisable(true);
			lblWeiteresVorgehen1.setDisable(true);
			rbtnWeiteresVorgehen2.setSelected(false);
			rbtnWeiteresVorgehen2.setDisable(true);
			cbRusszahlen.setDisable(true);
			cbRusszahlen.setSelected(false);
			cbCO.setDisable(true);
			cbCO.setSelected(false);
			cbAbgasverluste.setDisable(true);
			cbAbgasverluste.setSelected(false);
			cbOelanteile.setDisable(true);
			cbOelanteile.setSelected(false);
			cbNO2.setDisable(true);
			cbNO2.setSelected(false);
		}
	}
	
	/**
	 * 
	 * @author Nadine Lang
	 * @version 1.0
	 * 
	 * Methode blendet die Radio-Buttons f�r das weitere Vorgehen und die Checkboxen f�r die Beanstandungen ein,
	 * wenn auf den Radio-Button rbtnBeurteilung2 geklickt wird.
	 *
	 */
	@FXML
	public void rbtnBeurteilung2Clicked(ActionEvent event) { 
		if(rbtnBeurteilung2.isSelected()){
			rbtnWeiteresVorgehen1.setDisable(false);
			lblWeiteresVorgehen1.setDisable(false);
			rbtnWeiteresVorgehen2.setDisable(false);
			cbRusszahlen.setDisable(false);
			cbCO.setDisable(false);
			cbAbgasverluste.setDisable(false);
			cbOelanteile.setDisable(false);
			cbNO2.setDisable(false);
		}
	}
	
	/**
	 * 
	 * @author Nadine Lang
	 * @version 1.0
	 * 
	 * Methode f�llt die Objekte Brenner, Waermeerzeuger, Kontrolle und vier Messergebniss Objekte ab.
	 * Anschliessend werden die Objekte in die Datenbank gespeichert.
	 *
	 */
	@FXML
	public void btnSpeichernClicked(ActionEvent event) {
		errors = "";
		Waermeerzeuger objWaermererzeuger = new Waermeerzeuger();
		Brenner objBrenner = new Brenner();
		Kontrolle objKontrolle = new Kontrolle();
		Messergebnis objMesserbegnis11 = new Messergebnis();
		Messergebnis objMesserbegnis12 = new Messergebnis();
		Messergebnis objMesserbegnis21 = new Messergebnis();
		Messergebnis objMesserbegnis22 = new Messergebnis();
		
		//ZulassungsNr bei Abnahmekontrolle setzen
		if(rbtnAbnahmekontrolle.isSelected()){
			
			if( objAnlagenstandort.getWaermeerzeuger() != null){
				if(txtZulassungsNrWaermeer.getText().isEmpty()){
					errors += "- Bitte geben Sie eine ZulassungsNr f�r den W�rmeerzeuger ein \n";
				}
				else{
					objWaermererzeuger = objAnlagenstandort.getWaermeerzeuger();
						objWaermererzeuger.setZulassungsNr(txtZulassungsNrWaermeer.getText());
				}
			}
			
			if( objAnlagenstandort.getBrenner() != null){
				if(txtZulassungsNrBrenner.getText().isEmpty()){
					errors += "- Bitte geben Sie eine ZulassungsNr f�r den Brenner ein \n";
				}
				else{
					objAnlagenstandort.getBrenner().setZulassungsNr(txtZulassungsNrBrenner.getText());
				}
			}
		}
	
		//Kontrollart setzen
		if(rbtnRoutinekontrolle.isSelected()){
			objKontrolle.setKontrollart("Routinekontrolle"); 
		}
		else if(rbtnAbnahmekontrolle.isSelected()){
			objKontrolle.setKontrollart("Abnahmekontrolle"); 
		}
		else{
			errors += "- Bitte geben Sie eine Kontrollart an \n";
		}
		
		//Beurteilung
		if(rbtnBeurteilung1.isSelected()){
			objKontrolle.setBestanden(true);
			objKontrolle.setFailRusszahl(false);
			objKontrolle.setFailCo(false);
			objKontrolle.setFailAbgasverlust(false);
			objKontrolle.setFailOel(false);
			objKontrolle.setFailNo(false);
		}
		
		else if (!rbtnBeurteilung1.isSelected() && !rbtnBeurteilung2.isSelected()){
			errors += "- Bitte geben Sie eine Beurteilung ab \n";
		}
		else if(rbtnBeurteilung2.isSelected()){
			objKontrolle.setBestanden(false);
			
			if(!cbRusszahlen.isSelected() && !cbCO.isSelected() && !cbAbgasverluste.isSelected() & !cbOelanteile.isSelected() && !cbNO2.isSelected()){
				errors += "- Bitte geben Sie mindestens eine �berschreitung an \n";
			}
			else{
				
				if(cbRusszahlen.isSelected()){
					objKontrolle.setFailRusszahl(true);
				}
				else{
					objKontrolle.setFailRusszahl(false);
				}
				if(cbCO.isSelected()){
					objKontrolle.setFailCo(true);
				}
				else{
					objKontrolle.setFailCo(false);
				}
				if(cbAbgasverluste.isSelected()){
					objKontrolle.setFailAbgasverlust(true);
				}
				else{
					objKontrolle.setFailAbgasverlust(false);
				}	
				if(cbOelanteile.isSelected()){
					objKontrolle.setFailOel(true);
				}
				else{
					objKontrolle.setFailOel(false);
				}
				if(cbNO2.isSelected()){
					objKontrolle.setFailNo(true);
				}
				else{
					objKontrolle.setFailNo(false);
				}	
			}
		}
		
		//Weiteres Vorgehen
		if(rbtnBeurteilung2.isSelected()){
			
			if(rbtnWeiteresVorgehen1.isSelected()){
				objKontrolle.setEinregulierungMoeglich(true);
			}
			
			else if(rbtnWeiteresVorgehen2.isSelected()){
				objKontrolle.setEinregulierungMoeglich(false);
			}
			else{
				errors += "- Geben Sie bitte das weitere Vorgehen an \n";
			}
			
		}
		
		//Bemerkung
		objKontrolle.setBemerkungen(txtBemerkung.getText());
		
		//Bestaetigung
		if(cbBestaetigung.isSelected()){
			objKontrolle.setKenntnisnahmeKunde(true);
		}
		else{
			errors += "- Bitte Feuerungs-Rapport wahrheitsgetreu und korrekt ausgef�llt best�tigen \n";
		}
		
		try{

		// Messerbegnis11 Objekt erstellen
		objMesserbegnis11.setStufe(1);
		objMesserbegnis11.setMessungNr(1);
		objMesserbegnis11.setRusszahl(Double.parseDouble(txtRusszal11.getText()));
		objMesserbegnis11.setCo3PVol(Double.parseDouble(txtCO11.getText()));
		if (rbtnJa11.isSelected()){
			objMesserbegnis11.setOelanteile(true);
		}
		else if (rbtnNein11.isSelected()){
			objMesserbegnis11.setOelanteile(false);
		}
		else{
			errors += "- Angabe zu �lanteile fehlt bei mindestem einem Messergebnis \n";
		}
		objMesserbegnis11.setNox3PVol(Double.parseDouble(txtNo11.getText()));
		objMesserbegnis11.setAbgastemperatur(Double.parseDouble(txtAbgastemperatur11.getText()));
		objMesserbegnis11.setWaermeErzTemp(Double.parseDouble(txtWaermeerzeugertem11.getText()));
		objMesserbegnis11.setVerbrennLuftTemp(Double.parseDouble(txtVerbrennungsllufttem11.getText()));
		objMesserbegnis11.setO2Gehalt(Double.parseDouble(txtO2Gehaltin11.getText()));
		objMesserbegnis11.setAbgasverluste(Double.parseDouble(txtAbgasverluste11.getText()));
		
		
		// Messerbegnis12 Objekt erstellen
		objMesserbegnis12.setStufe(1);
		objMesserbegnis12.setMessungNr(2);
		objMesserbegnis12.setRusszahl(Double.parseDouble(txtRusszal12.getText()));
		objMesserbegnis12.setCo3PVol(Double.parseDouble(txtCO12.getText()));
		if (rbtnJa12.isSelected()){
			objMesserbegnis12.setOelanteile(true);
		}
		else if (rbtnNein12.isSelected()){
			objMesserbegnis12.setOelanteile(false);
		}
		else{
			errors += "- Angabe zu �lanteile fehlt bei mindestem einem Messergebnis \n";
		}
		objMesserbegnis12.setNox3PVol(Double.parseDouble(txtNo12.getText()));
		objMesserbegnis12.setAbgastemperatur(Double.parseDouble(txtAbgastemperatur12.getText()));
		objMesserbegnis12.setWaermeErzTemp(Double.parseDouble(txtWaermeerzeugertem12.getText()));
		objMesserbegnis12.setVerbrennLuftTemp(Double.parseDouble(txtVerbrennungsllufttem12.getText()));
		objMesserbegnis12.setO2Gehalt(Double.parseDouble(txtO2Gehaltin12.getText()));
		objMesserbegnis12.setAbgasverluste(Double.parseDouble(txtAbgasverluste12.getText()));

		// Messerbegnis21 Objekt erstellen
		objMesserbegnis21.setStufe(2);
		objMesserbegnis21.setMessungNr(1);
		objMesserbegnis21.setRusszahl(Double.parseDouble(txtRusszahl21.getText()));
		objMesserbegnis21.setCo3PVol(Double.parseDouble(txtCO21.getText()));
		if (rbtnJa21.isSelected()){
			objMesserbegnis21.setOelanteile(true);
		}
		else if (rbtnNein21.isSelected()){
			objMesserbegnis21.setOelanteile(false);
		}
		else{
			errors += "- Angabe zu �lanteile fehlt bei mindestem einem Messergebnis \n";
		}
		objMesserbegnis21.setNox3PVol(Double.parseDouble(txtNo21.getText()));
		objMesserbegnis21.setAbgastemperatur(Double.parseDouble(txtAbgastemperatur21.getText()));
		objMesserbegnis21.setWaermeErzTemp(Double.parseDouble(txtWaermeerzeugertem21.getText()));
		objMesserbegnis21.setVerbrennLuftTemp(Double.parseDouble(txtVerbrennungsllufttem21.getText()));
		objMesserbegnis21.setO2Gehalt(Double.parseDouble(txtO2Gehaltin21.getText()));
		objMesserbegnis21.setAbgasverluste(Double.parseDouble(txtAbgasverluste21.getText()));
	
		// Messerbegnis22 Objekt erstellen
		objMesserbegnis22.setStufe(2);
		objMesserbegnis22.setMessungNr(2);
		objMesserbegnis22.setRusszahl(Double.parseDouble(txtRusszahl22.getText()));
		objMesserbegnis22.setCo3PVol(Double.parseDouble(txtCO22.getText()));
		if (rbtnJa22.isSelected()){
			objMesserbegnis22.setOelanteile(true);
		}
		else if (rbtnNein22.isSelected()){
			objMesserbegnis22.setOelanteile(false);
		}
		else{
			errors += "- Angabe zu �lanteile fehlt bei mindestem einem Messergebnis \n";
		}
		objMesserbegnis22.setNox3PVol(Double.parseDouble(txtNo22.getText()));
		objMesserbegnis22.setAbgastemperatur(Double.parseDouble(txtAbgastemperatur22.getText()));
		objMesserbegnis22.setWaermeErzTemp(Double.parseDouble(txtWaermeerzeugertem22.getText()));
		objMesserbegnis22.setVerbrennLuftTemp(Double.parseDouble(txtVerbrennungsllufttem22.getText()));
		objMesserbegnis22.setO2Gehalt(Double.parseDouble(txtO2Gehaltin22.getText()));
		objMesserbegnis22.setAbgasverluste(Double.parseDouble(txtAbgasverluste22.getText()));
		
		//bereits persistierter Termin der Kontrolle zuweisen
		objKontrolle.setTermin(objTermin);  
		
		}catch(Exception e){
			errors += "- Falsche Zahlenwerte \n";
		}

		//falls die L�nge des String Error gr�sser als 0 ist, wird der Feuerungsrapport nicht gepsiechert
		if(errors.length() > 0){
			Alert alert = new Alert(AlertType.INFORMATION, errors);
			alert.show();
		}

		// falls keine Errors vorhanden sind, wird der Feuerungsrapport gespeichert
		else {
			//Objekt Waermeerzeuger speichern
			try {
				objWaermererzeuger = serviceProvider.getWaermeerzeugerService().saveWaermeerzeuger(objWaermererzeuger);
				//Objekt Brenner speichern
				objBrenner = serviceProvider.getBrennerService().saveBrenner(objBrenner);
			
				//Objekt Kontrolle speichern
				objKontrolle = serviceProvider.getKontrolleService().saveKontrolle(objKontrolle);
					
				//Messergebnis Objekte speichern
				objMesserbegnis11.setKontrolle(objKontrolle);
				serviceProvider.getMessergebnisService().saveMessergebnis(objMesserbegnis11);
				
				objMesserbegnis12.setKontrolle(objKontrolle);
				serviceProvider.getMessergebnisService().saveMessergebnis(objMesserbegnis12);
				
				objMesserbegnis21.setKontrolle(objKontrolle);
				serviceProvider.getMessergebnisService().saveMessergebnis(objMesserbegnis21);
				
				objMesserbegnis22.setKontrolle(objKontrolle);
				serviceProvider.getMessergebnisService().saveMessergebnis(objMesserbegnis22);
				
			} catch (RemoteException e) {
				logger.error(e.getMessage());
			}
			alrtPrint.setTitle("Rapport gepseichert");
			alrtPrint.setContentText("Der Rapport wurde erfolgreich gespeichert");
			alrtPrint.show();
			
			close(event);
			
			alrtPrint.setTitle("Druckansicht anzeigen?");
			alrtPrint.setHeaderText("Rapport drucken?");
			alrtPrint.setContentText("Wollen Sie die Druckansicht des Rapports anzeigen?");
			alrtPrint.getButtonTypes().clear();
			alrtPrint.getButtonTypes().addAll(ButtonType.YES,ButtonType.NO);
			if (alrtPrint.showAndWait().get() == ButtonType.YES) {
				erzeugeRapportDruck(objKontrolle);
			}
		}
	}
	
	
	/**
	 * Ruft die Druckansicht des Feuerungsrapports auf.
	 * @author Philipp Anderhub
	 * @version 1.0
	 * @since 1.0
	 * @param kontrolle
	 */
	@FXML
	public void erzeugeRapportDruck(Kontrolle kontrolle){
		Parent root;

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/RapportDruck.fxml"));
			RapportDruckController controller = new RapportDruckController(kontrolle);
			fxmlLoader.setController(controller);
			root = fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Rapport Druckansicht Vorschau");
			stage.setScene(new Scene(root));
			stage.setResizable(false);
			stage.show();

		} catch (Exception e) {
			logger.error("Rapport Druckfenster kann nicht geladen werden:");
			logger.error(e.getMessage());
		}
	}
	
}
