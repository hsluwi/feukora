package com.feukora.client.controller;

import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Anlagenstandort;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * AnlagenController f�r die View Anlagen.fxml
 * @author Marco Hostettler
 * @version 1.0
 * @since 18.11.15
 *
 */
public class AnlagenController extends MainController implements Initializable {
	@FXML
	private GridPane grdAnlage;
	@FXML
	private Button btnSuchen;
	@FXML
	private Button btnBearbeiten;
	@FXML
	private Button btnNeu;
	@FXML
	private Button btnLoeschen;
	@FXML
	private TextField txtSuche;
	@FXML
	private TextField txtAnlageID;
	@FXML
	private TextField txtStrasse;
	@FXML
	private TextField txtBauWaerme;
	@FXML
	private TextField txtTypWaerme;
	@FXML
	private TextField txtBrennWaerme;
	@FXML
	private TextField txtBauBrenner;
	@FXML
	private TextField txtTypBrenner;
	@FXML
	private TextField txtBrenner;
	@FXML
	private TextField txtLeistung;
	@FXML
	private TextField txtPlz;
	@FXML
	private TextField txtOrt;
	@FXML
	private TextField txtKundeFirma;

	@FXML
	private TableColumn<Anlagenstandort, String> colANummer;
	@FXML
	private TableColumn<Anlagenstandort, String> colStandort;
	@FXML
	private TableColumn<Anlagenstandort, String> colStrasse;
	@FXML
	private TableColumn<Anlagenstandort, String> colFirma;
	@FXML
	private TableView<Anlagenstandort> tblAnlage;
	MainController mainCo;
	public static Stage secondStage;
	private static final Logger logger = LogManager.getLogger();
	protected ObservableList<Anlagenstandort> olAnlagen;
	protected Anlagenstandort clickedAnlage;

	public AnlagenController(MainController mainCo) {
		super();
		this.mainCo = mainCo;

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		setCellValueFactory();
		updateData(mainCo.anlagenListe);
	}

	/**
	 * Update der Tabelle mit allen Anlagen Daten. Hinzuf�gen von
	 * Filterfunnktion.
	 * 
	 * @param anlagen,
	 *            welche im MainController geladen worden sind
	 */
	public void updateData(List<Anlagenstandort> anlagen) {
		this.olAnlagen = FXCollections.observableArrayList(anlagen);
		tblAnlage.setItems(this.olAnlagen);

		FilteredList<Anlagenstandort> filteredData = new FilteredList<>(olAnlagen, p -> true);

		txtSuche.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(anlage -> {
				// TxtSuche = leer , Zeige alle Daten an
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}

				String lowerCaseFilter = newValue.toLowerCase();
				if (String.valueOf(anlage.getId()) != null
						&& String.valueOf(anlage.getId()).toLowerCase().contains(lowerCaseFilter)) {
					return true; // Filter = Anlagen ID

				} else if (anlage.getPlzOrt().getOrt() != null
						&& anlage.getPlzOrt().getOrt().toLowerCase().contains(lowerCaseFilter)) {
					return true; // Filter = Ort
				} else if (anlage.getStrasse() != null && anlage.getStrasse().toLowerCase().contains(lowerCaseFilter)) {
					return true; // Filter = Strasse
				} else if (anlage.getKunde().getFirma() != null
						&& anlage.getKunde().getFirma().toLowerCase().contains(lowerCaseFilter)) {
					return true; // Filter = Kunden Firma
				}
				return false; // Filter !=
			});
		});
		SortedList<Anlagenstandort> sortedData = new SortedList<>(filteredData);

		sortedData.comparatorProperty().bind(tblAnlage.comparatorProperty());
		tblAnlage.setItems(sortedData);
	}

	/**
	 * Konfiguration f�r die Tabellenbef�llung, f�r colKunde und colStandort da
	 * diese nicht direkt vom Anlagenstandort-Objekt kommen - FXML limitation
	 */
	private void setCellValueFactory() {

		colStandort.setCellValueFactory(
				new Callback<TableColumn.CellDataFeatures<Anlagenstandort, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TableColumn.CellDataFeatures<Anlagenstandort, String> p) {
						return new SimpleObjectProperty<String>(p.getValue().getPlzOrt().getOrt());
					}
				});
		colFirma.setCellValueFactory(
				new Callback<TableColumn.CellDataFeatures<Anlagenstandort, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TableColumn.CellDataFeatures<Anlagenstandort, String> p) {
						return new SimpleObjectProperty<String>(p.getValue().getKunde().getFirma());
					}
				});
	}
	/**
	 * Abf�llung der Daten der ausgew�hlten Anlage
	 */
	@FXML
	public void getAnlageDetails() {
		this.btnBearbeiten.setDisable(false);
		this.btnLoeschen.setDisable(false);
		this.clickedAnlage = tblAnlage.getSelectionModel().getSelectedItem();
		int anlageID = clickedAnlage.getId().intValue();
		this.txtAnlageID.setText(String.valueOf(anlageID));
		int plz = clickedAnlage.getPlzOrt().getPlz().intValue();
		this.txtPlz.setText(String.valueOf(plz));
		this.txtOrt.setText(clickedAnlage.getPlzOrt().getOrt());
		this.txtStrasse.setText(clickedAnlage.getStrasse());
		this.txtKundeFirma.setText(clickedAnlage.getKunde().getFirma());
		int wBaujahr = clickedAnlage.getWaermeerzeuger().getBaujahr();
		this.txtBauWaerme.setText(String.valueOf(wBaujahr));
		this.txtTypWaerme.setText(clickedAnlage.getWaermeerzeuger().getWaermeerzeugermodell().getBezeichnung());
		this.txtBrennWaerme
				.setText(clickedAnlage.getWaermeerzeuger().getWaermeerzeugermodell().getBrennstoff().getBezeichnung());
		int bBaujahr = clickedAnlage.getBrenner().getBaujahr();
		this.txtBauBrenner.setText(String.valueOf(bBaujahr));
		this.txtTypBrenner.setText(clickedAnlage.getBrenner().getBrennermodell().getBezeichnung());
		this.txtBrenner.setText(clickedAnlage.getBrenner().getZulassungsNr());
		int leistung = clickedAnlage.getBrenner().getBrennermodell().getFeuerungswaermeleistung();
		this.txtLeistung.setText(String.valueOf(leistung));

	}


	@FXML
	public void btnBearbeitenClicked() {
		Parent root;

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/AnlageVerwalten.fxml"));
			AnlagenVerwaltenController avcontroller = new AnlagenVerwaltenController(this);
			fxmlLoader.setController(avcontroller);

			root = fxmlLoader.load();

			Stage stage = new Stage();
			stage.initModality(Modality.WINDOW_MODAL);
			stage.initOwner(mainCo.mainstage.getScene().getWindow());
			stage.setTitle("Anlage bearbeiten");
			stage.setScene(new Scene(root));
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Anlage Bearbeiten View konnte nicht geladen werden" + e);
		}
	}

	@FXML
	public void btnNeuClicked() {
		Parent root;
		clickedAnlage = null;

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/AnlageVerwalten.fxml"));
			AnlagenVerwaltenController avcontroller = new AnlagenVerwaltenController(this);
			fxmlLoader.setController(avcontroller);
			root = fxmlLoader.load();

			Stage stage = new Stage();
			stage.initModality(Modality.WINDOW_MODAL);
			stage.initOwner(mainCo.mainstage.getScene().getWindow());
			stage.setTitle("Anlage erfassen");
			stage.setScene(new Scene(root));
			stage.show();
		} catch (IOException e) {
			logger.error("Anlage Erfassen View konnte nicht geladen werden" + e);
			e.printStackTrace();
		}

	}

	@FXML
	public void btnLoeschenClicked() {

		Alert alert = new Alert(AlertType.CONFIRMATION, "Wollen Sie diese Anlage endg�ltg l�schen?");
		alert.showAndWait().ifPresent(response -> {
			if (response == ButtonType.OK) {
				anlageLoeschen();
			}

		});
	}
	/**
	 * L�schfunktion auf dem Server aufrufen und Ergebnisse loggen
	 */
	private void anlageLoeschen() {
		try {
			if (serviceProvider.getAnlagenstandortService().removeAnlagenstandort(clickedAnlage.getId())) {
				Alert alert = new Alert(AlertType.INFORMATION, "Anlage wurde erfolgreich gel�scht.");
				alert.show();
				loadSpecificData(this);
				txtSuche.clear();
				logger.info("Anlage " + clickedAnlage.getId() + " " + clickedAnlage.getStrasse() + " wurde gel�scht.");
			} else {
				Alert alert = new Alert(AlertType.ERROR, "Kontrolleur konnte nicht gel�scht werden");
				alert.show();
				logger.error("Anlage " + clickedAnlage.getId() + " " + clickedAnlage.getStrasse() + "  konnte nicht gel�scht werden.");
			}
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}

	}
}
