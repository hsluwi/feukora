package com.feukora.client.controller;

import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Mitarbeiter;
import com.feukora.server.models.MitarbeiterRolle;
import com.feukora.server.models.PlzOrt;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
/**
 * Kontroller Klasse der View MitarbeiterVerwalten.fxml
 * @author Marco Hostettler
 * @version 3.0
 * @since 3.12.15
 */
public class MitarbeiterVerwaltenController extends MainController implements Initializable {
	@FXML
	private TextField txtVorname;
	@FXML
	private TextField txtNachname;
	@FXML
	private TextField txtStrasse;
	@FXML
	private TextField txtTelefon;
	@FXML
	private TextField txtEmail;
	@FXML
	private TextField txtPlz;
	@FXML
	private TextField txtMobile;
	@FXML
	private ComboBox<String> cbRolle;
	@FXML
	private ComboBox<String> cbOrt;
	@FXML
	private Label lblTitel;
	@FXML
	private Label lblMitarbeiterID;
	@FXML
	private TextField txtUsername;
	@FXML
	private PasswordField pwdPasswort1;
	@FXML
	private PasswordField pwdPasswort2;

	private static final Logger logger = LogManager.getLogger();
	protected MitarbeiterController miCo;

	private List<PlzOrt> plzOrt;
	private List<MitarbeiterRolle> mitarbeiterRolle;
	private ObservableList<String> olmitarbeiterRolle = FXCollections.observableArrayList("Administrator",
			"Kontrolleur", "Sachbearbeiter");
	private ObservableList<String> olPlzOrt = FXCollections.observableArrayList();
	private Mitarbeiter mitarbeiter;
	private PlzOrt obPlzOrt;
	private MitarbeiterRolle obRolle;

	public MitarbeiterVerwaltenController(MitarbeiterController miCo) {
		super();
		this.miCo = miCo;
	}

	/**
	 * Setze die Daten des ausgew�hlten Mitarbeiter in die FXML Felder.
	 * 
	 * @author Marco Hostettler
	 * 
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try {
			this.mitarbeiterRolle = new ArrayList<MitarbeiterRolle>(serviceProvider.getMitarbeiterRolleService().getListMitarbeiterRolle());
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		this.cbRolle.setItems(olmitarbeiterRolle);

		if (miCo.clickedMitarbeiter != null) {
			this.lblTitel.setText("Mitarbeiter bearbeiten");
			this.mitarbeiter = miCo.clickedMitarbeiter;
			int mitarbeiterID = mitarbeiter.getId().intValue();
			this.lblMitarbeiterID.setText(String.valueOf(mitarbeiterID));
			this.cbRolle.setValue(mitarbeiter.getRolle().getBezeichnung());
			this.txtVorname.setText(mitarbeiter.getVorname());
			this.txtNachname.setText(mitarbeiter.getNachname());
			this.txtStrasse.setText(mitarbeiter.getStrasse());
			this.txtMobile.setText(mitarbeiter.getMobile());
			this.txtTelefon.setText(mitarbeiter.getTelefon());
			this.txtEmail.setText(mitarbeiter.getEmail());
			int plz = mitarbeiter.getPlzOrt().getPlz();
			this.txtPlz.setText(String.valueOf(plz));
			this.cbOrt.setValue(mitarbeiter.getPlzOrt().getOrt());
			this.txtUsername.setText(mitarbeiter.getUserName());
			this.pwdPasswort1.setText(mitarbeiter.getPasswort());
			this.pwdPasswort2.setText(mitarbeiter.getPasswort());
			obPlzOrt = mitarbeiter.getPlzOrt();
			obRolle = mitarbeiter.getRolle();
		}
		// Mitarbeiter Objekt erstellen wenn Mitarbeiter noch nicht existiert
		else {
			this.txtUsername.setDisable(false);
			this.mitarbeiter = new Mitarbeiter();
		}
	}

	public void btnAbbrechenClicked(ActionEvent event) {
		close(event);

	}

	private void close(ActionEvent e) {
		Node node = (Node) e.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		stage.close();

	}

	/**
	 * "On Key Typed" FXML Methode. Sobald 4 Stellen eingegeben werden, wird
	 * beim Server nach den Ortschaften mit diesen 4 Zahlen als Plz nachgefragt.
	 * Diese stehen dann im CbOrt zur Auswahl
	 * 
	 * @author Marco Hostettler
	 * @version 1.0
	 * @since 1.12.2015
	 */
	@FXML
	public void txtPlzChanged() {
		cbOrt.getItems().clear();
		if (txtPlz.getText().length() == 4) {

			try {
				this.plzOrt = new ArrayList<PlzOrt>(serviceProvider.getPlzOrtService().getPlzOrtByPlz(Integer.parseInt(txtPlz.getText())));
			} catch (NumberFormatException e) {
				logger.error(e.getMessage());
			} catch (RemoteException e) {
				logger.error(e.getMessage());
			}

			plzOrt.forEach(p -> olPlzOrt.add(p.getOrt()));
			this.cbOrt.setItems(olPlzOrt);
			this.cbOrt.setValue("Bitte w�hlen Sie...");

		}

	}

	/**
	 * "On Action" FXML Methode. Das PlzOrt-Objekt erstellen aus der Auswahl des
	 * cbOrt.
	 * 
	 * @author Marco Hostettler
	 * @version 1.0
	 * @since 1.12.2015
	 */
	@FXML
	void cbOrtChanged() {
		for (PlzOrt p : plzOrt) {
			if (p.getOrt().equals(cbOrt.getSelectionModel().getSelectedItem())) {
				obPlzOrt = p;
			}
		}

	}

	/**
	 * "On Action" FXML Methode. Das MitarbeiterRolle-Objekt erstellen aus der
	 * Auswahl des cbRolle Dropdown.
	 */
	@FXML
	void cbRolleChanged() {
		for (MitarbeiterRolle r : mitarbeiterRolle) {
			if (r.getBezeichnung().equals(cbRolle.getSelectionModel().getSelectedItem())) {
				obRolle = r;
			}
		}
	}
	/**
	 * Speicher das Mitarbeiter Objekt in der Datenbank
	 * @param event
	 */
	public void btnSpeichernClicked(ActionEvent event) {

		mitarbeiter.setVorname(txtVorname.getText());
		mitarbeiter.setNachname(txtNachname.getText());
		mitarbeiter.setStrasse(txtStrasse.getText());
		mitarbeiter.setTelefon(txtTelefon.getText());
		mitarbeiter.setMobile(txtMobile.getText());
		mitarbeiter.setEmail(txtEmail.getText());
		mitarbeiter.setUserName(txtUsername.getText());
		mitarbeiter.setPasswort(pwdPasswort2.getText());
		mitarbeiter.setPlzOrt(obPlzOrt);
		mitarbeiter.setRolle(obRolle);

		String errors = validate(mitarbeiter);

		if (!pwdPasswort1.getText().equals(pwdPasswort2.getText()) || pwdPasswort1.getText().length() < 6) {
			errors = errors + " Passw�rter stimmen nicht �berein. Das Passwort muss mindestens 6 Zeichen haben";
		}
		try {
			if (miCo.clickedMitarbeiter == null
					&& !serviceProvider.getMitarbeiterService().usernameAvailable(txtUsername.getText())) {
				errors = errors + "Username existiert bereits. Bitte w�hlen Sie einen anderen Username aus";
			}
		} catch (RemoteException e1) {
			logger.error(e1.getMessage());
		}
		// Wenn Fehler vorhanden sind, gibe diese aus.
		if (errors != "") {
			Alert alert = new Alert(AlertType.WARNING, errors);
			alert.show();
			logger.error("Daten unvollst�ndig/Fehlerhaft. Keine weiterleitung an Server");
		}

		// Wenn keine Fehler dann speichern; Serveraufruf
		else {
			try {
				if ((serviceProvider.getMitarbeiterService().saveMitarbeiter(mitarbeiter)) == null) {
					Alert alert = new Alert(AlertType.ERROR, "Mitarbeiter konnte nicht gespeichert werden");
					alert.show();
					logger.error("Mitarbeiter konnte nicht in Datenbank gespeichert werden.");
				} else {
					Alert alert = new Alert(AlertType.INFORMATION, "Mitarbeiter wurde erfolgreich gespeichert");
					alert.showAndWait().ifPresent(response -> {
						if (response == ButtonType.OK) {
							close(event);

						}
					});
					// Reload Daten vom Server, sowie Reload der TableView

					loadSpecificData(this);

				}
			} catch (RemoteException e) {
				logger.error(e.getMessage());
			}

		}

	};

}
