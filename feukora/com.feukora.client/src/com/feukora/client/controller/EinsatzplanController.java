
package com.feukora.client.controller;

import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Kontrolle;
import com.feukora.server.models.Mitarbeiter;
import com.feukora.server.models.Termin;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Einsatzplankontroller f�r Einsatzplan S.fxml Hier wird der
 * Einsatzplan/Terminliste verwaltet.
 * 
 * @author Michael Schr�der
 * @version 5.0
 * @since 27.11.2015
 *
 */

public class EinsatzplanController extends MainController implements Initializable {

	@FXML
	private Button btnTermin;
	@FXML
	private Button btnRapport;
	@FXML
	private DatePicker dpEinsatzPlan = new DatePicker();
	@FXML
	private TableColumn<Termin, String> colEins;
	@FXML
	private TableColumn<Termin, String> colZwei;
	@FXML
	private TableView<Mitarbeiter> tblMitarbeiter;
	@FXML
	private GridPane grdTerminPlan;
	@FXML
	private Label lblWochenNr;
	@FXML
	private Label lbl11;
	@FXML
	private Label lbl12;
	@FXML
	private Label lbl13;
	@FXML
	private Label lbl14;
	@FXML
	private Label lbl21;
	@FXML
	private Label lbl22;
	@FXML
	private Label lbl23;
	@FXML
	private Label lbl24;
	@FXML
	private Label lbl31;
	@FXML
	private Label lbl32;
	@FXML
	private Label lbl33;
	@FXML
	private Label lbl34;
	@FXML
	private Label lbl41;
	@FXML
	private Label lbl42;
	@FXML
	private Label lbl43;
	@FXML
	private Label lbl44;
	@FXML
	private Label lbl51;
	@FXML
	private Label lbl52;
	@FXML
	private Label lbl53;
	@FXML
	private Label lbl54;
	@FXML
	private Label lbl61;
	@FXML
	private Label lbl62;
	@FXML
	private Label lbl63;
	@FXML
	private Label lbl64;

	private static final Logger logger = LogManager.getLogger();
	private HashMap<String, Label> lblHm;
	private HashMap<Label, Termin> terminHm = new HashMap<Label, Termin>();
	protected MainController mainCo;
	protected ObservableList<Termin> olEinsatzplan;
	protected ObservableList<Termin> olEinsatzplanAll;
	protected Termin terminMarkiert = null;
	protected ObservableList<Mitarbeiter> olKontrolleur;

	public EinsatzplanController(MainController mainCo) {
		super();
		this.mainCo = mainCo;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		// olEinsatzplanAll = FXCollections.observableArrayList(mainCo.termine);
		// Liest Feuerungskontrolleure in Tabelle ein und w�hlt ersten
		// Mitarbeiter
		updateData(mainCo.mitarbeiter);

		// Generiert String <-> Label HashMap
		this.lblHm = generateLblHashMap();

		// Initialsiert DatePicker mit aktuellem Datum
		dpEinsatzPlan.setValue(LocalDate.now());
		lblWochenNr.setText("Woche " + String.valueOf(getWochenNr()));

		// legt Action Event auf dpEinsatzPlan. olEinsatzplan wird nur mit
		// Terminen des gew�hlten Datums bef�llt
		dpEinsatzPlan.setOnAction(event -> {
			olEinsatzplanFuellen();
			terminAbfrage();
			lblWochenNr.setText("Woche " + String.valueOf(getWochenNr()));
		});

		// legt listener auf Mitarbeiter Table. olEinsatzplan wird nur mit
		// Terminen des gew�hlten Mitarbeiters bef�llt
		tblMitarbeiter.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			olEinsatzplanFuellen();
			terminAbfrage();
		});

		// �bergibt Termine an lokale Observable List zur weiteren verarbeitung
		olEinsatzplanFuellen();

		addMouseEventToLbl();

		terminAbfrage();

		lbl63.setStyle("-fx-background-color: lightgrey;");
		lbl63.setText("Geschlossen");
		lbl64.setStyle("-fx-background-color: lightgrey;");
		lbl64.setText("Geschlossen");

	}

	/**
	 * Generiert eine HashMap mit String zu den korrespondierende Strings zu
	 * Labels
	 * 
	 * @return gibt die Label HashMap an das Einsatzplan Objekt zur�ck
	 */
	public HashMap<String, Label> generateLblHashMap() {

		HashMap<String, Label> lblHm = new HashMap<String, Label>();
		lblHm.put("11", lbl11);
		lblHm.put("12", lbl12);
		lblHm.put("13", lbl13);
		lblHm.put("14", lbl14);
		lblHm.put("21", lbl21);
		lblHm.put("22", lbl22);
		lblHm.put("23", lbl23);
		lblHm.put("24", lbl24);
		lblHm.put("31", lbl31);
		lblHm.put("32", lbl32);
		lblHm.put("33", lbl33);
		lblHm.put("34", lbl34);
		lblHm.put("41", lbl41);
		lblHm.put("42", lbl42);
		lblHm.put("43", lbl43);
		lblHm.put("44", lbl44);
		lblHm.put("51", lbl51);
		lblHm.put("52", lbl52);
		lblHm.put("53", lbl53);
		lblHm.put("54", lbl54);
		lblHm.put("61", lbl61);
		lblHm.put("62", lbl62);

		// Erweiterbar, falls Samstag auch Arbeit ist
		// lblHm.put("63", lbl63);
		// lblHm.put("64", lbl64);

		return lblHm;

	}

	/**
	 * F�llt die Tabelle "tblMitarbeiter" mit Kontrolleuren aus Mitarbeiter und
	 * w�hlt den ersten Eintrag aus. F�gt zus�tzlich eine Filterfunktion hinzu.
	 * 
	 * @param allmitarbeiter
	 *            Alle Mitarbeiter. Werden nachher auf Kontrolleure gefiltert.
	 */
	public void updateData(List<Mitarbeiter> allmitarbeiter) {
		List<Mitarbeiter> listKontrolleur = new ArrayList<Mitarbeiter>();
		// Nur die Mitarbeiter mit der Rolle 'Kontrolleur' anzeigen
		if (mainCo.loggedInMitarbeiter.getRolle().getBezeichnung().equals("Kontrolleur")) {
			listKontrolleur.add(mainCo.loggedInMitarbeiter);
		} else {
			for (Mitarbeiter m : allmitarbeiter) {
				if (m.getRolle().getBezeichnung().equals("Kontrolleur")) {
					listKontrolleur.add(m);
				}
			}
		}

		this.olKontrolleur = FXCollections.observableArrayList(listKontrolleur);
		this.tblMitarbeiter.setItems(this.olKontrolleur);
		this.tblMitarbeiter.getSelectionModel().selectFirst();
	}

	/**
	 * Iteriert durch die Termin Labels und f�gt Klickevents hinzu.
	 * <p>
	 * Beim Klick, wird der gew�hlte Termin markiert und die Buttons angepasst.
	 */
	public void addMouseEventToLbl() {
		for (Label lbl : lblHm.values()) {

			lbl.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {

				terminAbfrage();
				lbl.setStyle("-fx-border-color: black; -fx-background-color: #82d2fa;");
				btnTermin.setDisable(false);

				if (terminHm.get(lbl) != null) {
					terminMarkiert = terminHm.get(lbl);
					btnTermin.setText("Termin bearbeiten");
					btnRapport.setDisable(false);

					if (getKontrollRapport(terminMarkiert).isEmpty()) {
						btnRapport.setText("Rapport erzeugen");
						if (mainCo.loggedInMitarbeiter.getRolle().getBezeichnung().equals("Sachbearbeiter")) {
							btnRapport.setDisable(true);
						}
					} else {
						btnRapport.setText("Rapport anzeigen");
						btnTermin.setDisable(true);

					}

				} else {
					btnTermin.setText("Termin erzeugen");
					btnRapport.setDisable(true);
					terminMarkiert = new Termin();
					terminMarkiert.setAnlagenstandort(null);
					terminMarkiert.setDatumZeit(getDateOfWeekDay(lbl).toGregorianCalendar());
					terminMarkiert.setEinsatz(Character.getNumericValue(lbl.getId().charAt(4)));
					terminMarkiert.setMitarbeiter(getTblMitarbeiter());

				}

				// TODO: Optional: Vergangene Termine anders Darstellen und
				// nicht mehr ver�nderbar machen
				// if (cal.before(when)) {
				//
				// Buttons und Termine disablen, bzw, ausgrauen
				// lbl.setOpacity(0.5);
				// btn.setDisable(true);
				// }
			});
		}
	}

	/**
	 * Gibt das Datum eines gew�hlten Labels als XMLGregorianCalender zur�ck.
	 * 
	 * @param lbl
	 *            Das Label eines Zeitfensters, dass per Mausklick markiert
	 *            wurde.
	 * @return Ein XMLGregorianCalender Objekt, dass das Datum des gew�hlten
	 *         Labels enth�lt.
	 */
	private XMLGregorianCalendar getDateOfWeekDay(Label lbl) {

		XMLGregorianCalendar date2 = null;
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, dpEinsatzPlan.getValue().getYear());
		cal.set(Calendar.WEEK_OF_YEAR, getWochenNr());
		cal.set(Calendar.DAY_OF_WEEK, 1 + Character.getNumericValue(lbl.getId().charAt(3)));
		Date cDate = cal.getTime();
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(cDate);
		try {
			date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			return date2;
		} catch (DatatypeConfigurationException e) {
			logger.error(e.getMessage());
		}
		return date2;

	}

	/**
	 * Holt den Wochentag eines Termins
	 * 
	 * @param a
	 *            Die Position im Terminlisten-Array
	 * @return Der Wochentag, ein Wert von 1-6, wird -1 gerechnet, da Woche in
	 *         Java mit Sonntag beginnt
	 */
	public int getWochentag(Termin termin) {
		return termin.getDatumZeit().get(Calendar.DAY_OF_WEEK) - 1;
	}

	/**
	 * Holt den Wochentag aus dem DatePicker.
	 * 
	 * @return Gibt die Wochennummer anhand des DatePicker Datums zur�ck.
	 */
	public int getWochenNr() {
		LocalDate date = dpEinsatzPlan.getValue(); // input from your date
													// picker
		Locale locale = Locale.GERMANY;
		int weekOfYear = date.get(WeekFields.of(locale).weekOfWeekBasedYear());
		return weekOfYear;
	}

	/**
	 * Methode welche aus dem aLbl (Tag und Zeitfenster) String das
	 * entsprechende Label zur�ck gibt.
	 * 
	 * @param aLbl
	 *            String �bergabewert. Die erste Zahl steht f�r den Tag, die
	 *            Zweite f�r das Zeitfenster. z.B. "12", Montag von 10:00 -
	 *            12:00.
	 * @return Label des entsprechenden aLbl, z.B. lbl32.
	 */
	public Label getLabel(String aLbl) {

		return lblHm.get(aLbl);

	}

	/**
	 * Fragt zu Beginn ab, ob Termine vorhanden sind und �ndert die Zeitfenster
	 * Labels entsprechend ab.
	 */
	public void terminAbfrage() {
		// leert terminHM, damit nur die neu bef�llten Termine ausgelesen werden
		terminHm.clear();
		terminMarkiert = null;

		btnRapport.setDisable(true);
		btnTermin.setDisable(true);

		try {

			// Setzt vorl�ufig alle Termin Label auf "Frei"
			for (Label lbl : lblHm.values()) {

				// Labelhintergrund von freien Terminen wird gr�n gemacht
				lbl.setStyle("-fx-background-color: #deffe9;");
				lbl.setText("Frei");

			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		try {
			// Legt Termine aus olEinsatzplan auf Labels
			for (Termin termin : olEinsatzplan) {

				String aLbl = Integer.toString(getWochentag(termin)) + Integer.toString(termin.getEinsatz());
				getLabel(aLbl).setText(termin.getAnlagenstandort().getStrasse() + System.getProperty("line.separator")
						+ termin.getAnlagenstandort().getPlzOrt().getOrt() + " "
						+ termin.getAnlagenstandort().getPlzOrt().getPlz());
				getLabel(aLbl).setStyle("-fx-background-color: #82d2fa;");
				terminHm.put(getLabel(aLbl), termin);

			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

	/**
	 * Holt das Mitarbeiter Objekt, des ausgew�hlten Mitarbeiters aus der
	 * Tabelle.
	 * 
	 * @return Gibt das markierte Mitarbeiter Objekt zur�ck.
	 */
	public Mitarbeiter getTblMitarbeiter() {
		Mitarbeiter mtb = tblMitarbeiter.getSelectionModel().getSelectedItem();
		return mtb;
	}

	/**
	 * Ruft den EinsatplanVerwaltenkontroller mit
	 * EinsatzplanVerwaltenController.fxml auf. Hier wird das Termin
	 * erzeugen/bearbeite Fenster erzeugt.
	 */
	public void erzeugeTermin() {

		Parent root;

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/EinsatzplanVerwaltenController.fxml"));
			EinsatzplanVerwaltenController controller = new EinsatzplanVerwaltenController(this);
			fxmlLoader.setController(controller);
			root = fxmlLoader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.show();
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Ruft den EinsatplanDruckkontroller mit EinsatzplanDruck.fxml auf. Hier
	 * wird das Druck Fenster mit der �bersicht des gew�hlten Kontrolleurs f�r
	 * die aktuelle Woche erzeugt.
	 */
	public void erzeugeDruckSicht() {
		Parent root;

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/EinsatzplanDruck.fxml"));
			EinsatzplanDruckController controller = new EinsatzplanDruckController(this);
			fxmlLoader.setController(controller);
			root = fxmlLoader.load();

			Stage stage = new Stage();
			stage.setTitle("Einsatzplan Drucken");
			stage.setScene(new Scene(root));
			stage.setResizable(false);
			stage.show();
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Ruft erzeugeTermin() auf.
	 */
	@FXML
	public void btnTerminClicked() {

		erzeugeTermin();
	}

	/**
	 * Ruft den Rapportkontroller mit Rapport.fxml auf. Hier wird das Rapport
	 * Fenster zum gew�hlten Termin erzeugt.
	 */
	public void erzeugeRapport() {
		Parent root;

		try {

			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/feukora/client/views/Rapport.fxml"));
			RapportController controller = new RapportController(terminMarkiert);
			fxmlLoader.setController(controller);
			root = fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Rapport erfassen");
			stage.setScene(new Scene(root));
			stage.show();

		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Ruft den RapportDruckkontroller mit RapportDruck.fxml auf. Hier wird das
	 * Rapport Drucken Fenster zum gew�hlten Termin erzeugt.
	 */
	public void zeigeRapport() {
		Parent root;

		try {

			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/RapportDruck.fxml"));
			RapportDruckController controller = new RapportDruckController(getKontrollRapport(terminMarkiert).get(0));
			fxmlLoader.setController(controller);
			root = fxmlLoader.load();
			Stage stage = new Stage();
			stage.setTitle("Rapport Druckansicht Vorschau");
			stage.setScene(new Scene(root));
			stage.setResizable(false);
			stage.show();

		} catch (Exception e) {
			logger.error("Rapport Druckfenster kann nicht geladen werden:");
			logger.error(e.getMessage());
		}
	}

	/**
	 * Ruft erzeugeDruckSicht() auf.
	 */
	@FXML
	public void btnUebersichtClicked() {
		erzeugeDruckSicht();
	}

	/**
	 * Ruft erzeugeRapport() oder zeigeRapport() auf. Je nach dem, ob bereits
	 * ein Rapport vorhanden ist.
	 */
	@FXML
	public void btnRapportClicked() {
		if (!getKontrollRapport(terminMarkiert).isEmpty()) {
			zeigeRapport();
		} else {
			erzeugeRapport();
		}
	}

	/**
	 * Falls ein Rapport zum gegebenen Termin existiert, wird dieser zur�ck
	 * gegeben.
	 * 
	 * @param termin
	 *            Der ausgew�hlte Termin, zu dem, anhand der ID, der Rapport
	 *            geholt wird.
	 * @return Der Rapport zum mitgegebenen Termin.
	 */
	public List<Kontrolle> getKontrollRapport(Termin termin) {
		try {
			return new ArrayList<Kontrolle>(
					serviceProvider.getKontrolleService().getKontrolleByTerminId(termin.getId()));
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * �bergibt Termine an lokale Observable List zur weiteren verarbeitung.
	 */
	public void olEinsatzplanFuellen() {
		List<Termin> tlist = null;
		try {
			tlist = new ArrayList<Termin>(serviceProvider.getTerminService().getListTermin());
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		tlist.removeIf(t -> !String.valueOf(t.getDatumZeit().get(Calendar.WEEK_OF_YEAR))
				.equals(String.valueOf(getDateOfWeekDay(lbl11).toGregorianCalendar().get(Calendar.WEEK_OF_YEAR))));
		this.olEinsatzplanAll = FXCollections.observableArrayList(tlist);
		tlist.removeIf(
				t -> !String.valueOf(t.getMitarbeiter().getId()).equals(String.valueOf(getTblMitarbeiter().getId())));
		this.olEinsatzplan = FXCollections.observableArrayList(tlist);
	}

}
