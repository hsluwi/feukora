package com.feukora.client.controller;

import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Mitarbeiter;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Kontroller Klasse f�r die View Kontrolleur.fxml
 * @author Marco Hostettler
 * @version 3.0
 * @since 03.12.15
 *
 */
public class KontrolleurController extends MainController implements Initializable {
	@FXML
	TableView<Mitarbeiter> tblKontrolleur;
	@FXML
	TableColumn<Mitarbeiter, String> colVorname;
	@FXML
	TableColumn<Mitarbeiter, String> colNachname;
	@FXML
	TextField txtSuche;
	@FXML
	TextField txtGeburtsdatum;
	@FXML
	TextField txtFirma;
	@FXML
	TextField txtStrasse;
	@FXML
	TextField txtPLZ;
	@FXML
	TextField txtOrt;
	@FXML
	TextField txtUsername;
	@FXML
	TextField txtVorname;
	@FXML
	TextField txtNachname;
	@FXML
	Button btnBearbeiten;
	@FXML
	Button btnLoeschen;
	
	protected MainController mainCo;
	protected ObservableList<Mitarbeiter> olKontrolleur;
	protected Mitarbeiter clickedMitarbeiter;
	private static final Logger logger = LogManager.getLogger();

	public KontrolleurController(MainController mainCo) {
		super();
		this.mainCo = mainCo;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		updateData(mainCo.mitarbeiter);

	}
	/**
	 * F�llt die Tabelle mit den Mitarbeiter Daten, welche initial vom MainController geladen worden sind.
	 * @param allmitarbeiter
	 */
	public void updateData(List<Mitarbeiter> allmitarbeiter) {
		List<Mitarbeiter> listKontrolleur = new ArrayList<Mitarbeiter>();
		// Nur die Mitarbeiter mit der Rolle 'Kontrolleur' anzeigen
		for (Mitarbeiter m : allmitarbeiter) {
			if (m.getRolle().getBezeichnung().equals("Kontrolleur")) {
				listKontrolleur.add(m);
			}
		}

		this.olKontrolleur = FXCollections.observableArrayList(listKontrolleur);
		this.tblKontrolleur.setItems(this.olKontrolleur);
	}

	@FXML
	void btnBearbeitenClicked() {
		Parent root;

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/KontrolleurVerwalten.fxml"));

			KontrolleurVerwaltenController controller = new KontrolleurVerwaltenController(this);
			fxmlLoader.setController(controller);

			root = fxmlLoader.load();

			Stage stage = new Stage();
			stage.initModality(Modality.WINDOW_MODAL);
			stage.initOwner(mainCo.mainstage.getScene().getWindow());
			stage.setTitle("Kontrolleur bearbeiten");
			stage.setScene(new Scene(root));
			stage.show();

		} catch (IOException e) {
			logger.error("Laden der Bearbeitungs-View 'kontrolleurVerwalten' fehlgeschlagen." + e);
		}
	}

	@FXML
	public void btnNeuClicked() {
		Parent root;
		clickedMitarbeiter = null;
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/KontrolleurVerwalten.fxml"));
			KontrolleurVerwaltenController controller = new KontrolleurVerwaltenController(this);
			fxmlLoader.setController(controller);
			root = fxmlLoader.load();

			Stage stage = new Stage();
			stage.initModality(Modality.WINDOW_MODAL);
			stage.initOwner(mainCo.mainstage.getScene().getWindow());
			stage.setTitle("Kontrolleur erfassen");
			stage.setScene(new Scene(root));
			stage.show();
		} catch (IOException e) {
			logger.error("Laden der Erstellungs-View 'kontrolleurVerwalten' fehlgeschlagen." + e);
		}

	}

	@FXML
	public void btnLoeschenClicked() {

		Alert alert = new Alert(AlertType.CONFIRMATION, "Wollen Sie diesen Kontrolleur endg�ltig l�schen?");
		alert.showAndWait().ifPresent(response -> {
			if (response == ButtonType.OK) {
				kontrolleurLoeschen();
			}

		});
	}

	/**
	 * L�schen des Kontrolleurs auf dem Server mit entpsrechendem logging.
	 */
	private void kontrolleurLoeschen() {
		try {
			if (serviceProvider.getMitarbeiterService().removeMitarbeiter(clickedMitarbeiter.getId())) {
				Alert alert = new Alert(AlertType.INFORMATION, "Kontrolleur wurde erfolgreich gel�scht.");
				alert.show();
				loadSpecificData(this);
				txtSuche.clear();
				logger.info("Kontrolleur " + clickedMitarbeiter.getVorname() + " " + clickedMitarbeiter.getNachname()
						+ " wurde gel�scht.");
			} else {
				Alert alert = new Alert(AlertType.ERROR, "Kontrolleur konnte nicht gel�scht werden");
				alert.show();
				logger.error("Kontrolleur " + clickedMitarbeiter.getVorname() + " " + clickedMitarbeiter.getNachname()
						+ "  konnte nicht gel�scht werden.");
			}
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}

	}

	/**
	 * Bef�llung der FXML Felder des ausgew�hlten Kontrolleurs
	 * 
	 */
	@FXML
	void getKontrolleurDetails() {
		this.btnBearbeiten.setDisable(false);
		this.btnLoeschen.setDisable(false);
		this.clickedMitarbeiter = (Mitarbeiter) tblKontrolleur.getSelectionModel().getSelectedItem();
		this.txtStrasse.setText(clickedMitarbeiter.getStrasse());
		int plz = clickedMitarbeiter.getPlzOrt().getPlz();
		this.txtPLZ.setText(String.valueOf(plz));
		this.txtOrt.setText(clickedMitarbeiter.getPlzOrt().getOrt());
		this.txtUsername.setText(clickedMitarbeiter.getUserName());
		this.txtVorname.setText(clickedMitarbeiter.getVorname());
		this.txtNachname.setText(clickedMitarbeiter.getNachname());

	}

}
