package com.feukora.client.controller;

import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Brennerart;
import com.feukora.server.models.Brennermodell;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
/**
 * 
 * @author Sven M�ller
 * @version 3.0
 * @since 10.12.2015
 * 
 * Controller Klasse f�r ein �bergebenes Brennermodell bearbeiten oder einen neuen 
 * Brennermodell erstellen.
 *
 */
public class BrennerModellVerwaltenController extends MainController implements Initializable {
	@FXML private Button tbnAbbrechen;
	@FXML private Label lblBrennerModellID;
	@FXML private Label lblViewTitel;
	@FXML private AnchorPane anpBrennerErfassen;
	@FXML private TextField txtBaujahr;
	@FXML private TextField txtTyp;
	@FXML private TextField txtLeistung;
	@FXML private TextField txtHersteller;
	@FXML private ComboBox<String> cbBrennerart;
	@FXML private TextField txtBrennerart;
	
	private static final Logger logger = LogManager.getLogger();
	
	private BrennerModellController brMoCo;
	private ObservableList<String> olbrennerartDaten = FXCollections.observableArrayList();
	private List<Brennerart> brennerartListe;
	private Brennerart objBrennerart;
	
	private Brennermodell brennerModell;
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 2.0
	 * @param brMoCo  Brennermodell Controller Objekt der Klasse BrennerModellController
	 *
	 * Der Konstruktor wird in der Klasse BrennerModellController verwendet. 
	 * Durch den Konstruktor wird der selektierte Brenner �bergeben.
	 */
	public BrennerModellVerwaltenController (BrennerModellController brMoCo){
		super();
		this.brMoCo = brMoCo;
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * 
	 * @param Standardparameter von implementierter Initializable Klasse
	 * 
	 * Bereitet Brennerart-Combobox vor
	 * Pr�ft ob ein Objekt bearbeitet oder neu erstellt wird
	 *
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		//Bef�lle Combobox mit Brennerarten aus DB
		try {
			this.brennerartListe = new ArrayList<Brennerart>(serviceProvider.getBrennerartService().getListBrennerart());
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		brennerartListe.forEach(b -> olbrennerartDaten.add(b.getBezeichnung()));
		this.cbBrennerart.setItems(olbrennerartDaten);
		this.cbBrennerart.setValue("Bitte w�hlen Sie...");
		
		//Wenn "Bearbeiten"
		if (brMoCo.clickedBrennerModell != null){ 
			this.brennerModell = brMoCo.clickedBrennerModell;
			this.objBrennerart = brennerModell.getBrennerart();
			
			// GUI mit Brennerrmodell Daten bef�llen
			lblViewTitel.setText("Brennermodell bearbeiten");
			lblBrennerModellID.setText(Integer.toString(brennerModell.getId()));
			txtHersteller.setText(brennerModell.getHersteller());
			txtTyp.setText(brennerModell.getBezeichnung());
			txtLeistung.setText(Integer.toString(brennerModell.getFeuerungswaermeleistung()));
			cbBrennerart.setValue(brennerModell.getBrennerart().getBezeichnung());
			
		//Wenn "Neu"	
		} else { 
			this.brennerModell = new Brennermodell();
		}
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 3.0
	 * 
	 * �bernimmt bei Auswahl der Combobox den entsprechend Wert f�r die sp�tere Persistierung
	 *
	 */
	@FXML
	private void cbBrennerartChoice(){
		for (Brennerart b : brennerartListe) {
			if (b.getBezeichnung().equals(cbBrennerart.getSelectionModel().getSelectedItem())) {
				objBrennerart = b;
			}
		}
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 2.0
	 * @param Standardparameter von implementierter Initializable Klasse
	 * 
	 * Ruft den Webservice zur Persistierung des Brennermodell auf
	 * Gibt entsprechende Meldung aus
	 * Updatet das Fenster Brennermodell (BrennerModellController) mit dem neuen Wert 
	 * Schliesst dieses Fenster (BrennerModellVerwaltenController)
	 */
	@FXML
	public void btnSpeichernClicked (ActionEvent event){
		
		//BrennerModell Objekt erstelle
		brennerModell.setHersteller(txtHersteller.getText());
		brennerModell.setBezeichnung(txtTyp.getText());
		brennerModell.setBrennerart(objBrennerart);
		brennerModell.setFeuerungswaermeleistung(Integer.parseInt(txtLeistung.getText()));
		
		//Objekt validieren
		String errors = validate(brennerModell);
		
		//Wenn Validierung ok...
		if (errors != "") {
			Alert alert = new Alert(AlertType.WARNING, errors);
			alert.show();
			logger.error("Daten unvollst�ndig/Fehlerhaft. Keine weiterleitung an Server");
		
			
			//Wenn Persistierung erfolgreich gebe Erfolgsmeldung aus....
		}else {
			//returnObjekt nach erfolgreicher Persistierung
			Brennermodell returnbrennerModell = new Brennermodell();
			
			try {
				if((returnbrennerModell = serviceProvider.getBrennermodellService().saveBrennermodell(brennerModell)) == null){
					Alert alert = new Alert(AlertType.ERROR, "Brennermodell konnte nicht gespeichert werden");
					alert.show();
					logger.error("Brennermodell konnte nicht in Datenbank gespeichert werden.");
				// ...sonst Fehlermeldung
				} else {
					Alert alert = new Alert(AlertType.INFORMATION, "Brennermodell wurde erfolgreich gespeichert");
					alert.showAndWait().ifPresent(response -> {
						if (response == ButtonType.OK) {
							close(event);
						}
					});
				
				//Reload Daten vom Server, sowie Reload der TableView
				loadSpecificData(brMoCo);
				brMoCo.txtSuche.clear();
				brMoCo.txtSuche.setText(returnbrennerModell.getBezeichnung());
				}
			} catch (RemoteException e) {
				logger.error(e.getMessage());
			}
		}
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * 
	 * Beim Klicken des Abbrechen Button wird das Fenster geschlossen
	 */
	@FXML
	public void btnAbbrechenClicked(ActionEvent event){
		close(event);
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * 
	 * Schliesst das Fenster
	 */
	private void close(ActionEvent e) {
		Node node = (Node) e.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		stage.close();
	}
}