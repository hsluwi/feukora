package com.feukora.client.controller;

import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Brennstoff;
import com.feukora.server.models.Waermeerzeugermodell;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * 
 * @author Sven Müller
 * @version 3.0
 * @since 10.12.2015
 *
 */
public class WaermeerzeugerModellVerwaltenController extends MainController implements Initializable {
	@FXML private Button tbnAbbrechen;
	@FXML private Label lblWaermeerzeugerModellID;
	@FXML private Label lblViewTitel;
	@FXML private AnchorPane anpWaermeerzeugerModellErfassen;
	@FXML private TextField txtHersteller;
	@FXML private TextField txtTyp;
	@FXML private ComboBox<String> cbBrennstoff;
	@FXML private TextField txtBrennstoff;
	
	private ObservableList<String> olbrennstoffDaten = FXCollections.observableArrayList();
	private Brennstoff objBrennstoff;
	private List<Brennstoff> brennstoffListe;
	
	private static final Logger logger = LogManager.getLogger();
	
	protected WaermeerzeugerModellController weMoCo;

	private Waermeerzeugermodell waermeerzeugerModell;
	 
	/**
	 * 
	 * @author Sven Müller
	 * @since 2.0
	 * @param weMoCo  Wärmeerzeuger Controller Objekt der Klasse WaermeerzeugerModellController
	 *
	 * Der Konstruktor wird in der Klasse WaermeerzeugerModellController verwendet. 
	 * Durch den Konstruktor wird der selektierte Wärmeerzeuger übergeben.
	 */
	
	public WaermeerzeugerModellVerwaltenController (WaermeerzeugerModellController weMoCo){
		super();
		this.weMoCo = weMoCo;
	}
	
	/**
	 * 
	 * @author Sven Müller
	 * @since 1.0
	 * 
	 * @param Standardparameter von implementierter Initializable Klasse
	 * 
	 * Bereitet Brennstoff-Combobox vor
	 * Prüft ob ein Objekt bearbeitet oder neu erstellt wird
	 *
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		//Befülle Combobox mit Brennstoffsorten aus DB
		try {
			this.brennstoffListe = new ArrayList<Brennstoff>(serviceProvider.getBrennstoffService().getListBrennstoff());
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
		brennstoffListe.forEach(b -> olbrennstoffDaten.add(b.getBezeichnung()));
		this.cbBrennstoff.setItems(olbrennstoffDaten);
		this.cbBrennstoff.setValue("Bitte wählen Sie...");
		
		//Wenn "Bearbeiten"
		if (weMoCo.clickedWaermeerzeugerModell != null){
			this.waermeerzeugerModell = weMoCo.clickedWaermeerzeugerModell;
			this.objBrennstoff = waermeerzeugerModell.getBrennstoff();
			
			// GUI mit Wärmeerzeugermodell Daten befüllen
			lblViewTitel.setText("Wärmeerzeugermodell bearbeiten");
			lblWaermeerzeugerModellID.setText(Integer.toString(waermeerzeugerModell.getId()));
			txtHersteller.setText(waermeerzeugerModell.getHersteller());
			txtTyp.setText(waermeerzeugerModell.getBezeichnung());
			cbBrennstoff.setValue(waermeerzeugerModell.getBrennstoff().getBezeichnung());
		//Wenn "Neu"
		} else {
			this.waermeerzeugerModell = new Waermeerzeugermodell();
		}
	}

	/**
	 * 
	 * @author Sven Müller
	 * @since 3.0
	 * 
	 * Übernimmt bei Auswahl der Combobox den entsprechend Wert für die spätere Persistierung
	 *
	 */
	@FXML
	private void cbBrennstoffChoice(){
		for (Brennstoff b : brennstoffListe) {
			if (b.getBezeichnung().equals(cbBrennstoff.getSelectionModel().getSelectedItem())) {
				objBrennstoff = b;
			}
		}
	}
	
	/**
	 * 
	 * @author Sven Müller
	 * @since 2.0
	 * @param Standardparameter von implementierter Initializable Klasse
	 * 
	 * Ruft den Webservice zur Persistierung des Wärmeerzeugersmodell auf
	 * Gibt entsprechende Meldung aus
	 * Updatet das Fenster Wärmeerzeugermodell (WaermeerzeugerModellController) mit dem neuen Wert 
	 * Schliesst dieses Fenster (WaermeerzeugerModellVerwaltenController)
	 */
	
	@FXML
	public void btnSpeichernClicked (ActionEvent event){
		
		//WärmeerzeugerModell Objekt erstelle
		waermeerzeugerModell.setHersteller(txtHersteller.getText());
		waermeerzeugerModell.setBezeichnung(txtTyp.getText());
		waermeerzeugerModell.setBrennstoff(objBrennstoff);
		
		//Objekt validieren
		String errors = validate(waermeerzeugerModell);
		
		//Wenn Validierung ok...
		if (errors != "") {
			Alert alert = new Alert(AlertType.WARNING, errors);
			alert.show();
			logger.error("Daten unvollständig/Fehlerhaft. Keine weiterleitung an Server");
		//Objekt speichern-----------------------------
		}else{				
			Waermeerzeugermodell returnwaermeerzeugerModell = new Waermeerzeugermodell();
			
			//Wenn Persistierung erfolgreich gebe Erfolgsmeldung aus....
			try {
				if((returnwaermeerzeugerModell = serviceProvider.getWaermeerzeugermodellService().saveWaermeerzeugermodell(waermeerzeugerModell)) == null){
					
					Alert alert = new Alert(AlertType.ERROR, "Wärmeerzeugermodell konnte nicht gespeichert werden");
					alert.show();
					logger.error("Wärmeerzeugermodell konnte nicht in Datenbank gespeichert werden.");
				// ...sonst Fehlermeldung	
				} else {
					Alert alert = new Alert(AlertType.INFORMATION, "Wärmeerzeugermodell wurde erfolgreich gespeichert");
					alert.showAndWait().ifPresent(response -> {
						if (response == ButtonType.OK) {
							close(event);
						}
					});
				
				//Reload Daten vom Server, sowie Reload der TableView
				loadSpecificData(weMoCo);
				weMoCo.txtSuche.clear();
				weMoCo.txtSuche.setText(returnwaermeerzeugerModell.getBezeichnung());
				}
			} catch (RemoteException e) {
				logger.error(e.getMessage());
			}
		}
	
	}
	/**
	 * 
	 * @author Sven Müller
	 * @since 1.0
	 * 
	 * Beim Klicken des Abbrechen Button wird das Fenster geschlossen
	 *
	 */
	@FXML
	public void btnAbbrechenClicked(ActionEvent event){
		
		close(event);
	}
	/**
	 * 
	 * @author Sven Müller
	 * @since 1.0
	 * 
	 * Schliesst das Fenster
	 *
	 */
	private void close(ActionEvent e) {
		Node node = (Node) e.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		stage.close();

	}
}
