package com.feukora.client.controller;

import java.net.URL;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Anlagenstandort;
import com.feukora.server.models.Termin;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * EinsatzplanVerwaltenkontroller f�r EinsatzplanVerwaltenController.fxml Hier
 * werden Termine erzeugt.
 * 
 * @author Michael Schr�der
 * @version 3.0
 * @since 10.12.2015
 */
public class EinsatzplanVerwaltenController extends MainController implements Initializable {

	@FXML
	private Label lblEi1;
	@FXML
	private Label lblEi2;
	@FXML
	private Label lblEi3;
	@FXML
	private Label lblEi4;
	@FXML
	private Label lblTermin;
	@FXML
	private TextField txtfDate;
	@FXML
	private TextField txtfHeizMon;
	@FXML
	private TextField txtSuche;
	@FXML
	private Button btnLoeschen;
	@FXML
	private TableColumn<Anlagenstandort, String> colANummer;
	@FXML
	private TableColumn<Anlagenstandort, String> colStandort;
	@FXML
	private TableColumn<Anlagenstandort, String> colStrasse;
	@FXML
	private TableColumn<Anlagenstandort, String> colFirma;
	@FXML
	private TableView<Anlagenstandort> tblAnlage;

	private static final Logger logger = LogManager.getLogger();
	protected EinsatzplanController eiCo;
	private ObservableList<Anlagenstandort> olAnlagen;

	public EinsatzplanVerwaltenController(EinsatzplanController eiCo) {
		super();
		this.eiCo = eiCo;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		setCellValueFactory();
		txtfHeizMon.setText(eiCo.terminMarkiert.getMitarbeiter().getVorname() + " "
				+ eiCo.terminMarkiert.getMitarbeiter().getNachname());
		txtfDate.setText(formateDate());

		setEinsatz();

		updateData(eiCo.mainCo.anlagenListe);

		// F�gt listener hinzu, der die markierte Anlage im eiCo.terminMarkiert
		// speichert
		tblAnlage.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				eiCo.terminMarkiert.setAnlagenstandort(tblAnlage.getSelectionModel().getSelectedItem());
			}
		});

		if (eiCo.terminMarkiert.getAnlagenstandort() != null) {

			lblTermin.setText("Termin bearbeiten");
			txtSuche.setText(String.valueOf(eiCo.terminMarkiert.getAnlagenstandort().getId()));
			tblAnlage.getSelectionModel().selectFirst();
			txtSuche.clear();

		} else {
			btnLoeschen.setDisable(true);
			tblAnlage.getSelectionModel().clearSelection();
		}

	}

	/**
	 * Update der Tabelle mit allen Anlage Daten und Hinzuf�gen von
	 * Filterfunnktion.
	 * 
	 * @param anlagen
	 *            Anlagen welche im MainController geladen worden sind
	 */
	public void updateData(List<Anlagenstandort> anlagen) {

		this.olAnlagen = FXCollections.observableArrayList(anlagen);
		tblAnlage.setItems(this.olAnlagen);

		FilteredList<Anlagenstandort> filteredData = new FilteredList<>(olAnlagen, p -> true);

		txtSuche.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(anlage -> {
				// TxtSuche = leer , Zeige alle Daten an
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}

				String lowerCaseFilter = newValue.toLowerCase();
				if (String.valueOf(anlage.getId()) != null
						&& String.valueOf(anlage.getId()).toLowerCase().contains(lowerCaseFilter)) {
					return true; // Filter = Anlagen ID

				} else if (anlage.getPlzOrt().getOrt() != null
						&& anlage.getPlzOrt().getOrt().toLowerCase().contains(lowerCaseFilter)) {
					return true; // Filter = Ort
				} else if (anlage.getStrasse() != null && anlage.getStrasse().toLowerCase().contains(lowerCaseFilter)) {
					return true; // Filter = Strasse
				} else if (anlage.getKunde().getFirma() != null
						&& anlage.getKunde().getFirma().toLowerCase().contains(lowerCaseFilter)) {
					return true; // Filter = Kunden Firma
				}
				return false; // Filter !=
			});
		});
		SortedList<Anlagenstandort> sortedData = new SortedList<>(filteredData);

		sortedData.comparatorProperty().bind(tblAnlage.comparatorProperty());
		tblAnlage.setItems(sortedData);
	}

	/**
	 * Konfiguration f�r die Tabellenbef�llung, f�r colKunde und colStandort da
	 * diese nicht direkt vom Anlagenstandort-Objekt kommen - FXML limitation
	 */
	public void setCellValueFactory() {
		colStandort.setCellValueFactory(
				new Callback<TableColumn.CellDataFeatures<Anlagenstandort, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TableColumn.CellDataFeatures<Anlagenstandort, String> p) {
						return new SimpleObjectProperty<String>(p.getValue().getPlzOrt().getOrt());
					}
				});
		colFirma.setCellValueFactory(
				new Callback<TableColumn.CellDataFeatures<Anlagenstandort, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TableColumn.CellDataFeatures<Anlagenstandort, String> p) {
						return new SimpleObjectProperty<String>(p.getValue().getKunde().getFirma());
					}
				});
	}

	/**
	 * Bricht die Termin Erzeugung/Bearbeitung ab. Schliesst das Fenster.
	 * 
	 * @param event
	 *            - ActionEvent
	 */
	@FXML
	public void btnAbbrechenClicked(ActionEvent event) {

		close(event);

	}

	private void close(ActionEvent e) {
		Node node = (Node) e.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		stage.close();

	}

	/**
	 * Formatiert komplexes xmlGreogrian format in leserliches "dd.MM.yyyy"
	 * Format
	 * 
	 * @return String Wert mit Datum vom gew�hlten Termin im "dd.MM.yyyy" Format
	 */
	public String formateDate() {

		Calendar calendar = eiCo.terminMarkiert.getDatumZeit();
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		sdf.setTimeZone(calendar.getTimeZone());
		String dateString = sdf.format(calendar.getTime());
		return dateString;
	}

	/**
	 * Markiert den Einsatz aus dem markierten Termin
	 */
	public void setEinsatz() {

		switch (eiCo.terminMarkiert.getEinsatz()) {
		case 1:
			lblEi1.setStyle("-fx-border-color: black; -fx-background-color: #82d2fa;");
			break;
		case 2:
			lblEi2.setStyle("-fx-border-color: black; -fx-background-color: #82d2fa;");
			break;
		case 3:
			lblEi3.setStyle("-fx-border-color: black; -fx-background-color: #82d2fa;");
			break;
		case 4:
			lblEi4.setStyle("-fx-border-color: black; -fx-background-color: #82d2fa;");
			break;

		default:
			logger.error("Falscher Einsatzwert");
			break;
		}

	}

	/**
	 * Speichert das Termin Objekt in der Datenbank
	 * 
	 * @param event
	 *            - ActionEvent
	 */
	@FXML
	public void btnSpeichernClicked(ActionEvent event) {

		if (eiCo.terminMarkiert.getAnlagenstandort() == null) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Kein Anlagestandort gew�hlt");
			alert.setHeaderText("Es wurde kein Anlagestandort gew�hlt");
			alert.setContentText("W�hlen Sie in der Anlagestandort Tabelle einen Anlagestandort f�r den Termin.");
			alert.showAndWait();
		} else {

			// Objekt speichern-----------------------------

			// Wenn Persistierung erfolgreich gebe Erfolgsmeldung aus....
			try {
				Termin returnTermin = new Termin();
				if ((returnTermin = serviceProvider.getTerminService().saveTermin(eiCo.terminMarkiert)) == null) {
					Alert alert = new Alert(AlertType.WARNING);
					alert.setTitle("Termin Speichern Fehlgeschlagen");
					alert.setHeaderText("Termin nicht gespeichert");
					alert.setContentText("Termin konnte nicht gespeichert werden");
					alert.showAndWait();
					logger.error("Termin konnte nicht in Datenbank gespeichert werden.");
					// ...sonst Fehlermeldung
				} else {
					Alert alert = new Alert(AlertType.INFORMATION, "Termin wurde erfolgreich gespeichert");
					alert.showAndWait().ifPresent(response -> {
						if (response == ButtonType.OK) {
							close(event);
						}
					});
				}
			} catch (RemoteException e) {
				logger.error(e.getMessage());
			}
			eiCo.olEinsatzplanFuellen();
			eiCo.terminAbfrage();
		}

	}

	/**
	 * Fragt ob ein Termin wirklich gel�scht werden soll und ruft
	 * terminLoeschen(event) auf.
	 * 
	 * @param event
	 *            - ActionEvent
	 */
	@FXML
	public void btnLoeschenClicked(ActionEvent event) {

		Alert alert = new Alert(AlertType.CONFIRMATION, "Wollen Sie diesen Termin endg�ltig l�schen?");
		alert.showAndWait().ifPresent(response -> {
			if (response == ButtonType.OK) {
				terminLoeschen(event);
			}

		});

	}

	/**
	 * L�scht das Termin Objekt aus der Datenbank
	 * 
	 * @param event
	 *            - ActionEvent
	 */
	public void terminLoeschen(ActionEvent event) {

		// L�scht den Termin vom Server und loggt das entsprechende Ergebniss

		try {
			if (serviceProvider.getTerminService().removeTermin(eiCo.terminMarkiert.getId())) {
				Alert alert = new Alert(AlertType.INFORMATION, "Termin wurde erfolgreich gel�scht");
				alert.show();
				eiCo.olEinsatzplanFuellen();
				eiCo.terminAbfrage();

				close(event);

				logger.info("Termin am " + eiCo.terminMarkiert.getDatumZeit() + " vom Kontrolleur "
						+ eiCo.terminMarkiert.getMitarbeiter().getNachname() + " wurde gel�scht.");
			} else {

				Alert alert = new Alert(AlertType.ERROR, "Termin konnte nicht gel�scht werden");
				alert.show();
				logger.error("Termin am " + eiCo.terminMarkiert.getDatumZeit() + " vom Kontrolleur "
						+ eiCo.terminMarkiert.getMitarbeiter().getNachname() + " konnte nicht gel�scht werden.");

			}
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}

	}
}
