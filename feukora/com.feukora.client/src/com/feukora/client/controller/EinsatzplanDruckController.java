package com.feukora.client.controller;



import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Termin;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.print.PageLayout;
import javafx.print.PageOrientation;
import javafx.print.Paper;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * Stellt die Druckansicht f�r den Einsatzplan zur Verf�gung.
 * @author Philipp Anderhub
 * @version 1.0
 * @since 1.0
 *
 */
public class EinsatzplanDruckController extends MainController implements Initializable {
	@FXML
	private Button btnDrucken;
	@FXML
	private Button btnAbbrechen;
	@FXML
	private AnchorPane mainPane;
	@FXML
	private TableView<Termin> tblTermine;
	
	@FXML
	private TableColumn<Termin, String> colDatum;
	@FXML
	private TableColumn<Termin, String> colZeit;
	@FXML
	private TableColumn<Termin, String> colStrasse;
	@FXML
	private TableColumn<Termin, String> colOrt;
	@FXML
	private TableColumn<Termin, String> colKontrolleur;
	@FXML
	private TableColumn<Termin, String> colKunde;
	@FXML
	private TableColumn<Termin, String> colStatus;


	
	protected EinsatzplanController eiCo;
	
	private String ergebnis = "offen";
	
	private static final Logger logger = LogManager.getLogger();


	public EinsatzplanDruckController(EinsatzplanController einsatzplanController) {
		super();
		this.eiCo = einsatzplanController;
		
	}
	
	private void setCellValueFactory() {
		colDatum.setCellValueFactory(
				new Callback<TableColumn.CellDataFeatures<Termin, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TableColumn.CellDataFeatures<Termin, String> p) {
						Calendar calendar = p.getValue().getDatumZeit();
						SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
						format.setTimeZone(calendar.getTimeZone());
						String dateString = format.format(calendar.getTime());
						return new SimpleObjectProperty<String>(dateString);
					}
				});
	
	
		colZeit.setCellValueFactory(
			new Callback<TableColumn.CellDataFeatures<Termin, String>, ObservableValue<String>>() {
				@Override
				public ObservableValue<String> call(TableColumn.CellDataFeatures<Termin, String> p) {
					String zeit = null;
					switch (p.getValue().getEinsatz()) {
					case 1: 		zeit = "08:00 Uhr";
									break;
					case 2:			zeit = "10:00 Uhr";
									break;
					case 3:			zeit = "13:00 Uhr";
									break;
					case 4:			zeit = "15:00 Uhr";
									break;
					
					}
					return new SimpleObjectProperty<String>(zeit);
				}
			});

		colKontrolleur.setCellValueFactory(
				new Callback<TableColumn.CellDataFeatures<Termin, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TableColumn.CellDataFeatures<Termin, String> p) {
						return new SimpleObjectProperty<String>(p.getValue().getMitarbeiter().getNachname() + " " +
									p.getValue().getMitarbeiter().getVorname());
					}
				});
		colOrt.setCellValueFactory(
				new Callback<TableColumn.CellDataFeatures<Termin, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TableColumn.CellDataFeatures<Termin, String> p) {
						return new SimpleObjectProperty<String>(p.getValue().getAnlagenstandort().getPlzOrt().getOrt());
					}
				});
		colStrasse.setCellValueFactory(
				new Callback<TableColumn.CellDataFeatures<Termin, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TableColumn.CellDataFeatures<Termin, String> p) {
						return new SimpleObjectProperty<String>(p.getValue().getAnlagenstandort().getStrasse());
					}
				});
		colKunde.setCellValueFactory(
				new Callback<TableColumn.CellDataFeatures<Termin, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TableColumn.CellDataFeatures<Termin, String> p) {
						return new SimpleObjectProperty<String>(p.getValue().getAnlagenstandort().getKunde().getNachname()
								+ " " +	p.getValue().getAnlagenstandort().getKunde().getVorname());
					}
				});
		colStatus.setCellValueFactory(
				new Callback<TableColumn.CellDataFeatures<Termin, String>, ObservableValue<String>>() {
					@Override
					public ObservableValue<String> call(TableColumn.CellDataFeatures<Termin, String> p) {
						// Jedes Kontrolle-Objekt auf den aktuellen Termin pr�fen
						try {
							ergebnis = "offen";
							serviceProvider.getKontrolleService().getKontrolleByTerminId(p.getValue().getId()).forEach(e -> {
								if (e.getBestanden()) {
										ergebnis = "bestanden";
									}
									else {
										ergebnis = "nicht bestanden";
									}
								}
								);
						} catch (Exception e) {
							logger.error("Terminstatus konnte nicht geladen werden:");
							logger.error(e.getMessage());
						}
						return new SimpleObjectProperty<String>(ergebnis);
					}
				});
	}
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		setCellValueFactory();
		if(eiCo.mainCo.loggedInMitarbeiter.getRolle().getBezeichnung().equals("Sachbearbeiter")){
			tblTermine.setItems(eiCo.olEinsatzplanAll);
		}
		else{
		tblTermine.setItems(eiCo.olEinsatzplan);
		}
	}

	@FXML
	public void btnAbbrechenClicked(ActionEvent event) {

		close(event);

	}

	private void close(ActionEvent e) {
		Node node = (Node) e.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		stage.close();

	}

	@FXML
	public void btnDruckenClicked(ActionEvent event) {
		Printer printer = Printer.getDefaultPrinter();
	    PageLayout pageLayout = printer.createPageLayout(Paper.A4, PageOrientation.LANDSCAPE, 30, 30, 30, 30);
	    PrinterJob printerJob = PrinterJob.createPrinterJob();
	    double scaleX
	        = pageLayout.getPrintableWidth() / tblTermine.getBoundsInParent().getWidth();
	    double scaleY
	        = pageLayout.getPrintableHeight() / mainPane.getBoundsInParent().getHeight();
	    Scale scale = new Scale(scaleX, scaleY);

	    btnAbbrechen.setVisible(false);
	    btnDrucken.setVisible(false);
	    
	    mainPane.getTransforms().add(scale);
	    printerJob.getJobSettings().setJobName("Einsatzplan FEUKORA");
	    if (printerJob != null && printerJob.showPrintDialog(mainPane.getScene().getWindow())) {
	      if (printerJob.printPage(pageLayout, mainPane)) {
	    	  printerJob.endJob();
	      }
	    }
	    mainPane.getTransforms().remove(scale);
	    btnAbbrechen.setVisible(true);
	    btnDrucken.setVisible(true);
	  }
}
