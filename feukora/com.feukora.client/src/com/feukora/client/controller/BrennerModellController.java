package com.feukora.client.controller;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.feukora.server.models.Brennermodell;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * 
 * @author Sven M�ller
 * @version 2.0
 * @since 30.11.2015
 * 
 *  * Controller Klasse zum anzeigen und verwalten aller erfassten Brennermodellen
 *
 */

public class BrennerModellController extends MainController implements Initializable {
	@FXML private GridPane grdBrenner;
	@FXML private Button btnBearbeiten;
	@FXML private Button btnNeu;
	@FXML private Button btnWaehle;
	@FXML protected TextField txtSuche;
	@FXML private TextField txtBrennerID;
	@FXML private TextField txtHersteller;
	@FXML private TextField txtTyp;
	@FXML private TextField txtBrennerart;
	@FXML private TextField txtLeistung;
	@FXML TableView<Brennermodell> tblBrennermodell;
	@FXML TableColumn<Brennermodell, String> colHersteller;
	@FXML TableColumn<Brennermodell, String> colModell;
	
	private BrennerVerwaltenController brVeCo;
	protected Brennermodell clickedBrennerModell;

	private ObservableList<Brennermodell> olBrennerModell;
	
	public BrennerModellController(BrennerVerwaltenController brVeCo){
		super();
		this.brVeCo = brVeCo;
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 2.0
	 * @param Standardparameter von implementierter Initializable Klasse
	 * 
	 * Lade alle Brennerdaten aus dem MainController
	 *
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		loadSpecificData(this);
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 2.0
	 * @param brennerModell Liste s�mtlicher Brennermodelle
	 * 
	 * Wird durch MainController.loadSpecificData aufgerufen und �bergibt die Daten so
	 * an diesen Controller und Tabelle wir neu eingelesen
	 *
	 */
	public void updateTable(List<Brennermodell> brennerModell){
		
		this.olBrennerModell = FXCollections.observableArrayList(brennerModell);
		this.tblBrennermodell.setItems(this.olBrennerModell);
	
	
	FilteredList<Brennermodell> filteredData = new FilteredList<Brennermodell>(olBrennerModell, p -> true);
	 
	txtSuche.textProperty().addListener((observable, alterWert, neuerWert) -> {
            filteredData.setPredicate(weModell -> {
                // Wenn nichts eingegeben ist, zeige alle W�rmeerzeugermodelle an
                if (neuerWert == null || neuerWert.isEmpty()) {
                    return true;
                }
             // Vergleich von Hersteller und Modell
                String lowerCaseFilter = neuerWert.toLowerCase();
                if (weModell.getHersteller().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter = Hersteller
                } else if (weModell.getBezeichnung().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter = Modellname
                }
                return false; // Filter ungleich Hersteller/Modellname
            });
        });
	 
	 SortedList<Brennermodell> sortedData = new SortedList<>(filteredData);
	 
	 sortedData.comparatorProperty().bind(tblBrennermodell.comparatorProperty());
	 tblBrennermodell.setItems(sortedData); 
	}	

	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * 
	 * Zeigt gew�hltes Modell in den Textfelder an um mehr Details zu sehen
	 *
	 */
	@FXML
	public void getBrennerDetails(){
		this.btnBearbeiten.setDisable(false);
		this.btnWaehle.setDisable(false);
		clickedBrennerModell = tblBrennermodell.getSelectionModel().getSelectedItem();
		int kundID = clickedBrennerModell.getId().intValue();
		txtBrennerID.setText(String.valueOf(kundID));
		txtBrennerart.setText(clickedBrennerModell.getBrennerart().getBezeichnung());
		txtHersteller.setText(clickedBrennerModell.getHersteller());
		txtTyp.setText(clickedBrennerModell.getBezeichnung());	
		txtLeistung.setText(Integer.toString(clickedBrennerModell.getFeuerungswaermeleistung()));
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * @throws IOException
	 * 
	 * �ffnet neues GUI Fenster "Brenner Modell bearbeiten" und �bergibt dabei 
	 * sich selber (BrennerModellController)an den aufzurufen Konstruktor BrennerModellVerwalten
	 *
	 */
	@FXML
	public void btnBearbeitenClicked() {
		Parent root;

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/BrennerModellVerwalten.fxml"));
			BrennerModellVerwaltenController controller = new BrennerModellVerwaltenController(this);
			fxmlLoader.setController(controller);
			
			root = fxmlLoader.load();

			Stage stage = new Stage();
			stage.setTitle("Brennermodell bearbeiten");
			stage.setScene(new Scene(root));
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * @throws IOException
	 * 
	 * �ffnet neues GUI Fenster "Brenner Modell erfassen" und �bergibt dabei 
	 * sich selber (BrennerModellController)an den aufzurufen Konstruktor BrennerModellVerwalten
	 *
	 */
	@FXML
	public void btnNeuClicked() {
		Parent root;
		clickedBrennerModell = null;

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/BrennerModellVerwalten.fxml"));
			
			BrennerModellVerwaltenController controller = new BrennerModellVerwaltenController(this);
			fxmlLoader.setController(controller);
			root = fxmlLoader.load();
			
			Stage stage = new Stage();
			stage.setTitle("Brennermodell erfassen");
			stage.setScene(new Scene(root));
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 2.0
	 * 
	 * Gibt das gew�hlte Modell zur�ck and den BrennerVerwaltenController
	 *
	 */
	@FXML
	public void btnWaehleClicked(ActionEvent event){
		if (txtBrennerID.getText() == null || txtBrennerID.getText() == "" ){
			Alert alert = new Alert(AlertType.INFORMATION, "Sie haben keinen Brenner gew�hlt.");
			alert.show();
			}else{
				brVeCo.objBrennerModell = clickedBrennerModell;
				brVeCo.reloadAuswahl();
				close(event);
		}
	}	
	
	/**
	 * 
	 * @author Sven M�ller
	 * @since 1.0
	 * 
	 * Schliesst das Fenster
	 *
	 */
	private void close(ActionEvent e) {
		Node node = (Node) e.getSource();
		Stage stage = (Stage) node.getScene().getWindow();
		stage.close();
	}
}
