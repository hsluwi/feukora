package com.feukora.client.controller;

import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.Mitarbeiter;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Kontroller Klasse von der View Mitarbeiter.fxml
 * @author Marco Hostettler
 * @version 3.0
 * @since 3.12.15
 *
 */
public class MitarbeiterController extends MainController implements Initializable {
	@FXML
	private TableView<Mitarbeiter> tblMitarbeiter;
	@FXML
	private TableColumn<Mitarbeiter, String> colVorname;
	@FXML
	private TableColumn<Mitarbeiter, String> colNachname;
	@FXML
	protected TextField txtSuche;
	@FXML
	private TextField txtStrasse;
	@FXML
	private TextField txtPLZ;
	@FXML
	private TextField txtOrt;
	@FXML
	private TextField txtRolle;
	@FXML
	private TextField txtUsername;
	@FXML
	private TextField txtVorname;
	@FXML
	private TextField txtNachname;
	@FXML
	private Button btnBearbeiten;
	@FXML
	private Button btnLoeschen;
	
	MainController mainCo;
	private ObservableList<Mitarbeiter> olMitarbeiter;
	protected Mitarbeiter clickedMitarbeiter;
	private static final Logger logger = LogManager.getLogger();

	public MitarbeiterController(MainController mainCo) {
		super();
		this.mainCo = mainCo;
	}

	public void initialize(URL location, ResourceBundle resources) {

		updateData(mainCo.mitarbeiter);

	}

	public void updateData(List<Mitarbeiter> allmitarbeiter) {

		this.olMitarbeiter = FXCollections.observableArrayList(allmitarbeiter);
		this.tblMitarbeiter.setItems(this.olMitarbeiter);
	}

	@FXML
	void getMitarbeiterDetails() {
		this.btnBearbeiten.setDisable(false);
		this.btnLoeschen.setDisable(false);
		this.clickedMitarbeiter = (Mitarbeiter) tblMitarbeiter.getSelectionModel().getSelectedItem();
		this.txtRolle.setText(clickedMitarbeiter.getRolle().getBezeichnung());
		this.txtStrasse.setText(clickedMitarbeiter.getStrasse());
		int plz = clickedMitarbeiter.getPlzOrt().getPlz();
		this.txtPLZ.setText(String.valueOf(plz));
		this.txtOrt.setText(clickedMitarbeiter.getPlzOrt().getOrt());
		this.txtUsername.setText(clickedMitarbeiter.getUserName());
		this.txtVorname.setText(clickedMitarbeiter.getVorname());
		this.txtNachname.setText(clickedMitarbeiter.getNachname());

	}

	@FXML
	public void btnLoeschenClicked() {

		Alert alert = new Alert(AlertType.CONFIRMATION, "Wollen Sie diesen Mitarbeiter endg�ltig l�schen?");
		alert.showAndWait().ifPresent(response -> {
			if (response == ButtonType.OK) {
				mitarbeiterLoeschen();
			}

		});
	}
	/**
	 * L�scht das Mitarbeiter-Objekt und loggt das entsprechende Ergebniss
	 */
	private void mitarbeiterLoeschen() {
		try {
			if (serviceProvider.getMitarbeiterService().removeMitarbeiter(clickedMitarbeiter.getId())) {
				Alert alert = new Alert(AlertType.INFORMATION, "Mitarbeiter wurde erfolgreich gel�scht.");
				alert.show();
				logger.info("Mitarbeiter " + clickedMitarbeiter.getVorname() + " " + clickedMitarbeiter.getNachname()
						+ " wurde gel�scht.");
				mainCo.loadSpecificData(this);
				txtSuche.clear();

			} else {
				Alert alert = new Alert(AlertType.ERROR, "Mitarbeiter konnte nicht gel�scht werden");
				alert.show();
				logger.error("Mitarbeiter" + clickedMitarbeiter.getVorname() + " " + clickedMitarbeiter.getNachname()
						+ " konnte nicht gel�scht werden.");
			}
		} catch (RemoteException e) {
			logger.error(e.getMessage());
		}
	}

	@FXML
	public void btnNeuClicked() {
		Parent root;
		clickedMitarbeiter = null;

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/MitarbeiterVerwalten.fxml"));
			MitarbeiterVerwaltenController controller = new MitarbeiterVerwaltenController(this);
			fxmlLoader.setController(controller);
			root = fxmlLoader.load();

			Stage stage = new Stage();
			stage.initModality(Modality.WINDOW_MODAL);
			stage.initOwner(mainCo.mainstage.getScene().getWindow());
			stage.setTitle("Mitarbeiter erfassen");
			stage.setScene(new Scene(root));
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	void btnBearbeitenClicked() {
		Parent root;

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(
					getClass().getResource("/com/feukora/client/views/MitarbeiterVerwalten.fxml"));
			MitarbeiterVerwaltenController controller = new MitarbeiterVerwaltenController(this);
			fxmlLoader.setController(controller);

			root = fxmlLoader.load();

			Stage stage = new Stage();
			stage.initModality(Modality.WINDOW_MODAL);
			stage.initOwner(mainCo.mainstage.getScene().getWindow());
			stage.setTitle("Mitarbeiter bearbeiten");
			stage.setScene(new Scene(root));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

}
