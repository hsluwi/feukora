package com.feukora.client.application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.client.controller.MainController;
import com.feukora.client.utils.ServiceProvider;
import com.feukora.server.models.Mitarbeiter;
import com.feukora.server.webservice.*;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * LoginController f�r die View login.fxml welches den User einw�hlt.
 * @author Marco Hostettler
 * @version 3.0
 * @since 11.12.2015
 *
 */
public class LoginController implements Initializable {

	@FXML
	private TextField txtUsername;
	@FXML
	private PasswordField pwdPassword;
	@FXML
	private boolean username;
	private boolean passwort;
	private MainController mainController;
	private static final Logger logger = LogManager.getLogger();
	private ServiceProvider serviceProvider;

	public LoginController() {
		super();
		serviceProvider = new ServiceProvider();

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	@FXML
	private void pwdPasswortChanged() {
		if (pwdPassword.getText() != null) {
			passwort = true;
		} else {
			passwort = false;
		}
	}

	@FXML
	private void txtUsernameChanged() {
		if (txtUsername.getText() != null) {
			username = true;
		} else {
			username = false;
		}
	}

	@FXML
	private void btnLoginClicked() {

		if (username && passwort) {
			try {

				Mitarbeiter loggedInMitarbeiter = 
						serviceProvider.getMitarbeiterService().getMitarbeiterLogin(txtUsername.getText(), pwdPassword.getText());

				loader(loggedInMitarbeiter);

			} catch (Exception e) {
				Alert alert = new Alert(AlertType.ERROR, "Username und/oder Passwort stimmen nicht �berein.");
				alert.show();
				logger.error("Login Fehlgeschlagen: " + e);
			}
		} else {
			Alert alert = new Alert(AlertType.INFORMATION, "Bitte geben Sie Username und Passwort ein.");
			alert.show();

		}
	}
	/**
	 * L�dt die Applikation mit der View RootLayout.fxml.
	 * @param loggedInMitarbeiter - �bergabe des eingew�hlten Users f�r die Applikationssteuerung (Rollenkonzept etc.)
	 */
	private void loader(Mitarbeiter loggedInMitarbeiter) {
		try {
			Stage stage = MainApp.getPrimaryStage();
			stage.setTitle("FEUKORA - " + loggedInMitarbeiter.getRolle().getBezeichnung());
			stage.setResizable(false);

			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/feukora/client/views/RootLayout.fxml"));

			Pane myPane = fxmlLoader.load();

			MainController controller = fxmlLoader.<MainController> getController();
		
			controller.setLoggedInMitarbeiter(loggedInMitarbeiter);
			controller.setStage(stage);

			Scene scene = new Scene(myPane);
			stage.setScene(scene);
			stage.centerOnScreen();
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
