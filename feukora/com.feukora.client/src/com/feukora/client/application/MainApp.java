package com.feukora.client.application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.apache.logging.log4j.Logger;

import com.feukora.client.utils.ServiceProvider;
import com.feukora.server.webservice.IAnlagenstandortController;

import org.apache.logging.log4j.LogManager;
/**
 * 
 * @author Marco Hostettler
 * @version 1.0
 * @since 15.11.2015
 *
 */
public class MainApp extends Application {
	//Logger Definition: added by MuS
	private static final Logger logger = LogManager.getLogger();
	
	public static Stage primaryStage;

	public static void main(String[] args) {
		launch(args);
	}

	public void setPrimaryStage(Stage primaryStage) {
		MainApp.primaryStage = primaryStage;
	}

	public static Stage getPrimaryStage() {
		return primaryStage;
	}

	/**
	 * Startet die Applikation mit der Login Ansicht.
	 * 
	 * @param primaryStage
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			setPrimaryStage(primaryStage);
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/feukora/client/views/Login.fxml"));
			Pane myPane = (Pane) fxmlLoader.load();
			Scene scene = new Scene(myPane);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			ServiceProvider serviceProvider = new ServiceProvider();
			IAnlagenstandortController service = serviceProvider.getAnlagenstandortService();
			service.getListAnlagenstandort();
			primaryStage.setScene(scene);
			primaryStage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		logger.info("Login_View laden fehlgeschlagen" + e);
		}

	}
}
