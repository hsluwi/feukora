package com.feukora.server.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Klasse um die Config File auszulesen
 * @author Andrew
 */
public class ConfigManager {
	
	private final String serverPort;
	
	// Database
	private final String ddlGeneration;
	private final String databasePasswort;
	private final String databaseUser;
	private final String databaseUrl;
	private final String databaseDriver;
	private final String databaseName;
	private final String testDatabaseName;
	private final String initializeData;

	
	
	private final String propertiesPath;
	
	public ConfigManager() {
		this.propertiesPath = "config.properties";
		
		Properties properties = new Properties();
		ClassLoader cLoader = this.getClass().getClassLoader();
        InputStream is = cLoader.getResourceAsStream(propertiesPath);
        try {
        	properties.load(is);
        	
		} catch (IOException e) {

		}
        this.serverPort = properties.getProperty("serverPort"); 
        this.databaseName = properties.getProperty("databaseName");
        this.testDatabaseName = properties.getProperty("persistenceTest");
        this.ddlGeneration =  properties.getProperty("ddlGeneration");
        this.databasePasswort = properties.getProperty("databasePasswort");
        this.databaseUser = properties.getProperty("databaseUser");
        this.databaseUrl = properties.getProperty("databaseUrl");
        this.databaseDriver = properties.getProperty("databaseDriver");
        this.initializeData = properties.getProperty("initializeData");
	}

	public Map<String, String> getDatabaseSettings() {
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("javax.persistence.driver.class", databaseDriver);
		properties.put("javax.persistence.jdbc.url", databaseUrl);
		properties.put("javax.persistence.jdbc.user", databaseUser);
		properties.put("javax.persistence.jdbc.password", databasePasswort);
		properties.put("eclipselink.ddl-generation.output-mode", "database");
		properties.put("eclipselink.ddl-generation", ddlGeneration);
		return properties;
	}
	

	/**
	 * @return the databaseDriver
	 */
	public String getDatabaseDriver() {
		return databaseDriver;
	}

	/**
	 * @return the serverPort
	 */
	public String getServerPort() {
		return serverPort;
	}

	/**
	 * @return the ddlGeneration
	 */
	public String getDdlGeneration() {
		return ddlGeneration;
	}

	/**
	 * @return the databasePasswort
	 */
	public String getDatabasePasswort() {
		return databasePasswort;
	}

	/**
	 * @return the databaseUser
	 */
	public String getDatabaseUser() {
		return databaseUser;
	}

	/**
	 * @return the databaseUrl
	 */
	public String getDatabaseUrl() {
		return databaseUrl;
	}

	/**
	 * @return the databaseName
	 */
	public String getDatabaseName() {
		return databaseName;
	}

	/**
	 * @return the testDatabaseName
	 */
	public String getTestDatabaseName() {
		return testDatabaseName;
	}
	
	/**
	 * @return the initializeData
	 */
	public String getinitializeData() {
		return initializeData;
	}
}
