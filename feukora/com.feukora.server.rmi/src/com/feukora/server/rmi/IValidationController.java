package com.feukora.server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;

import javax.jws.WebService;

import com.feukora.server.models.Anlagenstandort;
import com.feukora.server.models.Brenner;
import com.feukora.server.models.Brennerart;
import com.feukora.server.models.Brennermodell;
import com.feukora.server.models.Brennstoff;
import com.feukora.server.models.Gemeinde;
import com.feukora.server.models.Kontrolle;
import com.feukora.server.models.Kunde;
import com.feukora.server.models.Messergebnis;
import com.feukora.server.models.Mitarbeiter;
import com.feukora.server.models.MitarbeiterRolle;
import com.feukora.server.models.PlzOrt;
import com.feukora.server.models.Termin;
import com.feukora.server.models.Waermeerzeuger;
import com.feukora.server.models.Waermeerzeugermodell;

@WebService
public interface IValidationController extends Remote {

	Collection<String> validateAnlagenstandort(Anlagenstandort anlagenstandort) throws RemoteException;

	Collection<String> validateBrennerart(Brennerart brennerart) throws RemoteException;

	Collection<String> validateBrenner(Brenner brenner) throws RemoteException;

	Collection<String> validateBrennermodell(Brennermodell brennermodell) throws RemoteException;

	Collection<String> validateBrennstoff(Brennstoff brennstoff) throws RemoteException;

	Collection<String> validateGemeinde(Gemeinde brenner) throws RemoteException;

	Collection<String> validateKontrolle(Kontrolle kontrolle) throws RemoteException;

	Collection<String> validateKunde(Kunde kunde) throws RemoteException;

	Collection<String> validateMessergebnis(Messergebnis messergebnis) throws RemoteException;

	Collection<String> validateMitarbeiter(Mitarbeiter mitarbeiter) throws RemoteException;

	Collection<String> validateMitarbeiterRolle(MitarbeiterRolle mitarbeiterRolle) throws RemoteException;

	Collection<String> validatePlzOrt(PlzOrt plzOrt) throws RemoteException;

	Collection<String> validateTermin(Termin termin) throws RemoteException;

	Collection<String> validateWaermeerzeuger(Waermeerzeuger waermeerzeuger) throws RemoteException;

	Collection<String> validateWaermeerzeugermodell(Waermeerzeugermodell waermeerzeugermodell) throws RemoteException;

}