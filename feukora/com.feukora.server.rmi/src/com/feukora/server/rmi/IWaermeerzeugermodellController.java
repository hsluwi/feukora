package com.feukora.server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import com.feukora.server.models.Waermeerzeugermodell;

@WebService
public interface IWaermeerzeugermodellController extends Remote {
		
		public Set<Waermeerzeugermodell> getListWaermeerzeugermodell() throws RemoteException;
		
		
		public Waermeerzeugermodell getWaermeerzeugermodell(Integer id) throws RemoteException;
		
		
		public Waermeerzeugermodell saveWaermeerzeugermodell(Waermeerzeugermodell object) throws RemoteException;
		
		
		public boolean saveMultipleWaermeerzeugermodell(Waermeerzeugermodell... object) throws RemoteException;
		

		public boolean saveListWaermeerzeugermodell(Collection<Waermeerzeugermodell> objectList) throws RemoteException;

		
		public boolean removeWaermeerzeugermodell(Integer id) throws RemoteException;
	}
