package com.feukora.server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import com.feukora.server.models.MitarbeiterRolle;

@WebService
public interface IMitarbeiterRolleController extends Remote {
		
		public Set<MitarbeiterRolle> getListMitarbeiterRolle() throws RemoteException ;
		
		
		public MitarbeiterRolle getMitarbeiterRolle(Integer id) throws RemoteException;
		
		
		public MitarbeiterRolle saveMitarbeiterRolle(MitarbeiterRolle object) throws RemoteException;
		
		
		public boolean saveMultipleMitarbeiterRolle(MitarbeiterRolle... object) throws RemoteException;
		

		public boolean saveListMitarbeiterRolle(Collection<MitarbeiterRolle> objectList) throws RemoteException;


		public boolean removeMitarbeiterRolle(Integer id) throws RemoteException;
	}
