package com.feukora.server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import com.feukora.server.models.Brennerart;

@WebService
public interface IBrennerartController extends Remote {
		
		public Set<Brennerart> getListBrennerart() throws RemoteException;
		
		
		public Brennerart getBrennerart(Integer id) throws RemoteException;
		
		
		public Brennerart saveBrennerart(Brennerart object) throws RemoteException;
		
		
		public boolean saveMultipleBrennerart(Brennerart... object) throws RemoteException;
		

		public boolean saveListBrennerart(Collection<Brennerart> objectList) throws RemoteException;

		
		public boolean removeBrennerart(Integer id) throws RemoteException;
	}
