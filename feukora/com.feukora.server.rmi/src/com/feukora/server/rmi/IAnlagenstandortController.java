package com.feukora.server.rmi;

import java.util.Collection;
import java.util.Set;

import javax.jws.WebMethod;
import javax.jws.WebService;


import com.feukora.server.models.Anlagenstandort;

import java.rmi.Remote;
import java.rmi.RemoteException;

@WebService
public interface IAnlagenstandortController extends Remote {
		
		@WebMethod
		public Set<Anlagenstandort> getListAnlagenstandort() throws RemoteException;
		
		@WebMethod
		public Anlagenstandort getAnlagenstandort(Integer id) throws RemoteException;
		
		@WebMethod
		public Anlagenstandort saveAnlagenstandort(Anlagenstandort object) throws RemoteException;
		
		@WebMethod
		public boolean saveMultipleAnlagenstandort(Anlagenstandort... object) throws RemoteException;
		
		@WebMethod
		public boolean saveListAnlagenstandort(Collection<Anlagenstandort> objectList) throws RemoteException;

		@WebMethod
		public boolean removeAnlagenstandort(Integer id) throws RemoteException;
	}
