package com.feukora.server.rmi;

import javax.jws.WebMethod;
import javax.jws.WebService;


import java.rmi.Remote;
import java.rmi.RemoteException;

@WebService
public interface ISetupController extends Remote {
		
		@WebMethod
		public void init() throws RemoteException;
	
}
