package com.feukora.server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import com.feukora.server.models.Brennstoff;

@WebService
public interface IBrennstoffController extends Remote {
		
		public Set<Brennstoff> getListBrennstoff() throws RemoteException;
		
		
		public Brennstoff getBrennstoff(Integer id) throws RemoteException;
		
		
		public Brennstoff saveBrennstoff(Brennstoff object) throws RemoteException;
		
		
		public boolean saveMultipleBrennstoff(Brennstoff... object) throws RemoteException;
		

		public boolean saveListBrennstoff(Collection<Brennstoff> objectList) throws RemoteException;


		public boolean removeBrennstoff(Integer id) throws RemoteException;
	}
