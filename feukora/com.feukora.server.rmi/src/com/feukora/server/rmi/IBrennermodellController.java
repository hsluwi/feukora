package com.feukora.server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import com.feukora.server.models.Brennermodell;

@WebService
public interface IBrennermodellController extends Remote {
		
		public Set<Brennermodell> getListBrennermodell() throws RemoteException;
		
		
		public Brennermodell getBrennermodell(Integer id) throws RemoteException;
		
		
		public Brennermodell saveBrennermodell(Brennermodell object) throws RemoteException;
		
		
		public boolean saveMultipleBrennermodell(Brennermodell... object) throws RemoteException;
		

		public boolean saveListBrennermodell(Collection<Brennermodell> objectList) throws RemoteException;

		
		public boolean removeBrennermodell(Integer id) throws RemoteException;
	}
