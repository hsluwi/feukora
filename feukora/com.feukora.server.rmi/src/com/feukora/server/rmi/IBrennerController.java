package com.feukora.server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import com.feukora.server.models.Brenner;

@WebService
public interface IBrennerController extends Remote {
		
		public Set<Brenner> getListBrenner() throws RemoteException;
		
		
		public Brenner getBrenner(Integer id) throws RemoteException;
		
		
		public Brenner saveBrenner(Brenner object) throws RemoteException;
		
		
		public boolean saveMultipleBrenner(Brenner... object) throws RemoteException;
		

		public boolean saveListBrenner(Collection<Brenner> objectList) throws RemoteException;

		
		public boolean removeBrenner(Integer id) throws RemoteException;
	}
