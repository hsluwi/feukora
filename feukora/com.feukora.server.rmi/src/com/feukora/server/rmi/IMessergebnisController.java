package com.feukora.server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import com.feukora.server.models.Messergebnis;

@WebService
public interface IMessergebnisController extends Remote {
		
		public Set<Messergebnis> getListMessergebnis() throws RemoteException;
		
		
		public Set<Messergebnis> getMessergebnisByKontrolleId(Integer kontrolleId) throws RemoteException;
		
		
		public Messergebnis getMessergebnis(Integer id) throws RemoteException;
		
		
		public Messergebnis saveMessergebnis(Messergebnis object) throws RemoteException;
		
		
		public boolean saveMultipleMessergebnis(Messergebnis... object) throws RemoteException;
		

		public boolean saveListMessergebnis(Collection<Messergebnis> objectList) throws RemoteException;


		public boolean removeMessergebnis(Integer id) throws RemoteException;
	}
