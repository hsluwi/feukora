package com.feukora.server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import com.feukora.server.models.Gemeinde;

@WebService
public interface IGemeindeController extends Remote {
		
		public Set<Gemeinde> getListGemeinde() throws RemoteException;
		
		
		public Gemeinde getGemeinde(Integer id) throws RemoteException;
		
		
		public Gemeinde saveGemeinde(Gemeinde object) throws RemoteException;
		
		
		public boolean saveMultipleGemeinde(Gemeinde... object) throws RemoteException;
		

		public boolean saveListGemeinde(Collection<Gemeinde> objectList) throws RemoteException;

		
		public boolean removeGemeinde(Integer id) throws RemoteException;

	}
