package com.feukora.server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Set;

import javax.jws.WebService;

import com.feukora.server.models.Kunde;

@WebService
public interface IKundeController extends Remote {
		
		public Set<Kunde> getListKunde() throws RemoteException;
		
		
		public Kunde getKunde(Integer id) throws RemoteException;
		
		
		public Kunde saveKunde(Kunde object) throws RemoteException;
		
		
		public boolean saveMultipleKunde(Kunde... object) throws RemoteException;
		

		public boolean saveListKunde(Collection<Kunde> objectList) throws RemoteException;


		public boolean removeKunde(Integer id) throws RemoteException;
	}
