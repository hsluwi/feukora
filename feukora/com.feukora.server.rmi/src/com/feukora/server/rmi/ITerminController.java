package com.feukora.server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Set;

import javax.jws.WebService;

import com.feukora.server.models.Termin;

@WebService
public interface ITerminController extends Remote {

	public Set<Termin> getListTermin() throws RemoteException;

	public Set<Termin> getListTerminByUserId(Integer userId) throws RemoteException;

	public Termin getTermin(Integer id) throws RemoteException;

	public Set<Termin> getTerminWeek(GregorianCalendar date) throws RemoteException;
	
	public Set<Termin> getTerminWeekByUser(GregorianCalendar date, Integer id) throws RemoteException;

	public Termin saveTermin(Termin termin) throws RemoteException;

	public boolean saveMultipleTermin(Termin... termin) throws RemoteException;

	public boolean saveListTermin(Collection<Termin> terminListe) throws RemoteException;
	
	public boolean removeTermin(Integer id) throws RemoteException;

}