package com.feukora.server.initializer;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Anlagenstandort;
import com.feukora.server.models.Brenner;
import com.feukora.server.models.Brennerart;
import com.feukora.server.models.Brennermodell;
import com.feukora.server.models.Brennstoff;
import com.feukora.server.models.Gemeinde;
import com.feukora.server.models.Kontrolle;
import com.feukora.server.models.Kunde;
import com.feukora.server.models.Messergebnis;
import com.feukora.server.models.Mitarbeiter;
import com.feukora.server.models.MitarbeiterRolle;
import com.feukora.server.models.PlzOrt;
import com.feukora.server.models.Termin;
import com.feukora.server.models.Waermeerzeuger;
import com.feukora.server.models.Waermeerzeugermodell;
import com.feukora.server.repositories.IRepositoryFactory;
import com.feukora.server.repositories.RepositoryFactory;
import com.feukora.server.rmi.IAnlagenstandortController;
import com.feukora.server.rmi.IBrennerController;
import com.feukora.server.rmi.IBrennerartController;
import com.feukora.server.rmi.IBrennermodellController;
import com.feukora.server.rmi.IBrennstoffController;
import com.feukora.server.rmi.IGemeindeController;
import com.feukora.server.rmi.IKontrolleController;
import com.feukora.server.rmi.IKundeController;
import com.feukora.server.rmi.IMessergebnisController;
import com.feukora.server.rmi.IMitarbeiterController;
import com.feukora.server.rmi.IMitarbeiterRolleController;
import com.feukora.server.rmi.IPlzOrtController;
import com.feukora.server.rmi.ITerminController;
import com.feukora.server.rmi.IValidationController;
import com.feukora.server.rmi.IWaermeerzeugerController;
import com.feukora.server.rmi.IWaermeerzeugermodellController;
import com.feukora.server.services.AnlagenstandortController;
import com.feukora.server.services.BrennerController;
import com.feukora.server.services.BrennerartController;
import com.feukora.server.services.BrennermodellController;
import com.feukora.server.services.BrennstoffController;
import com.feukora.server.services.GemeindeController;
import com.feukora.server.services.KontrolleController;
import com.feukora.server.services.KundeController;
import com.feukora.server.services.MessergebnisController;
import com.feukora.server.services.MitarbeiterController;
import com.feukora.server.services.MitarbeiterRolleController;
import com.feukora.server.services.PlzOrtController;
import com.feukora.server.services.TerminController;
import com.feukora.server.services.ValidationController;
import com.feukora.server.services.WaermeerzeugerController;
import com.feukora.server.services.WaermeerzeugermodellController;
import com.feukora.server.services.util.AES;


/**
 * Klasse f�r Initialisierung f�r Daten, RMI und WebService
 * @author Andrew
 *
 */
public class Initializer {

	private static IRepositoryFactory rf = new RepositoryFactory();
	private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	private final static String encryptionKey = "MZygpewJsCpRrfOr";
	
	public static void main(String[] args) throws Exception{	
		init();
	}
	
	/**
	 * Initialisierungs Methode
	 */
	public static void init() {
		try {
			initTestData();
			rmiBind();
		} catch (Exception e) {
			System.out.println("--------------- Fehler beim initialisieren ---------------");
       	System.out.println(e.getMessage());
       	e.printStackTrace();
		}
	}
	
	/**
	 * Bindet die RMI Objekte
	 * @throws Exception
	 */
	private static void rmiBind() throws Exception {
		try {
			IAnlagenstandortController anlagenstandortController = new AnlagenstandortController();
			IBrennerartController brennerartController = new BrennerartController();
			IBrennerController brennerController = new BrennerController();
			IBrennermodellController brennermodellController = new BrennermodellController();
			IBrennstoffController brennstoffController = new BrennstoffController();
			IGemeindeController gemeindeController = new GemeindeController();
			IKontrolleController kontrolleController = new KontrolleController();
			IKundeController kundeController = new KundeController();
			IMessergebnisController messergebnisController = new MessergebnisController();
			IMitarbeiterController mitarbeiterController = new MitarbeiterController();
			IMitarbeiterRolleController mitarbeiterRolleController = new MitarbeiterRolleController();
			IPlzOrtController plzOrtController = new PlzOrtController();
			ITerminController terminController = new TerminController();
			IWaermeerzeugerController waermeerzeugerController = new WaermeerzeugerController();
			IWaermeerzeugermodellController waermeerzeugermodellController = new WaermeerzeugermodellController();
			IValidationController validationController = new ValidationController();
			
			Registry reg = LocateRegistry.createRegistry(Integer.parseInt(new ConfigManager().getServerPort()));
			
			Object loopObj = new Object();
			
			if (reg != null) {
				reg.rebind("IAnlagenstandortController", anlagenstandortController);
				System.out.println("Binded: IAnlagenstandortController");
				
				reg.rebind("IBrennerartController", brennerartController);
				System.out.println("Binded: IBrennerartController");
				
				reg.rebind("IBrennerController", brennerController);
				System.out.println("Binded: IBrennerController");
				
				reg.rebind("IBrennermodellController", brennermodellController);
				System.out.println("Binded: IBrennermodellController");
				
				reg.rebind("IBrennstoffController", brennstoffController);
				System.out.println("Binded: IBrennstoffController");
				
				reg.rebind("IGemeindeController", gemeindeController);
				System.out.println("Binded: IGemeindeController");
				
				reg.rebind("IKontrolleController", kontrolleController);
				System.out.println("Binded: IKontrolleController");
				
				reg.rebind("IKundeController", kundeController);
				System.out.println("Binded: IKundeController");
				
				reg.rebind("IMessergebnisController", messergebnisController);
				System.out.println("Binded: IMessergebnisController");
				
				reg.rebind("IMitarbeiterController", mitarbeiterController);
				System.out.println("Binded: IMitarbeiterController");
				
				reg.rebind("IMitarbeiterRolleController", mitarbeiterRolleController);
				System.out.println("Binded: IMitarbeiterRolleController");
				
				reg.rebind("IPlzOrtController", plzOrtController);
				System.out.println("Binded: IPlzOrtController");
				
				reg.rebind("ITerminController", terminController);
				System.out.println("Binded: ITerminController");
				
				reg.rebind("IWaermeerzeugerController", waermeerzeugerController);
				System.out.println("Binded: IWaermeerzeugerController");
				
				reg.rebind("IWaermeerzeugermodellController", waermeerzeugermodellController);
				System.out.println("Binded: IWaermeerzeugermodellController");
				
				reg.rebind("IValidationController", validationController);
				System.out.println("Binded: IValidationController");
				System.out.println("--------------- Binded ---------------");
				
				/**
				 * Forever objekt, da das RMI Binding nach einer gewissen Zeit die Verbindung verliert
				 */
				synchronized (loopObj) {
					loopObj.wait();
				}
				
			}
		} catch (Exception e) {
			System.out.println("--------------- Fehler beim RMI Initialisieren ---------------");
        	System.out.println(e.getMessage());
        	throw e;
		}	
	}
	
	
    private static void initTestData() throws Exception {
    	ConfigManager configManager = new ConfigManager();
       
    	if (configManager.getinitializeData().equals("Ja")){
    		
    	
		EntityManager em = Persistence.createEntityManagerFactory("FeukoraPersistence", configManager.getDatabaseSettings())
				.createEntityManager();
		EntityManager em2 = Persistence.createEntityManagerFactory("FeukoraPersistenceTest", configManager.getDatabaseSettings())
				.createEntityManager();
		
        try {
			System.out.println("Generate Mitarbeiterrolle");
			GenerateMitarbeiterRolle();
			System.out.println("Generate Brenner");
			GenerateBrenner();
			System.out.println("Generate PLZ/ORT");
			GeneratePlzOrt("initializeData/plzOrt.txt");
			System.out.println("Generate Gemeinde");
			GenerateGemeinde("initializeData/gemeinde.txt");
			System.out.println("Generate Kunden");
			GenerateKunden("initializeData/kunde.txt");
			System.out.println("Generate Mitarbeiter");
			GenerateMitarbeiter("initializeData/mitarbeiter.txt");
			System.out.println("Generate Waermeerzeuger");
			GenerateWaermeerzeuger();
			System.out.println("Generate Brenner");
			GenerateBrenner();
			System.out.println("Generate Anlagestandort");
			GenerateAnlagestandort();
			System.out.println("Generate Messergebnisse");
			GenerateMessergebnisse();
			System.out.println("Data created successfully");

        } catch (Exception e) {
        	System.out.println("--------------- Fehler beim initialisieren ---------------");
        	System.out.println(e.getMessage());
        	throw e;
        }
    	}
    }
	
	private static void GenerateMitarbeiterRolle() {

		MitarbeiterRolle mr = new MitarbeiterRolle();
		MitarbeiterRolle mr2 = new MitarbeiterRolle();
		MitarbeiterRolle mr3 = new MitarbeiterRolle();
		
		mr.setBezeichnung("Administrator");
		mr2.setBezeichnung("Kontrolleur");
		mr3.setBezeichnung("Sachbearbeiter");
		MitarbeiterRolle id1 = rf.get_mitarbeiterRolleRepository().persist(mr);
		MitarbeiterRolle id2 = rf.get_mitarbeiterRolleRepository().persist(mr2);
		MitarbeiterRolle id3 = rf.get_mitarbeiterRolleRepository().persist(mr3);
	}
	
	
	
	private static void GenerateAnlagestandort() {
		Anlagenstandort a1 = new Anlagenstandort();
		a1.setBrenner(rf.get_brennerRepository().get(1).get());
		a1.setGemeinde(rf.get_gemeindeRepository().get(1).get());
		a1.setHauswart(rf.get_kundeRepository().get(1).get());
		a1.setKunde(rf.get_kundeRepository().get(3).get());
		a1.setPlzOrt(rf.get_plzOrtRepository().get(1).get());
		a1.setStrasse("W�sserwiesenstrasse 21");
		a1.setWaermeerzeuger(rf.get_waermeerzeugerRepository().get(1).get());

		Anlagenstandort a2 = new Anlagenstandort();
		a2.setBrenner(rf.get_brennerRepository().get(2).get());
		a2.setGemeinde(rf.get_gemeindeRepository().get(2).get());
		a2.setHauswart(rf.get_kundeRepository().get(2).get());
		a2.setKunde(rf.get_kundeRepository().get(4).get());
		a2.setPlzOrt(rf.get_plzOrtRepository().get(2).get());
		a2.setStrasse("Wieshofstrasse 64");
		a2.setWaermeerzeuger(rf.get_waermeerzeugerRepository().get(2).get());
		
		Validator validator = factory.getValidator();
		
		Set<ConstraintViolation<Anlagenstandort>> constraintViolations = validator.validate(a1);
		
		for (ConstraintViolation<Anlagenstandort> constraintViolation : constraintViolations) {
			System.out.println(constraintViolation.getMessage());
		}
		rf.get_anlagenstandortRepository().persist(a1, a2);
	}
	
	private static void GenerateMessergebnisse() {	
		
		Termin t1 = new Termin();
		t1.setAnlagenstandort(rf.get_anlagenstandortRepository().get(1).get());
		GregorianCalendar date1 = new GregorianCalendar();
		date1.add(Calendar.DAY_OF_MONTH, 4);
		t1.setDatumZeit(date1);
		t1.setMitarbeiter(rf.get_mitarbeiterRepository().get(101).get());
		t1.setEinsatz(1);
		
		Kontrolle k1 = new Kontrolle();
		k1.setBemerkungen("Erste Kontrolle");
		k1.setBestanden(true);
		k1.setEinregulierungMoeglich(true);
		k1.setFailAbgasverlust(false);
		k1.setFailCo(false);
		k1.setFailNo(false);
		k1.setFailOel(false);
		k1.setFailRusszahl(false);
		k1.setKenntnisnahmeKunde(true);
		k1.setKontrollart("Feuerungskontrolle");
		k1.setTermin(t1);
		
		
		Messergebnis me1 = new Messergebnis();
		me1.setAbgastemperatur(120.00);
		me1.setAbgasverluste(23.00);
		me1.setCo3PVol(30.00);
		me1.setKontrolle(k1);
		me1.setMessungNr(10001);
		me1.setNox3PVol(40.00);
		me1.setO2Gehalt(56.60);
		me1.setOelanteile(true);
		me1.setRusszahl(33.67);
		me1.setStufe(5);
		me1.setVerbrennLuftTemp(123.50);
		me1.setWaermeErzTemp(220.50);
		
		Termin t2 = new Termin();
		t2.setAnlagenstandort(rf.get_anlagenstandortRepository().get(1).get());
		GregorianCalendar date2 = new GregorianCalendar();
		date2.add(Calendar.DAY_OF_MONTH, 3);
		t2.setDatumZeit(date2);
		t2.setMitarbeiter(rf.get_mitarbeiterRepository().get(101).get());
		t2.setEinsatz(2);
		
		Kontrolle k2 = new Kontrolle();
		k2.setBemerkungen("Erste Kontrolle");
		k2.setBestanden(true);
		k2.setEinregulierungMoeglich(true);
		k2.setFailAbgasverlust(false);
		k2.setFailCo(false);
		k2.setFailNo(false);
		k2.setFailOel(false);
		k2.setFailRusszahl(false);
		k2.setKenntnisnahmeKunde(true);
		k2.setKontrollart("Feuerungskontrolle");
		k2.setTermin(t2);
		
		
		Messergebnis me2 = new Messergebnis();
		me2.setAbgastemperatur(120.00);
		me2.setAbgasverluste(23.00);
		me2.setCo3PVol(30.00);
		me2.setKontrolle(k2);
		me2.setMessungNr(10001);
		me2.setNox3PVol(40.00);
		me2.setO2Gehalt(56.60);
		me2.setOelanteile(true);
		me2.setRusszahl(33.67);
		me2.setStufe(5);
		me2.setVerbrennLuftTemp(123.50);
		me2.setWaermeErzTemp(220.50);
		
		Termin t3 = new Termin();
		t3.setAnlagenstandort(rf.get_anlagenstandortRepository().get(1).get());
		GregorianCalendar date3 = new GregorianCalendar();
		date3.add(Calendar.DAY_OF_MONTH, 3);
		t3.setDatumZeit(date3);
		t3.setMitarbeiter(rf.get_mitarbeiterRepository().get(101).get());
		t3.setEinsatz(3);
		
		Kontrolle k3 = new Kontrolle();
		k3.setBemerkungen("Erste Kontrolle");
		k3.setBestanden(true);
		k3.setEinregulierungMoeglich(true);
		k3.setFailAbgasverlust(false);
		k3.setFailCo(false);
		k3.setFailNo(false);
		k3.setFailOel(false);
		k3.setFailRusszahl(false);
		k3.setKenntnisnahmeKunde(true);
		k3.setKontrollart("Feuerungskontrolle");
		k3.setTermin(t3);
		
		
		Messergebnis me3 = new Messergebnis();
		me3.setAbgastemperatur(120.00);
		me3.setAbgasverluste(23.00);
		me3.setCo3PVol(30.00);
		me3.setKontrolle(k3);
		me3.setMessungNr(10001);
		me3.setNox3PVol(40.00);
		me3.setO2Gehalt(56.60);
		me3.setOelanteile(true);
		me3.setRusszahl(33.67);
		me3.setStufe(5);
		me3.setVerbrennLuftTemp(123.50);
		me3.setWaermeErzTemp(220.50);
		
		Termin t4 = new Termin();
		t4.setAnlagenstandort(rf.get_anlagenstandortRepository().get(1).get());
		GregorianCalendar date4 = new GregorianCalendar();
		date4.add(Calendar.DAY_OF_MONTH, 2);
		t4.setDatumZeit(date4);
		t4.setMitarbeiter(rf.get_mitarbeiterRepository().get(101).get());
		t4.setEinsatz(4);
		
		Kontrolle k4 = new Kontrolle();
		k4.setBemerkungen("Erste Kontrolle");
		k4.setBestanden(true);
		k4.setEinregulierungMoeglich(true);
		k4.setFailAbgasverlust(false);
		k4.setFailCo(false);
		k4.setFailNo(false);
		k4.setFailOel(false);
		k4.setFailRusszahl(false);
		k4.setKenntnisnahmeKunde(true);
		k4.setKontrollart("Feuerungskontrolle");
		k4.setTermin(t4);
		
		
		Messergebnis me4 = new Messergebnis();
		me4.setAbgastemperatur(120.00);
		me4.setAbgasverluste(23.00);
		me4.setCo3PVol(30.00);
		me4.setKontrolle(k4);
		me4.setMessungNr(10001);
		me4.setNox3PVol(40.00);
		me4.setO2Gehalt(56.60);
		me4.setOelanteile(true);
		me4.setRusszahl(33.67);
		me4.setStufe(5);
		me4.setVerbrennLuftTemp(123.50);
		me4.setWaermeErzTemp(220.50);
		
		Termin t5 = new Termin();
		t5.setAnlagenstandort(rf.get_anlagenstandortRepository().get(1).get());
		GregorianCalendar date5 = new GregorianCalendar();
		date5.add(Calendar.DAY_OF_MONTH, 1);
		t5.setDatumZeit(date5);
		t5.setMitarbeiter(rf.get_mitarbeiterRepository().get(101).get());
		t5.setEinsatz(1);
		
		Kontrolle k5 = new Kontrolle();
		k5.setBemerkungen("Erste Kontrolle");
		k5.setBestanden(true);
		k5.setEinregulierungMoeglich(true);
		k5.setFailAbgasverlust(false);
		k5.setFailCo(false);
		k5.setFailNo(false);
		k5.setFailOel(false);
		k5.setFailRusszahl(false);
		k5.setKenntnisnahmeKunde(true);
		k5.setKontrollart("Feuerungskontrolle");
		k5.setTermin(t5);
		
		
		Messergebnis me5 = new Messergebnis();
		me5.setAbgastemperatur(120.00);
		me5.setAbgasverluste(23.00);
		me5.setCo3PVol(30.00);
		me5.setKontrolle(k5);
		me5.setMessungNr(10001);
		me5.setNox3PVol(40.00);
		me5.setO2Gehalt(56.60);
		me5.setOelanteile(true);
		me5.setRusszahl(33.67);
		me5.setStufe(5);
		me5.setVerbrennLuftTemp(123.50);
		me5.setWaermeErzTemp(220.50);
		
		Termin t6 = new Termin();
		t6.setAnlagenstandort(rf.get_anlagenstandortRepository().get(1).get());
		GregorianCalendar date6 = new GregorianCalendar();
		date6.add(Calendar.DAY_OF_MONTH, 1);
		t6.setDatumZeit(new GregorianCalendar());
		t6.setMitarbeiter(rf.get_mitarbeiterRepository().get(101).get());
		t6.setEinsatz(2);
		
		Kontrolle k6 = new Kontrolle();
		k6.setBemerkungen("Erste Kontrolle");
		k6.setBestanden(true);
		k6.setEinregulierungMoeglich(true);
		k6.setFailAbgasverlust(false);
		k6.setFailCo(false);
		k6.setFailNo(false);
		k6.setFailOel(false);
		k6.setFailRusszahl(false);
		k6.setKenntnisnahmeKunde(true);
		k6.setKontrollart("Feuerungskontrolle");
		k6.setTermin(t6);
		
		
		Messergebnis me6 = new Messergebnis();
		me6.setAbgastemperatur(120.00);
		me6.setAbgasverluste(23.00);
		me6.setCo3PVol(30.00);
		me6.setKontrolle(k6);
		me6.setMessungNr(10001);
		me6.setNox3PVol(40.00);
		me6.setO2Gehalt(56.60);
		me6.setOelanteile(true);
		me6.setRusszahl(33.67);
		me6.setStufe(5);
		me6.setVerbrennLuftTemp(123.50);
		me6.setWaermeErzTemp(220.50);
		
		
		Termin t7 = new Termin();
		t7.setAnlagenstandort(rf.get_anlagenstandortRepository().get(1).get());
		GregorianCalendar date7 = new GregorianCalendar();
		date7.add(Calendar.DAY_OF_MONTH, 1);
		t7.setDatumZeit(date7);
		t7.setMitarbeiter(rf.get_mitarbeiterRepository().get(101).get());
		t7.setEinsatz(3);
		
		Kontrolle k7 = new Kontrolle();
		k7.setBemerkungen("Erste Kontrolle");
		k7.setBestanden(true);
		k7.setEinregulierungMoeglich(true);
		k7.setFailAbgasverlust(false);
		k7.setFailCo(false);
		k7.setFailNo(false);
		k7.setFailOel(false);
		k7.setFailRusszahl(false);
		k7.setKenntnisnahmeKunde(true);
		k7.setKontrollart("Feuerungskontrolle");
		k7.setTermin(t7);
		
		
		Messergebnis me7 = new Messergebnis();
		me7.setAbgastemperatur(120.00);
		me7.setAbgasverluste(23.00);
		me7.setCo3PVol(30.00);
		me7.setKontrolle(k7);
		me7.setMessungNr(10001);
		me7.setNox3PVol(40.00);
		me7.setO2Gehalt(56.60);
		me7.setOelanteile(true);
		me7.setRusszahl(33.67);
		me7.setStufe(5);
		me7.setVerbrennLuftTemp(123.50);
		me7.setWaermeErzTemp(220.50);
		
		Termin t8 = new Termin();
		t8.setAnlagenstandort(rf.get_anlagenstandortRepository().get(1).get());
		GregorianCalendar date8 = new GregorianCalendar();
		date8.add(Calendar.DAY_OF_MONTH, 1);
		t8.setDatumZeit(date8);
		t8.setMitarbeiter(rf.get_mitarbeiterRepository().get(101).get());
		t8.setEinsatz(1);
		
		Kontrolle k8 = new Kontrolle();
		k8.setBemerkungen("Erste Kontrolle");
		k8.setBestanden(true);
		k8.setEinregulierungMoeglich(true);
		k8.setFailAbgasverlust(false);
		k8.setFailCo(false);
		k8.setFailNo(false);
		k8.setFailOel(false);
		k8.setFailRusszahl(false);
		k8.setKenntnisnahmeKunde(true);
		k8.setKontrollart("Feuerungskontrolle");
		k8.setTermin(t8);
		
		Messergebnis me8 = new Messergebnis();
		me8.setAbgastemperatur(120.00);
		me8.setAbgasverluste(23.00);
		me8.setCo3PVol(30.00);
		me8.setKontrolle(k8);
		me8.setMessungNr(10001);
		me8.setNox3PVol(40.00);
		me8.setO2Gehalt(56.60);
		me8.setOelanteile(true);
		me8.setRusszahl(33.67);
		me8.setStufe(5);
		me8.setVerbrennLuftTemp(123.50);
		me8.setWaermeErzTemp(220.50);
		
		rf.get_messergebnisRepository().persist(me1, me2, me3, me4, me5, me6, me7, me8);
	}
	
	private static void GenerateWaermeerzeuger() {
		Brennstoff b1 = new Brennstoff();
		Brennstoff b2 = new Brennstoff();
		Brennstoff b3 = new Brennstoff();
		Brennstoff b4 = new Brennstoff();
		Brennstoff b5 = new Brennstoff();
		
		b1.setBezeichnung("Benzin");
		b2.setBezeichnung("Diesel");
		b3.setBezeichnung("Holz");
		b4.setBezeichnung("Pellets");
		b5.setBezeichnung("Gas");
		
		Waermeerzeugermodell wm1 = new Waermeerzeugermodell();
		Waermeerzeugermodell wm2 = new Waermeerzeugermodell();
		Waermeerzeugermodell wm3 = new Waermeerzeugermodell();
		Waermeerzeugermodell wm4 = new Waermeerzeugermodell();
		Waermeerzeugermodell wm5 = new Waermeerzeugermodell();
		
		wm1.setBezeichnung("Elektrow�rmer");
		wm1.setHersteller("Siemens");
		wm1.setBrennstoff(b1);
		
		wm2.setBezeichnung("Gasw�rmer");
		wm2.setBrennstoff(b2);
		wm2.setHersteller("Trisa");
		
		wm3.setBezeichnung("Holzw�rmer");
		wm3.setBrennstoff(b3);
		wm3.setHersteller("Intertronic");
		
		wm4.setBezeichnung("�lw�rmer");
		wm4.setBrennstoff(b4);
		wm4.setHersteller("Rotel");
		
		wm5.setBezeichnung("Pelletw�rmer");
		wm5.setBrennstoff(b5);
		wm5.setHersteller("V-Zug");
		
		Waermeerzeuger w1 = new Waermeerzeuger();
		Waermeerzeuger w2 = new Waermeerzeuger();
		Waermeerzeuger w3 = new Waermeerzeuger();
		Waermeerzeuger w4 = new Waermeerzeuger();
		Waermeerzeuger w5 = new Waermeerzeuger();
		
		w1.setBaujahr(2001);
		w1.setWaermeerzeugermodell(wm1);
		w1.setZulassungsNr("HADJH-DBASD-ASDSA-SADSA");
		
		w2.setBaujahr(2009);
		w2.setWaermeerzeugermodell(wm2);
		w2.setZulassungsNr("FDSFD-DSADA-LODSA-POWPA");
		
		w3.setBaujahr(2015);
		w3.setWaermeerzeugermodell(wm3);
		w3.setZulassungsNr("TARDA-DSADA-ASDOP-OFASD");
		
		w4.setBaujahr(2010);
		w4.setWaermeerzeugermodell(wm4);
		w4.setZulassungsNr("PWADA-ASDWE-ASDWA-POWAD");
		
		w5.setBaujahr(2013);
		w5.setWaermeerzeugermodell(wm5);
		w5.setZulassungsNr("WZETD-WUDSA-OFPDA-ASDWA");
		
		Validator validator = factory.getValidator();
		
		Set<ConstraintViolation<Waermeerzeuger>> constraintViolations = validator.validate(w2);
		
		for (ConstraintViolation<Waermeerzeuger> constraintViolation : constraintViolations) {
			System.out.println(constraintViolation.getMessage());
		}
		rf.get_waermeerzeugerRepository().persist(w1, w2, w3, w4, w5);
	}
	
	
	private static void GenerateBrenner() {

		Brennerart ba1 = new Brennerart();
		Brennerart ba2 = new Brennerart();
		Brennerart ba3 = new Brennerart();
		Brennerart ba4 = new Brennerart();
		Brennerart ba5 = new Brennerart();
		
		ba1.setBezeichnung("Gas");
		ba2.setBezeichnung("Holz");
		ba3.setBezeichnung("�l");
		ba4.setBezeichnung("Pellets");
		ba5.setBezeichnung("Elektro");
		
		Brennermodell bm1 = new Brennermodell();
		Brennermodell bm2 = new Brennermodell();
		Brennermodell bm3 = new Brennermodell();
		Brennermodell bm4 = new Brennermodell();
		Brennermodell bm5 = new Brennermodell();
		
		bm1.setBezeichnung("Gasbrenner");	
		bm1.setBrennerart(ba1);
		bm1.setFeuerungswaermeleistung(400);
		bm1.setHersteller("Trisa");
		
		bm2.setBezeichnung("Holzbrenner");
		bm2.setBrennerart(ba2);
		bm2.setFeuerungswaermeleistung(800);
		bm2.setHersteller("V-Zug");
		
		bm3.setBezeichnung("�lbrenner");
		bm3.setBrennerart(ba3);
		bm3.setFeuerungswaermeleistung(600);
		bm3.setHersteller("Siemens");
		
		bm4.setBezeichnung("Elektrobrenner");
		bm4.setBrennerart(ba4);
		bm4.setFeuerungswaermeleistung(1000);
		bm4.setHersteller("Rotel");
		
		bm5.setBezeichnung("Pelletbrenner");
		bm5.setBrennerart(ba5);
		bm5.setFeuerungswaermeleistung(800);
		bm5.setHersteller("Samsung");
		
		Brenner br1 = new Brenner();
		Brenner br2 = new Brenner();
		Brenner br3 = new Brenner();
		Brenner br4 = new Brenner();
		Brenner br5 = new Brenner();
		
		br1.setBaujahr(2001);
		br1.setZulassungsNr("ZHAHW-JAGSHA-HWIAS-HAJDSW");
		br1.setBrennermodell(bm1);
		
		br2.setBaujahr(2009);
		br2.setZulassungsNr("ADADA-APODJ-LOFFJ-KPEOF-AJDOE");
		br2.setBrennermodell(bm2);
		
		br3.setBaujahr(2013);
		br3.setZulassungsNr("UEZFG-AHWZE-EODUF-ISUDH-ODUIF");
		br3.setBrennermodell(bm3);
		
		br4.setBaujahr(2015);
		br4.setBrennermodell(bm4);
		br4.setZulassungsNr("ZTETD-PEOFG-WTADU-FJADF-EOFUF");
		
		br5.setBaujahr(2012);
		br5.setBrennermodell(bm5);
		br5.setZulassungsNr("WQEEW-WEWAD-JZDSG-POSDF");
		
		rf.get_brennerRepository().persist(br1, br2, br3, br4, br5);
	}
	
	
	
	private static void GenerateMitarbeiter(String filePath) throws Exception {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader(filePath));
		JSONArray jsonarray = (JSONArray) obj;
		JSONObject mJsonObject = new JSONObject();
		AES aes = new AES(encryptionKey);
		ArrayList<Mitarbeiter> mList = new ArrayList<Mitarbeiter>();
	     
	    int rolleId = 1;
	     try {
	    	 for (int i = 0; i < jsonarray.size(); i++) {
		         mJsonObject = (JSONObject) jsonarray.get(i);
		
		         if(i % 2 == 0)
		         {
		        	 rolleId = 1;
		         }
		         else
		         {
		        	 rolleId = 2;
		         }
		   
		         Mitarbeiter m = new Mitarbeiter();
		         m.setEmail((String) mJsonObject.get("email"));
		         m.setMobile((String) mJsonObject.get("mobile"));
		         m.setNachname((String) mJsonObject.get("nachname"));
		         String passwort = aes.encrypt((String) mJsonObject.get("passwort"));
		         m.setPasswort(passwort);
		         System.out.println(passwort);
		         long id = (Long)mJsonObject.get("plzort_id");
		         if (m.getNachname().equals("Anderhub")){
		         	rolleId = 3;    
		         }
		         m.setPlzOrt(rf.get_plzOrtRepository().get((int)id).get());
		         m.setRolle(rf.get_mitarbeiterRolleRepository().get(rolleId).get());
		         m.setStrasse((String) mJsonObject.get("strasse"));
		         m.setTelefon((String) mJsonObject.get("telefon"));
		         m.setUserName((String) mJsonObject.get("username"));
		         m.setVorname((String) mJsonObject.get("vorname")); 
		         
		         mList.add(m);
		     }
		     
		     rf.get_mitarbeiterRepository().persist(mList);
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
	     
	}
	
	public static void GeneratePlzOrt(String filePath) throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader(filePath));
        
        JSONArray jsonarray = (JSONArray) obj;
        JSONObject mJsonObject = new JSONObject();
        
        ArrayList<PlzOrt> list = new ArrayList<PlzOrt>();
        
        for (int i = 0; i < jsonarray.size(); i++) {
            mJsonObject = (JSONObject) jsonarray.get(i);

            PlzOrt po = new PlzOrt();
            po.setPlz(Integer.parseInt((String) mJsonObject.get("PLZ")));
            po.setOrt((String) mJsonObject.get("Ort"));
 
            list.add(po);
        }
        
        rf.get_plzOrtRepository().persist(list);
	}
	
	
	private static void GenerateKunden(String filePath) throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader(filePath));
       
		JSONArray jsonarray = (JSONArray) obj;
		JSONObject mJsonObject = new JSONObject();
       
		ArrayList<Kunde> list = new ArrayList<Kunde>();
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
       
		for (int i = 0; i < jsonarray.size(); i++) {
			mJsonObject = (JSONObject) jsonarray.get(i);

			Kunde po = new Kunde();
			po.setFirma((String) mJsonObject.get("firma"));
			po.setEmail((String) mJsonObject.get("email"));
			po.setMobile((String) mJsonObject.get("mobile"));
			po.setVorname((String) mJsonObject.get("vorname"));
			po.setNachname((String) mJsonObject.get("nachname"));
			po.setStrasse((String) mJsonObject.get("strasse"));
			po.setTelefon((String) mJsonObject.get("telefon"));
			po.setKundenArt((String) mJsonObject.get("kundenart"));
			long id = (Long)mJsonObject.get("plzort_id");
			po.setPlzOrt(rf.get_plzOrtRepository().get((int)id).get());
           
        
   		   	Set<ConstraintViolation<Kunde>> constraintViolations = validator.validate(po);
   		
	   		for (ConstraintViolation<Kunde> constraintViolation : constraintViolations) {
	   			System.out.println(constraintViolation.getMessage());
	   		}
   		
	   		list.add(po);
       }
       rf.get_kundeRepository().persist(list);
	}
	
	private static void GenerateGemeinde(String filePath) throws FileNotFoundException, IOException, ParseException {

		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader(filePath));
		JSONArray jsonarray = (JSONArray) obj;
		JSONObject mJsonObject = new JSONObject();
      
		ArrayList<Gemeinde> list = new ArrayList<Gemeinde>();

		Validator validator = factory.getValidator();
      
		for (int i = 0; i < jsonarray.size(); i++) {
			mJsonObject = (JSONObject) jsonarray.get(i);

			Gemeinde g = new Gemeinde();
			g.setBezeichnung((String) mJsonObject.get("Bezeichnung"));
			String test = (String) mJsonObject.get("Bezeichnung");
			g.setKanton((String) mJsonObject.get("Kanton"));
			Set<ConstraintViolation<Gemeinde>> constraintViolations = validator.validate(g);
	
			for (ConstraintViolation<Gemeinde> constraintViolation : constraintViolations) {
				System.out.println(constraintViolation.getMessage());
			}
	
			list.add(g);
		}
		rf.get_gemeindeRepository().persist(list);
	}
}
