package com.feukora.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.MitarbeiterRolle;
import com.feukora.server.repositories.MitarbeiterRolleRepository;
import com.feukora.server.repositories.RepositoryFactory;
import com.feukora.server.rmi.IMitarbeiterRolleController;


/**
 * Business Layer f�r MitarbeiterRolle
 * @author Andrew
 *
 */
public class MitarbeiterRolleController extends UnicastRemoteObject implements IMitarbeiterRolleController {
	
	private static final long serialVersionUID = 1L;
	private String databaseString = new ConfigManager().getDatabaseName(); 
	private static final Logger log = LogManager.getLogger();
	private MitarbeiterRolleRepository repo;
	
	/**
	 * Konstruktor die ein MitarbeiterRolleRepository instanziert
	 * @throws RemoteException
	 */
	public MitarbeiterRolleController() throws RemoteException {
		super();
		repo = (MitarbeiterRolleRepository) new RepositoryFactory(databaseString).get_mitarbeiterRolleRepository();  
	}
		
	/**
	 * Gibt alle MitarbeiterRollen zur�ck
	 * @return MitarbeiterRolleliste
	 */
	public Set<MitarbeiterRolle> getListMitarbeiterRolle() {
		try {
			return repo.get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt eine spezifische MitarbeiterRolle zur�ck
	 * @param MitarbeiterRolle ID
	 * @return MitarbeiterRolle objekt
	 */
	public MitarbeiterRolle getMitarbeiterRolle(Integer id) {
		try {
			return repo.get(id).get();
		} catch (NoSuchElementException e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert eine MitarbeiterRolle
	 * @param MitarbeiterRolle objekt
	 * @return MitarbeiterRolle objekt
	 */
	public MitarbeiterRolle saveMitarbeiterRolle(MitarbeiterRolle mitarbeiterRolle) {
		try {
			return repo.persist(mitarbeiterRolle);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere MitarbeiterRollen
	 * @param mehrere MitarbeiterRollen
	 * @return true oder false
	 */
	public boolean saveMultipleMitarbeiterRolle(MitarbeiterRolle... mitarbeiterRolle) {
		try {
			repo.persist(mitarbeiterRolle);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert eine Liste von MitarbeiterRollen
	 * @param MitarbeiterRollenliste
	 * @return true oder false
	 */
	public boolean saveListMitarbeiterRolle(Collection<MitarbeiterRolle> mitarbeiterRolleListe) {
		try {
			repo.persist(mitarbeiterRolleListe);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht eine MitarbeiterRolle
	 * @param MitarbeiterRolle ID
	 * @return true oder false
	 */
	public boolean removeMitarbeiterRolle(Integer id) {
		try {
			repo.remove(id);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}

}
