package com.feukora.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Waermeerzeugermodell;
import com.feukora.server.repositories.RepositoryFactory;
import com.feukora.server.repositories.WaermeerzeugermodellRepository;
import com.feukora.server.rmi.IWaermeerzeugermodellController;



/**
 * Business Layer f�r Waermeerzeigermodell
 * @author Andrew
 *
 */
public class WaermeerzeugermodellController extends UnicastRemoteObject implements IWaermeerzeugermodellController {
	
	private static final long serialVersionUID = 1L;
	private String databaseString = new ConfigManager().getDatabaseName(); 
	private static final Logger log = LogManager.getLogger();
	private WaermeerzeugermodellRepository repo;
	
	/**
	 * Konstruktor die ein WaermeerzeugermodellRepository instanziert
	 * @throws RemoteException
	 */
	public WaermeerzeugermodellController() throws RemoteException {
		super();
		repo = (WaermeerzeugermodellRepository) new RepositoryFactory(databaseString).get_waermeerzeugermodellRepository();
	}

	/**
	 * Gibt alle Waermeerzeugermodell zur�ck
	 * @return Waermeerzeugerliste
	 */
	public Set<Waermeerzeugermodell> getListWaermeerzeugermodell() {
		try {
			return repo.get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt eine spezifische Waermeerzeugermodell zur�ck
	 * @param Waermeerzeugermodell ID
	 * @return Waermeerzeugermodell objekt
	 */
	public Waermeerzeugermodell getWaermeerzeugermodell(Integer id) {
		try {
			return repo.get(id).get();
		} catch (NoSuchElementException e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein Waermeerzeugermodell 
	 * @param Waermeerzeugermodell objekt
	 * @return Waermeerzeugermodell objekt
	 */
	public Waermeerzeugermodell saveWaermeerzeugermodell(Waermeerzeugermodell waermeerzeugermodell) {
		try {
			return repo.persist(waermeerzeugermodell);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Waermeerzeugermodelle
	 * @param mehrere Waermeerzeugermodell
	 * @return true oder false
	 */
	public boolean saveMultipleWaermeerzeugermodell(Waermeerzeugermodell... waermeerzeugermodell) {
		try {
			repo.persist(waermeerzeugermodell);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert eine Liste von Waermeerzeugermodelle
	 * @param Waermeerzeugermodellliste
	 * @return true oder false
	 */
	public boolean saveListWaermeerzeugermodell(Collection<Waermeerzeugermodell> waermeerzeugermodellListe) {
		try {
			repo.persist(waermeerzeugermodellListe);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht ein Waermeerzeugermodell
	 * @param Waermeerzeugermodell ID
	 * @return true oder false
	 */
	public boolean removeWaermeerzeugermodell(Integer id) {
		try {
			repo.remove(id);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}

}
