package com.feukora.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.PlzOrt;
import com.feukora.server.repositories.PlzOrtRepository;
import com.feukora.server.repositories.RepositoryFactory;
import com.feukora.server.rmi.IPlzOrtController;

/**
 * Business Layer f�r PLZ/Ort
 * @author Andrew
 *
 */
public class PlzOrtController extends UnicastRemoteObject implements IPlzOrtController {
	
	private static final long serialVersionUID = 1L;
	private String databaseString = new ConfigManager().getDatabaseName(); 
	private static final Logger log = LogManager.getLogger();
	private PlzOrtRepository repo;
	
	/**
	 * Konstruktor die ein PlzOrtRepository instanziert
	 * @throws RemoteException
	 */
	public PlzOrtController() throws RemoteException {
		super();
		repo = (PlzOrtRepository) new RepositoryFactory(databaseString).get_plzOrtRepository();
	}

	/**
	 * Gibt alle Plz/Orte zur�ck
	 * @return PlzOrt-Liste
	 */
	public Set<PlzOrt> getListPlzOrt() {
		try {
			return repo.get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}

	/**
	 * Gibt ein spezifischer PlzOrt zur�ck
	 * @param PlzOrt ID
	 * @return PlzOrt objekt
	 */
	public PlzOrt getPlzOrt(Integer id) {
		try {
			return repo.get(1).get();
		} catch (NoSuchElementException e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt eine Liste von PlzOrt �ber die PLZ zur�ck
	 * @param Postleitzahl
	 * @return PlzOrt-Liste
	 */
	public Set<PlzOrt> getPlzOrtByPlz(Integer plz){
		try {
			return repo.get(m -> m.getPlz().equals(plz));
		} catch (NoSuchElementException e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert eine PlzOrt
	 * @param PlzOrt objekt
	 * @return PlzOrt obekt
	 */
	public PlzOrt savePlzOrt(PlzOrt plzOrt) {
		try {
			return repo.persist(plzOrt);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere PlzOrte
	 * @param mehrere PlzOrte
	 * @return true oder false
	 */
	public boolean saveMultiplePlzOrt(PlzOrt... plzOrte) {
		try {
			repo.persist(plzOrte);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert eine Liste von PlzOrte
	 * @param PlzOrte-Liste
	 * @return true oder false
	 */
	public boolean saveListPlzOrt(Collection<PlzOrt> plzOrtListe) {
		try {
			repo.persist(plzOrtListe);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht ein PlzOrt �ber eine ID
	 * @param PlzOrt ID
	 * @return true oder false
	 */
	public boolean removePlzOrt(Integer id) {
		try {
			repo.remove(id);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
}
