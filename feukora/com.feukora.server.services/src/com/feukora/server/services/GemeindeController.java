package com.feukora.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Gemeinde;
import com.feukora.server.repositories.GemeindeRepository;
import com.feukora.server.repositories.RepositoryFactory;
import com.feukora.server.rmi.IGemeindeController;

/**
 * Business Layer f�r Gemeinde
 * @author Andrew
 *
 */
public class GemeindeController extends UnicastRemoteObject implements IGemeindeController {
	
	private static final long serialVersionUID = 1L;
	private String databaseString = new ConfigManager().getDatabaseName(); 
	private static final Logger log = LogManager.getLogger();
	private GemeindeRepository repo;
	
	/**
	 * Konstruktor die ein GemeindeRepository instanziert
	 * @throws RemoteException
	 */
	public GemeindeController() throws RemoteException {
		super();
		repo = (GemeindeRepository) new RepositoryFactory(databaseString).get_gemeindeRepository();  
	}

	/**
	 * Gibt alle Gemeinde zur�ck
	 * @return Gemeindeliste
	 */
	public Set<Gemeinde> getListGemeinde() {
		try {
			return repo.get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein spezifische Gemeinde zur�ck
	 * @param Gemeinde ID
	 * @return Gemeinde objekt
	 */
	public Gemeinde getGemeinde(Integer id) {
		try {
			return repo.get(id).get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert eine Gemeinde
	 * @param Gemeinde objekt
	 * @return Gemeinde objekt
	 */
	public Gemeinde saveGemeinde(Gemeinde gemeinde) {
		try {
			return repo.persist(gemeinde);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Gemeinde
	 * @param mehrere Gemeinde
	 * @return true oder false
	 */
	public boolean saveMultipleGemeinde(Gemeinde... gemeinde) {
		try {
			repo.persist(gemeinde);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert eine Liste von Gemeinde
	 * @param Gemeindeliste
	 * @return true oder false
	 */
	public boolean saveListGemeinde(Collection<Gemeinde> gemeindeListe) {
		try {
			repo.persist(gemeindeListe);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht eine Gemeinde �ber eine ID
	 * @param Gemeinde ID
	 * @return true oder false
	 */
	public boolean removeGemeinde(Integer id) {
		try {
			repo.remove(id);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}

}
