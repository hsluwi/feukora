package com.feukora.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.models.*;
import com.feukora.server.rmi.IValidationController;


/**
 * Business Layer f�r Validierung. Pr�ft 
 * @author Andrew
 *
 */
public class ValidationController extends UnicastRemoteObject implements IValidationController {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
	private Validator validator;
	private ValidatorFactory validatorFactory;
	
	public ValidationController() throws RemoteException {
		super();
		validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
	}

	/**
	 * Validiert ein Anlagestandort
	 * @param Anlagenstandort objekt
	 * @return Eine Collection von Fehlermeldungen
	 */
	public Collection<String> validateAnlagenstandort(Anlagenstandort anlagenstandort) throws RemoteException {
		Collection<String> errors = new ArrayList<String>();
		Set<ConstraintViolation<Anlagenstandort>> violations = validator.validate(anlagenstandort);
		violations.forEach(v -> errors.add(v.getMessage()));
		return errors;
	}
	
	/**
	 * Validiert ein Brennerart
	 * @param Brennerart objekt
	 * @return Eine Collection von Fehlermeldungen
	 */
	public Collection<String> validateBrennerart(Brennerart brennerart) throws RemoteException {
		Collection<String> errors = new ArrayList<String>();
		Set<ConstraintViolation<Brennerart>> violations = validator.validate(brennerart);
		violations.forEach(v -> errors.add(v.getMessage()));
		return errors;
	}
	
	/**
	 * Validiert ein Brenner
	 * @param Brenner objekt
	 * @return Eine Collection von Fehlermeldungen
	 */
	public Collection<String> validateBrenner(Brenner brenner) throws RemoteException {
		Collection<String> errors = new ArrayList<String>();
		Set<ConstraintViolation<Brenner>> violations = validator.validate(brenner);
		violations.forEach(v -> errors.add(v.getMessage()));
		return errors;
	}
	
	/**
	 * Validiert ein Brennermodell
	 * @param Brennermodell objekt
	 * @return Eine Collection von Fehlermeldungen
	 */
	public Collection<String> validateBrennermodell(Brennermodell brennermodell) throws RemoteException {
		Collection<String> errors = new ArrayList<String>();
		Set<ConstraintViolation<Brennermodell>> violations = validator.validate(brennermodell);
		violations.forEach(v -> errors.add(v.getMessage()));
		return errors;
	}
	
	/**
	 * Validiert ein Brennstoff
	 * @param Brennstoff objekt
	 * @return Eine Collection von Fehlermeldungen
	 */
	public Collection<String> validateBrennstoff(Brennstoff brennstoff) throws RemoteException {
		Collection<String> errors = new ArrayList<String>();
		Set<ConstraintViolation<Brennstoff>> violations = validator.validate(brennstoff);
		violations.forEach(v -> errors.add(v.getMessage()));
		return errors;
	}
	
	/**
	 * Validiert ein Gemeinde
	 * @param Gemeinde objekt
	 * @return Eine Collection von Fehlermeldungen
	 */
	public Collection<String> validateGemeinde(Gemeinde brenner) throws RemoteException {
		Collection<String> errors = new ArrayList<String>();
		Set<ConstraintViolation<Gemeinde>> violations = validator.validate(brenner);
		violations.forEach(v -> errors.add(v.getMessage()));
		return errors;
	}
	
	/**
	 * Validiert ein Kontrolle
	 * @param Kontrolle objekt
	 * @return Eine Collection von Fehlermeldungen
	 */
	public Collection<String> validateKontrolle(Kontrolle kontrolle) throws RemoteException {
		Collection<String> errors = new ArrayList<String>();
		Set<ConstraintViolation<Kontrolle>> violations = validator.validate(kontrolle);
		violations.forEach(v -> errors.add(v.getMessage()));
		return errors;
	}
	
	/**
	 * Validiert ein Kunde
	 * @param Kunde objekt
	 * @return Eine Collection von Fehlermeldungen
	 */
	public Collection<String> validateKunde(Kunde kunde) throws RemoteException {
		Collection<String> errors = new ArrayList<String>();
		Set<ConstraintViolation<Kunde>> violations = validator.validate(kunde);
		violations.forEach(v -> errors.add(v.getMessage()));
		return errors;
	}
	
	/**
	 * Validiert ein Messergebnis
	 * @param Messergebnis objekt
	 * @return Eine Collection von Fehlermeldungen
	 */
	public Collection<String> validateMessergebnis(Messergebnis messergebnis) throws RemoteException {
		Collection<String> errors = new ArrayList<String>();
		Set<ConstraintViolation<Messergebnis>> violations = validator.validate(messergebnis);
		violations.forEach(v -> errors.add(v.getMessage()));
		return errors;
	}
	
	/**
	 * Validiert ein Mitarbeiter
	 * @param Mitarbeiter objekt
	 * @return Eine Collection von Fehlermeldungen
	 */
	public Collection<String> validateMitarbeiter(Mitarbeiter mitarbeiter) throws RemoteException {
		Collection<String> errors = new ArrayList<String>();
		Set<ConstraintViolation<Mitarbeiter>> violations = validator.validate(mitarbeiter);
		violations.forEach(v -> errors.add(v.getMessage()));
		return errors;
	}
	
	/**
	 * Validiert ein MitarbeiterRolle
	 * @param MitarbeiterRolle objekt
	 * @return Eine Collection von Fehlermeldungen
	 */
	public Collection<String> validateMitarbeiterRolle(MitarbeiterRolle mitarbeiterRolle) throws RemoteException {
		Collection<String> errors = new ArrayList<String>();
		Set<ConstraintViolation<MitarbeiterRolle>> violations = validator.validate(mitarbeiterRolle);
		violations.forEach(v -> errors.add(v.getMessage()));
		return errors;
	}
	
	/**
	 * Validiert ein PlzOrt
	 * @param PlzOrt objekt
	 * @return Eine Collection von Fehlermeldungen
	 */
	public Collection<String> validatePlzOrt(PlzOrt plzOrt) throws RemoteException {
		Collection<String> errors = new ArrayList<String>();
		Set<ConstraintViolation<PlzOrt>> violations = validator.validate(plzOrt);
		violations.forEach(v -> errors.add(v.getMessage()));
		return errors;
	}
	
	/**
	 * Validiert ein Termin
	 * @param Termin objekt
	 * @return Eine Collection von Fehlermeldungen
	 */
	public Collection<String> validateTermin(Termin termin) throws RemoteException {
		Collection<String> errors = new ArrayList<String>();
		Set<ConstraintViolation<Termin>> violations = validator.validate(termin);
		violations.forEach(v -> errors.add(v.getMessage()));
		return errors;
	}
	
	/**
	 * Validiert ein Waermererzeuger
	 * @param Waermererzeuger objekt
	 * @return Eine Collection von Fehlermeldungen
	 */
	public Collection<String> validateWaermeerzeuger(Waermeerzeuger waermeerzeuger) throws RemoteException {
		Collection<String> errors = new ArrayList<String>();
		Set<ConstraintViolation<Waermeerzeuger>> violations = validator.validate(waermeerzeuger);
		violations.forEach(v -> errors.add(v.getMessage()));
		return errors;
	}
	
	/**
	 * Validiert ein Waermererzeugermodell
	 * @param Waermererzeugermodell objekt
	 * @return Eine Collection von Fehlermeldungen
	 */
	public Collection<String> validateWaermeerzeugermodell(Waermeerzeugermodell waermeerzeugermodell) throws RemoteException {
		Collection<String> errors = new ArrayList<String>();
		Set<ConstraintViolation<Waermeerzeugermodell>> violations = validator.validate(waermeerzeugermodell);
		violations.forEach(v -> errors.add(v.getMessage()));
		return errors;
	}
}
