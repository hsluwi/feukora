package com.feukora.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Brennstoff;
import com.feukora.server.repositories.BrennstoffRepository;
import com.feukora.server.repositories.RepositoryFactory;
import com.feukora.server.rmi.IBrennstoffController;

/**
 * Business Layer f�r Brennstoffe
 * @author Andrew
 *
 */
public class BrennstoffController extends UnicastRemoteObject implements IBrennstoffController {
	
	private static final long serialVersionUID = 1L;
	private String databaseString = new ConfigManager().getDatabaseName(); 
	private static final Logger log = LogManager.getLogger();
	private BrennstoffRepository repo;
	
	/**
	 * Konstruktor die ein BrennstoffRepository instanziert
	 * @throws RemoteException
	 */
	public BrennstoffController() throws RemoteException {
		super();
		repo = (BrennstoffRepository) new RepositoryFactory(databaseString).get_brennstoffRepository();  
	}
	
	/**
	 * Gibt alle Brennstoffe zur�ck
	 * @return Brennstoffliste
	 */
	public Set<Brennstoff> getListBrennstoff() {
		try {
			return repo.get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein spezifischer Brennstoff zur�ck
	 * @param Brennstoff ID
	 * @return Brennstoff objekt
	 */
	public Brennstoff getBrennstoff(Integer id) {
		try {
			return repo.get(id).get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein Brennstoff
	 * @param Brennstoff objekt
	 * @return Brennstoff objekt
	 */
	public Brennstoff saveBrennstoff(Brennstoff brennstoff) {
		try {
			return repo.persist(brennstoff);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Brennstoffe
	 * @param mehrere Brennstoffe
	 * @return true oder false
	 */
	public boolean saveMultipleBrennstoff(Brennstoff... brennstoffe) {
		try {
			repo.persist(brennstoffe);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert eine Liste von Brennstoffe
	 * @param Brennstoffliste
	 * @return true oder false
	 */
	public boolean saveListBrennstoff(Collection<Brennstoff> brennstoffListe) {
		try {
			repo.persist(brennstoffListe);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht ein Brennstoff �ber eine ID
	 * @param Brennstoff ID
	 * @return true oder false
	 */
	public boolean removeBrennstoff(Integer id) {
		try {
			repo.remove(id);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
}
