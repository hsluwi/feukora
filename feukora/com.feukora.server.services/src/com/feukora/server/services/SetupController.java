package com.feukora.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.initializer.Initializer;
import com.feukora.server.rmi.ISetupController;

/**
 * Business Layer f�r das Setup
 * @author Andrew
 *
 */
public class SetupController extends UnicastRemoteObject implements ISetupController {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
	
	/**
	 * Standard Konstruktor
	 * @throws RemoteException
	 */
	public SetupController() throws RemoteException {
		super();
	}

	/**
	 * Initialisiert Testdaten und den RMI Service
	 */
	@Override
	public void init() throws RemoteException {
		try {
			Initializer.init();
		} catch (Exception e) {
			log.error(e.getMessage());
		}

	}
 

}
