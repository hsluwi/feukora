package com.feukora.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Kontrolle;
import com.feukora.server.repositories.KontrolleRepository;
import com.feukora.server.repositories.RepositoryFactory;
import com.feukora.server.rmi.IKontrolleController;


/**
 * Business Layer f�r Kontrolle
 * @author Andrew
 *
 */
public class KontrolleController extends UnicastRemoteObject implements IKontrolleController {

	private static final long serialVersionUID = 1L;
	private String databaseString = new ConfigManager().getDatabaseName(); 
	private static final Logger log = LogManager.getLogger();
	private KontrolleRepository repo;
	
	/**
	 * Konstruktor die ein KontrolleRepository instanziert
	 * @throws RemoteException
	 */
	public KontrolleController() throws RemoteException {
		super();
		repo = (KontrolleRepository) new RepositoryFactory(databaseString).get_kontrolleRepository();  
	}
	
	/**
	 * Gibt alle Kontrolle zur�ck
	 * @return Kontrolleliste
	 */
	public Set<Kontrolle> getListKontrolle() {
		try {
			return repo.get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt Set Kontrolle nach �bergebener Termin-ID zur�ck.
	 * Im Fehlerfall wird null zur�ckgegeben.
	 * @param Termin-ID
	 * @author Philipp Anderhub
	 * @return Kontrolleliste
	 */
	public Set<Kontrolle> getKontrolleByTerminId(Integer terminId) {
		try {
			return repo.get(m -> m.getTermin().getId().equals(terminId));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt eine spezifische Kontrolle
	 * @param Kontrolle ID
	 * @return Kontrolle objekt
	 */
	public Kontrolle getKontrolle(Integer id) {
		try {
			return repo.get(id).get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert eine Kontrolle
	 * @param Kontrolle objekt
	 * @return Kontrolle objekt
	 */
	public Kontrolle saveKontrolle(Kontrolle kontrolle) {
		try {
			return repo.persist(kontrolle);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Kontrolle
	 * @param mehrere Kontrolle
	 * @return true oder false
	 */
	public boolean saveMultipleKontrolle(Kontrolle... kontrolle) {
		try {
			repo.persist(kontrolle);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert eine Liste von Kontrolle
	 * @param Kontrolleliste
	 * @return true oder false
	 */
	public boolean saveListKontrolle(Collection<Kontrolle> kontrolleListe) {
		try {
			repo.persist(kontrolleListe);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht eine Kontrolle �ber eine ID
	 * @param Kontrolle ID
	 * @return true oder false
	 */
	public boolean removeKontrolle(Integer id) {
		try {
			repo.remove(id);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	

}
