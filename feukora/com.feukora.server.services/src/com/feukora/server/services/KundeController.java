package com.feukora.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Kunde;
import com.feukora.server.repositories.KundeRepository;
import com.feukora.server.repositories.RepositoryFactory;
import com.feukora.server.rmi.IKundeController;


/**
 * Business Layer f�r Kunden
 * @author Andrew
 *
 */
public class KundeController extends UnicastRemoteObject implements IKundeController {
	
	private static final long serialVersionUID = 1L;
	private String databaseString = new ConfigManager().getDatabaseName(); 
	private static final Logger log = LogManager.getLogger();
	private KundeRepository repo;
	
	/**
	 * Konstruktor die ein KundeRepository instanziert
	 * @throws RemoteException
	 */
	public KundeController() throws RemoteException {
		super();
		repo = (KundeRepository) new RepositoryFactory(databaseString).get_kundeRepository();  
	}

	/**
	 * Gibt alle Kunden zur�ck
	 * @return Kundenliste
	 */
	public Set<Kunde> getListKunde() {
		try {
			return repo.get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein spezifischer Kunde zur�ck
	 * @param Kunden ID
	 * @return Kunden objekt
	 */
	public Kunde getKunde(Integer id) {
		try {
			return repo.get(id).get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein Kunde
	 * @param Kunde objekt
	 * @return Kunde objekt
	 */
	public Kunde saveKunde(Kunde kunde) {
		try {
			return repo.persist(kunde);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Kunden
	 * @param mehrere Kunden
	 * @return true oder false
	 */
	public boolean saveMultipleKunde(Kunde... kunden) {
		try {
			repo.persist(kunden);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert eine Liste von Kunden
	 * @param Kundenliste
	 * @return true oder false
	 */
	public boolean saveListKunde(Collection<Kunde> kundeListe) {
		try {
			repo.persist(kundeListe);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht ein Kunde �ber eine ID
	 * @param Kunde ID
	 * @return true oder false
	 */
	public boolean removeKunde(Integer id) {
		try {
			repo.remove(id);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
}
