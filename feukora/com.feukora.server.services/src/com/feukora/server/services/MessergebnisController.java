package com.feukora.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Messergebnis;
import com.feukora.server.repositories.MessergebnisRepository;
import com.feukora.server.repositories.RepositoryFactory;
import com.feukora.server.rmi.IMessergebnisController;


/**
 * Business Layer Messergebnis
 * @author Andrew
 *
 */
public class MessergebnisController extends UnicastRemoteObject implements IMessergebnisController {

	private static final long serialVersionUID = 1L;
	private String databaseString = new ConfigManager().getDatabaseName(); 
	private static final Logger log = LogManager.getLogger();
	private MessergebnisRepository repo;
	
	/**
	 * Konstruktor die ein MessergebnisRepository instanziert
	 * @throws RemoteException
	 */
	public MessergebnisController() throws RemoteException {
		super();
		repo = (MessergebnisRepository) new RepositoryFactory(databaseString).get_messergebnisRepository();  
	}
	
	/**
	 * Gibt alle Messergebnisse zur�ck
	 * @return Messergebnisliste
	 */
	public Set<Messergebnis> getListMessergebnis() {
		try {
			return repo.get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein spezifischer Messergebnis zur�ck
	 * @param Messergebnis ID
	 * @return Messergebnis objekt
	 */
	public Messergebnis getMessergebnis(Integer id) {
		try {
			return repo.get(id).get();
		} catch (NoSuchElementException e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein Messegebnis
	 * @param Messergebnis objekt
	 * @return Messergebnis objekt
	 */
	public Messergebnis saveMessergebnis(Messergebnis messergebnis) {
		try {
			return repo.persist(messergebnis);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Messergebnisse
	 * @param mehrere Messergebnisse
	 * @return true oder false
	 */
	public boolean saveMultipleMessergebnis(Messergebnis... messergebnis) {
		try {
			repo.persist(messergebnis);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert eine Liste von Messergebnisse
	 * @param Messergebnisliste
	 * @return true oder false
	 */
	public boolean saveListMessergebnis(Collection<Messergebnis> messergebnisListe) {
		try {
			repo.persist(messergebnisListe);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht ein Messergebnis �ber eine ID
	 * @param Messergebnis ID
	 * @return true oder false
	 */
	public boolean removeMessergebnis(Integer id) {
		try {
			repo.remove(id);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}

	/**
	 * Gibt alle Messergebnisse mit einer �bergebenen Kontrolle-ID zur�ck
	 * @author Philipp Anderhub
	 * @param Integer kontrolleId
	 * @return Set Messergebnis
	 * 
	 */
	@Override
	public Set<Messergebnis> getMessergebnisByKontrolleId(Integer kontrolleId) throws RemoteException {
		try {
			return repo.get(m -> m.getKontrolle().getId().equals(kontrolleId));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
		
	}
}
