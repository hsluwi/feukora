package com.feukora.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Mitarbeiter;
import com.feukora.server.repositories.MitarbeiterRepository;
import com.feukora.server.repositories.RepositoryFactory;
import com.feukora.server.rmi.IMitarbeiterController;

/**
 * Business Layer f�r Mitarbeiter
 * @author Andrew
 *
 */
public class MitarbeiterController extends UnicastRemoteObject implements IMitarbeiterController {
	
	private static final long serialVersionUID = 1L;
	private String databaseString = new ConfigManager().getDatabaseName(); 
	private static final Logger log = LogManager.getLogger();
	private MitarbeiterRepository repo;
	
	/**
	 * Konstruktor die ein MitarbeiterRepository instanziert
	 * @throws RemoteException
	 */
	public MitarbeiterController() throws RemoteException {
		super();
		repo = (MitarbeiterRepository) new RepositoryFactory(databaseString).get_mitarbeiterRepository();  
	}
	
	/**
	 * Gibt alle Mitarbeiter zur�ck
	 * @return Mitarbeiterliste
	 */
	public Set<Mitarbeiter> getListMitarbeiter() {
		try {
			return repo.get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein spezifischer Mitarbeiter zur�ck
	 * @param Mitarbeiter ID
	 * @return Mitarbeiter objekt
	 */
	public Mitarbeiter getMitarbeiter(Integer id) {
		try {
			return repo.get(id).get();
		} catch (NoSuchElementException e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein Mitarbeiter
	 * @param Mitarbeiter objekt
	 * @return Mitarbeiter objekt
	 */
	public Mitarbeiter saveMitarbeiter(Mitarbeiter mitarbeiter) {
		try {
			return repo.persist(mitarbeiter);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Mitarbeiter
	 * @param mehrere Mitarbeiter
	 * @return true oder false
	 */
	public boolean saveMultipleMitarbeiter(Mitarbeiter... mitarbeiterList) {
		try {
			repo.persist(mitarbeiterList);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert eine Liste von Mitarbeitern
	 * @param Mitarbeiterliste
	 * @return true oder false
	 */
	public boolean saveListMitarbeiter(Collection<Mitarbeiter> mitarbeiterListe) {
		try {
			repo.persist(mitarbeiterListe);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht ein Mitarbeiter �ber eine ID
	 * @param Mitarbeiter ID
	 * @return true oder false
	 */
	public boolean removeMitarbeiter(Integer id) {
		try {
			repo.remove(id);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Pr�ft ob ein Username bereits existiert
	 * @param Username
	 * @return true oder false
	 */
	public boolean usernameAvailable(String username){
		try {
			Set<Mitarbeiter> mitarbeiterListe = repo.get(m -> m.getUserName().equals(username));
			if (mitarbeiterListe.size() == 0){
				return true;
			}
		} catch (NoSuchElementException e) {
			log.error(e.getMessage());
		}
		return false;
	}

	/**
	 * Pr�ft mit der Username und Passwort ob ein Mitarbeiter ein Login hat
	 * @param Username und Passwort
	 * @return Mitarbeiter objekt
	 */
	public Mitarbeiter getMitarbeiterLogin(String username, String passwort) throws RemoteException {
		try {
			Set<Mitarbeiter> mita = repo.get(m -> m.getUserName().equals(username) && m.getPasswort().equals(passwort));
			if(mita.size() == 1)
			{
				return mita.iterator().next();
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
}
