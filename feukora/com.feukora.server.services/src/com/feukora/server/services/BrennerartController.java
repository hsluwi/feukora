package com.feukora.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Brennerart;
import com.feukora.server.repositories.BrennerartRepository;
import com.feukora.server.repositories.RepositoryFactory;
import com.feukora.server.rmi.IBrennerartController;


/**
 * Business Layer f�r Brennerarten
 * @author Andrew
 *
 */
public class BrennerartController extends UnicastRemoteObject implements IBrennerartController {
	
	private static final long serialVersionUID = 1L;
	private String databaseString = new ConfigManager().getDatabaseName(); 
	private static final Logger log = LogManager.getLogger();
	private BrennerartRepository repo;
	
	/**
	 * Kontruktor die ein BrennerartRepository instanziert
	 * @throws RemoteException
	 */
	public  BrennerartController() throws RemoteException {
		super();
		repo = (BrennerartRepository) new RepositoryFactory(databaseString).get_brennerartRepository();
	}
	
	/**
	 * Gibt alle Brennerarten zur�ck
	 * @return Brennertartliste
	 */
	public Set<Brennerart> getListBrennerart() {
		try {
			return repo.get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein spezifischer Brennerart zur�ck
	 * @param Brennerart ID
	 * @param Brennerart objekt
	 */
	public Brennerart getBrennerart(Integer id) {
		try {
			return repo.get(id).get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein Brennerart
	 * @param Brennerart objekt
	 * @return Brennerart objekt
	 */
	public Brennerart saveBrennerart(Brennerart brennerart) {
		try {
			return repo.persist(brennerart);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Brennerarten
	 * @param mehrere Brennerarten
	 * @return true oder false
	 */
	public boolean saveMultipleBrennerart(Brennerart... brennerart) {
		try {
			repo.persist(brennerart);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert eine Liste von Brennerarten
	 * @param Brennerartliste
	 * @return true oder false
	 */
	public boolean saveListBrennerart(Collection<Brennerart> brennerartListe) {
		try {
			repo.persist(brennerartListe);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return true;
	}
	
	/**
	 * L�scht ein Brennerart �ber eine ID
	 * @param Brennerart ID
	 * @return true oder false
	 */
	public boolean removeBrennerart(Integer id) {
		try {
			repo.remove(id);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return true;
	}

}
