package com.feukora.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Brenner;
import com.feukora.server.repositories.BrennerRepository;
import com.feukora.server.repositories.RepositoryFactory;
import com.feukora.server.rmi.IBrennerController;


/**
 * Business Layer f�r Brenner
 * @author Andrew
 *
 */
public class BrennerController extends UnicastRemoteObject implements IBrennerController {
	
	private static final long serialVersionUID = 1L;
	private String databaseString = new ConfigManager().getDatabaseName(); 
	private static final Logger log = LogManager.getLogger();
	private BrennerRepository repo;
	
	/**
	 * Konstruktor die ein BrennerRepository instanziert
	 * @throws RemoteException
	 */
	public BrennerController() throws RemoteException {
		super();
		repo = (BrennerRepository) new RepositoryFactory(databaseString).get_brennerRepository(); 
	}

	/**
	 * Gibt alle Brenner zur�ck
	 * @return Brennerliste
	 */
	public Set<Brenner> getListBrenner() {
		try {
			return repo.get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein spezifischer Brenner zur�ck
	 * @param Brenner ID
	 * @return Brenner objekt
	 */
	public Brenner getBrenner(Integer id) {
		try {
			return repo.get(id).get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein Brenner
	 * @param Brenner objekt
	 * @return Brenner objekt
	 */
	public Brenner saveBrenner(Brenner brenner) {
		try {
			return repo.persist(brenner);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Brenner
	 * @param mehrere Brenner
	 * @return true oder false
	 */
	public boolean saveMultipleBrenner(Brenner... brenner) {
		try {
			repo.persist(brenner);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert eine Liste von Brenner
	 * @param Brennerliste
	 * @return true oder false
	 */
	public boolean saveListBrenner(Collection<Brenner> brennerListe) {
		try {
			repo.persist(brennerListe);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return true;
	}
	
	/**
	 * L�scht ein Brenner �ber eine ID
	 * @param Brenner ID
	 * @return true oder false
	 */
	public boolean removeBrenner(Integer id) {
		try {
			repo.remove(id);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
