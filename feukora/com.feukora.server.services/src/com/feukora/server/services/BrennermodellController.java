package com.feukora.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Brennermodell;
import com.feukora.server.repositories.BrennermodellRepository;
import com.feukora.server.repositories.RepositoryFactory;
import com.feukora.server.rmi.IBrennermodellController;

/**
 * Business Layer f�r Brennermodelle
 * @author Andrew
 *
 */
public class BrennermodellController extends UnicastRemoteObject implements IBrennermodellController  {
	
	private static final long serialVersionUID = 1L;
	private String databaseString = new ConfigManager().getDatabaseName(); 
	private static final Logger log = LogManager.getLogger();
	private BrennermodellRepository repo;
	
	/**
	 * Konstruktor die ein BrennerRepositry instanziert
	 * @throws RemoteException
	 */
	public BrennermodellController() throws RemoteException {
		super();
		repo = (BrennermodellRepository) new RepositoryFactory(databaseString).get_brennermodellRepository(); 
	}
	
	/**
	 * Gibt alle Brennermodelle zur�ck
	 * @return Brennermodellliste
	 */
	public Set<Brennermodell> getListBrennermodell() {
		try {
			return repo.get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt ein spezifisches Brennermodell zur�ck
	 * @param Brennermodell ID
	 * @return Brennermodell objekt
	 */
	public Brennermodell getBrennermodell(Integer id) {
		try {
			return repo.get(id).get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein Brennermodell
	 * @param Brennermodell objekt
	 * @return Brennermodell objekt
	 */
	public Brennermodell saveBrennermodell(Brennermodell brennermodell) {
		try {
			return repo.persist(brennermodell);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Brennermodelle
	 * @param mehrere Brennermodelle
	 * @return true oder false
	 */
	public boolean saveMultipleBrennermodell(Brennermodell... brennermodell) {
		try {
			repo.persist(brennermodell);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert eine Liste von Brennermodelle
	 * @param Brennermodellliste
	 * @return true oder false
	 */
	public boolean saveListBrennermodell(Collection<Brennermodell> brennermodellListe) {
		try {
			repo.persist(brennermodellListe);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht ein Brennermodell �ber eine ID
	 * @param Brennermodell ID
	 * @return true oder false
	 */
	public boolean removeBrennermodell(Integer id) {
		try {
			repo.remove(id);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}

}
