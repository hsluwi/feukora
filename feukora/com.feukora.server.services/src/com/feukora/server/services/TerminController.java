package com.feukora.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.NoSuchElementException;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Termin;
import com.feukora.server.repositories.RepositoryFactory;
import com.feukora.server.repositories.TerminRepository;
import com.feukora.server.rmi.ITerminController;

/**
 * Business Layer f�r Termine
 * @author Andrew
 *
 */
public class TerminController extends UnicastRemoteObject implements ITerminController {
	
	private static final long serialVersionUID = 1L;
	private String databaseString = new ConfigManager().getDatabaseName(); 
	private static final Logger log = LogManager.getLogger();
	private TerminRepository repo;
	
	/**
	 * Konstruktor die ein TerminRepository instanziert
	 * @throws RemoteException
	 */
	public TerminController() throws RemoteException {
		super();
		repo = (TerminRepository) new RepositoryFactory(databaseString).get_terminRepository();
	}

	/**
	 * Gibt alle Termine zur�ck
	 * @return Terminliste
	 */
	public Set<Termin> getListTermin() {
		try {
			return repo.get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Holt alle Termine von einem Mitarbeiter
	 * @param Mitarbeiter ID
	 * @return Termine vom Mitarbeiter
	 */
	public Set<Termin> getListTerminByUserId(Integer userId){
		try {
			return repo.get(u -> u.getMitarbeiter().getId() == userId);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Holt ein spezifischer Termin �ber die ID
	 * @param Termin ID
	 * @return Termin objekt
	 */
	public Termin getTermin(Integer id) {
		try {
			return repo.get(id).get();
		} catch (NoSuchElementException e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt Termine von einem User einer bestimmten Jahreswoche zur�ck.
	 * @param Datum und Mitarbeiter ID
	 * @return Terminliste
	 */
	public Set<Termin> getTerminWeekByUser(GregorianCalendar date, Integer userId) {
		try {
			return repo.get(m -> m.getDatumZeit().WEEK_OF_YEAR == date.WEEK_OF_YEAR 
					&& m.getMitarbeiter().getId() == userId);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gibt Termine von einer bestimmten Jahreswoche zur�ck.
	 * @param Datum
	 * @return Terminliste
	 */
	public Set<Termin> getTerminWeek(GregorianCalendar date) {
		try {
			return repo.get(m -> m.getDatumZeit().WEEK_OF_YEAR == date.WEEK_OF_YEAR);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein Termin
	 * @param Termin objekt
	 * @return Termin objekt
	 */
	public Termin saveTermin(Termin termin) {
		try {
			return repo.persist(termin);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Termine ab
	 * @param mehrere Termine
	 * @return true oder false
	 */
	public boolean saveMultipleTermin(Termin... termin) {
		try {
			repo.persist(termin);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert eine Liste von Termine
	 * @param Terminliste
	 * @return true oder false
	 */
	public boolean saveListTermin(Collection<Termin> terminListe) {
		try {
			repo.persist(terminListe);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht ein Termin �ber ein ID
	 * @param Termin ID
	 * @return true oder false
	 */
	public boolean removeTermin(Integer id) {
		try {
			repo.remove(id);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
}
