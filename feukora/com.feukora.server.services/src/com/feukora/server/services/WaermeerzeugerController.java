package com.feukora.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Waermeerzeuger;
import com.feukora.server.repositories.RepositoryFactory;
import com.feukora.server.repositories.WaermeerzeugerRepository;
import com.feukora.server.rmi.IWaermeerzeugerController;

/**
 * Business Layer f�r Waermeerzeuger
 * @author Andrew
 *
 */
public class WaermeerzeugerController extends UnicastRemoteObject implements IWaermeerzeugerController {
	
	private static final long serialVersionUID = 1L;
	private String databaseString = new ConfigManager().getDatabaseName(); 
	private static final Logger log = LogManager.getLogger();
	private WaermeerzeugerRepository repo;
	
	/**
	 * Konstruktor die ein WaermeerzeugerRepository instanziert
	 * @throws RemoteException
	 */
	public WaermeerzeugerController() throws RemoteException {
		super();
		repo = (WaermeerzeugerRepository) new RepositoryFactory(databaseString).get_waermeerzeugerRepository();
	}
	
	/**
	 * Gibt alle Waermeerzeuger zur�ck
	 * @return Waermeerzeugerliste
	 */
	public Set<Waermeerzeuger> getListWaermeerzeuger() {
		try {
			repo.get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Gib ein spezifischer Waermeerzeuger zur�ck
	 * @param Waermeerzeuger ID
	 * @return Waermeerzeuger objekt
	 */
	public Waermeerzeuger getWaermeerzeuger(Integer id) {
		try {
			return repo.get(id).get();
		} catch (NoSuchElementException e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein Waermerzeuger
	 * @param Waermeerzeuger objekt
	 * @return Waermeerzeuger objekt
	 */
	public Waermeerzeuger saveWaermeerzeuger(Waermeerzeuger waermeerzeuger) {
		try {
			return repo.persist(waermeerzeuger);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Waermeerzeuger
	 * @param mehrere Waermeerzeuger
	 * @return true oder false
	 */
	public boolean saveMultipleWaermeerzeuger(Waermeerzeuger... waermeerzeuger) {
		try {
			repo.persist(waermeerzeuger);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert eine Lsite von Waermeerzeuger
	 * @param Waermeerzeugerliste
	 * @return true oder false
	 */
	public boolean saveListWaermeerzeuger(Collection<Waermeerzeuger> waermeerzeugerListe) {
		try {
			repo.persist(waermeerzeugerListe);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * L�scht ein Waermeerzeuger �ber die ID
	 * @param Waermerzeuger ID
	 * @return true oder false
	 */
	public boolean removeWaermeerzeuger(Integer id) {
		try {
			repo.remove(id);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
}
