package com.feukora.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.feukora.server.config.ConfigManager;
import com.feukora.server.models.Anlagenstandort;
import com.feukora.server.repositories.AnlagenstandortRepository;
import com.feukora.server.repositories.RepositoryFactory;
import com.feukora.server.rmi.IAnlagenstandortController;

/**
 * Business Layer f�r Validierungen
 * Validiert Entit�ten und sendet Fehlermeldungen zum Client zur�ck
 * @author Andrew
 *
 */
public class AnlagenstandortController extends UnicastRemoteObject implements IAnlagenstandortController {
 
	private static final long serialVersionUID = 1L;
	private String databaseString = new ConfigManager().getDatabaseName();
	private static final Logger log = LogManager.getLogger();
	private AnlagenstandortRepository repo;
	
	/**
	 * Kontruktor die ein AnlagestandortRepository instanziert
	 * @throws RemoteException
	 */
	public AnlagenstandortController() throws RemoteException {
		super();
		repo = (AnlagenstandortRepository) new RepositoryFactory(databaseString).get_anlagenstandortRepository();
	}
		
	/**
	 * Gibt alle Anlagenstandorte zur�ck
	 * @return Anlagestandortliste
	 */
	public Set<Anlagenstandort> getListAnlagenstandort() {
		try {
			return repo.get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}

	/**
	 * Gibt ein spezifischer Anlagestandort zur�ck
	 * @param Anlagestandort ID
	 * @return Anlagestandort objekt
	 */
	public Anlagenstandort getAnlagenstandort(Integer id) {
		try {
			return repo.get(id).get();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert ein Anlagestandort
	 * @param Anlagestandort objekt
	 * @return Anlagestandort objekt
	 */
	public Anlagenstandort saveAnlagenstandort(Anlagenstandort anlagenstandort) {
		try {
			return repo.persist(anlagenstandort);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	/**
	 * Speichert mehrere Anlagestandorte
	 * @param mehrere Anlagestandorte
	 * @return true oder false
	 */
	public boolean saveMultipleAnlagenstandort(Anlagenstandort... anlagenstandort) {
		try {
			repo.persist(anlagenstandort);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}
	
	/**
	 * Speichert eine Liste von Anlagestandorte
	 * @param Collection Anlagenstandort
	 * @return true oder false
	 */
	public boolean saveListAnlagenstandort(Collection<Anlagenstandort> anlagenstandortListe) {
		try {
			repo.persist(anlagenstandortListe);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return true;
	}
	
	/**
	 * L�scht ein Anlagestandort �ber eine ID
	 * @param Anlagestandort ID
	 * @return true oder false
	 */
	public boolean removeAnlagenstandort(Integer id) {
		try {
			repo.remove(id);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return false;
	}


	
}
