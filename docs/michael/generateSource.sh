#!/bin/bash
cd C:/Users/Michael/Documents/feukora/dev/feukora/com.feukora.client
enscript -r -2 --file-align=2 --highlight --color=1 --line-numbers -o - `find . -name '*.java'` | ps2pdf - C:/Users/Michael/Documents/feukora/dev/docs/SourceCodePDF/Source_client.pdf
wait
echo client done!
cd C:/Users/Michael/Documents/feukora/dev/feukora/com.feukora.helper
enscript -r -2 --file-align=2 --highlight --color=1 --line-numbers -o - `find . -name '*.java'` | ps2pdf - C:/Users/Michael/Documents/feukora/dev/docs/SourceCodePDF/Source_helper.pdf
wait
echo helperdone!
cd C:/Users/Michael/Documents/feukora/dev/feukora/com.feukora.server.config
enscript -r -2 --file-align=2 --highlight --color=1 --line-numbers -o - `find . -name '*.java'` | ps2pdf - C:/Users/Michael/Documents/feukora/dev/docs/SourceCodePDF/Source_server_config.pdf
wait
echo config done!
cd C:/Users/Michael/Documents/feukora/dev/feukora/com.feukora.server.model
enscript -r -2 --file-align=2 --highlight --color=1 --line-numbers -o - `find . -name '*.java'` | ps2pdf - C:/Users/Michael/Documents/feukora/dev/docs/SourceCodePDF/Source_server_model.pdf
wait
echo model done!
cd C:/Users/Michael/Documents/feukora/dev/feukora/com.feukora.server.repositories
enscript -r -2 --file-align=2 --highlight --color=1 --line-numbers -o - `find . -name '*.java'` | ps2pdf - C:/Users/Michael/Documents/feukora/dev/docs/SourceCodePDF/Source_server_repositories.pdf
wait
echo repositories done!
cd C:/Users/Michael/Documents/feukora/dev/feukora/com.feukora.server.rmi
enscript -r -2 --file-align=2 --highlight --color=1 --line-numbers -o - `find . -name '*.java'` | ps2pdf - C:/Users/Michael/Documents/feukora/dev/docs/SourceCodePDF/Source_server_rmi.pdf
wait
echo rmi done!
cd C:/Users/Michael/Documents/feukora/dev/feukora/com.feukora.server.services
enscript -r -2 --file-align=2 --highlight --color=1 --line-numbers -o - `find . -name '*.java'` | ps2pdf - C:/Users/Michael/Documents/feukora/dev/docs/SourceCodePDF/Source_server_services.pdf
wait
echo services done!
cd C:/Users/Michael/Documents/feukora/dev/feukora/com.feukora.server.util
enscript -r -2 --file-align=2 --highlight --color=1 --line-numbers -o - `find . -name '*.java'` | ps2pdf - C:/Users/Michael/Documents/feukora/dev/docs/SourceCodePDF/Source_server_util.pdf
wait
echo util done!
cd C:/Users/Michael/Documents/feukora/dev/feukora/com.feukora.server.webservice
enscript -r -2 --file-align=2 --highlight --color=1 --line-numbers -o - `find . -name '*.java'` | ps2pdf - C:/Users/Michael/Documents/feukora/dev/docs/SourceCodePDF/Source_server_webservice.pdf
echo webservice done!