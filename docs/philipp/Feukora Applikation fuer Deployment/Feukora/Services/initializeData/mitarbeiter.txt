[
	{
		"firma": "Hochschule Luzern",
		"passwort": "123456",
		"username": "andrew",
		"email": "andrew.algabre@stud.hslu.ch",
		"mobile": "(032998) 808359",
		"vorname": "Andrew",
		"nachname": "Algabre",
		"strasse": "Ambar 12",
		"telefon": "(035309) 263741",
		"plzort_id": 74
	},
	{
		"firma": "Hochschule Luzern",
		"passwort": "123456",
		"username": "nadine",
		"email": "nadine.lang@stud.hslu.ch",
		"mobile": "(07903) 7699321",
		"vorname": "Nadine",
		"nachname": "Lang",
		"strasse": "667-1473 Pede Avenue",
		"telefon": "(091) 34026513",
		"plzort_id": 13
	},
	{
		"firma": "Hochschule Luzern",
		"passwort": "123456",
		"username": "philipp",
		"email": "philipp.anderhub@stud.hslu.ch",
		"mobile": "(070) 91495787",
		"vorname": "Philipp",
		"nachname": "Anderhub",
		"strasse": "565 Fusce Av.",
		"telefon": "(094) 58715466",
		"plzort_id": 36
	},
	{
		"firma": "Hochschule Luzern",
		"passwort": "123456",
		"username": "noah",
		"email": "noah.bellwald@stud.hslu.ch",
		"mobile": "(066) 56612629",
		"vorname": "Noah",
		"nachname": "Bellwald",
		"strasse": "277-5390 Molestie Rd.",
		"telefon": "(05636) 9883581",
		"plzort_id": 55
	},
	{
		"firma": "Hochschule Luzern",
		"passwort": "123456",
		"username": "sven",
		"email": "sven.mueller@stud.hslu.ch",
		"mobile": "(047) 65630593",
		"vorname": "Sven",
		"nachname": "M�ller",
		"strasse": "P.O. Box 168, 2231 Quis Av.",
		"telefon": "(035757) 116451",
		"plzort_id": 5
	},
	{
		"firma": "Hochschule Luzern",
		"passwort": "123456",
		"username": "marco",
		"email": "marco.hostettler@stud.hslu.ch",
		"mobile": "(008) 05806086",
		"vorname": "Marco",
		"nachname": "Hostettler",
		"strasse": "Ap #412-8199 Quam Road",
		"telefon": "(031818) 131841",
		"plzort_id": 29
	}
]